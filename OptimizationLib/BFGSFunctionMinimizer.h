#pragma once

#include "ObjectiveFunction.h"
#include "GradientBasedFunctionMinimizer.h"
#include "BFGSHessianApproximator.h"
#include "OptimizationLib/LBFGS.h"

class BFGSFunctionMinimizer : public GradientBasedFunctionMinimizer {
public:
	BFGSFunctionMinimizer(int p_maxIterations = 100, double p_solveResidual = 0.0001, int p_maxLineSearchIterations = 15, bool p_printOutput = false) : GradientBasedFunctionMinimizer(p_maxIterations, p_solveResidual, p_maxLineSearchIterations, p_printOutput) {
		optName = "BFGS";
	}

	BFGSFunctionMinimizer() {
		optName = "BFGS";
	}

	virtual ~BFGSFunctionMinimizer() {}

	// Since the gradient of a function gives the direction of steepest descent, all one needs to do is go in that direction...
	virtual void computeSearchDirection(ObjectiveFunction *function, const dVector &p, dVector& dp);

public:
	BFGSHessianApproximator bfgsApproximator;
};

/*
//Beichen Li: functor class for LBFGSFunctionMinimizer
class LBFGSFunctor {
public:
	ObjectiveFunction *function = NULL;

	LBFGSFunctor(ObjectiveFunction *function) : function(function) {}

	double operator () (const dVector& x, dVector& grad, bool savedFlag = true) {
		grad = dVector::Zero(x.size());
		function->addGradientTo(grad, x);
		return function->computeValue(x);
	}
};

//Beichen Li: class LBFGSFunctionMinimizer is an adaptor between LBFGS external library and system interfaces

//Namespace for external library
using namespace LBFGSpp;

class LBFGSFunctionMinimizer : public GradientBasedFunctionMinimizer {
public:
	LBFGSParam<double> param;
	LBFGSSolver<double> solver;
	LBFGSFunctor func;

	LBFGSFunctionMinimizer(ObjectiveFunction *function, int p_maxIterations = 100, double p_solveResidual = 0.0001, int p_maxLineSearchIterations = 15, bool p_printOutput = false) :
		GradientBasedFunctionMinimizer(p_maxIterations, p_solveResidual, p_maxLineSearchIterations, p_printOutput),
		param(),
		solver(param),
		func(function) {
		optName = "LBFGS";
		updateParameters();
	}

	~LBFGSFunctionMinimizer() {}

	bool minimize(dVector& p, double& functionValue) {
		solver.minimize(func, p, functionValue);
		return true;
	}

	void updateParameters() {
		param.max_iterations = maxIterations;
		param.max_linesearch = maxLineSearchIterations;
		param.epsilon = solveResidual;
	}

	//Beichen Li: This is only used for overloading and should never be called
	void computeSearchDirection(ObjectiveFunction *function, const dVector& p, dVector& dp) {}
};
*/
