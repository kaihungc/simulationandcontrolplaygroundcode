#pragma once

#include "ObjectiveFunction.h"
#include "GradientBasedFunctionMinimizer.h"

class GradientMomentumFunctionMinimizer : public GradientBasedFunctionMinimizer {
public:
	GradientMomentumFunctionMinimizer(int p_maxIterations = 100, double p_solveResidual = 0.0001, int p_maxLineSearchIterations = 15, bool p_printOutput = false) : GradientBasedFunctionMinimizer(p_maxIterations, p_solveResidual, p_maxLineSearchIterations, p_printOutput) {
		optName = "Gradient Descent with Momentum";
	}

	GradientMomentumFunctionMinimizer() {
		optName = "Gradient Descent with Momentum";
	}

	virtual ~GradientMomentumFunctionMinimizer() {}

	// Since the gradient of a function gives the direction of steepest Momentum, all one needs to do is go in that direction...
	virtual void computeSearchDirection(ObjectiveFunction *function, const dVector &p, dVector& dp);

	//Beichen Li: last update dp is stored in dpLast
	static dVector dpLast;

	//Beichen Li: coefficient for self momentum
	double coefficient = 0.5;
};
