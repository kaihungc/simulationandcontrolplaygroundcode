#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include "TestAppDynamixel.h"

/**
 * Robot Design and Simulation interface
 */
class TestAppRobotArm : public TestAppDynamixel {
private:
	double amplitude1 = 0.4, amplitude2 = -0.4;
	double phase2 = 0.0;
	double frequency = 1.0;

	double motionPhase = 0.0;



public:

	// constructor
	TestAppRobotArm();
	// destructor
	virtual ~TestAppRobotArm(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	virtual bool onKeyEvent(int key, int action, int mods);
};
