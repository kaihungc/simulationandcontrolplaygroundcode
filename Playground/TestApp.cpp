#include <GUILib/GLUtils.h>
#include "TestApp.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/Matrix.h>
#include <GUILib/PlotWindow.h>
#include <OptimizationLib/SoftUnilateralConstraint.h>

PlotWindow* plotWindow;
//double const1 = 1;
//double const2 = 0.5;

double const1 = -1;
double const2 = 0.5;

//#define EXP_DECAY_DEMO

TestApp::TestApp() {
	setWindowTitle("Test Application...");
	TwAddSeparator(mainMenuBar, "sep2", "");
//	TwAddVarRW(mainMenuBar, "const1", TW_TYPE_DOUBLE, &const1, " label='const1' group='Viz2' step = 0.1");
	TwAddVarRW(mainMenuBar, "timestep", TW_TYPE_DOUBLE, &const2, " label='time step' group='Viz2' step = 0.05");
	
	plotWindow = new PlotWindow(260 * 1.14, 0, mainWindowWidth - 260 * 1.14, mainWindowHeight);

	showConsole = false;
	showGroundPlane = false;

}

TestApp::~TestApp(void){

}

//triggered when mouse moves
bool TestApp::onMouseMoveEvent(double xPos, double yPos) {
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestApp::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestApp::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestApp::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestApp::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestApp::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
}

void TestApp::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestApp::process() {
	//do the work here...
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everqthing drawn by this method
void TestApp::drawScene() {
	//draw here...

	plotWindow->clearData(true);

//	SoftUnilateralConstraint c(10, 1);
//	double start = -1.0;
//	double end = 1.2 * 1;

	SmoothBarrierConstraint c(1);
	double start = 0.0;
	double end = 1.1;

	double range = end - start;

	plotWindow->createData("derivative", 0, 1, 0);
	for (double x = start; x < end; x += range / 100.0) {
		plotWindow->addDataPoint(0, x, c.computeDerivative(x));
	}

	plotWindow->createData("2nd derivative", 0, 0, 1);
	for (double x = start; x < end; x += range / 100.0) {
		plotWindow->addDataPoint(1, x, c.computeSecondDerivative(x));
	}

	plotWindow->createData("smooth penalty function", 1, 0, 0);
	for (double x = start; x < end; x += range / 100.0) {
		plotWindow->addDataPoint(2, x, c.computeValue(x));
	}
	plotWindow->draw();
	return;

#ifdef EXP_DECAY_DEMO


	double a = -1;
	double start = 0;
	double end = 6.0;
	double range = end - start;
	double dt = const2;
	int nSteps = (int)(range / dt + 0.5);

	//xDot = a * x;

	plotWindow->setYRange(a * 1.5, - a * 1.5);
	plotWindow->setXRange(start, end);

	plotWindow->createData("analytic solution", 0, 0, 0);
	for (double t = start; t <= end + 0.01; t += 0.01)
		plotWindow->addDataPoint(t, exp(a * t));

	dt /= 2.0;
	nSteps *= 2;

	double q0 = 1;
	double qt = q0;
	plotWindow->createData("forward euler", 0, 1, 0);
	for (int i = 0; i <= nSteps+1; i++) {
		plotWindow->addDataPoint(i*dt, qt);
		qt = qt + dt * a * qt;
	}

	dt *= 2.0;
	nSteps /= 2;


/*
	qt = q0;
	plotWindow->createData("midpoint method", 0, 0, 1);
	for (int i = 0; i <= nSteps + 1; i++) {
		plotWindow->addDataPoint(i*dt, qt);
		double qt_half = qt + dt/2 * a * qt;
		qt = qt + dt * a * qt_half;
	}
	*/

	qt = q0;
	plotWindow->createData("backward euler", 0, 1, 1);
	for (int i = 0; i <= nSteps + 1; i++) {
		plotWindow->addDataPoint(i*dt, qt);
		qt = qt / (1 - a * dt);
	}

	qt = q0;
	plotWindow->createData("trapezoid rule", 1, 0, 0);
	for (int i = 0; i <= nSteps + 1; i++) {
		plotWindow->addDataPoint(i*dt, qt);
		qt = qt * (1 + dt/2 * a) / (1 - dt /2 * a);
	}

	plotWindow->draw();
#else
	DynamicArray<P3D> pointTraj;
	P3D x0(0, 1, 0);
	V3D v0(0.3, 0, 0);
	double dt = 0.5 / 10.0;
	double a = -0.1;

	P3D xt = x0;
	V3D vt = v0;

	//xDotDot = a * x

	//forward euler...
	int nSteps = (int)(200 / dt + 0.5);
	for (int i = 0; i <= nSteps + 1; i++) {
		pointTraj.push_back(xt);
		P3D old_xt = xt; 
		xt = xt + vt * dt;
		vt = vt + old_xt * dt * a;
	}
//	drawPointTrajectory(pointTraj, V3D(1,0,0), 3);

	pointTraj.clear();
	xt = x0;
	vt = v0;
	//midpoint method...
	for (int i = 0; i <= nSteps + 1; i++) {
		pointTraj.push_back(xt);
		P3D xt_half = xt + vt * dt/2;
		V3D vt_half = vt + xt * a * dt/2;

		xt = xt + vt_half * dt;
		vt = vt + xt_half * a * dt;
	}
//	drawPointTrajectory(pointTraj, V3D(0, 1, 1), 3);

	pointTraj.clear();
	xt = x0;
	vt = v0;
	//implicit integration...
	for (int i = 0; i <= nSteps + 1; i++) {
		pointTraj.push_back(xt);
		xt = (xt + vt * dt) / (1-dt*dt*a);
		vt = vt + xt * dt * a;
	}
//	drawPointTrajectory(pointTraj, V3D(0, 0, 1), 3);


	pointTraj.clear();
	xt = x0;
	vt = v0;

	//symplectic euler...
	for (int i = 0; i <= nSteps + 1; i++) {
		pointTraj.push_back(xt);
		vt = vt + xt * dt * a;
		xt = xt + vt * dt;
	}
//	drawPointTrajectory(pointTraj, V3D(0, 1, 0), 3);
	
//	draw the trajectory, and then a point that travels on it through time...


#endif

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestApp::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestApp::restart() {

}

bool TestApp::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

