#include <GUILib/GLUtils.h>

#include "RobotAnimationApp.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <RobotDesignerLib/KinematicRobotController.h>
#include <RobotDesignerLib/ControlUtil.h>
#include <ControlLib/RobotMesh.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <windows.h>

/*
Color scheme in motion tree viewer:
- blue: non-selected.
- green: highlighted (mouse over).
- red: selected.

To generate a new motion clip:
- setup footfall pattern in the MOPT window.
- click on button "newMP", which will add a node to the motion tree.
- keep optimizing the plan by running the process, be sure that you are in MOPT optimization (key '1') mode.
- visualize motion plan by switching to MOPT animation (key '2').

To generate a new motion transition:
- select one node in the motion graph, hold on 'SHIFT' to select another node. Then the MOPT window
will show three footfall pattern viewers. The two viewers below can select the transition time indices in both start and end motion.
- click on button "newMP", which will add an edge to the motion tree.
- keep optimizing the plan by running process.
- again, visualize the transitional motion plan by switching to MOPT animation (key '2').

In robot animation mode:
- click on nodes to switch between different motions, if there is an edge connecting them,
a transitional motion will be performed, otherwise, a sudden change occurs.


Key '1 - 3': switch between {MOPT optimization, MOPT animation, robot animation}.
Key 'D': delete selected node or edge.
*/

// ******************* ThreeLinkIKObj *******************

ThreeLinkIKObj::ThreeLinkIKObj(double r1, double r2, double r3, double k, double targetLen)
{
	weight = 1e5;
	regularizer = 1e1;
	this->r1 = r1;
	this->r2 = r2;
	this->r3 = r3;
	this->k = k;
	this->targetLen = targetLen;
}

double ThreeLinkIKObj::computeValue(const dVector& p)
{
	double alpha = p[0];

	return computeValueWithoutRegularizer(p);
	// return computeValueWithoutRegularizer(p) + regularizer * alpha * alpha;
}

double ThreeLinkIKObj::computeValueWithoutRegularizer(const dVector& p)
{
	double alpha = p[0];
	double beta = k * alpha;

	double x = r1 + r2 * cos(alpha) + r3 * cos(alpha + beta);
	double y = r2 * sin(alpha) + r3 * sin(alpha + beta);

	double diff = (x * x + y * y) - (targetLen * targetLen);

	return 0.5 * weight * diff * diff;
}

/**
UI code:
*/
static void TW_CALL buttonAction(void* clientData) {
	RobotAnimationApp* app = dynamic_cast<RobotAnimationApp*>(GLApplication::getGLAppInstance());

	if (strcmp((const char*)clientData, "newMP") == 0)
	{
		app->generateNewMP();
	}
	else if (strcmp((const char*)clientData, "newWalkAndTrotMPs") == 0)
	{
		app->generateWalkTrotMPs();
	}
	else if (strcmp((const char*)clientData, "loadRobotFromModeler") == 0)
	{
		app->loadRobotFromModeler();
	}
}

RobotAnimationApp::RobotAnimationApp() {
	setWindowTitle("RobotAnimationApp");
	TwAddButton(mainMenuBar, "LoadRobotFromModeler", buttonAction, "loadRobotFromModeler", "");
	TwAddButton(mainMenuBar, "NewMP", buttonAction, "newMP", "");
	TwAddButton(mainMenuBar, "NewWalkAndTrotMPs", buttonAction, "newWalkAndTrotMPs", "");
	TwAddSeparator(mainMenuBar, "sep4", "");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Robot Animation");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "WindowOptions", TwDefineEnumFromString("WindowOptions", "..."), &windowOption, "group='Sim'");
	DynamicArray<std::string> windowOptionList;
	windowOptionList.push_back("\\Skeleton Window");
	windowOptionList.push_back("\\MOPT Window");
	generateMenuEnumFromFileList("MainMenuBar/WindowOptions", windowOptionList);

	TwAddVarRW(mainMenuBar, "FollowPath", TW_TYPE_BOOLCPP, &followPath, "group='Sim'");
	TwAddVarRW(mainMenuBar, "RecordAnimation", TW_TYPE_BOOLCPP, &recordAnimation, "group='Sim'");
	TwAddVarRW(mainMenuBar, "ShowRobotSimp", TW_TYPE_BOOLCPP, &showRobotSimp, "group='Sim'");
	// TwAddVarRW(mainMenuBar, "FootSwingAngle", TW_TYPE_DOUBLE, &footSwingAngle, "group='Sim' step=1.0 max=50 min=0");
	// TwAddVarRW(mainMenuBar, "RearKneeAngle", TW_TYPE_DOUBLE, &rearKneeAngle, "group='Sim' step=1.0 max=50 min=0");
	// TwAddVarRW(mainMenuBar, "IKOffset", TW_TYPE_DOUBLE, &IKOffsetScale, "group='Sim' step=1.0");

	// TwAddVarRW(mainMenuBar, "FrontLimbAngleRatio", TW_TYPE_DOUBLE, &frontLimbAngleRatio, "group='FrontLeg' step=1 max=5 min=-5");
	TwAddVarRW(mainMenuBar, "FrontLimbFlip", TW_TYPE_BOOLCPP, &frontLimbFlip, "group='FrontLeg'");
	TwAddVarRW(mainMenuBar, "FrontShoulderFlip", TW_TYPE_BOOLCPP, &frontShoulderFlip, "group='FrontLeg'");
	TwAddVarRW(mainMenuBar, "FrontLimbStanceLen", TW_TYPE_DOUBLE, &frontLimbStanceLen, "group='FrontLeg' step=0.01 max=1.0 min=0.8");
	TwAddVarRW(mainMenuBar, "FrontLimbSwingLen", TW_TYPE_DOUBLE, &frontLimbSwingLen, "group='FrontLeg' step=0.01 max=1.0 min=0.8");
	TwAddVarRW(mainMenuBar, "FrontFeetStanceAngle", TW_TYPE_DOUBLE, &frontFeetStanceAngle, "group='FrontLeg' step=1.0 max=120 min=-120");
	TwAddVarRW(mainMenuBar, "FrontFeetSwingAngle", TW_TYPE_DOUBLE, &frontFeetSwingAngle, "group='FrontLeg' step=5.0 max=120 min=-120");

	// TwAddVarRW(mainMenuBar, "RearLimbAngleRatio", TW_TYPE_DOUBLE, &rearLimbAngleRatio, "group='RearLeg' step=1 max=5 min=-5");
	TwAddVarRW(mainMenuBar, "RearLimbFlip", TW_TYPE_BOOLCPP, &rearLimbFlip, "group='RearLeg'");
	TwAddVarRW(mainMenuBar, "RearShoulderFlip", TW_TYPE_BOOLCPP, &rearShoulderFlip, "group='RearLeg'");
	TwAddVarRW(mainMenuBar, "RearLimbStanceLen", TW_TYPE_DOUBLE, &rearLimbStanceLen, "group='RearLeg' step=0.01 max=1.0 min=0.8");
	TwAddVarRW(mainMenuBar, "RearLimbSwingLen", TW_TYPE_DOUBLE, &rearLimbSwingLen, "group='RearLeg' step=0.01 max=1.0 min=0.8");
	TwAddVarRW(mainMenuBar, "RearFeetStanceAngle", TW_TYPE_DOUBLE, &rearFeetStanceAngle, "group='RearLeg' step=1.0 max=120 min=-120");
	TwAddVarRW(mainMenuBar, "RearFeetSwingAngle", TW_TYPE_DOUBLE, &rearFeetSwingAngle, "group='RearLeg' step=5.0 max=120 min=-120");

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();

	showGroundPlane = true;
	rotateWidget.visible = false;

	skeletonWindow = new SkeletonMorphWindow(size[0], 0, w, h, this);
	moptWindow = new MOPTWindow(size[0], 0, w, h, this);

	motionGraphViewer = new MotionGraphViewer(size[0] + w, h - w / 3, w / 3, w / 3);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);
	setViewportParameters(size[0] + w, 0, w, h);

	loadConfig("../data/robotAnimation/animationConfig.cfg");

	// load template
	if (animConfig.templateConfig.rbsPath.length() > 0) {
		skeletonWindow->loadRobot(animConfig.templateConfig.rbsPath.c_str(),
			animConfig.templateConfig.rsPath.length() > 0 ? animConfig.templateConfig.rsPath.c_str() : NULL);
	}
}

void RobotAnimationApp::loadRobot(const char* fName, const char* fNameRS) {
	delete robot;
	delete startState;
	delete rbEngine;
	delete controller;
	delete worldOracle;
	delete motionGraph;
	delete pathFollower;
	motionGraphViewer->reset();
	moptWindow->reset();
	robotPath.reset();

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);

	// *************************** load robot ***************************
	rbEngine = new ODERBEngine();
	rbEngine->loadRBsFromFile(fName);

	robot = new Robot(rbEngine->rbs[0]);
	setupSimpleRobotStructure(robot);

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");

	robot->bFrame->updateStateInformation();

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
		robot->getRigidBody(i)->meshes.clear();

	startState = new ReducedRobotState(robot);
	if (fNameRS)
		startState->readFromFile(fNameRS);
	robot->setState(startState);

	// *************************** load window ***************************
	moptWindow->loadRobot(robot, startState);

	// *************************** setup controller ***************************
	controller = new KinematicRobotController(robot, moptWindow->locomotionManager->motionPlan);

	motionGraph = new MotionGraph();
	motionGraphViewer->motionGraph = motionGraph;

	pathFollower = new PathFollower(robot, motionGraph, &robotPath);
}

void RobotAnimationApp::loadAnimRobot(const char* fName)
{
	delete animRobot;
	delete animRbEngine;
	delete animStartState;

	// ******************* load animation robot *******************
	animRbEngine = new ODERBEngine();
	animRbEngine->loadRBsFromFile(fName);

	animRobot = new Robot(animRbEngine->rbs[0]);
	setupSimpleRobotStructure(animRobot);

	animRobot->bFrame->updateStateInformation();

	for (int i = 0; i < animRobot->getRigidBodyCount(); i++)
		animRobot->getRigidBody(i)->meshes.clear();

	animStartState = new ReducedRobotState(animRobot);
	inferLegParamsFromZeroAnimState();

	ikSolver = new IK_Solver(animRobot);
}

void RobotAnimationApp::loadRobotFromModeler()
{
	string tmpRBSPath = "../out/tmpSimpRobot.rbs";
	string tmpAnimRBSPath = "../out/tmpAnimRobot.rbs";
	skeletonWindow->saveRobotSimp(tmpRBSPath.c_str());
	skeletonWindow->saveRobot(tmpAnimRBSPath.c_str());
	loadRobot(tmpRBSPath.c_str());
	loadAnimRobot(tmpAnimRBSPath.c_str());
	// loadFBXFile(skeletonWindow->FBXPath.c_str());

	windowOption = SKELETON_WINDOW;
}

void RobotAnimationApp::loadFBXFile(const char* fName)
{
	delete m_pSkeleton;
	delete m_pMesh;
	delete m_pRBSMesh;

	FBXImporter fImporter(fName);

	delete m_pSkeleton;
	m_pSkeleton = new Skeleton();
	fImporter.createSkeletonAndMesh(*m_pSkeleton, &m_pMesh);

	m_pSkeleton->setScaleForRootHeightInWorldToBe(0.2);

	///< Assume everything is createad
	{
		delete m_pRBSMesh;
		m_pRBSMesh = new RBSMesh_BranchesAndChainsMap();

		m_pRBSMesh->m_pSkeleton = m_pSkeleton;
		m_pRBSMesh->m_pRobot = robot;
		m_pRBSMesh->m_pMesh = m_pMesh;

		m_pRBSMesh->initialise();
	}
}

void RobotAnimationApp::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (windowOption == SKELETON_WINDOW && fNameExt.compare("fbx") == 0) {
		skeletonWindow->loadFile(fName);
		return;
	}

	if (fNameExt.compare("ra") == 0) {
		loadContextFromFile(fName);
		return;
	}

	if (fNameExt.compare("rbs") == 0) {
		loadRobot(fName);
		return;
	}

	if (fNameExt.compare("cfg") == 0) {
		loadConfig(fName);
		return;
	}
}

RobotAnimationApp::~RobotAnimationApp(void) {

	delete motionGraph;
	delete motionGraphViewer;
	delete moptWindow;
	delete skeletonWindow;
	delete startState;
	delete robot;
	delete rbEngine;
	delete controller;
	delete worldOracle;
	delete animRobot;
	delete animRbEngine;
	delete animStartState;

	delete m_pSkeleton;
	delete m_pMesh;
	delete m_pRBSMesh;
}

// Restart the application.
void RobotAnimationApp::restart() {

}


void RobotAnimationApp::generateNewMP()
{
	if (motionGraphViewer->selectedNode)
	{
		motionGraphViewer->selectedNode->manager->locked = true;
	}
	if (motionGraphViewer->selectedEdge)
	{
		motionGraphViewer->selectedEdge->manager->locked = true;
	}

	// ******************* generate motion transition *******************
	if (motionGraphViewer->secondSelectedNode)
	{
		MotionGraphEdge* oEdge = motionGraph->findEdge(motionGraphViewer->selectedNode, motionGraphViewer->secondSelectedNode);
		if (oEdge) motionGraph->removeEdge(oEdge);

		// MotionTree is responsible for releasing memory of both manager and node.
		LocomotionEngineManager* nManager = moptWindow->initializeNewTransitionMP();
		motionGraphViewer->selectedNode->manager->locked = true;
		motionGraphViewer->secondSelectedNode->manager->locked = true;
		MotionGraphEdge* nEdge = new MotionGraphEdge(nManager, motionGraphViewer->selectedNode, motionGraphViewer->secondSelectedNode);
		motionGraph->addEdge(nEdge);
		motionGraphViewer->selectedEdge = nEdge;
		motionGraphViewer->selectedNode = NULL;
		motionGraphViewer->secondSelectedNode = NULL;
	}
	// ******************* generate motion plan *******************
	else {
		// MotionTree is responsible for releasing memory of both manager and node.
		LocomotionEngineManager* nManager = moptWindow->initializeNewMP();
		MotionGraphNode* nNode = new MotionGraphNode(nManager);
		motionGraph->addNode(nNode);
		motionGraphViewer->selectedNode = nNode;
	}

	runOption = MOTION_PLAN_OPTIMIZATION;
	windowOption = MOPT_WINDOW;
}

void RobotAnimationApp::handleMotionGraphViewer(int mods)
{
	if (motionGraphViewer->secondSelectedNode)
	{
		moptWindow->prepareForTransitionMOPT(motionGraphViewer->selectedNode, motionGraphViewer->secondSelectedNode);
		return;
	}

	if (motionGraphViewer->selectedNode)
	{
		if (runOption == ROBOT_ANIMATION)
		{
			prepareForRobotAnimation();
		}

		moptWindow->syncWithMotionGraphNode(motionGraphViewer->selectedNode);
		return;
	}

	if (motionGraphViewer->selectedEdge)
	{
		moptWindow->syncWithMotionGraphEdge(motionGraphViewer->selectedEdge);
		return;
	}

	moptWindow->reset();
}

void RobotAnimationApp::deleteMTNodeOrEdge()
{
	if (motionGraphViewer->selectedNode || motionGraphViewer->selectedEdge)
	{
		if (motionGraphViewer->selectedNode)
			motionGraph->removeNode(motionGraphViewer->selectedNode);
		else
			motionGraph->removeEdge(motionGraphViewer->selectedEdge);
		motionGraphViewer->reset();
		moptWindow->reset();
	}
}

void RobotAnimationApp::prepareForRobotAnimation()
{
	if (motionGraphViewer->transitionEdge)
		return;

	if (!controller->node) {
		controller->loadMotionPlan(motionGraphViewer->selectedNode->motionPlan);
		controller->node = motionGraphViewer->selectedNode;
		motionGraphViewer->selectedNode->manager->locked = true;
	}
	else if (controller->node != motionGraphViewer->selectedNode)
	{
		MotionGraphEdge* edge = motionGraph->findEdge(controller->node, motionGraphViewer->selectedNode);
		if (edge)
		{
			motionGraphViewer->startTransition = false;
			motionGraphViewer->transitionEdge = edge;
		}
		else {
			controller->loadMotionPlan(motionGraphViewer->selectedNode->motionPlan);
			controller->node = motionGraphViewer->selectedNode;
			motionGraphViewer->selectedNode->manager->locked = true;
		}
	}
}

void RobotAnimationApp::handleTransition()
{
	if (motionGraphViewer->transitionEdge)
	{
		LocomotionEngineMotionPlan* transMotionPlan = motionGraphViewer->transitionEdge->motionPlan;
		// Logger::print("phase1: %lf, phase2: %lf\n", controller->stridePhase, transMotionPlan->transitionStartPhase);
		if (!motionGraphViewer->startTransition && (fabs(controller->stridePhase - transMotionPlan->transitionStartPhase) < 1e-5
			|| fabs(controller->stridePhase - transMotionPlan->transitionStartPhase - 1) < 1e-5))
		{
			motionGraphViewer->startTransition = true;
			controller->loadMotionPlan(transMotionPlan);
		}
		if (motionGraphViewer->startTransition && controller->stridePhase > 1.0 - 1e-5)
		{
			motionGraphViewer->startTransition = false;
			controller->loadMotionPlan(motionGraphViewer->selectedNode->motionPlan, transMotionPlan->transitionEndPhase);
			controller->node = motionGraphViewer->selectedNode;
			motionGraphViewer->transitionEdge = NULL;
		}
	}
}

void RobotAnimationApp::loadConfig(const char* fName)
{
	FILE* fp = fopen(fName, "r");

	char buffer[200];
	char keyword[50];

	animConfig = AnimationConfig();
	MotionClipConfig* curClipConfig = NULL;

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "MotionClipConfig") == 0)
		{
			animConfig.motionClipConfigs.push_back(MotionClipConfig());
			curClipConfig = &animConfig.motionClipConfigs.back();
		}
		else if (strcmp(keyword, "SimRobotPath") == 0)
		{
			char content[500];
			int num = sscanf(line + strlen(keyword), "%s", &content);
			animConfig.simRobotPath = content;
		}
		else if (strcmp(keyword, "AnimRobotPath") == 0)
		{
			char content[500];
			int num = sscanf(line + strlen(keyword), "%s", &content);
			animConfig.animRobotPath = content;
		}
		else if (strcmp(keyword, "ModelerTemplate") == 0)
		{
			char content1[500];
			char content2[500];
			int num = sscanf(line + strlen(keyword), "%s %s", &content1, &content2);
			animConfig.templateConfig.rbsPath = content1;
			animConfig.templateConfig.rsPath = content2;
		}
		else if (strcmp(keyword, "MotionClipName") == 0)
		{
			char content[500];
			int num = sscanf(line + strlen(keyword), "%s", &content);
			curClipConfig->motionClipName = content;
		}
		else if (strcmp(keyword, "FootFallPatternPath") == 0)
		{
			char content[500];
			int num = sscanf(line + strlen(keyword), "%s", &content);
			curClipConfig->footFallPatternPath = content;
		}
		else if (strcmp(keyword, "SpeedX") == 0)
		{
			double val;
			int num = sscanf(line + strlen(keyword), "%lf", &val);
			curClipConfig->speedX = val;
		}
		else if (strcmp(keyword, "SpeedZ") == 0)
		{
			double val;
			int num = sscanf(line + strlen(keyword), "%lf", &val);
			curClipConfig->speedZ = val;
		}
		else if (strcmp(keyword, "TurningAngle") == 0)
		{
			double val;
			int num = sscanf(line + strlen(keyword), "%lf", &val);
			curClipConfig->turningAngle = val;
		}
	}

	fclose(fp);

	loadRobot(animConfig.simRobotPath.c_str());
	loadAnimRobot(animConfig.animRobotPath.c_str());
}

void RobotAnimationApp::saveContextToFile(const char* fName)
{
	FILE* fp = fopen(fName, "w+");

	motionGraph->saveToFile(fp);

	// ******************* Selected Node or Edge *******************
	if (motionGraphViewer->selectedNode)
		fprintf(fp, "SelectedNode %d\n", motionGraph->getNodeIndex(motionGraphViewer->selectedNode));
	if (motionGraphViewer->secondSelectedNode)
		fprintf(fp, "SecondSelectedNode %d\n", motionGraph->getNodeIndex(motionGraphViewer->secondSelectedNode));
	if (motionGraphViewer->selectedEdge)
		fprintf(fp, "SelectedEdge %d\n", motionGraph->getEdgeIndex(motionGraphViewer->selectedEdge));
	fprintf(fp, "SyncMOPT\n\n");

	moptWindow->saveToFile(fp);
	robotPath.saveToFile(fp);

	fclose(fp);
}

void RobotAnimationApp::loadContextFromFile(const char* fName)
{
	reset();

	FILE* fp = fopen(fName, "r");

	char buffer[200];
	char keyword[50];

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "MotionGraph") == 0)
		{
			motionGraph->loadFromFile(fp, robot);
		}
		else if (strcmp(keyword, "SelectedNode") == 0)
		{
			int index;
			int num = sscanf(line + strlen(keyword), "%d", &index);
			motionGraphViewer->selectedNode = motionGraph->getNode(index);
		}
		else if (strcmp(keyword, "SecondSelectedNode") == 0)
		{
			int index;
			int num = sscanf(line + strlen(keyword), "%d", &index);
			motionGraphViewer->secondSelectedNode = motionGraph->getNode(index);
		}
		else if (strcmp(keyword, "SelectedEdge") == 0)
		{
			int index;
			int num = sscanf(line + strlen(keyword), "%d", &index);
			motionGraphViewer->selectedEdge = motionGraph->getEdge(index);
		}
		else if (strcmp(keyword, "SyncMOPT") == 0)
		{
			handleMotionGraphViewer(0);
		}
		else if (strcmp(keyword, "MOPTWindow") == 0)
		{
			moptWindow->loadFromFile(fp);
		}
		else if (strcmp(keyword, "RobotPath") == 0)
		{
			robotPath.loadFromFile(fp);
		}
	}

	fclose(fp);
}

void RobotAnimationApp::reset()
{
	delete pathFollower;
	delete motionGraph;
	motionGraph = new MotionGraph();
	motionGraphViewer->motionGraph = motionGraph;
	motionGraphViewer->reset();
	moptWindow->reset();
	robotPath.reset();

	pathFollower = new PathFollower(robot, motionGraph, &robotPath);
}

void RobotAnimationApp::createNewPathPts(double xPos, double yPos)
{
	Ray mouseRay = getRayFromScreenCoords(xPos, yPos);

	double t = -mouseRay.origin[1] / mouseRay.direction[1];
	if (t <= 0) return;

	robotPath.pathPts.push_back(mouseRay.getPointAt(t));
}

void RobotAnimationApp::handleMotionSwitch()
{
	if (motionGraphViewer->transitionEdge) return;
	if (robotPath.pathPts.size() < 2) return;

	if (!((fabs(controller->stridePhase) < 1e-3
		|| fabs(controller->stridePhase - 1) < 1e-3))) return;

	bool res = pathFollower->updateTargetPoint();

	if (res)
		motionGraphViewer->selectedNode = pathFollower->calBestMotionToFollow();
	else
		motionGraphViewer->selectedNode = NULL;
}

void RobotAnimationApp::generateWalkTrotMPs()
{
	int zeroStateOptIterNum = 200;
	int perStageOptIterNum = 100;
	int stageNum = 2;

	for (auto& clipConfig : animConfig.motionClipConfigs)
	{
		moptWindow->loadFFPFromFile(clipConfig.footFallPatternPath.c_str());

		LocomotionEngineManager* nManager = moptWindow->initializeNewMP();
		for (int j = 0; j < zeroStateOptIterNum; j++)
			nManager->runMOPTStep();

		for (int i = 0; i < stageNum; i++)
		{
			nManager->motionPlan->desDistanceToTravel[0] = clipConfig.speedX / stageNum * (i + 1);
			nManager->motionPlan->desDistanceToTravel[2] = clipConfig.speedZ / stageNum * (i + 1);
			nManager->motionPlan->desTurningAngle = clipConfig.turningAngle / stageNum * (i + 1);

			for (int j = 0; j < perStageOptIterNum; j++)
				nManager->runMOPTStep();
		}

		MotionGraphNode* nNode = new MotionGraphNode(nManager);
		motionGraph->addNode(nNode);

		string folderName = "../out/" + clipConfig.motionClipName + "/";
		exportMotion(nNode, folderName.c_str());
	}

	moptWindow->reset();
	runOption = MOTION_PLAN_OPTIMIZATION;
	windowOption = MOPT_WINDOW;
}

void RobotAnimationApp::syncAnimRobotWithRobot()
{
	ReducedRobotState curRobotState(robot);
	ReducedRobotState curAnimState(animRobot);
	curAnimState.setPosition(curRobotState.getPosition());
	curAnimState.setOrientation(curRobotState.getOrientation());
	Quaternion heading = getRotationQuaternion(curRobotState.getHeading(), V3D(0, 1, 0));

	auto& robotLimbs = robot->getEndEffectorRBs();
	auto& animLimbs = animRobot->getEndEffectorRBs();
	vector<bool> isFrontLimb;
	for (uint i = 0; i < robotLimbs.size(); i++) {
		RigidBody* robotLimb = robotLimbs[i];
		Joint* rootJoint = robotLimb->pJoints[0];
		while (rootJoint->parent != robot->getRoot())
			rootJoint = rootJoint->parent->pJoints[0];
		P3D rootJPos = rootJoint->getWorldPosition();
		if (V3D(curRobotState.getPosition(), rootJPos).dot(heading.rotate(V3D(0, 0, 1))) > 0)
		{
			isFrontLimb.push_back(true);
		}
		else {
			isFrontLimb.push_back(false);
		}
	}

	set<Joint*> boundaryJoints;
	for (uint i = 0; i < robotLimbs.size(); i++)
	{
		RigidBody* robotLimb = robotLimbs[i];
		boundaryJoints.insert(robotLimb->pJoints[0]->parent->pJoints[0]);
	}

	for (uint i = 0; i < robot->getRoot()->cJoints.size(); i++)
	{
		Joint* robotJoint = robot->getRoot()->cJoints[i];
		Joint* animJoint = animRobot->getRoot()->cJoints[i];

		syncNonLimbJointRelOrientation(robotJoint, animJoint, curRobotState, curAnimState, boundaryJoints);
	}

	animRobot->setState(&curAnimState);

	for (uint i = 0; i < animLimbs.size(); i++)
	{
		RigidBody* animLimb = animLimbs[i];
		RigidBody* robotLimb = robotLimbs[i];
		P3D EEPos = robotLimb->getWorldCoordinates(robotLimb->rbProperties.endEffectorPoints[0].coords);
		double swingScale = EEPos[1] / 0.01;

		Joint* joint1 = animLimb->pJoints[0];
		Joint* joint2 = joint1->parent->pJoints[0];
		Joint* joint3 = joint2->parent->pJoints[0];
		Joint* joint4 = joint3->parent->pJoints[0];

		if (isFrontLimb[i])
		{
			adjustAnimRobotAngle(joint1, RAD(footSwingAngle * swingScale - 180));
			adjustAnimRobotAngle(joint2, RAD(footSwingAngle * swingScale - 180));
		}
		else {
			adjustAnimRobotAngle(joint1, RAD(footSwingAngle * swingScale - 180));
			adjustAnimRobotAngle(joint2, RAD(-footSwingAngle * swingScale - rearKneeAngle - 180));
		}


		P3D jPos3 = joint3->getWorldPosition();
		P3D jPos4 = joint4->getWorldPosition();
		P3D animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);

		double r1 = (jPos3 - animEEPos).norm();
		double r2 = (jPos4 - jPos3).norm();
		V3D limbVec = jPos4 - EEPos;
		double r3 = limbVec.norm();

		// test triangle inequality
		if (r1 + r2 < r3 || r1 + r3 < r2 || r2 + r3 < r1) continue;

		double alpha = acos((r1 * r1 + r3 * r3 - r2 * r2) / (2 * r1 * r3));
		V3D jointAxis = joint3->parent->getWorldCoordinates(((HingeJoint*)joint3)->rotationAxis).normalized();

		P3D newJPos = EEPos + getRotationQuaternion(alpha * (isFrontLimb[i] ? -1 : 1), jointAxis).rotate(limbVec.normalized()) * r1;

		// adjust joint position first
		adjustAnimRobotJPos(joint4, jPos3, newJPos);

		// adjust EE position
		animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		adjustAnimRobotJPos(joint3, animEEPos, EEPos);
	}
}

void RobotAnimationApp::syncAnimRobotWithRobotV2()
{
	ReducedRobotState curRobotState(robot);
	ReducedRobotState curAnimState(animRobot);
	curAnimState.setPosition(curRobotState.getPosition());
	curAnimState.setOrientation(curRobotState.getOrientation());
	Quaternion heading = getRotationQuaternion(curRobotState.getHeading(), V3D(0, 1, 0));

	auto& robotLimbs = robot->getEndEffectorRBs();
	auto& animLimbs = animRobot->getEndEffectorRBs();
	vector<bool> isFrontLimb;
	for (uint i = 0; i < robotLimbs.size(); i++) {
		RigidBody* robotLimb = robotLimbs[i];
		Joint* rootJoint = robotLimb->pJoints[0];
		while (rootJoint->parent != robot->getRoot())
			rootJoint = rootJoint->parent->pJoints[0];
		P3D rootJPos = rootJoint->getWorldPosition();
		if (V3D(curRobotState.getPosition(), rootJPos).dot(heading.rotate(V3D(0, 0, 1))) > 0)
		{
			isFrontLimb.push_back(true);
		}
		else {
			isFrontLimb.push_back(false);
		}
	}

	set<Joint*> boundaryJoints;
	for (uint i = 0; i < robotLimbs.size(); i++)
	{
		RigidBody* robotLimb = robotLimbs[i];
		boundaryJoints.insert(robotLimb->pJoints[0]->parent->pJoints[0]);
	}

	for (uint i = 0; i < robot->getRoot()->cJoints.size(); i++)
	{
		Joint* robotJoint = robot->getRoot()->cJoints[i];
		Joint* animJoint = animRobot->getRoot()->cJoints[i];

		syncNonLimbJointRelOrientation(robotJoint, animJoint, curRobotState, curAnimState, boundaryJoints);
	}

	animRobot->setState(&curAnimState);

	for (uint i = 0; i < animLimbs.size(); i++)
	{
		RigidBody* animLimb = animLimbs[i];
		RigidBody* robotLimb = robotLimbs[i];
		P3D EEPos = robotLimb->getWorldCoordinates(robotLimb->rbProperties.endEffectorPoints[0].coords);
		double swingScale = EEPos[1] / 0.01;

		Joint* joint1 = animLimb->pJoints[0];
		Joint* joint2 = joint1->parent->pJoints[0];
		Joint* joint3 = joint2->parent->pJoints[0];
		Joint* joint4 = joint3->parent->pJoints[0];

		P3D jPos1 = joint1->getWorldPosition();
		P3D jPos2 = joint2->getWorldPosition();
		P3D jPos3 = joint3->getWorldPosition();
		P3D jPos4 = joint4->getWorldPosition();
		P3D animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		double l1 = (jPos3 - jPos2).norm();
		double l2 = (jPos2 - jPos1).norm();
		double l3 = (jPos1 - animEEPos).norm();
		double L = l1 + l2 + l3;
		V3D limbVec = jPos4 - EEPos;
		double r3 = limbVec.norm();
		double r2 = (jPos4 - jPos3).norm();

		double limbStanceLen = isFrontLimb[i] ? frontLimbStanceLen : rearLimbStanceLen;
		double limbSwingLen = isFrontLimb[i] ? frontLimbSwingLen : rearLimbSwingLen;
		double limbAngleRatio = isFrontLimb[i] ? frontLimbAngleRatio : rearLimbAngleRatio;
		bool flipLimb = isFrontLimb[i] ? frontLimbFlip : rearLimbFlip;
		bool flipShoulder = isFrontLimb[i] ? frontShoulderFlip : rearShoulderFlip;

		double targetLen = max((r3 - r2) * 1.02, L * ((1 - swingScale) * limbStanceLen + swingScale * limbSwingLen));
		double jAngle = solveJointAnglesForLimb(l1, l2, l3, limbAngleRatio, targetLen, flipLimb);

		adjustAnimRobotAngle(joint1, RAD(jAngle * limbAngleRatio - 180));
		adjustAnimRobotAngle(joint2, RAD(jAngle - 180));

		jPos3 = joint3->getWorldPosition();
		animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);

		double r1 = (jPos3 - animEEPos).norm();

		// test triangle inequality
		if (r1 + r2 < r3 || r1 + r3 < r2 || r2 + r3 < r1) continue;

		double alpha = acos((r1 * r1 + r3 * r3 - r2 * r2) / (2 * r1 * r3));
		V3D jointAxis = joint3->parent->getWorldCoordinates(((HingeJoint*)joint3)->rotationAxis).normalized();

		P3D newJPos = EEPos + getRotationQuaternion(alpha * (flipShoulder ? -1 : 1), jointAxis).rotate(limbVec.normalized()) * r1;

		// adjust joint position first
		adjustAnimRobotJPos(joint4, jPos3, newJPos);

		// adjust EE position
		animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		adjustAnimRobotJPos(joint3, animEEPos, EEPos);
	}
}

void RobotAnimationApp::syncAnimRobotWithRobotV3()
{
	ReducedRobotState curRobotState(robot);
	ReducedRobotState curAnimState(animRobot);
	curAnimState = *animStartState;
	curAnimState.setPosition(curRobotState.getPosition());
	curAnimState.setOrientation(curRobotState.getOrientation());
	Quaternion heading = getRotationQuaternion(curRobotState.getHeading(), V3D(0, 1, 0));

	auto& robotLimbs = robot->getEndEffectorRBs();
	auto& animLimbs = animRobot->getEndEffectorRBs();
	vector<bool> isFrontLimb;
	for (uint i = 0; i < robotLimbs.size(); i++) {
		RigidBody* robotLimb = robotLimbs[i];
		Joint* rootJoint = robotLimb->pJoints[0];
		while (rootJoint->parent != robot->getRoot())
			rootJoint = rootJoint->parent->pJoints[0];
		P3D rootJPos = rootJoint->getWorldPosition();
		if (V3D(curRobotState.getPosition(), rootJPos).dot(heading.rotate(V3D(0, 0, 1))) > 0)
		{
			isFrontLimb.push_back(true);
		}
		else {
			isFrontLimb.push_back(false);
		}
	}

	set<Joint*> boundaryJoints;
	for (uint i = 0; i < robotLimbs.size(); i++)
	{
		RigidBody* robotLimb = robotLimbs[i];
		boundaryJoints.insert(robotLimb->pJoints[0]->parent->pJoints[0]);
	}

	for (uint i = 0; i < robot->getRoot()->cJoints.size(); i++)
	{
		Joint* robotJoint = robot->getRoot()->cJoints[i];
		Joint* animJoint = animRobot->getRoot()->cJoints[i];

		syncNonLimbJointRelOrientation(robotJoint, animJoint, curRobotState, curAnimState, boundaryJoints);
	}

	animRobot->setState(&curAnimState);

	for (uint i = 0; i < animLimbs.size(); i++)
	{
		RigidBody* animLimb = animLimbs[i];
		RigidBody* robotLimb = robotLimbs[i];
		P3D EEPos = robotLimb->getWorldCoordinates(robotLimb->rbProperties.endEffectorPoints[0].coords);
		double swingScale = EEPos[1] / 0.01;

		double limbStanceLen = isFrontLimb[i] ? frontLimbStanceLen : rearLimbStanceLen;
		double limbSwingLen = isFrontLimb[i] ? frontLimbSwingLen : rearLimbSwingLen;
		double feetStanceAngle = isFrontLimb[i] ? frontFeetStanceAngle : rearFeetStanceAngle;
		double feetSwingAngle = isFrontLimb[i] ? frontFeetSwingAngle : rearFeetSwingAngle;
		bool flipLimb = isFrontLimb[i] ? frontLimbFlip : rearLimbFlip;
		bool flipShoulder = isFrontLimb[i] ? frontShoulderFlip : rearShoulderFlip;

		Joint* joint1 = animLimb->pJoints[0];
		Joint* joint2 = joint1->parent->pJoints[0];
		Joint* joint3 = joint2->parent->pJoints[0];
		Joint* joint4 = joint3->parent->pJoints[0];

		P3D jPos1 = joint1->getWorldPosition();
		P3D jPos2 = joint2->getWorldPosition();
		P3D jPos3 = joint3->getWorldPosition();
		P3D jPos4 = joint4->getWorldPosition();
		P3D animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		V3D jointAxis = joint4->parent->getWorldCoordinates(((HingeJoint*)joint4)->rotationAxis).normalized();
		double l1 = (jPos3 - jPos2).norm();
		double l2 = (jPos2 - jPos1).norm();
		double l3 = (jPos1 - animEEPos).norm();
		double L = l1 + l2;
		V3D EEUp = (V3D(0, 1, 0) - jointAxis * jointAxis[1]).normalized();
		double feetAngle = swingScale * feetSwingAngle + (1 - swingScale) * feetStanceAngle;
		P3D robotJPos1 = EEPos + getRotationQuaternion(RAD(feetAngle), jointAxis).rotate(EEUp) * l3;

		V3D limbVec = jPos4 - robotJPos1;
		double r2 = (jPos4 - jPos3).norm();
		double r3 = limbVec.norm();

		double r1 = min(0.999 * L, min((r3 + r2) * 0.999, max((r3 - r2) * 1.001, L * ((1 - swingScale) * limbStanceLen + swingScale * limbSwingLen))));
		double joint2Angle = acos((l1 * l1 + l2 * l2 - r1 * r1) / (2 * l1 * l2));
		adjustAnimRobotAngle(joint2, joint2Angle * (flipLimb ? 1 : -1));

		// test triangle inequality
		if (r1 + r2 < r3 || r1 + r3 < r2 || r2 + r3 < r1) {
			Logger::consolePrint("Cannot achieve current parameters because of triangle inequality!");
			continue;
		}

		double alpha = acos((r1 * r1 + r3 * r3 - r2 * r2) / (2 * r1 * r3));

		P3D newJPos3 = robotJPos1 + getRotationQuaternion(alpha * (flipShoulder ? -1 : 1), jointAxis).rotate(limbVec.normalized()) * r1;

		// adjust joint position first
		adjustAnimRobotJPos(joint4, joint3->getWorldPosition(), newJPos3);

		// adjust joint1 position
		jPos1 = joint1->getWorldPosition();
		adjustAnimRobotJPos(joint3, jPos1, robotJPos1);

		// adjust EE position
		animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		adjustAnimRobotJPos(joint1, animEEPos, EEPos);
	}
}

void RobotAnimationApp::inferLegParamsFromZeroAnimState()
{
	ReducedRobotState curRobotState(robot);
	Quaternion heading = getRotationQuaternion(curRobotState.getHeading(), V3D(0, 1, 0));

	auto& robotLimbs = robot->getEndEffectorRBs();
	auto& animLimbs = animRobot->getEndEffectorRBs();
	vector<bool> isFrontLimb;
	for (uint i = 0; i < robotLimbs.size(); i++) {
		RigidBody* robotLimb = robotLimbs[i];
		Joint* rootJoint = robotLimb->pJoints[0];
		while (rootJoint->parent != robot->getRoot())
			rootJoint = rootJoint->parent->pJoints[0];
		P3D rootJPos = rootJoint->getWorldPosition();
		if (V3D(curRobotState.getPosition(), rootJPos).dot(heading.rotate(V3D(0, 0, 1))) > 0)
		{
			isFrontLimb.push_back(true);
		}
		else {
			isFrontLimb.push_back(false);
		}
	}

	animRobot->setState(animStartState);

	for (uint i = 0; i < animLimbs.size(); i++)
	{
		RigidBody* animLimb = animLimbs[i];

		double& limbStanceLen = isFrontLimb[i] ? frontLimbStanceLen : rearLimbStanceLen;
		double& feetStanceAngle = isFrontLimb[i] ? frontFeetStanceAngle : rearFeetStanceAngle;
		double& feetSwingAngle = isFrontLimb[i] ? frontFeetSwingAngle : rearFeetSwingAngle;
		bool& flipLimb = isFrontLimb[i] ? frontLimbFlip : rearLimbFlip;
		bool& flipShoulder = isFrontLimb[i] ? frontShoulderFlip : rearShoulderFlip;

		Joint* joint1 = animLimb->pJoints[0];
		Joint* joint2 = joint1->parent->pJoints[0];
		Joint* joint3 = joint2->parent->pJoints[0];
		Joint* joint4 = joint3->parent->pJoints[0];

		P3D jPos1 = joint1->getWorldPosition();
		P3D jPos2 = joint2->getWorldPosition();
		P3D jPos3 = joint3->getWorldPosition();
		P3D jPos4 = joint4->getWorldPosition();
		P3D animEEPos = animLimb->getWorldCoordinates(animLimb->rbProperties.endEffectorPoints[0].coords);
		V3D jointAxis = joint3->parent->getWorldCoordinates(((HingeJoint*)joint3)->rotationAxis).normalized();
		V3D EEUp = (V3D(0, 1, 0) - jointAxis * jointAxis[1]).normalized();
		V3D feetVec = (jPos1 - animEEPos).normalized();

		flipShoulder = (jPos3 - jPos2).cross(jPos4 - jPos2).dot(jointAxis) > 0;
		flipLimb = (jPos2 - jPos1).cross(jPos3 - jPos1).dot(jointAxis) > 0;
		limbStanceLen = (jPos3 - jPos1).norm() / ((jPos3 - jPos2).norm() + (jPos2 - jPos1).norm());
		feetStanceAngle = DEG(acos(EEUp.dot(feetVec)));
		feetStanceAngle *= EEUp.cross(feetVec).dot(jointAxis) > 0 ? 1 : -1;
		feetSwingAngle = feetStanceAngle + 20.0;
	}
}

void RobotAnimationApp::syncNonLimbJointRelOrientation(Joint* robotJoint, Joint* animJoint, ReducedRobotState& robotState, ReducedRobotState& animState, set<Joint*>& boundaryJoints)
{
	if (boundaryJoints.count(robotJoint)) return;

	animState.setJointRelativeOrientation(robotState.getJointRelativeOrientation(robotJoint->jIndex), animJoint->jIndex);

	for (uint i = 0; i < robotJoint->child->cJoints.size(); i++)
	{
		Joint* childRobotJoint = robotJoint->child->cJoints[i];
		Joint* childAnimJoint = animJoint->child->cJoints[i];

		syncNonLimbJointRelOrientation(childRobotJoint, childAnimJoint, robotState, animState, boundaryJoints);
	}
}

void RobotAnimationApp::adjustAnimRobotAngle(Joint* joint, double angle)
{
	ReducedRobotState curState(animRobot);
	animRobot->setState(animStartState);

	RigidBody* parentRB = joint->parent;
	RigidBody* childRB = joint->child;

	P3D JPos = joint->getWorldPosition();
	P3D pJPos = parentRB->pJoints[0]->getWorldPosition();
	P3D cJPos = childRB->cJoints.size() > 0 ? childRB->cJoints[0]->getWorldPosition() : childRB->getWorldCoordinates(childRB->rbProperties.endEffectorPoints[0].coords);
	V3D JAxis = ((HingeJoint*)joint)->rotationAxis.normalized();
	V3D worldJAxis = parentRB->getWorldCoordinates(JAxis);

	V3D e1 = (pJPos - JPos).normalized();
	V3D e2 = (cJPos - JPos).normalized();
	double curAngle = acos(e1.dot(e2)) * (e1.cross(e2).dot(worldJAxis) > 0 ? 1 : -1);

	curState.setJointRelativeOrientation(getRotationQuaternion(angle - curAngle, JAxis), joint->jIndex);
	animRobot->setState(&curState);
}

void RobotAnimationApp::adjustAnimRobotJPos(Joint* parentJoint, P3D curPos, P3D newPos)
{
	if ((curPos - newPos).norm() < 1e-6) return;

	ReducedRobotState curState(animRobot);
	P3D pJPos = parentJoint->getWorldPosition();
	V3D JAxis = ((HingeJoint*)parentJoint)->rotationAxis.normalized();
	V3D worldJAxis = parentJoint->parent->getWorldCoordinates(JAxis).normalized();

	V3D e1 = (curPos - pJPos).normalized();
	V3D e2 = (newPos - pJPos).normalized();
	e1 = (e1 - worldJAxis * e1.dot(worldJAxis)).normalized();
	e2 = (e2 - worldJAxis * e2.dot(worldJAxis)).normalized();

	double rotAngle = acos(e1.dot(e2)) * (e1.cross(e2).dot(worldJAxis) > 0 ? 1 : -1);
	curState.setJointRelativeOrientation(getRotationQuaternion(rotAngle, JAxis) * curState.getJointRelativeOrientation(parentJoint->jIndex), parentJoint->jIndex);

	animRobot->setState(&curState);
}

double RobotAnimationApp::solveJointAnglesForLimb(double r1, double r2, double r3, double k, double targetLen, bool flip)
{
	dVector p(1);
	p[0] = PI / 2;
	double val;
	ThreeLinkIKObj linkObj(r1, r2, r3, k, targetLen);
	NewtonFunctionMinimizer minimizer(20);
	minimizer.minimize(&linkObj, p, val);
	// Logger::print("angle: %lf val: %lf\n", p[0], linkObj.computeValueWithoutRegularizer(p));
	double angle = p[0];
	while (angle > PI)
		angle -= 2 * PI;
	while (angle < -PI)
		angle += 2 * PI;

	return DEG(fabs(angle) * (flip ? -1 : 1));
}

// Run the App tasks
void RobotAnimationApp::process() {

	LocomotionEngineManager* manager = moptWindow->locomotionManager;

	if (runOption == MOTION_PLAN_OPTIMIZATION)
	{
		if (manager && !manager->locked)
			moptWindow->runMOPTStep();
	}

	if (runOption == MOTION_PLAN_ANIMATION)
	{
		if (!manager) return;

		motionPlanTime += (1.0 / desiredFrameRate) / manager->motionPlan->motionPlanDuration;
		if (motionPlanTime > 1) {
			motionPlanTime -= 1;
		}
		moptWindow->setAnimationParams(motionPlanTime, 0);
	}

	if (runOption == ROBOT_ANIMATION)
	{
		if (motionGraphViewer->selectedNode || followPath)
		{
			double simulationTime = 0;
			double maxRunningTime = 1.0 / desiredFrameRate;

			while (simulationTime / maxRunningTime < animationSpeedupFactor) {

				if (followPath)
					handleMotionSwitch();
				if (!motionGraphViewer->selectedNode)
					return;

				prepareForRobotAnimation();
				handleTransition();
				simulationTime += simTimeStep;
				controller->advanceInTime(simTimeStep);
				controller->computeDesiredState();
				robot->setState(&controller->desiredState);
				robot->bFrame->updateStateInformation();
			}

			P3D p = robot->computeCOM();
			p.y() = 0;
			camera->setCameraTarget(p);
		}

		if (recordAnimation)
		{
			char buffer[50];
			sprintf(buffer, "%04d", animationFrame);
			string fName = "../out/robotStates/robotState_" + string(buffer) + ".rs";
			ReducedRobotState tmpState(robot);
			tmpState.writeToFile(fName.c_str(), robot);
			animationFrame++;
		}
	}

}

//triggered when mouse moves
bool RobotAnimationApp::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (windowOption == MOPT_WINDOW && (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos)))
		if (moptWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (windowOption == SKELETON_WINDOW && (skeletonWindow->isActive() || skeletonWindow->mouseIsWithinWindow(xPos, yPos)))
		if (skeletonWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (!motionGraphViewer->transitionEdge)
	{
		if (motionGraphViewer->isActive() || motionGraphViewer->mouseIsWithinWindow(xPos, yPos))
			if (motionGraphViewer->onMouseMoveEvent(xPos, yPos)) {
				if (runOption == ROBOT_ANIMATION) {
					motionGraphViewer->selectedEdge = NULL;
					motionGraphViewer->highlightedEdge = NULL;
				}
				return true;
			}
	}

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool RobotAnimationApp::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)) {
			return true;
		}

	if (windowOption == MOPT_WINDOW && (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos)))
		if (moptWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (windowOption == SKELETON_WINDOW && (skeletonWindow->isActive() || skeletonWindow->mouseIsWithinWindow(xPos, yPos)))
		if (skeletonWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (motionGraphViewer->isActive() || motionGraphViewer->mouseIsWithinWindow(xPos, yPos))
		if (motionGraphViewer->onMouseButtonEvent(button, action, mods, xPos, yPos))
		{
			if (action == GLFW_PRESS)
				handleMotionGraphViewer(mods);
			return true;
		}

	if (rotateWidget.onMouseButtonEvent(button, action, mods, xPos, yPos)) {
		return true;
	}

	if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if ((mods & GLFW_MOD_SHIFT) > 0)
		{
			createNewPathPts(xPos, yPos);
		}
	}

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool RobotAnimationApp::onMouseWheelScrollEvent(double xOffset, double yOffset) {

	if (windowOption == MOPT_WINDOW && (moptWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY)))
		if (moptWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (windowOption == SKELETON_WINDOW && (skeletonWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY)))
		if (skeletonWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool RobotAnimationApp::onKeyEvent(int key, int action, int mods) {

	if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		windowOption = MOPT_WINDOW;
	}

	if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
	{
		windowOption = SKELETON_WINDOW;
	}

	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		reset();
	}

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		robotPath.reset();
	}

	if (key == GLFW_KEY_E && action == GLFW_PRESS)
	{
		pathFollower->initializeRobot();
		P3D p = robot->computeCOM();
		p.y() = 0;
		camera->setCameraTarget(p);
	}

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		loadContextFromFile("../out/tmpRobotAnimation.ra");
	}

	if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		saveContextToFile("../out/tmpRobotAnimation.ra");
	}

	if (key == GLFW_KEY_D && action == GLFW_PRESS)
	{
		deleteMTNodeOrEdge();
	}

	if (key == GLFW_KEY_X && action == GLFW_PRESS)
	{

	}

	if (key == GLFW_KEY_F && action == GLFW_PRESS)
	{
		if (motionGraphViewer->selectedNode)
			exportMotion(motionGraphViewer->selectedNode, "../out/robotStates/");
	}

	if (!motionGraphViewer->transitionEdge)
	{
		if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		{
			runOption = MOTION_PLAN_OPTIMIZATION;
		}

		if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		{
			runOption = MOTION_PLAN_ANIMATION;
		}

		if (key == GLFW_KEY_3 && action == GLFW_PRESS)
		{
			runOption = ROBOT_ANIMATION;
		}
	}

	if (windowOption == SKELETON_WINDOW)
		if (skeletonWindow->onKeyEvent(key, action, mods)) return true;

	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool RobotAnimationApp::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void RobotAnimationApp::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

void RobotAnimationApp::exportMotion(MotionGraphNode* node, const char* folderName)
{
	mkdir(folderName);

	ReducedRobotState oldState(robot);

	LocomotionEngineMotionPlan* mp = node->manager->motionPlan;
	double f = 0;

	int index = 0;
	while (f <= 1.0 + motionExportRate * 0.5)
	{
		char buffer[50];
		sprintf(buffer, "robotState_%04d.rs", index);
		string fName = folderName + string(buffer);

		ReducedRobotState tmpState(robot);
		mp->robotStateTrajectory.getRobotPoseAt(f, tmpState);
		robot->setState(&tmpState);

		syncAnimRobotWithRobotV3();
		ReducedRobotState tmpAnimState(animRobot);
		tmpAnimState.writeToFile(fName.c_str(), animRobot);
		f += motionExportRate;
		index++;
	}

	robot->setState(&oldState);
}

void RobotAnimationApp::drawRobotPath()
{
	auto& points = robotPath.pathPts;

	double radius = 0.01;
	glColor4d(1.0, 0.0, 0.0, 1.0);
	for (uint j = 0; j < points.size(); j++)
	{
		drawSphere(points[j], radius, 10);
	}

	glColor4d(1.0, 0.5, 0.0, 1.0);
	for (int j = 0; j < (int)points.size() - 1; j++)
	{
		drawCylinder(points[j], points[j + 1], 0.005, 10);
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void RobotAnimationApp::drawScene() {

	//ReducedRobotState oldState(robot);
	//adjustPoseByIK(oldState);

	if (runOption == ROBOT_ANIMATION)
	{
		syncAnimRobotWithRobotV3();
	}

	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	int flags = SHOW_ABSTRACT_VIEW | SHOW_MESH | SHOW_MATERIALS; // SHOW_CD_PRIMITIVES
	P3D p = robot->computeCOM();
	p.y() = 0;

	glEnable(GL_LIGHTING);

	if (showRobotSimp)
		rbEngine->drawRBs(flags);
	animRbEngine->drawRBs(flags);

	// robot->setState(&oldState);

	glColor3d(1, 0, 0);
	drawSphere(p, 0.005);

	drawRobotPath();

	if (m_pRBSMesh)
	{
		m_pRBSMesh->update();
		m_pRBSMesh->draw();
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void RobotAnimationApp::drawAuxiliarySceneInfo() {
	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_VIEWPORT_BIT | GL_SCISSOR_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_TRANSFORM_BIT);

	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);
	rotateWidget.pos = robot->root->getCMPosition();
	rotateWidget.draw();
	glPopAttrib();

	if (windowOption == MOPT_WINDOW)
	{
		moptWindow->draw();
		moptWindow->drawAuxiliarySceneInfo();
	}

	if (windowOption == SKELETON_WINDOW)
	{
		skeletonWindow->draw();
		skeletonWindow->drawAuxiliarySceneInfo();
	}

	if (runOption == ROBOT_ANIMATION) {
		motionGraphViewer->selectedEdge = NULL;
		motionGraphViewer->highlightedEdge = NULL;
	}
	motionGraphViewer->draw();
}

bool RobotAnimationApp::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void RobotAnimationApp::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();

	moptWindow->setViewportParameters(size[0], 0, w, h);
	skeletonWindow->setViewportParameters(size[0], 0, w, h);
	motionGraphViewer->setViewportParameters(size[0] + w, h - w / 3, w / 3, w / 3);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);
	setViewportParameters(size[0] + w, 0, w, h);
}

void RobotAnimationApp::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
