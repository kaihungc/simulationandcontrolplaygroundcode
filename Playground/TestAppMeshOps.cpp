#include <igl/signed_distance.h>
#include <igl/copyleft/marching_cubes.h>
#include <igl/readOBJ.h>

#include <GUILib/GLUtils.h>
#include "TestAppMeshOps.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/ConvexHull3D.h>
#include <iostream>

/*
- robust distance field computation
- voxelize a mesh
- convex hull (fix it!)
- marching cubes on distance field...
*/

using namespace std;

/*
- compute volume swept by a mesh: sometimes this works, sometimes it does not...
- compute convex hull of a mesh: done! Seems to work.

- integrate libIGL code
- write methods to convert meshes from one representation to another

- compute distance field of a mesh...
- build & visualize octree of a mesh...
- constraint solver (BFGS, linearized version of quadratic constraints, etc...)
- hook up the marching cubes implementation to different voxel grids...
*/

void TW_CALL action(void* clientData) {
	TestAppMeshOps* app = dynamic_cast<TestAppMeshOps*>(GLApplication::getGLAppInstance());
	if (app == NULL)
		return;

	if (strcmp((char*)clientData, "save") == 0)
		app->saveFile("../out/outputMesh.obj");

	if (strcmp((char*)clientData, "signedDist") == 0)
		app->testSignedDistance();

	if (strcmp((char*)clientData, "convexHull") == 0)
		app->testConvexHull();

	if (strcmp((char*)clientData, "levelSet") == 0)
		app->testLevelSetCSG();

	if (strcmp((char*)clientData, "project") == 0)
		app->testLevelSetProjection();

	if (strcmp((char*)clientData, "fastMarching") == 0)
		app->testFastMarching();
}

TestAppMeshOps::TestAppMeshOps() {
	setWindowTitle("Empty Test Application...");

	tWidget = new TranslateWidget(AXIS_X | AXIS_Y | AXIS_Z);
	//	rWidget = new RotateWidgetV1();
	rWidget = new RotateWidgetV2();
	tWidget->visible = false;
	rWidget->visible = false;

	loadFile("../data/rbs/dinoMeshes/body.obj");
//	loadFile("../data/3dModels/bunny.obj");

/*
	loadFile("../data/3dModels/bunny.obj");

	inputMesh2 = inputMesh1->clone();
	inputMesh2->rotate(getRotationQuaternion(RAD(180), V3D(0, 1, 0)), P3D());
	inputMesh2->translate(P3D(1, 0, 0));
	inputMesh2->calBoundingBox();
*/

	showGroundPlane = false;

	TwAddSeparator(mainMenuBar, "sep2", "");
	TwAddVarRW(mainMenuBar, "Draw Input Mesh1", TW_TYPE_BOOLCPP, &drawInputMesh1, "");
	TwAddVarRW(mainMenuBar, "Draw Output Mesh1", TW_TYPE_BOOLCPP, &drawOutputMesh1, "");
	TwAddVarRW(mainMenuBar, "Draw Input Mesh2", TW_TYPE_BOOLCPP, &drawInputMesh2, "");
	TwAddVarRW(mainMenuBar, "Draw Output Mesh2", TW_TYPE_BOOLCPP, &drawOutputMesh2, "");
	TwAddVarRW(mainMenuBar, "Draw Input Mesh3", TW_TYPE_BOOLCPP, &drawInputMesh3, "");
	TwAddVarRW(mainMenuBar, "Draw Output Mesh3", TW_TYPE_BOOLCPP, &drawOutputMesh3, "");
	TwAddVarRW(mainMenuBar, "VoxelSize", TW_TYPE_DOUBLE, &voxelSize, "min=0.0 max=1.0 step=0.01");

	TwAddButton(mainMenuBar, "Compute Swept Volume", action, "sweptVolume", "");
	TwAddButton(mainMenuBar, "Compute Convex Hull", action, "convexHull", "");
	TwAddButton(mainMenuBar, "Save", action, "save", "");
	TwAddButton(mainMenuBar, "Test Signed Distance", action, "signedDist", "");
	TwAddButton(mainMenuBar, "Test Level Set CSG", action, "levelSet", "");
	TwAddButton(mainMenuBar, "Test Level Set Projection", action, "project", "");
	TwAddButton(mainMenuBar, "Test Fast Marching", action, "fastMarching", "");
}

TestAppMeshOps::~TestAppMeshOps(void){

	delete inputMesh1;
	delete inputMesh2;
	delete inputMesh3;
	delete outputMesh1;
	delete outputMesh2;
	delete outputMesh3;
	delete rWidget;
	delete tWidget;
	delete voxelGrid;
	delete vertexGrid1;
	delete vertexGrid2;
}

bool testFunc(const double& A) {
	return A <= 0;
}

void TestAppMeshOps::testLevelSetCSG() {

	levelSets.clear();
	delete inputMesh1;
	delete inputMesh2;
	delete outputMesh1;
	delete outputMesh2;
	inputMesh1 =  GLContentManager::getGLMesh("../data/3dModels/bunny.obj");
	AxisAlignedBoundingBox bbox = inputMesh1->getBoundingBox();
	inputMesh1->translate(-bbox.center());
	inputMesh1->scale(3.0 / bbox.diameter(), P3D());
	bbox = inputMesh1->calBoundingBox();
	bbox.expand(2.0);

	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[0].updateLevelSet(inputMesh1);

	/*inputMesh2 = inputMesh1->clone();
	inputMesh2->translate(P3D(1.0, 0, 0));
	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[1].updateLevelSet(inputMesh2);

	LevelSet cylinderLevelSet(bbox, voxelSize);
	cylinderLevelSet.initCylinder(V3D(0, 0, 1), P3D(0.8, 0, 0), 0.4);
	outputMesh2 = cylinderLevelSet.extractMesh(0);

	levelSets[1].intersectLS(&cylinderLevelSet);
	levelSets[0].unionLS(&levelSets[1]);*/

	levelSets[0].minusCapsule(Segment(P3D(-1.0, 0.0, -1.0), P3D(1.0, 0.0, 1.0)), 0.05);

	outputMesh1 = levelSets[0].extractMesh(0);
	outputMesh1->writeToOFF("../out/unionRes.off");
}

void TestAppMeshOps::testLevelSetProjection() {

	levelSets.clear();
	delete inputMesh1;
	delete inputMesh2;
	delete outputMesh1;
	delete outputMesh2;
	inputMesh1 = GLContentManager::getGLMesh("../data/3dModels/sphere.obj");
	AxisAlignedBoundingBox bbox = inputMesh1->getBoundingBox();
	inputMesh1->translate(-bbox.center());
	//inputMesh1->scale(3.0 / bbox.diameter(), P3D());
	//bbox = inputMesh1->calBoundingBox();
	bbox.expand(2.0);

	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[0].updateLevelSet(inputMesh1);

	inputMesh2 = inputMesh1->clone();
	inputMesh2->translate(P3D(0.45, 0.5, 0));
	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[1].updateLevelSet(inputMesh2);

	inputMesh3 = inputMesh1->clone();
	inputMesh3->translate(P3D(0.9, 0, 0));
	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[2].updateLevelSet(inputMesh3);

	projectMultiLevelSets(levelSets);
	outputMesh1 = levelSets[0].extractMesh(0);
	outputMesh2 = levelSets[1].extractMesh(0);
	outputMesh3 = levelSets[2].extractMesh(0);
}

void TestAppMeshOps::testFastMarching()
{
	levelSets.clear();
	delete inputMesh1;
	delete inputMesh2;
	delete outputMesh1;
	delete outputMesh2;
	inputMesh1 = GLContentManager::getGLMesh("../data/3dModels/bunny.obj");
	AxisAlignedBoundingBox bbox = inputMesh1->getBoundingBox();
	inputMesh1->translate(-bbox.center());
	inputMesh1->scale(3.0 / bbox.diameter(), P3D());
	bbox = inputMesh1->calBoundingBox();
	bbox.expand(2.0);

	levelSets.push_back(LevelSet(bbox, voxelSize));
	levelSets[0].updateLevelSet(inputMesh1);

	Logger::print("Fast Marching Begin!\n");
	timer.restart();
	levelSets[0].fastMarchingOutward(2 * voxelSize, 4 * voxelSize);
	Logger::print("Fast Marching Done, total time: %lf!\n", timer.timeEllapsed());

	outputMesh1 = levelSets[0].extractMesh(0);
	//outputMesh2 = levelSets[0].extractMesh(3 * voxelSize);
	//outputMesh3 = levelSets[0].extractMesh(4 * voxelSize);

	drawInputMesh1 = false;
	drawInputMesh2 = false;
	drawInputMesh3 = false;
}

void TestAppMeshOps::calSignedDistance(VertexGrid<double>* grid, GLMesh* mesh)
{
	Eigen::MatrixXd V, N;
	Eigen::MatrixXi F;
	mesh->getMeshMatrices(V, F, N);

	Eigen::MatrixXd P;
	P.resize(grid->totalVertexNum, 3);
	int index = 0;
	for (int z = 0; z < grid->vertexNum[2]; z++)
		for (int y = 0; y < grid->vertexNum[1]; y++)
			for (int x = 0; x < grid->vertexNum[0]; x++) {
				P.row(index++) = grid->getVertex(x, y, z);
			}

	Eigen::VectorXd S;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;
	igl::signed_distance(P, V, F, igl::SIGNED_DISTANCE_TYPE_WINDING_NUMBER, S, I, C, N);

	for (int i = 0; i < S.size(); i++)
	{
		grid->setData(i, S[i]);
	}
}

GLMesh* TestAppMeshOps::marchingCubes(VertexGrid<double>* grid)
{
	Eigen::MatrixXd P;
	Eigen::VectorXd S;
	P.resize(grid->totalVertexNum, 3);
	S.resize(grid->totalVertexNum);
	int index = 0;
	for (int z = 0; z < grid->vertexNum[2]; z++)
		for (int y = 0; y < grid->vertexNum[1]; y++)
			for (int x = 0; x < grid->vertexNum[0]; x++) {
				P.row(index) = grid->getVertex(x, y, z);
				S[index] = grid->getData(index);
				index++;
			}

	Eigen::MatrixXd V;
	Eigen::MatrixXi F;
	igl::copyleft::marching_cubes(S, P, grid->vertexNum[0], grid->vertexNum[1], grid->vertexNum[2], V, F);

	return new GLMesh(V, F);
}

void TestAppMeshOps::transformSDF(VertexGrid<double>* SDFGrid, Quaternion q) {
	Quaternion qInv = q.getInverse();
	VertexGrid<double> origGrid = *SDFGrid;

	int index = 0;
	for (int z = 0; z < SDFGrid->vertexNum[2]; z++)
		for (int y = 0; y < SDFGrid->vertexNum[1]; y++)
			for (int x = 0; x < SDFGrid->vertexNum[0]; x++) {
				V3D p = SDFGrid->getVertex(x, y, z);
				V3D localP = qInv.rotate(p);
				if (origGrid.isInside(localP[0], localP[1], localP[2]))
				{
					SDFGrid->setData(index, origGrid.interpolateData(localP[0], localP[1], localP[2]));
				}
				else {
					SDFGrid->setData(index, 1.0);
				}
				index++;
			}
}

void TestAppMeshOps::testConvexHull() {
	if (outputMesh1)
		delete outputMesh1;

	outputMesh1 = new GLMesh();

	ConvexHull3D::computeConvexHullForMesh(inputMesh1, outputMesh1);
}

void TestAppMeshOps::testSignedDistance() {
	delete vertexGrid1;
	delete vertexGrid2;
	delete outputMesh1;
	delete outputMesh2;

	AxisAlignedBoundingBox bbox1 = inputMesh1->getBoundingBox();
	AxisAlignedBoundingBox bbox2 = inputMesh2->getBoundingBox();
	AxisAlignedBoundingBox bbox;
	bbox.setbmin(P3D(V3D(bbox1.bmin().array().min(bbox2.bmin().array()))));
	bbox.setbmax(P3D(V3D(bbox1.bmax().array().max(bbox2.bmax().array()))));

	vertexGrid1 = new VertexGrid<double>(bbox1, voxelSize);
	//vertexGrid2 = new VertexGrid<double>(bbox, voxelSize);
	Logger::print("VertexNum:%d %d %d\n", vertexGrid1->vertexNum[0], vertexGrid1->vertexNum[1], vertexGrid1->vertexNum[2]);
	//Logger::print("VertexNum:%d %d %d\n", vertexGrid2->vertexNum[0], vertexGrid2->vertexNum[1], vertexGrid2->vertexNum[2]);

	calSignedDistance(vertexGrid1, inputMesh1);
	//calSignedDistance(vertexGrid2, inputMesh2);

	transformSDF(vertexGrid1, getRotationQuaternion(RAD(30), V3D(0, 0, 1)));
	//projectGrid(vertexGrid1, vertexGrid2);

	outputMesh1 = marchingCubes(vertexGrid1);
	//outputMesh2 = marchingCubes(vertexGrid2);
}

void TestAppMeshOps::projectGrid(VertexGrid<double>* grid1, VertexGrid<double>* grid2)
{
	int index = 0;
	for (int z = 0; z < grid1->vertexNum[2]; z++)
		for (int y = 0; y < grid1->vertexNum[1]; y++)
			for (int x = 0; x < grid1->vertexNum[0]; x++) {
				double data1 = grid1->getData(index);
				double data2 = grid2->getData(index);
				double avg = 0.5 * (data1 + data2);
				if (avg < 0)
				{
					grid1->setData(index, data1 - avg);
					grid2->setData(index, data2 - avg);
				}
				index++;
			}



}

//triggered when mouse moves
bool TestAppMeshOps::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (rWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppMeshOps::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppMeshOps::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppMeshOps::onKeyEvent(int key, int action, int mods) {
	if (key == GLFW_KEY_D && action == GLFW_PRESS)
	{
		if (levelSets.size() > 0)
		{
			delete outputMesh1;
			levelSets[0].meanCurvatureFlow(0.0001, 0.001);
			outputMesh1 = levelSets[0].extractMesh(0);
		}
	}
	
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppMeshOps::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppMeshOps::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);
	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("obj") == 0) {
		GLMesh*& mesh = inputMesh1;

		delete mesh;
		mesh = GLContentManager::getGLMesh(fName);
		AxisAlignedBoundingBox bbox = mesh->getBoundingBox();
		mesh->translate(-bbox.center());
		mesh->scale(3.0 / bbox.diameter(), P3D());
		mesh->calBoundingBox();
	}
}

void TestAppMeshOps::saveFile(const char* fName) {
	Logger::consolePrint("Saving output mesh to file \'%s\'...\n", fName);
//	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
	if (outputMesh1) {
		FILE* fp = fopen(fName, "w");
		outputMesh1->renderToObjFile(fp, 0, Quaternion(), P3D());
		fclose(fp);
	}
}

// Run the App tasks
void TestAppMeshOps::process() {
	//do the work here...

	
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppMeshOps::drawScene() {
	// draw the widgets
	rWidget->pos = tWidget->pos;

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glColor3d(1, 1, 1);
	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	if (levelSets.size() > 0)
	{
		levelSets[0].glDrawGridBox();
	}

	if (drawOutputMesh1 && outputMesh1 != NULL) {		
		outputMesh1->getMaterial().setColor(1.0, 0.0, 0.0, 0.3);
		outputMesh1->drawMesh();
	}

	if (drawOutputMesh2 && outputMesh2 != NULL) {
		outputMesh2->getMaterial().setColor(0.0, 1.0, 0.0, 0.3);
		outputMesh2->drawMesh();
	}

	if (drawOutputMesh3 && outputMesh3 != NULL) {
		outputMesh3->getMaterial().setColor(0.0, 0.0, 1.0, 0.3);
		outputMesh3->drawMesh();
	}

	if (drawInputMesh1 && inputMesh1 != NULL) {
		inputMesh1->getMaterial().setColor(0.8, 0.8, 1.0, 0.6);
		inputMesh1->drawMesh();
	}

	if (drawInputMesh2 && inputMesh2 != NULL) {
		inputMesh2->getMaterial().setColor(1.0, 0.8, 0.8, 0.6);
		inputMesh2->drawMesh();
	}

	if (drawInputMesh3 && inputMesh3 != NULL) {
		inputMesh3->getMaterial().setColor(0.8, 1.0, 0.8, 0.6);
		inputMesh3->drawMesh();
	}

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppMeshOps::drawAuxiliarySceneInfo() {
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);

	// draw the widgets
	//tWidget->draw();
	//rWidget->draw();
}

// Restart the application.
void TestAppMeshOps::restart() {

}

bool TestAppMeshOps::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}


void TestAppMeshOps::setupLights() {

	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
