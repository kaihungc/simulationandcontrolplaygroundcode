#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include <RobotDesignerLib/ModularDesignWindow.h>
#include <RobotDesignerLib/ParameterizedRobotDesign.h>
#include <RobotDesignerLib/MOPTWindow.h>
#include <ControlLib/IK_Solver.h>
#include <RobotDesignerLib/PositionBasedRobotController.h>

/**
 * Robot Design and Simulation interface
 */
class TestAppRBSimAndDesign : public GLApplication {
public:
	AbstractDesignWindow *designWindow;
	MOPTWindow* moptWindow = NULL;

	// plots for cost function
	bool drawPlots = false;
	PlotWindow* plotWindow = NULL;

	AbstractRBEngine* rbEngine = NULL;
	WorldOracle* worldOracle = NULL;
	Robot* robot = NULL;

	double groundKPScale = -5;
	double groundKdVal = 2500;

	bool drawMeshes = true, drawMOIs = false, drawCDPs = false, drawSkeletonView = false, drawJoints = false, drawContactForces = true, drawOrientation = true;
	double simTimeStep;
	bool drawMotionPlan = false;

	double motionPlanTime = 0;
	ParameterizedRobotDesign* prd = NULL;
	
	int animationCycle = 0;

	LocomotionEngineManager* locomotionManager = NULL;

	PositionBasedRobotController* controller = NULL;
	PositionBasedRobotController* controllerIK = NULL;

	// for IK
	IK_Solver* ikSolver = NULL;
	RigidBody* selectedRigidBody = NULL;
	RigidBody* highlightedRigidBody = NULL;
	P3D selectedPoint;	//in local coordinates of selected rigid body
	P3D targetPoint;	//in world coordinates

	enum RUN_OPTIONS {
		MOTION_PLAN_OPTIMIZATION = 0,
		MOTION_PLAN_ANIMATION,
		PHYSICS_SIMULATION,
		PHYSICS_SIMULATION_SMOOTH,
		DESIGN_OPTMIZATION,
		IK_PLAN
	};
	int runOption = MOTION_PLAN_OPTIMIZATION;

	bool drawMOPTWindow = false;

	//Beichen Li: for LBFGS design optimization
	LBFGSpp::LBFGSParam<double> param;
	LBFGSpp::LBFGSSolver<double> solver;

	//Beichen Li: motion parameters need to be saved, especially during line search
	dVector savedMotionParameters;

	//Beichen Li: optimization of design parameters
	dVector pLast;
	double regCoefficient = 20.0;
	double regularizer = 1.0;
	double epsilon = 0.1;
	double dfLast = 0.0;
	double currentEnergy = 0.0;

	//Beichen Li: saved dmdp for line search optimization
	MatrixNxM dmdp;

	//Beichen Li: design parameter display
	DynamicArray<double> designParameters;
	DynamicArray<std::string> designParameterNames;

	//Beichen Li: OpenMP threads
	int numThreads = 4;

public:
	// constructor
	TestAppRBSimAndDesign();
	// destructor
	virtual ~TestAppRBSimAndDesign(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	void runMOPTStep();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	void warmStartMOPT();
	void loadToSim();

	//Beichen Li: design optimization step
	void optimizeDesign();

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

//	virtual GLTexture* getGroundTexture() {
//		return GLContentManager::getTexture("../data/textures/ground_TileLight.bmp");
//	}

	//Beichen Li: reload operator() for LBFGS
	double operator () (const dVector& p, dVector& grad, bool saveFlag = true);
	double operator () (const dVector& p, const dVector& dp, dVector& grad, bool saveFlag = true, bool MOPTFlag = true);

	//Beichen Li: regularizer related functions for LBFGS
	double computeRegularizationTerm(const dVector& p);
	void updateRegularizingSolution(const dVector& p);
	void updateRegularizer(double f, double df);

	//Beichen Li: computes gradient of energy function with respect to design parameter set p
	void addGradientOfDesignParametersTo(dVector& grad, const dVector& p);

	//Beichen Li: computes dmdp at design parameter set p
	void computeDmdp();

	//Beichen Li: design parameter display at menu
	void addParameterizedDesignParametersToMenu();
	void removeParameterizedDesignParametersFromMenu();

	//Beichen Li: converts dVector to DynamicArray<double>
	inline void dVectorToParametersList(const dVector& v, DynamicArray<double>& p) {
		p.resize(v.size());
		for (int i = 0; i < v.size(); i++)
			p[i] = v[i];
	}

	//Beichen Li: converts DynamicArray<double> to dVector
	inline void parametersListToDVector(const DynamicArray<double>& p, dVector& v) {
		v.resize(p.size());
		for (uint i = 0; i < p.size(); i++)
			v[i] = p[i];
	}

	//Beichen Li: parallelism test
	void testParallel();
};




