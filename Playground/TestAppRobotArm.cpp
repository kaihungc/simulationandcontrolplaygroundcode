#include <GUILib/GLUtils.h>

#include "TestAppRobotArm.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>


TestAppRobotArm::TestAppRobotArm(){
	setWindowTitle("RobotArmControl");
//	loadFile("../data/rbs/robotArm.rbs");
//	loadFile("../data/rbs/robotArm1DOF.rbs");
	loadFile("../data/rbs/puppy.rbs");


	TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddVarRW(mainMenuBar, "Motor1Amplitude", TW_TYPE_DOUBLE, &amplitude1, " label='Motor1Amplitude'");
	TwAddVarRW(mainMenuBar, "Motor2Amplitude", TW_TYPE_DOUBLE, &amplitude2, " label='Motor2Amplitude'");
	TwAddVarRW(mainMenuBar, "Motor2Phase", TW_TYPE_DOUBLE, &phase2, " label='Motor2Phase'");
	TwAddVarRW(mainMenuBar, "frequency", TW_TYPE_DOUBLE, &frequency, " label='motion frequency'");

	frequency = 2.0;
	desiredFrameRate = 60;
	waitForFrameRate = false;

	int a[14] = {12, 5, 8, 1, 13, 6, 9, 2, 14, 7, 10, 3, 11, 4};

	//assing some DXL ids for testing...
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		hj->dynamixelProperties.dxl_id = a[i];
	}


	//ReducedRobotState robotState(robot);
	//robotState.readFromFile("../out/puppyStartState.rs");
	//robot->setState(&robotState);
	//computeDXLControlInputsBasedOnSimRobot(1.0 / desiredFrameRate);
	//sendControlInputsToPhysicalRobot();
}

TestAppRobotArm::~TestAppRobotArm(void){
}

// Restart the application.
void TestAppRobotArm::restart() {

}

Timer tmpTimer;

// Run the App tasks
void TestAppRobotArm::process() {
	if (runMode == SYNC_SIM_FROM_ROBOT_STATE)
		syncSimulationWithPhysicalRobot();
	else{

		//do some very basic control for the arm...
		double angle1 = sin(motionPhase * amplitude1);
		double angle2 = sin(motionPhase * amplitude2 + phase2);

		double angle11 = sin((motionPhase + frequency * (1.0 / desiredFrameRate)) * amplitude1);
		double angle22 = sin((motionPhase + frequency * (1.0 / desiredFrameRate)) * amplitude2 + phase2);

//		double vel1 = (angle11 - angle1) / (1.0 / desiredFrameRate);
//		double vel2 = (angle22 - angle2) / (1.0 / desiredFrameRate);

		double vel1 = cos(motionPhase * amplitude1) * amplitude1 * frequency;
		double vel2 = cos(motionPhase * amplitude2 + phase2) * amplitude2 * frequency;

		Quaternion q;
		ReducedRobotState rs(robot);
		q.setRotationFrom(angle1, ((HingeJoint*)robot->getJoint(0))->rotationAxis);
		rs.setJointRelativeOrientation(q, 0);
		rs.setJointRelativeAngVelocity(((HingeJoint*)robot->getJoint(0))->rotationAxis * vel1, 0);

//		q.setRotationFrom(angle2, ((HingeJoint*)robot->getJoint(1))->rotationAxis);
//		rs.setJointRelativeOrientation(q, 1);
//		rs.setJointRelativeAngVelocity(((HingeJoint*)robot->getJoint(1))->rotationAxis * vel2, 1);

		robot->setState(&rs);

		//Staggered control method:
		// Issue commands with the notion that it will take dt to reach the positions
		// roughly speaking, after dt/2 time has passed, issue new commands. The motors will not
		// be there yet, so they had no chance to attemp to zero out velocities... 

		//trick the motors with this staggered scheme... they will think they'll have some time to go, but we'll issue the next command sooner...
//		while (tmpTimer.timeEllapsed() < 1.0 / desiredFrameRate * 0.5);

		computeDXLControlInputsBasedOnSimRobot(1.0 / desiredFrameRate);

		sendControlInputsToPhysicalRobot();

		double timeEllapsed = tmpTimer.timeEllapsed();
		//this is assuming that the process function takes exactly the amount of time that is allocated to it...
		motionPhase += frequency * (1.0 / desiredFrameRate);
		tmpTimer.restart();
		Logger::consolePrint("Timing error: %lfs (+ve means it has taken longer than expected, so the motor has had extra time to run...)\n", timeEllapsed - 1.0 / desiredFrameRate);
	}
}

//TODO: do try a mode where we command velocities and have a velocity profile to smooth things out...

bool TestAppRobotArm::onKeyEvent(int key, int action, int mods)
{
	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		ReducedRobotState robotState(robot);
		robotState.readFromFile("../out/puppyStartState.rs");
		robot->setState(&robotState);
		computeDXLControlInputsBasedOnSimRobot(1.0 / desiredFrameRate);
		sendControlInputsToPhysicalRobot();

		return true;
	}

	return TestAppDynamixel::onKeyEvent(key, action, mods);

}