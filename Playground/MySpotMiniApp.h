#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include "TestAppDynamixel.h"
#include <ControlLib/QPControlEngine.h>
#include <ControlLib/QPControlPlan.h>


class MOPTQPTrackingController;


/**
 * Robot Design and Simulation interface
 */
class MySpotMiniApp : public GLApplication {
private:
	LocomotionEngineMotionPlan* motionPlan = NULL;
	MOPTQPTrackingController* controller = NULL;
	WorldOracle* worldOracle = NULL;
	Robot* robot = NULL;
	AbstractRBEngine* rbEngine = NULL;

	bool runPhysics = true;
	bool drawControllerDebug = false;
	bool checkControlSolution = false;

	double stridePhase = 0;
	double simTimeStep = 1/120.0;
	int nPhysicsStepsPerControlStep = 4;

	V3D perturbationForce;
	double forceScale = 0;

	FootFallPattern footFallPattern;
	FootFallPatternViewer* ffpViewer;

public:

	// constructor
	MySpotMiniApp();
	// destructor
	virtual ~MySpotMiniApp(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();

	virtual P3D getCameraTarget();

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	void setPerturbationForceFromMouseInput(double xPos, double yPos);

	virtual bool processCommandLine(const std::string& cmdLine);

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	void loadRobot(const char* fName);
	virtual void loadFile(const char* fName);
	virtual void saveFile(const char* fName);

	void syncRobotToMotionPlan();

};

