#pragma once
#include <GUILib/GLUtils.h>
#include <GUILib/GLApplication.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>

#include <RBSimLib/AbstractRBEngine.h>
#include <RobotDesignerLib/Motor_MX28.h>
#include <RobotDesignerLib/EndEffectorFeature.h>
#include <RobotDesignerLib/BodyPartStructure.h>

/**
 * Test App for RB Simulations
 */
class TestAppRDStructure : public GLApplication {
private:
	TranslateWidget tWidget;

	Motor_MX28 motor1;
	Motor_MX28 motor2;
	EndEffectorFeature endEffector;

	BodyPartStructure s1 = BodyPartStructure(&motor1, &motor2);
	BodyPartStructure s2 = BodyPartStructure(&motor2, &endEffector);

	double motor1PosX = -0.11, motor1PosY = 0, motor1PosZ = 0;
	double motor1AngleX = -10, motor1AngleY = 5, motor1EmbeddingAngle = -90, motor1ChildEmbeddingAngle = 0;

	double motor2PosX = 0.1205, motor2PosY = 0.03, motor2PosZ = -0.07;
	double motor2AngleX = 38, motor2AngleY = 42, motor2EmbeddingAngle = -90, motor2ChildEmbeddingAngle = 0;

	double eePosX = 0.16, eePosY = -0.1, eePosZ = 0;

public:
	// constructor
	TestAppRDStructure();
	// destructor
	virtual ~TestAppRDStructure(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);


	void updateMotorAndStructureConfiguration();
};



