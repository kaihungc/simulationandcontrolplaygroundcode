#include <GUILib/GLUtils.h>

#include "TestAppRBSimAndDesign.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <omp.h>

/**

- The timing of the motion should be much easier to set than it is right now. Perhaps the same with the foot height (and fix the bug that has it be divided by two).
- Have an easy way to reset the footfall pattern...

- work towards MOPT without robot states, but with additional constraints to mimick the effect of the legs (don't stretch, regularizer for footfall positions so that we don't end up with degenerate support polygons)
	- because these inequality constraints are just a simplified notion of an arm's reachable state, we may want to model directly COM to EE distances, or at best, consider the distance from EE to shoulder that is on the floating base with its rotational DOFs as well...
- the brackets that Alex made are quite nice - how can we automate this?
- some appropriate feedback-based controller?
- add friction constraints...
- add pose constraints, for transitions...
	- pose constraints should be in motion plan, and read from there in constraints...
	- check feet sliding constraints to not overcount, and that the right pairs are checked (same as constraints)
	- all the inequality constraints to model abstract legs...
	- fast MOPT for feedback...	
		- computing hessian entries seems to be 10 times slower than solving the linear system...
*/

//TODO: get a stabilizing walking controller - it should modulate what the stance and swing feet are doing - both torque-based and position-control based solutions are needed...
//TODO: parameterize connections for modular robot with continuous params (relative position and orientation)
//TODO: reconstruct geometry of intermediate bracket structure through boundary-constrained parallel transport/rod simulation type of techniques
//TODO: implement L_BFGS and see if it works better... use SQP for constraints, but Pass an idenity as the hessian and a BFGS search direction as the gradient?
//TODO: constraint jacobians should be computed analytically
//TODO: gradient projection method when the QP solver fails - is it equivalent to calling OOQP with identity Hessian?
//TODO: use exponential maps to parameterize rotations of the main body - or, I suppose, some notion of baking in rotations and only applying deltas. 

//Beichen Li: LBFGSpp namespace for LBFGS external library
using namespace LBFGSpp;

//Beichen Li: divergence threshold for MOPT
#define THRESHOLD	1e8

#define AUTO(prd) (static_cast<AutoParameterizedRobotDesign *>(prd))

#define GET_PARAMETERS(prd, p) {\
	AUTO(prd)->getCurrentSetOfParameters(p);\
}

#define SET_PARAMETERS(prd, p, motionPlan) {\
	AUTO(prd)->setParameters(p);\
	(motionPlan)->updateRobotRepresentation();\
}

//#define RUN_MOPT(energy, converged) {\
//	do {\
//		energy = moptWindow->locomotionManager->runMOPTStep();\
//	} while (energy < THRESHOLD && !(converged));\
//}

#define RUN_MOPT(energy) {\
	energy = locomotionManager->runMOPTStep();\
}

void TW_CALL LoadRobotToSim(void* clientData) {
	((TestAppRBSimAndDesign*)clientData)->loadToSim();
}

void TW_CALL RestartMOPT(void* clientData) {
	((TestAppRBSimAndDesign*)clientData)->warmStartMOPT();
}

void TW_CALL DesignOptimize(void* clientData) {
	((TestAppRBSimAndDesign*)clientData)->optimizeDesign();
}

void TW_CALL ParallelTest(void *clientData) {
	((TestAppRBSimAndDesign*)clientData)->testParallel();
}

void TW_CALL setRobotScale(const void *value, void *clientData) {
	ParameterizedRobotDesign* prd = ((TestAppRBSimAndDesign*)clientData)->prd;
	if (!prd) {
		*(double*)value = 0;
		return;
	}
	DynamicArray<double> params;
	prd->getCurrentSetOfParameters(params);
	params[0] = *(double*)value;
	prd->setParameters(params);
}

void TW_CALL getRobotScale(void *value, void *clientData) {
	ParameterizedRobotDesign* prd = ((TestAppRBSimAndDesign*)clientData)->prd;
	if (!prd){
		*(double*)value = 0;
		return;
	}
	DynamicArray<double> params;
	prd->getCurrentSetOfParameters(params);
	*(double*)value = params[0];
}

TestAppRBSimAndDesign::TestAppRBSimAndDesign(): param(), solver(param) {
	bgColor[0] = bgColor[1] = bgColor[2] = 1;
	setWindowTitle("RobotDesigner");

	waitForFrameRate = true;

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawCDPs = false;
	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddButton(mainMenuBar, "LoadRobotToSim", LoadRobotToSim, this, " label='Load To Simulation' group='Sim' key='l' ");
	TwAddButton(mainMenuBar, "WarmstartMOPT", RestartMOPT, this, " label='WarmStart MOPT' group='Sim' key='w' ");
	TwAddButton(mainMenuBar, "OptimizeDesign", DesignOptimize, this, " label='Optimize Design' group='Sim' key='o' ");
	TwAddButton(mainMenuBar, "ParallelTest", ParallelTest, this, " label='Test Parallelism' group='Sim' key='t' ");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Simulation-SmoothControl");
	runOptionList.push_back("\\Design Optimization");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "drawMOPTWindow", TW_TYPE_BOOLCPP, &drawMOPTWindow, " label='drawMOPTWindow' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawMotionPlan", TW_TYPE_BOOLCPP, &drawMotionPlan, " label='drawMotionPlan' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMOIs", TW_TYPE_BOOLCPP, &drawMOIs, " label='drawMOIs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawCDPs", TW_TYPE_BOOLCPP, &drawCDPs, " label='drawCDPs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawContactForces", TW_TYPE_BOOLCPP, &drawContactForces, " label='drawContactForces' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawOrientation", TW_TYPE_BOOLCPP, &drawOrientation, " label='drawOrientation' group='Viz2'");

	TwAddSeparator(mainMenuBar, "sep3", "");
	
	TwAddVarRW(mainMenuBar, "groundKPScale", TW_TYPE_DOUBLE, &groundKPScale, "min=-5 max=10 step=1");
	TwAddVarRW(mainMenuBar, "groundKdVal", TW_TYPE_DOUBLE, &groundKdVal, "min=0 max=10000 step=100");

	TwAddSeparator(mainMenuBar, "sep4", "");

	TwAddVarRW(mainMenuBar, "PlanMotionPhase", TW_TYPE_DOUBLE, &motionPlanTime, "min=0 max=1 step=0.01 group='MotionOptimizationOptions'");
//	TwAddVarRW(mainMenuBar, "", TW_TYPE_DOUBLE, &robotScale, "min=0.5 max=1.5 step=0.1 group='MotionOptimizationOptions'");
//	TwAddVarCB(mainMenuBar, "Robot Scale", TW_TYPE_DOUBLE, setRobotScale, getRobotScale, this, "step=0.05");

	simTimeStep = 1 / 500.0;
//	Globals::g = 0;

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth()-size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	

//	designWindow = new RobotDesignWindow(size[0], 0, w, h, this);

//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-320.cfg");
//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-320-V2.cfg");
//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-430_2.cfg");
//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-430-V2.cfg");
	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-430-V3.cfg");
//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configAirbus.cfg");

	moptWindow = new MOPTWindow(size[0], 0, w, h, this);
	
	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
	plotWindow->createData("optValues", 1, 0, 0);
	plotWindow->setToFixedXRange(50);

	if (designWindow->type == MODULAR_DESIGN){
		//loadFile("../data/modularRobot/design/firstModularRobotDesign.dsn");
		//loadFile("../data/modularRobot/livingDesigns/spider_v2.dsn");
		//loadToSim();
		//loadFile("../out/optimalPlanSpider.p");
	}
	else{
		//loadFile("../data/rbs/snakeMonster.rbs");
		//loadFile("../data/rbs/scorpy.rbs");
		//loadFile("../data/rbs/babyelephant.rbs");
		//loadFile("../data/modularRobot/design/puppyV1.rbs");

		//loadFile("../data/rbs/dinoV4.rbs");
		//loadFile("../data/rbs/starlETH.rbs");

		loadFile("../data/motionplans/spotMini/robot.rbs");
		loadFile("../data/motionplans/spotMini/robot.rs");

//		loadFile("../data/motionplans/hexSpider/hexSpider.rbs");
//		loadFile("../data/motionplans/hexSpider/hexSpider.rs");


		/*loadFile("../data/rbs/starlETH.rbs");
		loadFile("../data/rbs/tripod.rbs");	
		loadFile("../data/rbs/cat.rbs");
		loadFile("../data/rbs/gorilla.rbs");
		loadFile("../data/rbs/babyRex.rbs");
		loadFile("../data/rbs/quadruped.rbs");
		loadFile("../data/rbs/scorpy.rbs");
		loadFile("../data/rbs/dog.rbs");
		loadFile("../data/rbs/tmp.rbs");

		loadFile("../data/rbs/snakeMonster.rbs");
		loadFile("../data/rbs/SehoonsPuppy.rbs");*/
	}

	showGroundPlane = true;

	TwDefine(" MainMenuBar alpha=255 ");   // semi-transparent blue bar

	bgColor[0] = bgColor[1] = bgColor[2] = 0.75;

	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutRightAxis = 0.25;
	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutUpAxis = 0.95;
	dynamic_cast<GLTrackingCamera*>(this->camera)->camDistance = -1.5;

	//Beichen Li: OpenMP
	omp_set_num_threads(numThreads);
}

TestAppRBSimAndDesign::~TestAppRBSimAndDesign(void){
}

//triggered when mouse moves
bool TestAppRBSimAndDesign::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (!drawMOPTWindow)
		if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
			if (designWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (drawMOPTWindow)
		if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
			if (moptWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppRBSimAndDesign::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)){
			designWindow->onMenuMouseButtonProcessedEvent();
			return true;
		}

	if (!drawMOPTWindow)
		if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
			if (designWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (drawMOPTWindow)
		if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
			if (moptWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppRBSimAndDesign::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (!drawMOPTWindow)
		if (designWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
			if (designWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (drawMOPTWindow)
		if (moptWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
			if (moptWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppRBSimAndDesign::onKeyEvent(int key, int action, int mods) {
	if (locomotionManager && locomotionManager->motionPlan){
		if (key == GLFW_KEY_UP && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() += 0.1;
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() -= 0.1;
		if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() += 0.1;
		if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() -= 0.1;
		if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle += 0.1;
		if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle -= 0.1;

		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.z(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.x(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desTurningAngle, -0.5, 0.5);
	}

	if (key == GLFW_KEY_O && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/tmpMPAngles.mpa");
	if (key == GLFW_KEY_P && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/angle.p");
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_OPTIMIZATION;
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_ANIMATION;
	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
		runOption = PHYSICS_SIMULATION;
	if (key == GLFW_KEY_4 && action == GLFW_PRESS)
		runOption = PHYSICS_SIMULATION_SMOOTH;
	if (key == GLFW_KEY_5 && action == GLFW_PRESS)
		runOption = DESIGN_OPTMIZATION;

	//Beichen Li: load a test robot design
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		loadFile("../out/tmpModularRobotDesign.dsn");
	if (key == GLFW_KEY_M && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeParamsToFile("../out/MPParams.p");

//	if (key == GLFW_KEY_5 && action == GLFW_PRESS)
//		robot->setState(&startingRobotState);
	
	if (!drawMOPTWindow && designWindow->onKeyEvent(key, action, mods)) return true;
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppRBSimAndDesign::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppRBSimAndDesign::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rs") == 0) {
		Logger::consolePrint("Load robot state from '%s'\n", fName);
		designWindow->setStartStateFName(fName);
		return;
	}

	if (designWindow->type == ROBOT_DESIGN && fNameExt.compare("rbs") == 0 ){ 
		delete rbEngine;
		rbEngine = new ODERBEngine();
		designWindow->loadFile(fName);
		designWindow->saveFile("../out/tmpRobotDesign.rbs");

		//loadToSim();

		return;
	}

	if (designWindow->type == MODULAR_DESIGN && fNameExt.compare("dsn") == 0) {
		delete rbEngine;
		rbEngine = new ODERBEngine();
		designWindow->loadFile(fName);
		designWindow->saveFile("../out/tmpRobotDesign.rbs");

		//loadToSim();

		return;
	}

	if ((fNameExt.compare("obj") == 0 || fNameExt.compare("mrb") == 0 || fNameExt.compare("rbs") == 0)
		&& designWindow->type == MODULAR_DESIGN) {
		designWindow->loadFile(fName);
		return;
	}

	if (fNameExt.compare("ffp") == 0) {
		moptWindow->footFallPattern.loadFromFile(fName);
		moptWindow->footFallPattern.writeToFile("..\\out\\tmpFFP.ffp");
		return;
	}



	if (fNameExt.compare("p") == 0) {
		if (locomotionManager && locomotionManager->motionPlan)
			locomotionManager->motionPlan->readParamsFromFile(fName);
		return;
	}

}


void TestAppRBSimAndDesign::loadToSim(){
	delete worldOracle;
	delete robot;
	delete rbEngine;
	delete prd;

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);
	rbEngine = new ODERBEngine();

/* ------ load the robot and set up an initial motion plan for it ------*/
	designWindow->saveFile("../out/tmpRobot.rbs");
	rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");

	robot = new Robot(rbEngine->rbs[0]);
	prd = new AutoParameterizedRobotDesign(robot);

	addParameterizedDesignParametersToMenu();

	ReducedRobotState startingRobotState = designWindow->getStartState(robot);
	startingRobotState.writeToFile("../out/tmpRobot.rs");
	designWindow->prepareForSimulation(rbEngine);
	robot->setState(&startingRobotState);

/* ---------- Set up the legs and the foot fall pattern ---------- */

	setupSimpleRobotStructure(robot);

	moptWindow->loadRobot(robot, &startingRobotState);


	int nLegs = robot->bFrame->limbs.size();
	int nPoints = 3 * nLegs;

	//now, start MOPT...
	warmStartMOPT();

	Logger::consolePrint("Warmstart successful...\n");
	Logger::consolePrint("The robot has %d legs, weighs %lfkgs and is %lfm tall...\n", robot->bFrame->limbs.size(), robot->getMass(), robot->root->getCMPosition().y());
}

void TestAppRBSimAndDesign::warmStartMOPT() {

	delete controller;
	delete controllerIK;
	delete locomotionManager;

	plotWindow->clearData();
	plotWindow->createData("optValues", 1, 0, 0);
	
	locomotionManager = moptWindow->initializeNewMP();

	controller = new PositionBasedRobotController(robot, locomotionManager->motionPlan);
	controllerIK = new PositionBasedRobotController(robot, locomotionManager->motionPlan);
	animationCycle = 0;

}

void TestAppRBSimAndDesign::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

void TestAppRBSimAndDesign::runMOPTStep() {
	double energyVal = moptWindow->runMOPTStep();
	Logger::consolePrint("Current energy: %.6lf\n", energyVal);

	plotWindow->addDataPoint(0, energyVal);
}

// Run the App tasks
void TestAppRBSimAndDesign::process() {

	if (designWindow->process()) return;

	//we need to sync the state of the robot with the motion plan when we first start physics-based tracking...
	static int lastRunOptionSelected = runOption + 1;
	if (lastRunOptionSelected != runOption && (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH)) {
		// TODO: should also set velocities based on the motion plan...
		Logger::consolePrint("Syncronizing robot state\n");
		ReducedRobotState moptRobotState(robot);
		moptRobotState.clearVelocities();
		locomotionManager->motionPlan->robotStateTrajectory.getRobotPoseAt(motionPlanTime, moptRobotState);

//		moptRobotState.setPosition(moptRobotState.getPosition() + V3D(0,1,0) * 0.01);
//		moptRobotState.setOrientation(Quaternion(1,0,0,0.1).toUnit());
//		moptRobotState.setAngularVelocity(V3D(0.5,.5,.5));

		robot->setState(&moptRobotState);
		controller->stridePhase = controllerIK->stridePhase = motionPlanTime;
//		lastRunOptionSelected = runOption;
//		return;
	}
	lastRunOptionSelected = runOption;


	if (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH) {
		double simulationTime = 0;
		double maxRunningTime = 1.0 / desiredFrameRate;

		while (simulationTime / maxRunningTime < animationSpeedupFactor) {
			simulationTime += simTimeStep;

			controller->advanceInTime(simTimeStep);
			controllerIK->advanceInTime(simTimeStep);
			if (runOption == PHYSICS_SIMULATION)
				controller->computeControlSignals(simTimeStep);
			else
				controllerIK->computeControlSignals(simTimeStep);

			//set up simulation parameters...
			ODERBEngine* odeRBEngine = dynamic_cast<ODERBEngine*>(rbEngine);
			if (odeRBEngine) {
				double kp = pow(10, groundKPScale);
				double kd = groundKdVal;
				odeRBEngine->contactStiffnessCoefficient = simTimeStep * kp / (simTimeStep * kp + kd);
				odeRBEngine->contactDampingCoefficient = 1 / (simTimeStep * kp + kd);
			}

			rbEngine->step(simTimeStep);
			//			break;
		}
	}
	else if (runOption == MOTION_PLAN_OPTIMIZATION) {
		animationCycle = 0;

		runMOPTStep();
	}
	else if (runOption == MOTION_PLAN_ANIMATION) {
		motionPlanTime += (1.0 / desiredFrameRate) / locomotionManager->motionPlan->motionPlanDuration;
		if (motionPlanTime > 1) {
			animationCycle++;
			motionPlanTime -= 1;
		}
		moptWindow->setAnimationParams(motionPlanTime, 0);
	}
	else if (runOption == DESIGN_OPTMIZATION) {
		optimizeDesign();
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppRBSimAndDesign::drawScene() {
	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	//in case the user is manipulating any of parameters of the motion plan, update them.
	
	if (locomotionManager)
		locomotionManager->drawMotionPlan(motionPlanTime, animationCycle,
			(runOption == MOTION_PLAN_ANIMATION || runOption == MOTION_PLAN_OPTIMIZATION || runOption == DESIGN_OPTMIZATION) && drawMeshes,
			false, drawMotionPlan, drawContactForces, drawOrientation);

/*
	if (drawMotionPlan) {
		glEnable(GL_LIGHTING);

		P3D com = robot->computeCOM();
		P3D bFrameCOM = robot->bFrame->bodyState.position;

		com[1] = 0; bFrameCOM[1] = 0.001;
		glColor3d(0, 1, 0);
		drawSphere(com, 0.003);
		glColor3d(0, 0, 1);
		drawSphere(bFrameCOM, 0.003);
		glDisable(GL_LIGHTING);
	}
*/

	if (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH) {
		int flags = 0;
		if (drawMeshes) flags |= SHOW_MESH | SHOW_MATERIALS;
		if (drawSkeletonView) flags |= SHOW_BODY_FRAME | SHOW_ABSTRACT_VIEW;
		if (drawMOIs) flags |= SHOW_MOI_BOX;
		if (drawCDPs) flags |= SHOW_CD_PRIMITIVES;
		if (drawJoints) flags |= SHOW_JOINTS;

		glEnable(GL_LIGHTING);
		rbEngine->drawRBs(flags);
		glDisable(GL_LIGHTING);
	}

	if (runOption == PHYSICS_SIMULATION)
		controller->drawDebugInfo();

	if (runOption == PHYSICS_SIMULATION_SMOOTH)
		controllerIK->drawDebugInfo();
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppRBSimAndDesign::drawAuxiliarySceneInfo() {

	if (drawMOPTWindow && moptWindow) {
		moptWindow->draw();
		moptWindow->drawAuxiliarySceneInfo();
	}
		

	if (!drawMOPTWindow)
	{
		designWindow->draw();
		designWindow->drawAuxiliarySceneInfo();
	}

	// plots of cost function
	if (drawPlots)
		plotWindow->draw();

}

// Restart the application.
void TestAppRBSimAndDesign::restart() {

}

bool TestAppRBSimAndDesign::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppRBSimAndDesign::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow->setViewportParameters(size[0], 0, w, h);
	setViewportParameters(size[0] + w, 0, w, h);
}


void TestAppRBSimAndDesign::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}

void TestAppRBSimAndDesign::addParameterizedDesignParametersToMenu() {
	prd->getCurrentSetOfParameters(designParameters);
	designParameterNames.resize(designParameters.size());
	for (uint i = 0; i < designParameters.size(); i++) {
		designParameterNames[i] = string("p") + to_string(i);
		TwAddVarRW(mainMenuBar, designParameterNames[i].c_str(), TW_TYPE_DOUBLE, &designParameters[i], "step=0.01 group ='DesignParams'");
	}
	//	prd->setParameters(params);
}

void TestAppRBSimAndDesign::removeParameterizedDesignParametersFromMenu() {

	for (uint i = 0; i < designParameterNames.size(); i++) {
		TwRemoveVar(mainMenuBar, designParameterNames[i].c_str());
	}
}

double TestAppRBSimAndDesign::operator () (const dVector& p, dVector& grad, bool saveFlag) {
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;

	dVector p0;
	GET_PARAMETERS(prd, p0);

	//Beichen Li: Save current parameters or load from saved copy
	if (saveFlag) {
		motionPlan->writeMPParametersToList(savedMotionParameters);
		updateRegularizingSolution(p0);
	}
	else
		motionPlan->setMPParametersFromList(savedMotionParameters);

	SET_PARAMETERS(prd, p, motionPlan);

	double energy = DBL_MAX;

	//Beichen Li: run MOPT steps till convergence
	RUN_MOPT(energy);

	if (energy < THRESHOLD)
		Logger::logPrint("Converged! Energy: %10.10lf\n", energy);
	else
		Logger::logPrint("Did not converge!\n");

	grad.resize(p.size());
	addGradientOfDesignParametersTo(grad, p);

	//If the energy diverges, we should set the original parameters back
	if (energy >= THRESHOLD) {
		motionPlan->setMPParametersFromList(savedMotionParameters);
		SET_PARAMETERS(prd, p0, motionPlan);
	}

	//Beichen Li: restore the regularizer value
	energyFunction->resetRegularizer();

	return energy + computeRegularizationTerm(p);
}

double TestAppRBSimAndDesign::operator () (const dVector& p, const dVector& dp, dVector& grad, bool saveFlag, bool MOPTFlag) {
	//Warning: computeDmdp(p) should be called before this function call

	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;

	if (saveFlag) {
		motionPlan->writeMPParametersToList(savedMotionParameters);
		updateRegularizingSolution(p);
	}
	else
		motionPlan->setMPParametersFromList(savedMotionParameters);

	dVector m, m_;
	motionPlan->writeMPParametersToList(m);
	//	dVector p0
	dVector p1;
	p1 = p + dp;
	//	GET_PARAMETERS(prd, p0);
	SET_PARAMETERS(prd, p1, motionPlan);

	m_ = m + dmdp * dp;
	motionPlan->setMPParametersFromList(m_);

	double energy = DBL_MAX;

	//Beichen Li: MOPTFlag decides whether to perform MOPT at current point
	//true - perform MOPT to get accurate value
	//false - use first order approximation
	//Profiler::startTimer("MOPT", "Evaluate");
	if (MOPTFlag) {
		for (int i = 0; i < 300; i++)
			RUN_MOPT(energy);
	}
	else
		energy = energyFunction->computeValue(m_);
	//Profiler::collectTimer("MOPT");

	double reg = computeRegularizationTerm(p1);

	if (energy < THRESHOLD)
		Logger::logPrint("Converged! Energy: %10.10lf, Reg: %lf\n", energy, reg);
	else
		Logger::logPrint("Did not converge!\n");

	//Profiler::startTimer("dfdp", "Evaluate");
	grad.resize(p.size());
	addGradientOfDesignParametersTo(grad, p1);
	//Profiler::collectTimer("dfdp");

	//Beichen Li: this was once a fail-safe for divergent situations but it can be controlled with saveFlag
	//	if (energy >= THRESHOLD) {
	//		motionPlan->setMPParametersFromList(m);
	//		SET_PARAMETERS(prd, p0, motionPlan);
	//	}

	energyFunction->resetRegularizer();

	return energy + reg;
}

void TestAppRBSimAndDesign::addGradientOfDesignParametersTo(dVector& grad, const dVector& p_) {
	//Beichen Li: here f = objFunction, g = gradient of energyFunction
	LocomotionEngine_EnergyFunction *energyFunction = moptWindow->locomotionManager->locomotionEngine->energyFunction;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;

	dVector m;
	motionPlan->writeMPParametersToList(m);

	SparseMatrix dgdm;
	MatrixNxM dgdp;
	dVector g_m, g_p;
	dVector dfdm;
	dVector p = p_;

	resize(dgdm, m.size(), m.size());
	resize(dgdp, m.size(), p.size());
	resize(dfdm, m.size());

	energyFunction->addGradientTo(dfdm, m);

	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	dgdm.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(dgdm);

	//Beichen Li: the adjoint method calculates lambda = -dgdm ^ -T * dfdm ^ T first
	dVector lambda;
	resize(lambda, m.size());
	lambda = solver.solve(-dfdm);

	double dp = 1e-4;
	//Beichen Li: the next step is to compute dgdp using finite differences
	for (int i = 0; i < p.size(); i++) {
		resize(g_m, m.size());
		resize(g_p, m.size());

		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_p, m);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_m, m);

		p[i] = pVal;

		dgdp.col(i) = (g_p - g_m) / (2 * dp);
	}

	SET_PARAMETERS(prd, p, motionPlan);

	//Beichen Li: finally, compute dfdp with adjoint equation dfdp = lambda ^ T * dgdp + (partial) dfdp
	//f here is equal to original f (used in MOPT) plus a regularizing term expressed by 0.5 * (p - pLast) ^ 2
	dVector pardfdp;
	double f_m, f_p;
	resize(pardfdp, p.size());

	dp = 1e-4;
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		f_p = energyFunction->computeValue(m) + computeRegularizationTerm(p);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		f_m = energyFunction->computeValue(m) + computeRegularizationTerm(p);

		p[i] = pVal;

		pardfdp[i] = (f_p - f_m) / (2 * dp);
	}

	SET_PARAMETERS(prd, p, motionPlan);

	grad = dgdp.transpose() * lambda + pardfdp;
}

void TestAppRBSimAndDesign::computeDmdp() {
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;

	double energy;
	RUN_MOPT(energy);
	
	dVector m, p;
	motionPlan->writeMPParametersToList(m);
	GET_PARAMETERS(prd, p);

	SparseMatrix dgdm(m.size(), m.size());
	MatrixNxM dgdp(m.size(), p.size());
	resize(dmdp, m.size(), p.size());

	//Beichen Li: first compute dgdm aka Hessian of f
	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	dgdm.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(dgdm);

	//Beichen Li: then compute dgdp using finite differences and solve for dmdp
	dVector g_p(m.size()), g_m(m.size()), dgdpi;

	double dp = 1e-4;
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_p, m);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_m, m);

		p[i] = pVal;

		dgdpi = (g_p - g_m) / (2 * dp);

		dmdp.col(i) = -solver.solve(dgdpi);
	}

	SET_PARAMETERS(prd, p, motionPlan);

	//	print("dmdp.txt", dmdp);
}

void TestAppRBSimAndDesign::optimizeDesign() {
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	ModularDesignWindow *designWindow = static_cast<ModularDesignWindow*>(this->designWindow);

	Timer timer;
	timer.restart();

	dVector pV;
	GET_PARAMETERS(prd, pV);

	solver.minimize(*this, pV, currentEnergy);

	dVectorToParametersList(pV, designParameters);

	motionPlan->writeParamsToFile("../out/MPParams.p");

	//designWindow->matchDesignWithRobot(robot);
	robot->setState(&designWindow->startRobotState);
	//Beichen Li: synchronize robot meshes
	designWindow->transferMeshes(robot);
	//rbEngine->reloadMeshes();

	Logger::consolePrint("Time Elapsed: %.3lfs\n", timer.timeEllapsed());
	Logger::consolePrint("Current Energy: %lf\n", currentEnergy);

	V3D v(motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(0), motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(motionPlan->nSamplePoints - 1));
	Logger::consolePrint("distance travelled: %lf %lf %lf\n", v.x(), v.y(), v.z());

	//	Profiler::displayResults();
}

double TestAppRBSimAndDesign::computeRegularizationTerm(const dVector& p) {
	if (pLast.size() != p.size())
		pLast = p;
	return 0.5 * regularizer * (p - pLast).squaredNorm();
}

void TestAppRBSimAndDesign::updateRegularizingSolution(const dVector& p) {
	pLast = p;
}

void TestAppRBSimAndDesign::updateRegularizer(double f, double df) {
	//	if (fabs(df) * regCoefficient < regularizer &&
	//		fabs(dfLast - df) < epsilon * fabs(df)) {
	//		regularizer *= 0.1;
	//		regCoefficient *= 2.0;
	//	}
	//	dfLast = df;
	double temp = regCoefficient * fabs(df);
	regularizer = temp < 5.0 ? temp : 5.0;
}

void TestAppRBSimAndDesign::testParallel() {
#pragma omp parallel for default(shared)
	for (int i = 0; i < numThreads; i++) {
#pragma omp critical
		{
			int threadNum = omp_get_thread_num();
			if (threadNum == 0)
				Logger::consolePrint("Number of threads: %d\n", omp_get_num_threads());
			Logger::consolePrint("Thread %d\n", threadNum);
		}
	}
}
