#include <GUILib/GLUtils.h>
#include "TestAppBulletCollisions.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>

#include <MathLib/Matrix.h>

#include <BulletCollision/btBulletCollisionCommon.h>

TestAppBulletCollisions::TestAppBulletCollisions() {
	setWindowTitle("Test Application for Bullet Collisions...");

	TwAddSeparator(mainMenuBar, "sep2", "");

	btCollisionConfiguration* bt_collision_configuration;
	btCollisionDispatcher* bt_dispatcher;
	btBroadphaseInterface* bt_broadphase;
	btCollisionWorld* bt_collision_world;

	double scene_size = 500;
	unsigned int max_objects = 16000;

	bt_collision_configuration = new btDefaultCollisionConfiguration();
	bt_dispatcher = new btCollisionDispatcher(bt_collision_configuration);

	btScalar sscene_size = (btScalar)scene_size;
	btVector3 worldAabbMin(-sscene_size, -sscene_size, -sscene_size);
	btVector3 worldAabbMax(sscene_size, sscene_size, sscene_size);
	//This is one type of broadphase, bullet has others that might be faster depending on the application
	bt_broadphase = new bt32BitAxisSweep3(worldAabbMin, worldAabbMax, max_objects, 0, true);  // true for disabling raycast accelerator

	bt_collision_world = new btCollisionWorld(bt_dispatcher, bt_broadphase, bt_collision_configuration);
	//Create two collision objects
	btCollisionObject* sphere_A = new btCollisionObject();
	btCollisionObject* sphere_B = new btCollisionObject();
	//Move each to a specific location
	sphere_A->getWorldTransform().setOrigin(btVector3((btScalar)2, (btScalar) 1.5, (btScalar)0));
	sphere_B->getWorldTransform().setOrigin(btVector3((btScalar)2, (btScalar)0, (btScalar)0));
	//Create a sphere with a radius of 1
	btSphereShape * sphere_shape = new btSphereShape(1);
	//Set the shape of each collision object
	sphere_A->setCollisionShape(sphere_shape);
	sphere_B->setCollisionShape(sphere_shape);
	//Add the collision objects to our collision world
	bt_collision_world->addCollisionObject(sphere_A);
	bt_collision_world->addCollisionObject(sphere_B);

	//Perform collision detection
	bt_collision_world->performDiscreteCollisionDetection();

	int numManifolds = bt_collision_world->getDispatcher()->getNumManifolds();
	//For each contact manifold
	for (int i = 0; i < numManifolds; i++) {
		btPersistentManifold* contactManifold = bt_collision_world->getDispatcher()->getManifoldByIndexInternal(i);
		btCollisionObject* obA = (btCollisionObject*)(contactManifold->getBody0());
		btCollisionObject* obB = (btCollisionObject*)(contactManifold->getBody1());
		contactManifold->refreshContactPoints(obA->getWorldTransform(), obB->getWorldTransform());
		int numContacts = contactManifold->getNumContacts();
		//For each contact point in that manifold
		for (int j = 0; j < numContacts; j++) {
			//Get the contact information
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			btVector3 ptA = pt.getPositionWorldOnA();
			btVector3 ptB = pt.getPositionWorldOnB();
			btVector3 n = pt.m_normalWorldOnB;
			double ptdist = pt.getDistance();
		}
	}
}

TestAppBulletCollisions::~TestAppBulletCollisions(void){
}


//triggered when mouse moves
bool TestAppBulletCollisions::onMouseMoveEvent(double xPos, double yPos) {
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppBulletCollisions::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppBulletCollisions::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppBulletCollisions::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppBulletCollisions::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppBulletCollisions::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

}

void TestAppBulletCollisions::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestAppBulletCollisions::process() {
	//do the work here...
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppBulletCollisions::drawScene() {
	//draw here...

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppBulletCollisions::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppBulletCollisions::restart() {

}

bool TestAppBulletCollisions::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

