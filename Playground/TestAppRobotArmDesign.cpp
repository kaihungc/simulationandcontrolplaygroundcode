#include <GUILib/GLUtils.h>

#include "TestAppRobotArmDesign.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>

void TW_CALL LoadRobotArmToSim(void* clientData) {
	((TestAppRobotArmDesign*)clientData)->loadToSim();
}

TestAppRobotArmDesign::TestAppRobotArmDesign(){
	bgColor[0] = bgColor[1] = bgColor[2] = 1;
	setWindowTitle("Robot Arm Designer");

	waitForFrameRate = true;

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawCDPs = false;
	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddButton(mainMenuBar, "LoadRobotArmToSim", LoadRobotArmToSim, this, " label='Load To Simulation' group='Sim' key='l' ");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Simulation-SmoothControl");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMOIs", TW_TYPE_BOOLCPP, &drawMOIs, " label='drawMOIs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawCDPs", TW_TYPE_BOOLCPP, &drawCDPs, " label='drawCDPs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawContactForces", TW_TYPE_BOOLCPP, &drawContactForces, " label='drawContactForces' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawOrientation", TW_TYPE_BOOLCPP, &drawOrientation, " label='drawOrientation' group='Viz2'");

	simTimeStep = 1 / 500.0;
	Globals::g = 0;

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth()-size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	
	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-320-V3.cfg");
//	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configAirbus.cfg");

	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

	showGroundPlane = false;

	TwDefine(" MainMenuBar alpha=255 ");   // semi-transparent blue bar

	bgColor[0] = bgColor[1] = bgColor[2] = 0.75;

	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutRightAxis = 0.25;
	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutUpAxis = 0.95;
	dynamic_cast<GLTrackingCamera*>(this->camera)->camDistance = -1.5;
}

TestAppRobotArmDesign::~TestAppRobotArmDesign(void){
}

//triggered when mouse moves
bool TestAppRobotArmDesign::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseMoveEvent(xPos, yPos)) return true;


	Ray ray = getRayFromScreenCoords(xPos, yPos);

	if (robot && selectedRigidBody == NULL) {
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
			robot->getRigidBody(i)->selected = false;

		highlightedRigidBody = NULL;
		P3D pLocal;
		double tMin = DBL_MAX;
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
			if (robot->getRigidBody(i)->getRayIntersectionPointTo(ray, &pLocal)) {
				double tVal = ray.getRayParameterFor(robot->getRigidBody(i)->getWorldCoordinates(pLocal));
				if (tVal < tMin) {
					tMin = tVal;
					highlightedRigidBody = robot->getRigidBody(i);
				}
			}
		if (highlightedRigidBody)
			highlightedRigidBody->selected = true;
	}

	if (selectedRigidBody) {
		V3D viewPlaneNormal = V3D(camera->getCameraPosition(), camera->getCameraTarget()).unit();
		ray.getDistanceToPlane(Plane(selectedRigidBody->getWorldCoordinates(selectedPoint), viewPlaneNormal), &targetPoint);
		if (ikSolver)
			ikSolver->ikPlan->endEffectors.back().targetEEPos = targetPoint;
		return true;
	}


	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppRobotArmDesign::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)){
			designWindow->onMenuMouseButtonProcessedEvent();
			return true;
		}

	if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;


	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		selectedRigidBody = highlightedRigidBody;
		if (selectedRigidBody) {
			selectedRigidBody->getRayIntersectionPointTo(getRayFromScreenCoords(xPos, yPos), &selectedPoint);
			getRayFromScreenCoords(xPos, yPos).getDistanceToPoint(selectedRigidBody->getWorldCoordinates(selectedPoint), &targetPoint);
			if (ikSolver) {
				ikSolver->ikPlan->endEffectors.push_back(IK_EndEffector());
				ikSolver->ikPlan->endEffectors.back().endEffectorLocalCoords = selectedPoint;
				ikSolver->ikPlan->endEffectors.back().endEffectorRB = selectedRigidBody;
				ikSolver->ikPlan->endEffectors.back().targetEEPos = targetPoint;
			}
		}
	}

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		if (ikSolver)
			ikSolver->ikPlan->endEffectors.clear();
		selectedRigidBody = NULL;
	}

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppRobotArmDesign::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (designWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
		if (designWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppRobotArmDesign::onKeyEvent(int key, int action, int mods) {
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		runOption = PHYSICS_SIMULATION;
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		runOption = IK_ONLY;
	
	if (designWindow->onKeyEvent(key, action, mods)) return true;
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppRobotArmDesign::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppRobotArmDesign::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rs") == 0) {
		Logger::consolePrint("Load robot state from '%s'\n", fName);
		designWindow->setStartStateFName(fName);
		return;
	}

	if (designWindow->type == MODULAR_DESIGN && fNameExt.compare("dsn") == 0) {
		delete rbEngine;
		rbEngine = new ODERBEngine();
		designWindow->loadFile(fName);
		designWindow->saveFile("../out/tmpRobotDesign.rbs");

		//loadToSim();

		return;
	}

	if ((fNameExt.compare("obj") == 0 || fNameExt.compare("mrb") == 0 || fNameExt.compare("rbs") == 0)
		&& designWindow->type == MODULAR_DESIGN) {
		designWindow->loadFile(fName);
		return;
	}

}


void TestAppRobotArmDesign::loadToSim(){
	delete worldOracle;
	delete robot;
	delete rbEngine;

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);
	rbEngine = new ODERBEngine();

/* ------ load the robot and set up an initial motion plan for it ------*/
	designWindow->freezeRobotRoot = true;
	designWindow->saveFile("../out/tmpRobot.rbs");
	rbEngine->autoGenerateCDPs = true;
	rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
//	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../data/rbs/obstacles.rbs");

	robot = new Robot(rbEngine->rbs[0]);

	ReducedRobotState startingRobotState = designWindow->getStartState(robot);
	startingRobotState.writeToFile("../out/tmpRobot.rs");
	designWindow->prepareForSimulation(rbEngine);
	robot->setState(&startingRobotState);
}

void TestAppRobotArmDesign::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Run the App tasks
void TestAppRobotArmDesign::process() {
	if (designWindow->process()) return;

	if (runOption == PHYSICS_SIMULATION) {
		double simulationTime = 0;
		double maxRunningTime = 1.0 / desiredFrameRate;

		while (simulationTime / maxRunningTime < animationSpeedupFactor) {
			simulationTime += simTimeStep;

			if (selectedRigidBody) {
				V3D force = V3D(selectedRigidBody->getWorldCoordinates(selectedPoint), targetPoint) * 1000;
				rbEngine->applyForceTo(selectedRigidBody, force, selectedPoint);
			}
			rbEngine->step(simTimeStep);
			ReducedRobotState rs(robot);
			rs.clearVelocities();
			robot->setState(&rs);
		}
	}
	else if (runOption == IK_ONLY) {
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppRobotArmDesign::drawScene() {
	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	//in case the user is manipulating any of parameters of the motion plan, update them.

	if (runOption == PHYSICS_SIMULATION && rbEngine) {
		int flags = 0;
		if (drawMeshes) flags |= SHOW_MESH | SHOW_MATERIALS;
		if (drawSkeletonView) flags |= SHOW_BODY_FRAME | SHOW_ABSTRACT_VIEW;
		if (drawMOIs) flags |= SHOW_MOI_BOX;
		if (drawCDPs) flags |= SHOW_CD_PRIMITIVES;
		if (drawJoints) flags |= SHOW_JOINTS;

		glEnable(GL_LIGHTING);
		rbEngine->drawRBs(flags);
		glDisable(GL_LIGHTING);
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppRobotArmDesign::drawAuxiliarySceneInfo() {
	designWindow->draw();
	designWindow->drawAuxiliarySceneInfo();
}

// Restart the application.
void TestAppRobotArmDesign::restart() {

}

bool TestAppRobotArmDesign::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppRobotArmDesign::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow->setViewportParameters(size[0], 0, w, h);
	setViewportParameters(size[0] + w, 0, w, h);
}


void TestAppRobotArmDesign::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
