#pragma once

#include <GUILib/GLApplication.h>
#include <string>
#include <map>

#include <GUILib/TranslateWidget.h>
#include <OptimizationLib/ObjectiveFunction.h>
#include <OptimizationLib/LBFGS.h>

//Beichen Li: class definition for isolevel points
class IsolevelPoint {
public:
	IsolevelPoint(int x, int y, int colorDeg): ix(x), iy(y), colorDeg(colorDeg) {}
	
	int ix;
	int iy;
	int colorDeg;
};

//Beichen Li: enum for colors
enum Color {COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_MAGENTA, COLOR_CYAN, COLOR_YELLOW, COLOR_BLACK, COLOR_GREY};

/**
 * Test App for shaders
 */
class TestAppOptimization : public GLApplication {
private:
	//this is a list of all objects that was loaded
	DynamicArray<std::string> loadedObjs;
	int selectedObjFile=0;

	DynamicArray<std::string> materialTextures;
	int selectedMaterialTexture = 0;

	float modelColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f};

	Ray lastClickedRay = Ray(P3D(-10000,-10000,-10000), V3D(1,0,0));

	TranslateWidget tWidget;

	//Beichen Li: scene drawing parameters
	double planeWidth = 5.0;
	int resolution = 250;
	double isolevelInterval = 0.5;
	
	//Beichen Li: maximum and minimum isolevel degrees
	int minDeg, maxDeg;

	//Beichen Li: objective function we are investigating
	ObjectiveFunction *function = NULL;

	//Beichen Li: cache array for contour drawing acceleration
	int *cache = NULL;

	//Beichen Li: isolevel points
	DynamicArray<IsolevelPoint> isolevelPoints;

	//Beichen Li: the following are paths (the points reached) of different optimization methods
	DynamicArray<P3D> pathGradient;
	DynamicArray<P3D> pathNewton;
	DynamicArray<P3D> pathGradientMomentum;
	DynamicArray<P3D> pathLBFGS;

	//Beichen Li: energy values of four algorithms
	double energyGradient = 0.0;
	double energyNewton = 0.0;
	double energyGradientMomentum = 0.0;
	double energyLBFGS = 0.0;

	//Beichen Li: LBFGS parameters and solver
	LBFGSpp::LBFGSParam<double> param;
	LBFGSpp::LBFGSSolver<double> solver;

	//Beichen Li: numIterations controls the number of iterations per step
	int numIterations = 1;

	//Beichen Li: sleepInterval determines how many milliseconds are spent waiting for the next iteration
	int sleepInterval = 1000;

	//Beichen Li: counter of iteration process
	int iter = 0;

	//Beichen Li: control panel
	bool enableContours = true;
	bool enableContourColors = true;
	bool enableAxes = true;
	bool enableGradient = true;
	bool enableNewton = true;
	bool enableGradientMomentum = true;
	bool enableLBFGS = true;
	bool enableHints = true;
	int colorGradient = COLOR_RED;
	int colorNewton = COLOR_GREEN;
	int colorGradientMomentum = COLOR_BLUE;
	int colorLBFGS = COLOR_MAGENTA;

public:
	// constructor
	TestAppOptimization();
	// destructor
	virtual ~TestAppOptimization(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();
	//Beichen Li: take one step back
	void resume();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	//Beichen Li: draw the contours (isolevel curves)
	void drawContours();

	//Beichen Li: draw the optimization paths for different methods
	void drawPaths();

	//Beichen Li: draw a line strip for optimization path
	void drawOptimizationPath(DynamicArray<P3D>& path);

	//Beichen Li: initialize isolevel points
	void initContours();

	//Beichen Li: optimization task initialization, including setting starting point etc.
	void initOptimization();

	//Beichen Li: restart the optimization process at point origin
	void resetOptimization(const P3D& origin);

	double operator () (const dVector& x, dVector& grad) {
		resize(grad, x.size());
		function->addGradientTo(grad, x);
		return function->computeValue(x);
	}

	//Beichen Li: converts 3D point to 2D vector (x and y only)
	dVector point2Vector(const P3D& point) {
		dVector v;
		resize(v, 2);
		v[0] = point.x(), v[1] = point.y();
		return v;
	}

	void logStatus() {
		Logger::consolePrint("--------\n");
		Logger::consolePrint("Iteration %d:\n", iter);
		Logger::consolePrint("    Gradient     %.6lf\n", energyGradient);
		Logger::consolePrint("    Newton       %.6lf\n", energyNewton);
		Logger::consolePrint("    Gradient M.  %.6lf\n", energyGradientMomentum);
		Logger::consolePrint("    LBFGS        %.6lf\n", energyLBFGS);
	}

	void switchColor(int color) {
		switch (color) {
		case COLOR_RED:
			glColor3d(1.0, 0.0, 0.0); break;
		case COLOR_GREEN:
			glColor3d(0.0, 1.0, 0.0); break;
		case COLOR_BLUE:
			glColor3d(0.0, 0.0, 1.0); break;
		case COLOR_MAGENTA:
			glColor3d(1.0, 0.0, 1.0); break;
		case COLOR_CYAN:
			glColor3d(0.0, 1.0, 1.0); break;
		case COLOR_YELLOW:
			glColor3d(1.0, 1.0, 0.0); break;
		case COLOR_BLACK:
			glColor3d(0.0, 0.0, 0.0); break;
		case COLOR_GREY:
			glColor3d(0.5, 0.5, 0.5); break;
		}
	}
};



