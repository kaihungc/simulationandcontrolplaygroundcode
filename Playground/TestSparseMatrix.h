#pragma once
#include "../MathLib/Matrix.h"
#include <cstdio>
#include <ctime>
#include <vector>
#include <iostream>

using namespace std;

class TestSparseMatrix {
    int size, operationCount, iterationCount;
public:
    TestSparseMatrix() {}
    TestSparseMatrix(int size, int operationCount, int iterationCount)
    {
        this->size = size;
        this->operationCount = operationCount;
        this->iterationCount = iterationCount;
    }
    void runTest();
};