#include "TestSparseMatrix.h"

#include <MathLib/MathLib.h>
#include <Utils/Utils.h>

void TestSparseMatrix::runTest()
{
    SparseMatrix matrixDirectly; // using writeSparseMatrixDenseBlockAdd
    SparseMatrix matrixByTriplet; // using triplet
    vector <pair<pair<int, int>, double> > operations;
    vector <MTriplet> triplets;

    matrixDirectly.resize(size, size);
    matrixByTriplet.resize(size, size);

    int amountTimeForDirectMethod = 0;
    int amountTimeForTripletMethod = 0;

    srand((uint)time(0));
    bool flag = true;
    for (int iteration = 0;iteration < iterationCount;++iteration)
    {
        matrixDirectly.setZero();
        matrixByTriplet.setZero();

        // generate random position & value
        operations.clear();
        for (int operation = 0;operation < operationCount;++operation)
        {
            int col = rand() % size;
            int row = rand() % size;
            double v = (double)rand()*rand()*rand() / RAND_MAX;
            double sign = ((double)rand() / RAND_MAX > 0.5) ? (-1) : (1);
            operations.push_back(make_pair(make_pair(row, col), v * sign));
        }
        // generate duplicate elements
        for (int operation = 0;operation < operationCount / 5;++operation)
        {
            int idx = rand() % operationCount;
            double v = (double)rand()*rand()*rand() / RAND_MAX;
            double sign = ((double)rand() / RAND_MAX > 0.5) ? (-1) : (1);
            operations.push_back(make_pair(make_pair(operations[idx].first.first, operations[idx].first.second), v * sign));
        }

        // test write directly
        int timeForDirectMethod = clock();
        for (uint i = 0;i < operations.size();++i)
            matrixDirectly.coeffRef(operations[i].first.first, operations[i].first.second) += operations[i].second;
        timeForDirectMethod = clock() - timeForDirectMethod;
        // test write by triplets
        int timeForTripletMethod = clock();
        triplets.clear();
        for (uint i = 0;i < operations.size();++i)
            triplets.push_back(MTriplet(operations[i].first.first, operations[i].first.second, operations[i].second));
        matrixByTriplet.setFromTriplets(triplets.begin(), triplets.end());
        timeForTripletMethod = clock() - timeForTripletMethod;

        for (uint i = 0;i < operations.size();++i)
            if (matrixByTriplet.coeff(operations[i].first.first, operations[i].first.second) != matrixDirectly.coeff(operations[i].first.first, operations[i].first.second))
                flag = false;
        amountTimeForDirectMethod += timeForDirectMethod;
        amountTimeForTripletMethod += timeForTripletMethod;
    }

    cerr << "|-----------------------------------" << endl;
    cerr << "|Test report:" << endl;
    cerr << "|-----------------------------------" << endl;
    cerr << "|Matrix size = " << size << " x " << size << endl;
    cerr << "|Operation count = " << operationCount << endl;
    cerr << "|Duplicate elements number = " << operationCount / 5 << endl;
    cerr << "|Iteration count = " << iterationCount << endl;
    cerr << "|-----------------------------------" << endl;
    cerr << "|Check result = " << ((flag)?"Correct":"Incorrect") << endl;
    cerr << "|-----------------------------------" << endl;
    cerr << "|Average time for Direct Method = " << (double)amountTimeForDirectMethod/iterationCount << " ms" << endl;
    cerr << "|-----------------------------------" << endl;
    cerr << "|Average time for Triplet Method = " << (double)amountTimeForTripletMethod/iterationCount << " ms" << endl;
    cerr << "|-----------------------------------" << endl;
}