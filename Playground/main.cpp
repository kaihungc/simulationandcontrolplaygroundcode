//#define RUN_UNIT_TESTS

#ifdef RUN_UNIT_TESTS
#include <OptimizationLib/OoqpEigenInterface_test.cpp>
#include <OptimizationLib/QuadraticProblemFormulation_test.cpp>
#include <OptimizationLib/NewtonFunctionMinimizer_test.cpp>
#include <OptimizationLib/FunctionConstraints_test.cpp>
//#include <OptimizationLib/SQPFunctionMinimizer_test.cpp>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <../GUILib/GLApplication.h>
#include "TestApp.h"
#include "TestAppShaders.h"
//#include "TestAppAndi.h"
//#include "TestAppRDStructure.h"
#include "TestAppSeaTurtle.h"
#include "TestAppRBSimAndDesign.h"
//#include "TestAppSingleParticle.h"
#include "TestAppBulletCollisions.h"
#include "TestAppMeshOps.h"
#include "TestAppModularRobot.h"
#include "TestAppDynamixel.h"
#include "TestAppRobotArm.h"
#include "TestAppDXLRobot.h"
#include "MySpotMiniApp.h"
#include "RobotAnimationApp.h"
#include "TestAppRobotWheel.h"
#include "TestAppConnectors.h"
#include "TestAppRobotArmDesign.h"

#include "TestAppRobotDesignOptimization.h"
#include "TestAppMorphology.h"
#include "TestAppLivingDesign.h"
#include "TestAppOptimization.h"

int main(void) {
#ifdef RUN_UNIT_TESTS
	int n = 1;
	char* argv[] = { "" };
	testing::InitGoogleTest(&n, argv);
	RUN_ALL_TESTS();
#endif

	GLApplication* theApp;

	//	theApp = new GLApplication(10, 45, 1024, 800);
	//	theApp = new GLApplication();
	//	theApp = new TestAppShaders();
	//  theApp = new TestAppAndi();
	//	theApp = new TestApp();
	//	theApp = new TestAppFEMSim();
	//  theApp = new TestAppFEMSim3D();
	//	theApp = new TestAppRBSim();
	//	theApp = new TestAppRDStructure();
	//	theApp = new TestAppSeaTurtle();
	//  theApp = new TestAppSingleParticle();
	//	theApp = new TestAppBulletCollisions();
	//	theApp = new TestAppMeshOps();
	//	theApp = new TestAppRDSimple();

	//	theApp = new TestAppPoseControl();

	//	theApp = new TestAppQPBasedControl();
	//	theApp = new MySpotMiniApp();
	//	theApp = new RobotAnimationApp();
	//	theApp = new SkeletonMorphApp();
	//	theApp = new TestAppModularRobot();

		theApp = new TestAppRBSimAndDesign(); 
	//	theApp = new TestAppRobotArmDesign();
	//	theApp = new TestAppRobotDesignOptimization();
	//	theApp = new TestAppLivingDesign();

	//  theApp = new TestAppMorphology();


	//  theApp = new TestAppConnectors();

	//	theApp = new TestAppDynamixel();
	//	theApp = new TestAppCarArm();
	//	theApp = new TestAppDXLRobot();
	//	theApp = new TestAppRobotWheel();
	//	theApp = new TestAppBluetooth();
	//	theApp = new TestAppBluetoothCar();

	//	theApp = new TestAppOptimization();

	theApp->runMainLoop();
	delete theApp;
	return 0;
}
