#include <GUILib/GLUtils.h>

#include "TestAppDXLRobot.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>

TestAppDXLRobot::TestAppDXLRobot(){
	setWindowTitle("RobotDXLRobot");
//	loadFile("../data/rbs/dinoV4.rbs");
//	loadFile("../data/mopt/dinoV4WalkForward.p");

//	loadFile("../data/rbs/SehoonsPuppy.rbs");
//	loadFile("../data/mopt/SehoonsPuppy.p");

	DXLPortName = "COM3";

	loadFile("../data/rbs/puppy.rbs");
	loadFile("../data/mopt/puppy.p");


	TwAddSeparator(mainMenuBar, "sep4", "");
	showGroundPlane = true;

	desiredFrameRate = 70;
}

void TestAppDXLRobot::loadRobot(const char* fName) {
	delete robot;
	delete rbEngine;
	delete controller;
	delete motionPlan;
	delete worldOracle;

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);

	rbEngine = new ODERBEngine();

	RobotDesign* robotDesign = new RobotDesign();
	robotDesign->readRobotFromFile(fName);
	robotDesign->saveRobotToFile("../out/tmpRobot.rbs");
	rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");
	robotDesign->transferMeshes(rbEngine->rbs);
	delete robotDesign;



//	rbEngine->loadRBsFromFile(fName);

	
	robot = new Robot(rbEngine->rbs[0]);
	setupSimpleRobotStructure(robot);

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");

	motionPlan = new LocomotionEngineMotionPlan(robot, 10);

	//load from file should read the number of sample points, as well as the motion plan duration...

	controller = new PositionBasedRobotController(robot, motionPlan);

}

void TestAppDXLRobot::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rbs") == 0)
		loadRobot(fName);

	if (fNameExt.compare("rs") == 0 && robot)
		robot->loadReducedStateFromFile(fName);

	if (fNameExt.compare("p") == 0)
		if (motionPlan)
			motionPlan->readParamsFromFile(fName);
}

TestAppDXLRobot::~TestAppDXLRobot(void){
}

// Restart the application.
void TestAppDXLRobot::restart() {

}

// Run the App tasks
void TestAppDXLRobot::process() {
	static int lastRunOptionSelected = runMode + 1;
	if (lastRunOptionSelected != runMode && runMode == PHYSICS_SIMULATION) {
		// TODO: should also set velocities based on the motion plan...
		Logger::consolePrint("Syncronizing robot state\n");
		controller->stridePhase = stridePhase;
		controller->computeDesiredState();
		robot->setState(&controller->desiredState);
	}
	lastRunOptionSelected = runMode;

	if (runMode == SYNC_SIM_FROM_ROBOT_STATE)
		syncSimulationWithPhysicalRobot();
	else if (runMode == ANIMATION_PLAYBACK){
		controller->stridePhase = stridePhase;
		controller->computeDesiredState();
		robot->setState(&controller->desiredState);

		stridePhase += (1.0 / desiredFrameRate) / motionPlan->motionPlanDuration;
		if (stridePhase > 1.0) stridePhase -= 1.0;
	} else if (runMode == ROBOT_CONTROL) {
		controller->stridePhase = stridePhase;
		controller->computeDesiredState();
		robot->setState(&controller->desiredState);

		stridePhase += (1.0 / desiredFrameRate) / motionPlan->motionPlanDuration;
		if (stridePhase > 1.0) stridePhase -= 1.0;

		//TODO: may want to compute FF velocities here!
		computeDXLControlInputsBasedOnSimRobot(1.0 / desiredFrameRate);
		sendControlInputsToPhysicalRobot();
	} else if (runMode == PHYSICS_SIMULATION) {
		controller->stridePhase = stridePhase;
		double simulationTime = 0;
		double maxRunningTime = 1.0 / desiredFrameRate;

		while (simulationTime / maxRunningTime < animationSpeedupFactor) {
			simulationTime += simTimeStep;

			controller->computeControlSignals(simTimeStep);
			rbEngine->step(simTimeStep);
			controller->advanceInTime(simTimeStep);
		}
		stridePhase = controller->stridePhase;
	}
}


