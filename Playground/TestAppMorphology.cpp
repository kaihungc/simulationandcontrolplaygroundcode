#include <GUILib/GLUtils.h>

#include "TestAppMorphology.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <iostream>
namespace Morphology {
	void TW_CALL LoadRobot(void* clientData) {
		((TestAppMorphology*)clientData)->loadToSim();
	}

	void TW_CALL WarmstartMOPT(void* clientData) {
		((TestAppMorphology*)clientData)->warmStartMOPT();
	}
}

TestAppMorphology::TestAppMorphology(){
	bgColor[0] = bgColor[1] = bgColor[2] = 1;
	setWindowTitle("RobotDesigner");

	waitForFrameRate = true;

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawCDPs = false;
	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddButton(mainMenuBar, "LoadRobotToSim", Morphology::LoadRobot, this, " label='Load To Simulation' group='Sim' key='l' ");
	TwAddButton(mainMenuBar, "WarmstartMOPT",  Morphology::WarmstartMOPT, this, " label='WarmStart MOPT' group='Sim' key='w' ");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Simulation-SmoothControl");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "drawMotionPlan", TW_TYPE_BOOLCPP, &drawMotionPlan, " label='drawMotionPlan' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMOIs", TW_TYPE_BOOLCPP, &drawMOIs, " label='drawMOIs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawCDPs", TW_TYPE_BOOLCPP, &drawCDPs, " label='drawCDPs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawContactForces", TW_TYPE_BOOLCPP, &drawContactForces, " label='drawContactForces' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawOrientation", TW_TYPE_BOOLCPP, &drawOrientation, " label='drawOrientation' group='Viz2'");

	TwAddSeparator(mainMenuBar, "sep3", "");
	
	TwAddVarRW(mainMenuBar, "groundKPScale", TW_TYPE_DOUBLE, &groundKPScale, "min=-5 max=10 step=1");
	TwAddVarRW(mainMenuBar, "groundKdVal", TW_TYPE_DOUBLE, &groundKdVal, "min=0 max=10000 step=100");

	TwAddSeparator(mainMenuBar, "sep4", "");

	TwAddVarRW(mainMenuBar, "PlanMotionPhase", TW_TYPE_DOUBLE, &motionPlanTime, "min=0 max=1 step=0.01 group='MotionOptimizationOptions'");

//	TwAddVarRW(mainMenuBar, "", TW_TYPE_DOUBLE, &robotScale, "min=0.5 max=1.5 step=0.1 group='MotionOptimizationOptions'");
//	TwAddVarCB(mainMenuBar, "Robot Scale", TW_TYPE_DOUBLE, setRobotScale, getRobotScale, this, "step=0.05");



	simTimeStep = 1 / 500.0;
//	Globals::g = 0;

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth()-size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	
	moptWindow = new MOPTWindow(size[0], 0, w, h, this);
	
	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

	loadFile("../data/motionplans/spotMini/robot.rbs");
	loadFile("../data/motionplans/spotMini/robot.rs");

	showGroundPlane = true;

	TwDefine(" MainMenuBar alpha=255 ");   // semi-transparent blue bar

	bgColor[0] = bgColor[1] = bgColor[2] = 0.75;

	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutRightAxis = 0.25;
	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutUpAxis = 0.95;
	dynamic_cast<GLTrackingCamera*>(this->camera)->camDistance = -1.5;
}

TestAppMorphology::~TestAppMorphology(void){
}

void TestAppMorphology::addParameterizedDesignParametersToMenu() {
	prd->getCurrentSetOfParameters(designParameters);
	designParameterNames.resize(designParameters.size());
	for (uint i = 0; i < designParameters.size(); i++) {
		designParameterNames[i] = string("p") + to_string(i);
		TwAddVarRW(mainMenuBar, designParameterNames[i].c_str(), TW_TYPE_DOUBLE, &designParameters[i], "step=0.1 group ='DesignParams'");
	}
	// EXPERIMENTAL
	TwAddVarRW(mainMenuBar, "TEST_DOUBLE", TW_TYPE_DOUBLE, &prd->TEST_DOUBLE, "min=0 max=5 step=0.01 group='DesignParams'");
	TwAddVarRW(mainMenuBar, "TEST_i", TW_TYPE_INT32,  &prd->TEST_i, "min=0 group='DesignParams'");
	TwAddVarRW(mainMenuBar, "morph step", TW_TYPE_BOOLCPP, &MORPH_OPT_STEP, "group='DesignParams'");
	TwAddVarRW(mainMenuBar, "morph opt", TW_TYPE_BOOLCPP, &MORPH_OPT, "group='DesignParams'");

//	prd->setParameters(params);
}

void TestAppMorphology::removeParameterizedDesignParametersFromMenu() {

	for (uint i = 0; i < designParameterNames.size(); i++) {
		TwRemoveVar(mainMenuBar, designParameterNames[i].c_str());
	}
}


//triggered when mouse moves
bool TestAppMorphology::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
		if (moptWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppMorphology::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)

	if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
		if (moptWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppMorphology::onMouseWheelScrollEvent(double xOffset, double yOffset) {

	if (moptWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
		if (moptWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppMorphology::onKeyEvent(int key, int action, int mods) {
	if (locomotionManager && locomotionManager->motionPlan){
		if (key == GLFW_KEY_UP && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() += 0.1;
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() -= 0.1;
		if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() += 0.1;
		if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() -= 0.1;
		if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle += 0.1;
		if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle -= 0.1;

		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.z(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.x(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desTurningAngle, -0.5, 0.5);
	}

	if (key == GLFW_KEY_O && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/tmpMPAngles.mpa");

	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_OPTIMIZATION;
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_ANIMATION;
	
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppMorphology::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppMorphology::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rs") == 0) {
		Logger::consolePrint("Load robot state from '%s'\n", fName);
		if (robot) {
			robot->loadReducedStateFromFile(fName);
		}
		return;
	}

	if (fNameExt.compare("rbs") == 0 ){ 
		removeParameterizedDesignParametersFromMenu();

		delete rbEngine;
		delete robot;
		delete prd;

		rbEngine = new ODERBEngine();
		rbEngine->loadRBsFromFile(fName);
		robot = new Robot(rbEngine->rbs[0]);
		prd = new SpotMiniParameterizedDesign(robot, this->moptWindow);
		// This also adds the design parameters to the menu, which references the list of parameters
		// The design is updated because setParameters is called in each drawScene
		addParameterizedDesignParametersToMenu();
		return;
	}

	if (fNameExt.compare("ffp") == 0) {
		moptWindow->footFallPattern.loadFromFile(fName);
		moptWindow->footFallPattern.writeToFile("..\\out\\tmpFFP.ffp");
		return;
	}

	if (fNameExt.compare("p") == 0) {
		if (locomotionManager && locomotionManager->motionPlan)
			locomotionManager->motionPlan->readParamsFromFile(fName);
		return;
	}

}

void TestAppMorphology::loadToSim(){
	ReducedRobotState startingRobotState(robot);
	setupSimpleRobotStructure(robot);
	moptWindow->loadRobot(robot, &startingRobotState);

	int nLegs = robot->bFrame->limbs.size();
	int nPoints = 3 * nLegs;

	//now, start MOPT...
	warmStartMOPT();

	Logger::consolePrint("Warmstart successful...\n");
	Logger::consolePrint("The robot has %d legs, weighs %lfkgs and is %lfm tall...\n", robot->bFrame->limbs.size(), robot->getMass(), robot->root->getCMPosition().y());
	
	Logger::consolePrint("Beginning sanity check of parameters vs objective. (Slow)\n");

	Logger::logPrint("--------SANITY CHECK------\n");
	double scale = .1;
	double step = .01;
	
	while (scale < 1.1) {
		
		scale += step;
		//cout << "[Sanity Check] scale " << scale << std::endl;
		dVector xi(1);
		xi[0] = scale;
		prd->objective->computeValue(xi);
	}
	Logger::logPrint("--------END CHECK------\n");
	
	
}

void TestAppMorphology::warmStartMOPT() {
	delete locomotionManager;
	locomotionManager = moptWindow->initializeNewMP();
	animationCycle = 0;
	mCount = 0;
	
}

void TestAppMorphology::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

void TestAppMorphology::runMOPTStep() {
	mCount++;
	double energyVal = moptWindow->runMOPTStep();
	dVector params;
	moptWindow->locomotionManager->motionPlan->writeMPParametersToList(params);
	Logger::consolePrint("count %d obj %s value: %lf\n", mCount, moptWindow->locomotionManager->locomotionEngine->energyFunction->objectives[11]->description.c_str(), moptWindow->locomotionManager->locomotionEngine->energyFunction->objectives[11]->computeValue(params));

}

void TestAppMorphology::process() {

	if (MORPH_OPT_STEP) {
		MORPH_OPT_STEP = false;
		prd->optimize();
		prd->getCurrentSetOfParameters(designParameters);
	} else if (MORPH_OPT) {
		prd->optimize();
		prd->getCurrentSetOfParameters(designParameters);
	}

	if (runOption == MOTION_PLAN_OPTIMIZATION) {
		animationCycle = 0;
		runMOPTStep();
	}
	else if (runOption == MOTION_PLAN_ANIMATION) {
		motionPlanTime += (1.0 / desiredFrameRate) / locomotionManager->motionPlan->motionPlanDuration;
		if (motionPlanTime > 1) {
			animationCycle++;
			motionPlanTime -= 1;
		}
		moptWindow->setAnimationParams(motionPlanTime, 0);
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppMorphology::drawScene() {
	// FORNOW
	prd->setParameters(designParameters);

	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	//in case the user is manipulating any of parameters of the motion plan, update them.
	
	if (locomotionManager)
		locomotionManager->drawMotionPlan(motionPlanTime, animationCycle, false, true, drawMotionPlan, drawContactForces, drawOrientation);

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppMorphology::drawAuxiliarySceneInfo() {
	moptWindow->draw();
	moptWindow->drawAuxiliarySceneInfo();
}

// Restart the application.
void TestAppMorphology::restart() {

}

bool TestAppMorphology::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppMorphology::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0] + w, 0, w, h);
}


void TestAppMorphology::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
