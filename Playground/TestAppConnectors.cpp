#include <GUILib/GLUtils.h>

#include "TestAppConnectors.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>

/**
	Living Brackets and Connectors:
		- generative design for mounting brackets

	- an opportunity to investigate "rubber" bones, customized brackets and connectors, that change as a 
		function of the connecting motors/end effectors/range of motion, etc... 
	- bones modeled as rod like structures
	- good also for the whole sensitivity analysis part of robot designer...
	- perhaps also an opportunity for topology optimization, lightweight yet strong structures, analysis of where supports/rivets/screws should go, based on loading forces or moments...

	- perhaps think of it still just as horn bracket and base bracket
	- the horn bracket is special, because it resizes itself based on range of motion
	- the bottom bracket is special, because it can "flow/crawl/goo up?" around the rigid part of the motor, always trying not to interfere with the motion of the horn, but connecting itself to as many mounting holes as possible for stability (and option to reach to connecting motor)

	- get little 2 motor dynamixel demo working again - and update the dynamixel library, and clean up the code/start over to make sure it's good... and with good baud rate too + feedback?
*/



/*
#define N_POINTS 300
double gridVal[N_POINTS][N_POINTS];
double bPosStartX = -bracketWidth/2;
double bPosStartY = -0.1;
double bPosEndX = bracketWidth/2;
double bPosEndY = bracketHeight - 0.1;
*/

double rotDir = 1;

TestAppConnectors::TestAppConnectors() {
	setWindowTitle("Connectors");

	TwAddSeparator(mainMenuBar, "sep4", "");
	showGroundPlane = true;

//	memset(gridVal, 0, N_POINTS * N_POINTS * sizeof(double));
	TwAddVarRW(mainMenuBar, "initial angle", TW_TYPE_DOUBLE, &lbh.bracketInitialAngle, "min=-3.14 max=3.14 step=0.1");
	TwAddVarRW(mainMenuBar, "motor angle min", TW_TYPE_DOUBLE, &lbh.motor->rotAngleMin, "min=-3.14 max=3.14 step=0.1");
	TwAddVarRW(mainMenuBar, "motor angle max", TW_TYPE_DOUBLE, &lbh.motor->rotAngleMax, "min=-3.14 max=3.14 step=0.1");
	TwAddVarRW(mainMenuBar, "bracket connector angle", TW_TYPE_DOUBLE, &lbh.bracketConnectorAngle, "min=-3.14 max=3.14 step=0.1");

	motor.hornBracket = &lbh;

	((GLTrackingCamera*)camera)->camDistance = -0.1;
	
	showGroundPlane = false;
}

void TestAppConnectors::loadFile(const char* fName) {
	Logger::consolePrint("Do not know what to do with this file \'%s\'...\n", fName);
}

TestAppConnectors::~TestAppConnectors(void){
}

// Restart the application.
void TestAppConnectors::restart() {

}

// Run the App tasks
void TestAppConnectors::process() {
	for (int i = 0; i < 3;i++){
		lbh.motor->rotAngle += 0.01 * rotDir;
		if (lbh.motor->rotAngle > lbh.motor->rotAngleMax)
			rotDir = -1.0;
		if (lbh.motor->rotAngle < lbh.motor->rotAngleMin)
			rotDir = 1.0;
	}
}

//triggered when mouse moves
bool TestAppConnectors::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppConnectors::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)) {
			return true;
		}

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppConnectors::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppConnectors::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppConnectors::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppConnectors::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppConnectors::drawScene() {
//	if (!appIsRunning)
//		lbh.motor->rotAngle = lbh.bracketInitialAngle;

	glPointSize(10);
	glBegin(GL_POINTS);
	glColor3d(0, 0, 0);
	glVertex3d(0, 0, 0);
	glEnd();

	glPointSize(1);

	//draw the motor box
	glDisable(GL_LIGHTING);
	glColor4d(0.4, 0.4, 0.8, 0.4);
	glBegin(GL_QUADS);
	glVertex3d(lbh.motor->boundingBox.bmin().x(), lbh.motor->boundingBox.bmin().y(), 0);
	glVertex3d(lbh.motor->boundingBox.bmin().x(), lbh.motor->boundingBox.bmax().y(), 0);
	glVertex3d(lbh.motor->boundingBox.bmax().x(), lbh.motor->boundingBox.bmax().y(), 0);
	glVertex3d(lbh.motor->boundingBox.bmax().x(), lbh.motor->boundingBox.bmin().y(), 0);
	glEnd();

	lbh.motor->draw();
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppConnectors::drawAuxiliarySceneInfo() {

}

bool TestAppConnectors::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppConnectors::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]);
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0], 0, w, h);
}


void TestAppConnectors::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
