// CSG.scad - Basic example of CSG usage

printingScale = 0.4; // the print scale
cubeHeight = 10; // 8mm
carveHeight = 10;
cubeLen = 20;
cubeWeidth = 20;
radius = cubeWeidth / 2;
pinRadius = 4.5;
gap = 0.01;
clearance = 0.22 / printingScale;
alpha = 0.667;
n = 20;

difference(){
        
        union(){
        
            translate([0, 0, -cubeHeight / 2]){
                cube(size = [cubeLen, cubeWeidth, cubeHeight], center = true);
            }
            
            rotate(a = 90, v = [0, 1, 0]) {
                cylinder(h=cubeLen, r1=radius, r2=radius, center=true, $fn = n);
            } 
        }
    
        cube(size = [cubeLen * alpha + 2 * gap, cubeWeidth + gap, 100], center = true);
}  

// *** socket carve model 
//union(){
//    translate([0, 0, 0]){
//     cube(size = [cubeLen, cubeWeidth, carveHeight * 2], center = true);
//    }
//}

//union(){
//            
//        translate([0, 0, cubeHeight / 2]){
//            cube(size = [cubeLen * alpha - clearance, cubeWeidth, cubeHeight], center = true);
//        }
//        
//        rotate(a = 90, v = [0, 1, 0]) {
//            cylinder(h= cubeLen * alpha - clearance, r1=radius, r2=radius, center=true, $fn = n);
//        } 
//}


// *** carve model 

//union(){
//    translate([0, 0, 0]){
//        cube(size = [cubeLen, cubeWeidth - 2 * gap, 2 * carveHeight], center = true);
//    }
//}





