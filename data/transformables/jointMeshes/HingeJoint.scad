// CSG.scad - Basic example of CSG usage

//translate([-24,0,0]) {
//    union() {
//        cube(15, center=true);
//        sphere(10);
//    }
//}
cubeHeight = 20;
carveHeight = 15;
cubeLen = 20;
cubeWeidth = 20;
radius = cubeWeidth / 2;
pinRadius = 4.5;
gap = 0.01;
alpha = 0.667;
beta = 0.64;
n = 20;

//difference(){
//    
//    difference(){
//        
//        union(){
//        
//            translate([0, 0, -cubeHeight / 2]){
//                cube(size = [cubeLen, cubeWeidth, cubeHeight], center = true);
//            }
//            
//            rotate(a = 90, v = [0, 1, 0]) {
//                cylinder(h=cubeLen, r1=radius, r2=radius, center=true, $fn = n);
//            } 
//        }
//    
//        cube(size = [cubeLen * alpha + 2 * gap, cubeWeidth + gap, 100], center = true);
//    }   
//    
//    rotate(a = 90, v = [0, 1, 0]) {
//            cylinder(h=cubeLen * 2, r1=pinRadius, r2=pinRadius, center=true, $fn = n);
//    } 
//}

// *** socket carve model 
//union(){
////     translate([0, 0, 0]){
////     cube(size = [cubeLen * alpha + 3 * gap, cubeWeidth, carveHeight * 2], center = true);
////    }
//    
//    rotate(a = 90, v = [0, 1, 0]) {
//     cylinder(h=1000, r1=pinRadius, r2=pinRadius, center=true, $fn = n);
//    } 
//}

difference(){
    
    union(){
            
        translate([0, 0, -cubeHeight / 2]){
            cube(size = [cubeLen * beta, cubeWeidth, cubeHeight], center = true);
        }
        
        rotate(a = 90, v = [0, 1, 0]) {
            cylinder(h= cubeLen * beta, r1=radius, r2=radius, center=true, $fn = n);
        } 
        
//        translate([0, 0, radius + cubeHeight / 2 + gap]){
//            cube(size = [cubeLen , cubeWeidth, cubeHeight], center = true);
//        }
    }
    
    rotate(a = 90, v = [0, 1, 0]) {
            cylinder(h=cubeLen * 2, r1=pinRadius, r2=pinRadius, center=true, $fn = n);
    } 
}


// *** carve model 

//union(){
//    translate([0, 0, 0]){
//    cube(size = [cubeLen, cubeWeidth - gap, 2 * carveHeight], center = true);
//    }
    
//    rotate(a = 90, v = [0, 1, 0]) {
//     cylinder(h=100, r1=pinRadius, r2=pinRadius, center=true, $fn = 100);
//    } 
//}






echo(version=version());
// Written by Marius Kintel <marius@kintel.net>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
