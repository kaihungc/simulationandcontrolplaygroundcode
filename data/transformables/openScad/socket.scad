scale = 0.001;
sx = 12.000 * 2 * scale;
sy = 15.005 * 2 * scale;
sz = 12.000 * 2 * scale;
socketDepth = 12 * scale;
socketOutR = 10 * scale;
socketInR = 4 * scale;
numerical = 1e-2 * scale;
clearance = 0.3 * scale;
D_clearance = 0.3 * scale;
n = 3;

ph = 100 * scale;


// socket
//union(){
//    difference(){
//        cube(size = [sx, sy, sz], center = true);
//
//        translate([0, 0, (sz - socketDepth) / 2]){
//             cylinder(h=socketDepth + numerical, r1=socketOutR + clearance, r2=socketOutR + clearance, center=true, $fn = n);
//        } 
//    }
//    
//    translate([0, 0, (sz - socketDepth) / 2]){
//         cylinder(h=socketDepth + numerical, r1=socketInR - clearance, r2=socketInR - clearance, center=true, $fn = n);
//    } 
//}

// plug

//difference(){
//    translate([0, 0, (ph + sz) / 2 - socketDepth + D_clearance]){
//                 cylinder(h=ph, r1=socketOutR, r2=socketOutR, center=true, $fn = n);
//    } 
//    
//    translate([0, 0, (ph + sz) / 2 - socketDepth + D_clearance]){
//                 cylinder(h=ph + numerical, r1=socketInR, r2=socketInR, center=true, $fn = n);
//    } 
//}

// socket carve
difference(){
    translate([0, 0, (ph + sz) / 2 - socketDepth]){
                 cylinder(h=ph, r1=socketOutR + clearance, r2=socketOutR + clearance, center=true, $fn = n);
    } 
    
    translate([0, 0, (ph + sz) / 2 - socketDepth]){
                 cylinder(h=ph + numerical, r1=socketInR - clearance, r2=socketInR - clearance, center=true, $fn = n);
    } 
}