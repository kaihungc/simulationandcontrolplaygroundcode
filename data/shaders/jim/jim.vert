varying vec3 worldPosition;
varying vec3 N;

void main(){
    worldPosition = gl_Vertex.xyz;
    // N = normalize(gl_NormalMatrix * gl_Normal); 
    N = normalize(gl_Normal); 
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
