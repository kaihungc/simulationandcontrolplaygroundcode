varying vec3 worldPosition;
varying vec3 N;

void main(){ 
    gl_FragColor.rgb = .5*(vec3(1.)+N);
	gl_FragColor.w = 1.;
}
