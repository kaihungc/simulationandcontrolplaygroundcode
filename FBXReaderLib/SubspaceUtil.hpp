#pragma once

#include <MathLib/MathLib.h>
#include <MathLib/Matrix.h>

#include <FBXReaderLib\Skeleton.hpp>


///<
struct SubspaceUtil
{
	///< projection matrix:
	Eigen::MatrixXd		m_P;
	
	//	Average of samples:
	Eigen::MatrixXd		m_average;

	typedef Eigen::EigenSolver< Eigen::MatrixXd >::EigenvectorsType EigenVector;

	void GetMaxEigenVectors		(const Eigen::MatrixXd& _A, const int _numEigenModes);
	void ComputeAndRemoveMean	(Eigen::MatrixXd& _A);

	int					NumEigenVectors()const;
	Eigen::MatrixXd		GetEigenVector(const int _i) const;	

	static void ComputeSkeletonSubspace(const Skeleton& _skAnim, SubspaceUtil& _x, SubspaceUtil& _q, int _numDimX = 2, int _numDimQ = 8);

	///< Removes mean and projects.
	Eigen::MatrixXd Project(const Eigen::MatrixXd& _x) const;

	///< Un projects and adds mean
	Eigen::MatrixXd UnProject(const Eigen::MatrixXd& _x) const;


	///< Removes mean and adds it back.
	Eigen::MatrixXd ProjectAndUnproject(const Eigen::MatrixXd& _x) const;

};