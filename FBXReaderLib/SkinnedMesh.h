#pragma once

#include <fbxsdk.h>
#include <GUILib/GLUtils.h>
#include <Utils\Utils.h>

#include <GUILib\GLShader.h>

#define MAX_BONES 50
#define MAX_INFLUENCES 4


class NodeTransform
{
public:

	V3D				m_x;
	V3D				m_s;
	Quaternion		m_q;

	NodeTransform() : m_s(1, 1, 1) {}

	NodeTransform(const V3D& _x, const V3D& _s, const Quaternion& _q) : m_x(_x), m_s(_s), m_q(_q) {}

	Matrix4x4 getMatrix() const
	{
		Matrix4x4 M = Matrix4x4::Identity();

		Matrix3x3 R = m_q.getRotationMatrix();
		Matrix4x4 mS;
		
		mS.setIdentity();

		for (int i = 0; i < 3; ++i)
			mS(i, i) = m_s[i];

		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				M(i, j) = (double)R(i, j);

		M = mS*M;// scale is already included in position.

		for (int i = 0; i < 3; ++i)
			M(i, 3) = (double)m_x[i];
		
		return M;
	}
		

};

class SkinnedMesh 
{
	friend class FBXExporter;

protected:

	struct BoneWeightPair 
	{
		int		boneId;
		double	boneWeight;
	};
	
	struct FVertex 
	{
		GLfloat 	m_x[4];
		GLfloat		m_n[4];

		GLfloat		_boneIds[MAX_INFLUENCES];
		GLfloat 	_boneWeights[MAX_INFLUENCES];

		FVertex()  
		{
			memset(m_x, 0, sizeof(m_x));
			memset(m_n, 0, sizeof(m_n));

			for (int i = 0; i < MAX_INFLUENCES; ++i)
			{
				_boneIds[i] = -1;
				_boneWeights[i] = 0;
			}
		}
	};

	struct GPUShader
	{

		struct Vertex
		{
			GLfloat 	m_x[4];
			GLfloat		m_n[4];

			GLfloat		_boneIds[MAX_INFLUENCES];
			GLfloat 	_boneWeights[MAX_INFLUENCES];

			Vertex()
			{
				memset(m_x, 0, sizeof(m_x));
				memset(m_n, 0, sizeof(m_n));

				for (int i = 0; i < MAX_INFLUENCES; ++i)
				{
					_boneIds[i] = -1;
					_boneWeights[i] = 0;
				}
			}
		};

		GLShaderProgram			m_program;

		///< All for vertex and index buffer:
		GLuint					m_vertexArrayID = 0;
		GLuint					m_vertexBufferID = 0;

		std::vector<Vertex>		m_vertexArray;

		void create(const std::vector<FVertex>& _srcVertices);
		void setVertexBuffer();
	};

	struct CPUShader
	{

		struct Vertex
		{
			GLfloat 	m_x[4];
			GLfloat		m_n[4];

			Vertex()
			{
				memset(m_x, 0, sizeof(m_x));
				memset(m_n, 0, sizeof(m_n));
			}
		};

		GLShaderProgram			m_program;

		GLuint					 m_vertexArrayID = 0;
		GLuint					 m_vertexBufferID = 0;

		///< CPU version, use along index array only.
		std::vector<Vertex>		m_vertexArray;

		void create				(const std::vector<FVertex>& _srcVertices);
		void setVertexBuffer	();
	};

	bool m_bDrawWireFrame	= false;
	bool m_bDrawCPU			= true;

	///< shaders:
	CPUShader			m_cpuShader;
	GPUShader			m_gpuShader;

	friend class FBXImporter;

public:
	typedef std::vector<Matrix4x4, Eigen::aligned_allocator<Matrix4x4> >		TransformsContainer;
	struct GLFloatMatrix { GLfloat m_d[16]; }; 	///< (Must to be continuous float data)

protected:
	GLuint									m_indexBufferID = 0;
	std::vector<GLuint>						m_indexArray;	

	std::vector<FVertex>					m_sourceVertices;

	TransformsContainer						m_invBindMatrices;

	///< Mesh is containted in a node:
	NodeTransform							m_nodeTransform;

	std::vector<GLFloatMatrix>				m_skinningMatrices;

		
	///< For CPU rendering:
	std::vector<int>						m_boneCountsPerVertex;



	void transformMeshVertices_linearBlendSkinning();

public:
	SkinnedMesh()  {}
	virtual ~SkinnedMesh();

	SkinnedMesh& operator=(const SkinnedMesh& _fmesh);

	void	setCPUSkinning					(bool _bCPUSkinning) { m_bDrawCPU = _bCPUSkinning; }

	void	createGLBuffers					();

	virtual void	draw					();

	void	translateScaleRotateVertices	(const V3D& _offset, const double _scale, const Quaternion& _q);
	
	int		numBones						() { return m_skinningMatrices.size(); }
	void	updateSkinningMatrices			(const TransformsContainer& _boneTransforms);


	///< Be careful, this normally does not need to be used (with fbx skeletons...)
	TransformsContainer& getBindMatrices() { return m_invBindMatrices; }

	/// Old:
	void	writeVerticesToFile				(const char* filename);
	void	readVerticesFromFile			(const char* filename);




};
