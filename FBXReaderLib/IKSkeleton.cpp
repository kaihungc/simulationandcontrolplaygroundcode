
#include <FBXReaderLib/IKSkeleton.hpp>
#include <OptimizationLib\GradientDescentFunctionMinimizer.h>
#include <OptimizationLib\BFGSFunctionMinimizer.h>

#include <FBXReaderLib/SubspaceSkeleton.hpp>

///<
IKSkeleton::IKSkeleton(const Skeleton& _sk) : m_pSubspace(NULL)
{
	copy(_sk);
	
}

///<
IKSkeleton::~IKSkeleton()
{
	delete m_pSubspace;
}

///<
void IKSkeleton::CreateSubspace(const Skeleton& _skMitMotion)
{
	delete m_pSubspace;

	m_pSubspace = new SkeletonSubspace(_skMitMotion);
	m_pSubspace->ProjectPose(*this);
	m_pSubspace->UnProjectPose(*this);
	
}

void IKSkeleton::SetSubSpacePose(const Skeleton& _sk)
{
	m_pSubspace->ProjectPose(_sk);
}

///<
void IKSkeleton::Set2DTarget(Joint* _pJoint, const Vector3d& _screenX)
{
	m_target.m_pTargetJoint = _pJoint;
	m_target.Set2DScreenTarget(_screenX);
}

///<
void IKSkeleton::Set3DTarget(Joint* _pJoint, const Vector3d& _screenX)
{
	m_target.m_pTargetJoint = _pJoint;
	m_target.Set3DTarget(_screenX);
}

void IKSkeleton::SetCOMTarget(const P3D _xCOM0)
{
	m_target.m_bIsCom = true;

	P3D xInitialCom = _xCOM0;

	m_target.Set3DTarget(xInitialCom);
}

///<
class IKObjective : public ObjectiveFunction
{
	IKSkeleton& m_IKSk;
public:

	IKObjective(IKSkeleton& _ikSk) : m_IKSk(_ikSk){}

	double computeValue(const dVector& _p)
	{
		m_IKSk.UnSerializeParams(_p);

		double wRegu = 0.02;

		///< Diff between target and other.
		return m_IKSk.TargetError() + wRegu*_p.squaredNorm();
	}
};

///<
void IKSkeleton::SerializeParams(dVector& _p) 
{
	if (m_pSubspace)
	{
		m_pSubspace->SerializeParams(_p);
		m_pSubspace->UnProjectPose(*this);
		return;
	}
	else if(m_target.m_pTargetJoint)
	{
		Joint* jItt = m_target.m_pTargetJoint;

		int numJointsToRoot = 1;
		while (jItt->m_pPrevious != NULL)
		{
			numJointsToRoot++;
			jItt = jItt->m_pPrevious;
		}

		_p.resize(3 * (numJointsToRoot));
		jItt = m_target.m_pTargetJoint;
		for (int i = 0; i < numJointsToRoot; ++i)
		{
			Vector3d angles = jItt->m_q.toAxisAngle();
			jItt = jItt->m_pPrevious;
			for (int k = 0; k < 3; ++k)
				_p[i * 3 + k] = angles[k];
		}
	}
	else
	{
		int nJoints = numJoints()-1;///< root 
		_p.resize(3 * nJoints);

		for (int i = 0; i < nJoints; ++i)
		{
			Vector3d angles = FindJoint(i+1)->m_q.toAxisAngle();
			for (int k = 0; k < 3; ++k)
				_p[i * 3 + k] = angles[k];
		}
	}
	
}

///<
void IKSkeleton::UnSerializeParams(const dVector& _p) 
{
	if (m_pSubspace)
	{
		m_pSubspace->UnSerializeParams(_p);
		m_pSubspace->UnProjectPose(*this);
		return;
	}
	else if(m_target.m_pTargetJoint)
	{
		Joint* jItt = m_target.m_pTargetJoint;

		for (int i = 0; i < _p.size() / 3; ++i)
		{
			Vector3d angles;
			for (int k = 0; k < 3; ++k)
				angles[k] = _p[i * 3 + k];

			jItt->m_q = Quaternion::ExpMap(angles);
			jItt = jItt->m_pPrevious;
		}
	}
	else ///< All dofs
	{
		for (int i = 0; i <  _p.size() / 3; ++i)
		{
			Vector3d angles;
			for (int k = 0; k < 3; ++k)
				angles[k] = _p[i * 3 + k];

			FindJoint(i+1)->m_q = Quaternion::ExpMap(angles);
		}
	}
	
}


#include <GUILib/TranslateWidget.h>
double IKSkeleton::ScreenSpacePositionError()
{
	P3D x = X(m_target.m_pTargetJoint);
	Ray ray = InteractiveWidget::getRayFromScreenCoords((*m_target.m_p2DScreenTarget)[0], (*m_target.m_p2DScreenTarget)[1]);
	return ray.getDistanceToPoint(x);
}

///<
double IKSkeleton::COMError()
{
	return (*m_target.m_p3DTarget - ComputeCOM()).norm();
}

///<
double IKSkeleton::WorldPositionError()
{
	P3D x = X(m_target.m_pTargetJoint);
	return (*m_target.m_p3DTarget - x).norm();
}

///<
double IKSkeleton::TargetError()
{
	if (m_target.m_bIsCom)
	{
		return COMError();
	}
	else
	{
		if (m_target.m_p2DScreenTarget)
			return ScreenSpacePositionError();
		else if (m_target.m_p3DTarget)
			return WorldPositionError();
	}
	return 0.0;
}

///<
double IKSkeleton::TargetError(const dVector& _p)
{
	UnSerializeParams(_p);
	return TargetError();
}

///<
void IKSkeleton::SolveIK(int _maxItts)
{
	///<
	if (m_target.m_pTargetJoint != NULL || m_target.m_bIsCom)
	{
		double resid = 0.00001;
		///< Solve IK
		double fVal = TargetError();
		if (fVal > resid)
		{
			IKObjective ik(*this);
			dVector params;

			SerializeParams(params);

			BFGSFunctionMinimizer bfgsMiner(_maxItts, resid, 15, true);
			bfgsMiner.minimize(&ik, params, fVal);

			UnSerializeParams(params);
		}
		else
		{
			m_target.m_pTargetJoint = NULL;
		}
	}
}

