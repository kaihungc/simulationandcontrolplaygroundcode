

#pragma once

#include <FBXReaderLib\Skeleton.hpp>
#include <Utils/Utils.h>

class SkeletonSubspace;

class IKSkeleton : public Skeleton
{
	struct Target
	{

		bool m_bIsCom;///< Or is function of multiple joints/dofs?

		Target() :m_pTargetJoint(NULL), m_p2DScreenTarget(NULL), m_p3DTarget(NULL), m_bIsCom(false){}
		~Target() {
			delete m_p2DScreenTarget;
			delete m_p3DTarget;
		}

		Joint*		m_pTargetJoint;

		void Set2DScreenTarget(const Vector3d& _x)
		{
			if (!m_p2DScreenTarget)
				m_p2DScreenTarget = new Vector3d();

			*m_p2DScreenTarget = _x;
		}

		void Set3DTarget(const Vector3d& _x)
		{
			if (!m_p3DTarget)
				m_p3DTarget = new Vector3d();

			*m_p3DTarget = _x;
		}

		Vector3d*	m_p2DScreenTarget=NULL;
		Vector3d*	m_p3DTarget = NULL;

	};

	Target m_target;

	SkeletonSubspace* m_pSubspace=NULL;
	///< Should hold a skeleton...

	double	ScreenSpacePositionError	();
	double	WorldPositionError			();
	double	COMError					();

public:
	void	SetSubSpacePose				(const Skeleton& _sk);
	void	CreateSubspace				(const Skeleton& _skMitMotion);



	IKSkeleton() : m_pSubspace(NULL){}
	IKSkeleton(const Skeleton& _sk);
	~IKSkeleton();
	
	void	Set2DTarget			(Joint* _pJoint, const Vector3d& _screenX);
	void	Set3DTarget			(Joint* _pJoint, const Vector3d& _x3D);
	
	void	SetCOMTarget		(const P3D _xCOM0);///All bodies/dofs concerned.

	void	SerializeParams		(dVector& _p);
	void	UnSerializeParams	(const dVector& _p);	

	double	TargetError			();
	double	TargetError			(const dVector& _p);

	void	SolveIK				(int _maxItts);

};


