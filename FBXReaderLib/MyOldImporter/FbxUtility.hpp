

#ifndef __FBX_UTILITY_HPP__
#define __FBX_UTILITY_HPP__

#include <Graphics\MeshImport.hpp>

#include <iostream>

#include <fbxsdk.h>

///<
class FbxUtility
{
public:

	static void GetVertexAttributes(FbxMesh* _pFbxMesh, FbxLayerElementNormal** _pNormals,
	FbxLayerElementVertexColor** _pColors,
	FbxLayerElementUV**	_pUVs);
	///<
	static FbxTexture* GetTexture(FbxGeometry* _pGeometry);

	///<
	static void ImportMesh(FbxNode* _pNode, MeshImport& _meshData);
	static void ImportIndices(FbxNode* _pNode, MeshImport& _meshData);

	///<
	static void DisplayUnlayeredTexture(FbxTexture* _pTexture)
	{

		if (_pTexture)  
		{

		//	std::cout << "    Texture Filename: " << _pTexture->f() << std::endl;

			std::cout << "            Scale U: " << _pTexture->GetScaleU() << std::endl;
			std::cout << "            Scale V: " <<  _pTexture->GetScaleV() << std::endl;
			std::cout << "            Translation U: " <<  _pTexture->GetTranslationU() << std::endl;
			std::cout << "            Translation V: " <<  _pTexture->GetTranslationV() << std::endl;
			std::cout << "            Swap UV: " <<  _pTexture->GetSwapUV() << std::endl;
			std::cout << "            Rotation U: " <<  _pTexture->GetRotationU() << std::endl;
			std::cout << "            Rotation V: " <<  _pTexture->GetRotationV() << std::endl;
			std::cout << "            Rotation W: " <<  _pTexture->GetRotationW() << std::endl;

			char* lAlphaSources[] = { "None", "RGB Intensity", "Black" };

			std::cout << "            Alpha Source: " <<  lAlphaSources[_pTexture->GetAlphaSource()] << std::endl;
			std::cout << "            Cropping Left: " <<  _pTexture->GetCroppingLeft() << std::endl;
			std::cout << "            Cropping Top: " <<  _pTexture->GetCroppingTop() << std::endl;
			std::cout << "            Cropping Right: " <<  _pTexture->GetCroppingRight() << std::endl;
			std::cout << "            Cropping Bottom: " <<  _pTexture->GetCroppingBottom() << std::endl;

		}
	}

	//<
	static void DisplayAllVertices(MeshImport::VertexData* _pVertices, const int32 _NumVertices)
	{
		for (int i=0; i<_NumVertices; ++i)
		{

			std::cout << "x: " << _pVertices[i]._x.x() << " " << _pVertices[i]._x.y() << " " << _pVertices[i]._x.z()
				<< ", uv: " << _pVertices[i]._uv.x() << " " << _pVertices[i]._uv.y() << std::endl;

		}
		int iDebug=0;
		std::cin >> iDebug;
	}


};




#endif