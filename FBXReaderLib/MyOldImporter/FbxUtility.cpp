

#include <Conversion/FbxConverter/FbxUtility.hpp>
#include <fstream>

///<
void FbxUtility::ImportMesh(FbxNode* _pNode, MeshImport& _meshData)
{
	ASSERT(_pNode!=NULL, "Null Node !");
	if (_pNode)
	{   
		ASSERT(_pNode->GetNodeAttribute()!=NULL, "Null Attribute !");
		FbxNodeAttribute::EType AttributeType = (_pNode->GetNodeAttribute()->GetAttributeType());

		if (AttributeType==FbxNodeAttribute::eMesh)
		{
			FbxMesh* pFbxMesh = (FbxMesh*)_pNode->GetNodeAttribute();
			std::cout << "Importing Mesh!: " << pFbxMesh->GetName() << std::endl;
			std::cout << "Number of Polygons: " << pFbxMesh->GetPolygonCount() << std::endl;
			std::cout << "Number of Vertices: " << pFbxMesh->GetControlPointsCount() << std::endl;	
			std::cout << "Number of Indices: " << pFbxMesh->GetPolygonCount()*3 << std::endl;	
			
			if(_meshData.m_pVertices!=NULL)
				std::cout << "Already Initialised Vertices! " << std::endl;	

			_meshData.m_NumIndices = pFbxMesh->GetPolygonCount()*3;
			_meshData.m_NumVertices = pFbxMesh->GetControlPointsCount();			
	
			_meshData.m_pIndices = new uint32[_meshData.m_NumIndices];
			_meshData.m_pVertices = new MeshImport::VertexData[_meshData.m_NumVertices];	

						///<  Vertex Layer:
			FbxLayerElementNormal*			pNormals	= NULL;
			FbxLayerElementVertexColor*		pColors		= NULL;
			FbxLayerElementUV*				pUVs		= NULL;

			GetVertexAttributes(pFbxMesh, &pNormals, &pColors, &pUVs);

			_meshData.m_bHasColors	= pColors!=NULL;// ? 0 : 1;
			_meshData.m_bHasUVs		= pUVs!=NULL;// ? 0 : 1;
			_meshData.m_bHasNormals = pNormals!=NULL;// ? 0 : 1;

			if (pNormals)
				std::cout << "Number of Normals: " << pNormals->GetDirectArray().GetCount() << std::endl;	
			if (pUVs)
				std::cout << "Number of UVs: " << pUVs->GetDirectArray().GetCount() << std::endl;	
			if (pColors)
			{	
				std::cout << "Number of Colors: " << pColors->GetDirectArray().GetCount() << std::endl;	
				std::cout << "Number of Colors Indices: " << pColors->GetIndexArray().GetCount() << std::endl;	
			}


			////< Copy Time!
		
			{
				int* pIndices = pFbxMesh->GetPolygonVertices();

				for (uint32 i=0; i<pFbxMesh->GetPolygonCount()*3;++i)
				{
					int32 temp = pIndices[i];
					_meshData.m_pIndices[i]=(uint32)temp;
					
					if (pColors)
					{
						int32 cId = pColors->GetIndexArray().GetAt(i);
						FbxColor c = pColors->GetDirectArray().GetAt(cId);
						_meshData.m_pVertices[ temp ]._c = Vector4f((float32)c.mRed, (float32)c.mGreen, (float32)c.mBlue, (float32)c.mAlpha);

						FbxVector4 n = pNormals->GetDirectArray().GetAt(i);
						_meshData.m_pVertices[ temp ]._n = Vector3f(n[0],n[1],n[2]); 
					}
				}
			}

			/*
			FbxTexture* pTexture = GetTexture(pFbxMesh);
			if (pTexture)
			{
				const char* tFileName = pTexture->GetFileName();
				int32 nameLength = strlen(tFileName)+1;
				_meshData.m_pTextureFileName = new char[nameLength]; 
				memset(_meshData.m_pTextureFileName,0,nameLength);
				memcpy(_meshData.m_pTextureFileName,tFileName,nameLength);
			}*/
		
			///< Copy vertices:
			{
				FbxVector4* pVertices = pFbxMesh->GetControlPoints();
				for (int i=0; i<pFbxMesh->GetControlPointsCount();++i)
				{	
					//FbxVector4 normal(0,0,0,1);
					FbxVector2 uv(0,0);				
					
					if (pUVs)
						uv = pUVs->GetDirectArray().GetAt(i);
					/*
					if (pNormals)
						normal = pNormals->GetDirectArray().GetAt(i);
						*/
					///< Dont know if this is true:
					_meshData.m_pVertices[i]._uv = Vector2f((float32)uv[0],  (1.0f-(float32)uv[1]));		
					
					for (int k=0;k<3;++k)
					{
						_meshData.m_pVertices[i]._x[k] = (float32)pFbxMesh->GetControlPointAt(i)[k];
						//_meshData.m_pVertices[i]._n[k] = (float32)normal[k];
					}

					///< Bones !				
					if (_meshData.m_bHasBones)
					{
						MeshImport::SkinData::NormalizeToMaxFourBones(_meshData.m_pImportedSkinData[i], _meshData.m_pVertices[i]);
						
					}
				
				}
			}
		}
	}

	std::cout << std::endl << "Finished";	
}

void FbxUtility::GetVertexAttributes(FbxMesh* _pFbxMesh, FbxLayerElementNormal** _ppNormals,
	FbxLayerElementVertexColor** _ppColors,
	FbxLayerElementUV**	_ppUVs)
{
	const int iLayerCount = _pFbxMesh->GetLayerCount();
	for (int l=0; l<iLayerCount; ++l)
	{
		FbxLayer* pLayer = _pFbxMesh->GetLayer(l);

		if (pLayer)
		{
			*_ppColors = pLayer->GetVertexColors();
			*_ppUVs	= pLayer->GetUVs();
			*_ppNormals= pLayer->GetNormals();

			if (*_ppColors)
			{
				FbxLayerElement::EMappingMode colorMode = (*_ppColors)->GetMappingMode();
				//ASSERT(colorMode == FbxLayerElement::eVertexColor, "I don't know what else");
				//ASSERT((*_ppColors)->GetReferenceMode() == FbxLayerElement::eINDEX_TO_DIRECT, "I don't know what else kind of mapping!");
			}

			if (*_ppNormals)
			{
				FbxLayerElement::EMappingMode mode = (*_ppNormals)->GetMappingMode();
				FbxLayerElement::EReferenceMode refMode = (*_ppNormals)->GetReferenceMode();

				//ASSERT((*_ppNormals)->GetReferenceMode() == FbxLayerElement::eNormal, "");
			}
		}
	}
}

///<
FbxTexture* FbxUtility::GetTexture(FbxGeometry* _pGeometry)
{
	/*
	int lMaterialIndex;
	FbxProperty lProperty;    
	int lNbMat = _pGeometry->GetNode()->GetSrcObjectCount(FbxSurfaceMaterial::ClassId);
	std::cout << "Num Material: " << lNbMat << std::endl;
	for (lMaterialIndex = 0; lMaterialIndex < lNbMat; lMaterialIndex++)
	{
		FbxSurfaceMaterial* pMaterial = (FbxSurfaceMaterial *)_pGeometry->GetNode()->GetSrcObject(FbxSurfaceMaterial::ClassId, lMaterialIndex);

		if(pMaterial)
		{
			int lTextureIndex;
			FOR_EACH_TEXTURE(lTextureIndex)
			{
				lProperty = pMaterial->FindProperty(FbxLayerElement::TEXTURE_CHANNEL_NAMES[lTextureIndex]);
				if (lProperty.GetSrcObjectCount(FbxTexture::ClassId)>0)//< I don't handle more then one,
				{
					int j=0;
					FbxTexture* pTexture = FbxCast <FbxTexture> (lProperty.GetSrcObject(FbxTexture::ClassId,j));
					if(pTexture)
					{ 
						return pTexture;
					}
				}				
			}

		}//end if(lMaterial)

	}// end for lMaterialIndex 
	*/
	return NULL;
}
