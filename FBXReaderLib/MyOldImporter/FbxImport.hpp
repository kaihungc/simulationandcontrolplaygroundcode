

#ifndef __FBX_IMPORT_HPP__
#define __FBX_IMPORT_HPP__


#include <Graphics/MeshImport.hpp>
#include <Conversion/FbxConverter/FbxUtility.hpp>

#include <Common/Common.hpp>

#include <string>
#include <fstream>
#include <iostream>
#include <iterator>

#include <fbxsdk.h>
#include <fbxsdk/fbxsdk_nsbegin.h>
#include <fbxsdk/fbxsdk_nsend.h>

///<
class FbxImport
{

	FbxManager*		m_pSdkManager;
	FbxScene*      m_pScene;

public:

	FbxImport(){ memset(this,0,sizeof(FbxImport)); }

	///< Loads a Scene:
	bool Init(std::string& _strFbxFileName);
	void Release();


	void	ImportMeshes		(MeshImport& _meshData);
	void	ImportBones			(MeshImport& _meshData);	

private:	

	///<
	bool ImportScene(FbxManager* _pSDKManager, FbxScene* _pScene, const char* _pFileName);

	///<
	void IttImportMesh(FbxNode* _pNode, MeshImport& _meshData);

	///<
	void GetChannels(FbxAnimLayer* _pAnimLayer, FbxNode* _pRootNode, AnimationImport::Bone* _pBone);

	///<
	void IttParseHierarchy(FbxNode* _pRootNode, MeshImport& _meshData, const char* _csRootName);

	///<
	void IttParseWeights(FbxNode* _pRootNode, MeshImport& _meshData);

	
};


#endif