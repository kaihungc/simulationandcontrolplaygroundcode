

#include <FBXReaderLib\FBXImporter.h>
#include <FBXReaderLib\FBXCharacter.h>

///<
void FBXCharacter::FillChildJoint(const FBXJoint& _FBXJoint, Skeleton::Joint& _joint, int& _id) const
{
	_joint.m_q = _FBXJoint.orientation;
	_joint.m_offset = _FBXJoint.position;
	_id++;
	_joint.m_id = _id;

	int bodyPartsChilds = _FBXJoint.childrenIDs.size();
	_joint.m_childs.resize(bodyPartsChilds);
	for (int i = 0; i < bodyPartsChilds; ++i)
	{
		int childId = _FBXJoint.childrenIDs[i];
		const BodyPart& childBP = getBodyPart(childId);
		const FBXJoint& childFBXJoint = childBP.parentJoint;

		Skeleton::Joint& childSkJ = _joint.m_childs[i];

		childSkJ.m_pPrevious = &_joint;
		FillChildJoint(childFBXJoint, childSkJ, _id);
	}	
}

///<
void FBXCharacter::CreateSkeletonHierarchy(Skeleton& _sk)
{
	_sk.m_pRoot = new Skeleton::Joint();
	_sk.m_pRoot->m_strName = std::string("root");
	_sk.m_pRoot->m_id = 0;

	int numChilds = root.childrenJoints.size();
	_sk.m_pRoot->m_childs.resize(numChilds);
	
	int id = 0;
	for (int i = 0; i < (int)_sk.m_pRoot->m_childs.size(); ++i)
	{
		_sk.m_pRoot->m_childs[i].m_pPrevious = _sk.m_pRoot;
		FillChildJoint(root.childrenJoints[i], _sk.m_pRoot->m_childs[i], id);		
	}
}

BodyPart::BodyPart(int numOfChildren, int partID)
{
	this->partID = partID;
	this->childrenIDs = std::vector<int>(numOfChildren);
	this->childrenJoints = std::vector<FBXJoint>(numOfChildren);
}

BodyPart::~BodyPart()
{
	this->childrenIDs.clear();
	this->childrenJoints.clear();
}

FBXCharacter::FBXCharacter()
{

}

FBXCharacter::FBXCharacter(FBXImporter &importer)
{
	this->links = std::vector<BodyPart>(importer.maxNumOfParts);
	this->translation = std::vector<std::vector<V3D> >(importer.numOfPoses, std::vector<V3D>(importer.maxNumOfParts));
	this->orientation = std::vector<std::vector<Quaternion> >(importer.numOfPoses, std::vector<Quaternion>(importer.maxNumOfParts));
	this->drawingBoxes = std::vector<std::vector<P3D> >(importer.maxNumOfParts, std::vector<P3D>(8));
	this->drawingLinks = std::vector<std::vector<P3D> >(importer.maxNumOfParts, std::vector<P3D>(2));
	this->agentName = importer.sceneName;

	this->globalPositions = std::vector<std::vector<V3D> >(importer.numOfPoses, std::vector<V3D>(importer.maxNumOfParts));
	this->globalOrientations = std::vector<std::vector<Quaternion> >(importer.numOfPoses, std::vector<Quaternion>(importer.maxNumOfParts));
}
FBXCharacter::~FBXCharacter()
{
	this->links.clear();
	this->translation.clear();
	this->orientation.clear();
	this->drawingBoxes.clear();
	this->drawingLinks.clear();
	this->globalPositions.clear();
}

// It returns a reference to the right BodyPart having its unique ID as input
inline BodyPart & FBXCharacter::getBodyPart(int ID)
{
	return (ID == 0) ? (this->root) : (this->links.at(ID - 1));
}

inline const BodyPart & FBXCharacter::getBodyPart(int ID) const
{
	return (ID == 0) ? (this->root) : (this->links.at(ID - 1));
}

// It computes the entire FBXCharacter
// and also manages and updates the 'currentPose' attribute
void FBXCharacter::UpdateCharacter(Options &options)
{
	if (options.computeAnimation)
	{
		if (options.defaultPosition)
			currentPose = -1;

		computeAgentBoxesRecursively(options, currentPose, 0, tGlobal, qGlobal);

		this->currentPose++;
		if (currentPose == this->numOfPoses)
			currentPose = 0;
	}

}

// It draws the FBXCharacter using the information from the 'drawingBoxes' and 'drawingLinks' vectors
void FBXCharacter::drawCharacter()
{
	for (int partID = 0; partID < this->numOfBodyParts; partID++)
		drawBodyPart(partID);
	for (int jointID = 0; jointID < this->numOfJoints; jointID++)
		drawLink(drawingLinks.at(jointID).at(0), drawingLinks.at(jointID).at(1));
}

// It calculates the global position for the entire FBXCharacter and updates the 'drawingBoxes' and 'drawingLinks' data
// It is a recursive function which starts from the partID (a BodyPart) and goes over all of the children connected to this BodyPart
// The &options parameter selects the desired setting for the animation
// tParent and qParent are used for passing the Parent global positions and orientation for the next children (it doesn't need to be initialized)
void FBXCharacter::computeAgentBoxesRecursively(Options &options, int poseNum, int partID, V3D tParent, Quaternion qParent)
{
	BodyPart &currentBodyPart = getBodyPart(partID);
	int parentID = currentBodyPart.parentID;
	int nChildrenJ = currentBodyPart.childrenJoints.size();
	Quaternion qChild = Quaternion();
	V3D        tChild;
	int jointParentID = currentBodyPart.parentJoint.jointID;
	int jointID = 0;
	V3D currentT = V3D(0, 0, 0);
	Quaternion currentQ = Quaternion();
	qParent.toUnit();
	if (poseNum >= 0)
	{
		currentQ = orientation.at(poseNum).at(jointParentID);
		if (!partID)
		{
			tParent = qParent.rotate(translation.at(poseNum).at(0));
			if (!options.globalTranslation)
				tParent.zero();
		}
	}

	qChild = qParent*(currentBodyPart.parentJoint.orientation.toUnit());
	qChild = qChild*currentQ;
	computeBox(currentBodyPart, tParent, qChild, poseNum);
	for (int j = 0; j < nChildrenJ; j++)
	{
		jointID = currentBodyPart.childrenJoints.at(j).jointID;
		currentT = (options.considerOffsets) ? translation.at(poseNum).at(jointID) : V3D(0, 0, 0);
		tChild = currentBodyPart.childrenJoints.at(j).position - currentBodyPart.parentJoint.position;
		tChild = qChild.rotate(tChild + currentT) + tParent;
		drawingLinks.at(jointID).at(0) = tParent;
		drawingLinks.at(jointID).at(1) = tChild;
		int childID = currentBodyPart.childrenIDs.at(j);
		if (childID != -1)
		{
			BodyPart &child = getBodyPart(childID);
			computeAgentBoxesRecursively(options, poseNum, childID, tChild, qChild);
		}
	}
}

// It computes the global positions for the 'drawingBoxes' data from a given &bodyPart and the current global position and orientation of its jointParent attribute
void FBXCharacter::computeBox(BodyPart &bodyPart, V3D &tParent, Quaternion &qParent, int poseNum)
{
	int partID = bodyPart.partID;
	V3D vParent = bodyPart.parentJoint.position;
	P3D p[8];
	getBoxRelative2Center(p, bodyPart, poseNum);

	P3D globalP = P3D(0.0, 0.0, 0.0);
	for (int i = 0; i < 8; i++)
	{
		p[i] = ((tParent)+qParent.rotate(V3D(p[i]) - vParent));
		p[i] = p[i] * this->scale;
		drawingBoxes.at(partID).at(i) = p[i];
		globalP += p[i];
	}

	globalPositions.at(poseNum).at(partID) = globalP / 8.0;
	globalOrientations.at(poseNum).at(partID) = qParent.toUnit();
}

// It returns the local vertexes positions of the box that fits the &bodyPart for the desired poseNum
void FBXCharacter::getBoxRelative2Center(P3D(&vertex)[8], BodyPart &bodyPart, int poseNum)
{
	int nJoints = bodyPart.childrenJoints.size();
	V3D vParent = bodyPart.parentJoint.position;
	V3D vChild;
	V3D currentT;
	V3D axis;
	int childID = 0;
	double length = bodyPart.length;//x
	double width = bodyPart.width; //y
	double height = bodyPart.height;//z
									//if (nJoints == 1) //It is a skeleton with only a Parent and a Child
									//{
									//	//currentT = translation.at(bodyPart.childrenJoints.at(0).jointID).evaluate_linear(t);
									//	vChild = bodyPart.childrenJoints.at(0).position;// +currentT;
									//	V3D  direction = vChild - vParent;
									//	V3D  n, t, axis = direction.unit();
									//	axis.getOrthogonalVectors(n, t);
									//	double size = direction.length()*0.1;
									//	n = n.unit() * size;
									//	for (int i = 0; i < 4; i++)
									//	{
									//		vertex[i]     = n.rotate(2 * i*PI / 4, axis) + vParent;
									//		vertex[i + 4] = n.rotate(2 * i*PI / 4, axis) + vChild;
									//	}
									//}
									//else
									//{
	vertex[0] = P3D(length / 2, width / 2, height / 2);
	vertex[1] = P3D(length / 2, width / 2, -height / 2);
	vertex[2] = P3D(length / 2, -width / 2, height / 2);
	vertex[3] = P3D(length / 2, -width / 2, -height / 2);
	vertex[4] = P3D(-length / 2, width / 2, height / 2);
	vertex[5] = P3D(-length / 2, width / 2, -height / 2);
	vertex[6] = P3D(-length / 2, -width / 2, height / 2);
	vertex[7] = P3D(-length / 2, -width / 2, -height / 2);
	//}

}

// It draws a BodyPart from the current 'drawingBoxes' information for the desired partID
void FBXCharacter::drawBodyPart(int partID)
{
	P3D p[8];
	for (int i = 0; i < 8; i++)
		p[i] = drawingBoxes.at(partID).at(i);

	glDisable(GL_LIGHTING);
	for (int i = 0; i < 8; i++)
	{
		for (int j = i + 1; j < 8; j++)
		{
			glBegin(GL_LINES);
			glLineWidth(3);
			glColor3d(0.0, 0.498039, 1.0);
			glVertex3d(p[i].at(0), p[i].at(1), p[i].at(2));
			glVertex3d(p[j].at(0), p[j].at(1), p[j].at(2));
			glEnd();
		}
	}
	glEnable(GL_LIGHTING);

	for (int i = 0; i < 8; i++)
	{
		for (int j = i + 1; j < 8; j++)
		{
			for (int k = i + 2; k < 8; k++)
			{
				//glEnable(GL_LIGHTING);
				glDisable(GL_TEXTURE_2D);
				glBegin(GL_TRIANGLE_STRIP);
				//glLineWidth(3);
				glColor3d(double((((this->agentID) % 100) / 100.)), double((((this->agentID) % 100) / 100.)), double((((this->agentID) % 66) / 100.)));
				//glColor3d(0.137255, 0.419608, 0.556863);
				glVertex3d(p[i].at(0), p[i].at(1), p[i].at(2));
				glVertex3d(p[j].at(0), p[j].at(1), p[j].at(2));
				glVertex3d(p[k].at(0), p[k].at(1), p[k].at(2));
				glEnd();
			}
		}
	}
}

// It draws a link which may be just a line between two joints
void FBXCharacter::drawLink(P3D &start, P3D &end)
{
	P3D a;
	P3D b;
	a = start;
	b = end;
	a *= this->scale;
	b *= this->scale;

	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glLineWidth(3);
	glColor3d(1, 0, 0);
	glVertex3d(a.at(0), a.at(1), a.at(2));
	glVertex3d(b.at(0), b.at(1), b.at(2));
	glEnd();
	glEnable(GL_LIGHTING);
}


// It returns a Quaternion having the euler angles (in degrees), being the rotation order: q = qZ*qY*qX
Quaternion FBXCharacter::getQuaternionFromEuler(double x, double y, double z)
{
	Quaternion qRefX, qRefY, qRefZ;
	qRefX.setRotationFrom((x / 180)*PI, V3D(1, 0, 0));
	qRefY.setRotationFrom((y / 180)*PI, V3D(0, 1, 0));
	qRefZ.setRotationFrom((z / 180)*PI, V3D(0, 0, 1));
	return qRefZ*qRefY*qRefX;
}

std::vector<P3D> FBXCharacter::getTranslationTrajectoryOf(int partID) {
	//BodyPart &bodyPart = getBodyPart(partID);
	std::vector<P3D> bodyPosTrajectory;

	for (int i = 0; i < currentPose; i++) {
		//P3D p = getGlobalPosRecursively(partID, i, V3D(0.0, 0.0, 0.0), Quaternion(1.0, 0.0, 0.0, 0.0));
		P3D p = P3D() + globalPositions.at(i).at(partID);
		bodyPosTrajectory.push_back(p);
	}

	return bodyPosTrajectory;
}

P3D FBXCharacter::getCurrentGlobalPositionOf(int partID) {
	return P3D() + globalPositions.at(currentPose).at(partID);
}

Quaternion FBXCharacter::getCurrentGlobalOrientationOf(int partID) {
	return globalOrientations.at(currentPose).at(partID);
}


