#pragma once

#include <fbxsdk.h>

#include <MathLib/Trajectory.h>
#include <GUILib/GLUtils.h>
#include <../GUILib/GLApplication.h>

#include <Windows.h>

#include <FBXReaderLib\FBXJoint.h>
#include <FBXReaderLib\Skeleton.hpp>
#include <FBXReaderLib/Import/ArmatureImport.hpp>
#include <FBXReaderLib\FMesh.h>

class AnimationUtils;
class FBXCharacter;
class FBXImporter;
struct BodyPart;

/**
* Options
- It contains the settings and control variables for the animation
- It also includes the setting for exporting the current animation
- The attributes my be changed by the the user through buttons on a menu, and the parameters can be passed to the FBXCharacter class
*/
struct Options
{
	//Animation options and attributes
	bool computeAnimation = true;
	bool drawAnimation = true;
	bool considerOffsets = false;
	bool defaultPosition = false;
	bool newSAH = false;
	bool cleanScene = false;
	bool playSequence = false;
	bool globalTranslation = true;
	bool resetValues = false;
	int  currentCharacter = 0;
	double mocapSamplingRate = 2.0;
	//Importing and Exporting options
	bool export2SAH = false;

	int  nImportedCharacters = 0;
};


/**
* FBXImporter
- Each FBX file is composed by a hierarchy of Nodes which can be: a Skeleton part, Mesh, Camera...
- This class is used for importing all the Skeletons and their animation information from an FBX file
- It has functions for filling up an FBXCharacter from the FBX data and also it handles all the hierarchy properly
*/
class FBXImporter
{
private:
	FbxTime		frameTime;
	FbxTime		startTime;
	FbxTime		stopTime;
	FbxTime		currentTime;
public:
	std::vector<FBXJoint > joints;

	FbxManager	       *fbxManager;
	FbxScene           *scene;
	FbxNode	           *root;

	std::string         sceneName;
	int			        numOfBodyParts = 0;
	int                 numOfJoints = 0;
	int			        maxNumOfParts = 0;
	int			        numOfPoses = 0;
	/**
	Contructors and destructor
	*/
	FBXImporter(std::string fileName, double _mocapSamplingRate = -1);
	~FBXImporter();

	/**
	Methods
	*/
	static void loadSkeleton(const char* _csFileName, Skeleton& _sk);
	///<
	void FillJoint(Skeleton::Joint& _jnt, const AnimationImport::Bone& _importedBone, int& _id);
	void createSkeleton(Skeleton& _sk, FbxNode *pNode);
	void createSkeleton(Skeleton& _sk);
	void loadMotion(Skeleton& _sk);

	void IttParseHierarchy(FbxNode* _pFbxNode, AnimationImport& _animImport, const char* _csRootName);



	void initImporterAtts(FbxImporter *fbxImporter, double _mocapSamplingRate);

	// It gets the node Type of a Node, it is being used to determine whether or not the current Node is a Skeleton
	std::string nodeTypeName(FbxNodeAttribute::EType attrType);

	// It gets the Skeleton type (This property is not being taken into account)
	std::string skeletonTypeName(FbxSkeleton::EType skeletonType);

	// It basically generates an FBXCharacter (&agent) from an already declared FBXCharacter and the Node origin from the Fbx File
	// The pNode is being set to the root Node of the FBX file, which means that it starts analyzing from the first one of the file
	// This function works recursively from the pNode going through all the other ones ignoring the ones which are not Skeleton type
	// The variables level, parentID and childK are only used for keeping the function working well in the hierarchy of the file
	int createAgent(FBXCharacter &agent, FbxNode *pNode, int level = 0, int parentID = 0, int childK = 0);





	///<
	void loadMesh(Skeleton& _sk, FMesh& _mesh, FbxNode *pNode);
	void loadMesh(Skeleton& _sk, FMesh& _mesh);

	// It gets the local tranformation data from the current pNode for the currentTime
	// The information is saved to the tChild, qChild and rChild variables
	void getNodeLocalTransform(FbxNode* pNode, FbxTime currentTime, V3D &tChild, Quaternion &qChild, V3D &rChild);

	// It gets the position information from the current pNode for all the Animation time
	// It gets and saves two kind of informations:
	// The Default Pose: in which is saved into the &joint.orientation and &joint.position
	// The Animation Information: in which are the rotations and the offsets translations related to the default pose that allow the animation
	// of the FBXCharacter over time (these information are saved directly into the FBXCharacter(translation and orientation vectors)
	void getJointPositions(FBXCharacter &agent, FBXJoint &joint, FbxNode* pNode);

	// Once there is the this->joints information, it is possbile to create a BodyPart with the right orientation
	// and positions for all the joints because it is necessary to make all of them displaced in way that their local position informations are related to
	// the center of the box which represents the BodyPart
	void fillBodyPart(BodyPart &bodyPart, FBXJoint &joint);

	//Once there are all the BodyParts are filled with all joints informations, it is needed to create a properly hierarchy for them, assigning the correct IDs value to make them virtualy connected
	void creatingHierarchy(FBXCharacter &agent);

	// It prints all the hierarchy attributes for checking if they match with the expected hierarchy 
	void checking(FBXCharacter &agent);

private:
	///<
	void loadBoneWeights(Skeleton& _sk, FMesh& _mesh, FbxMesh* mesh);
};
