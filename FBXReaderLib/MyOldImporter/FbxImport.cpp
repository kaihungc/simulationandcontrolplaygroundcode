
#include <Conversion/FbxConverter/FbxImport.hpp>
#include <Math\Quaternion\Quaternion.hpp>

///<
void FbxImport::ImportBones(MeshImport& _meshData)
{
	FbxNode* pRootNode = m_pScene->GetRootNode();

	const char* strRootName = pRootNode->GetName();

	IttParseHierarchy(pRootNode, _meshData, strRootName);
	IttParseWeights(pRootNode, _meshData);

	/*
	const int32 iNumAnimationsStacks = m_pScene->GetSrcObjectCount<FbxAnimStack>();
	for (int32 i=0; i<iNumAnimationsStacks; ++i)
	{
		FbxAnimStack* pAnimStack = m_pScene->GetSrcObject<FbxAnimStack>(i);

		//const char* strStackName = pAnimStack->GetName();
		
	
		///<
		const int32 iNumLayers = pAnimStack->GetSrcObjectCount<FbxAnimLayer>();
		for (int32 l=0; l<iNumLayers; ++l)
		{
			FbxAnimLayer* pAnimLayer = pAnimStack->GetSrcObject<FbxAnimLayer>(l); 

			if(_pFbxNode->GetNodeAttribute())
			{
			if (_pFbxNode->GetNodeAttribute()->GetAttributeType()==FbxNodeAttribute::eSkeleton)
			{

			}
			///< Recurrent:
			
		}*/

	//	for (int32 l=0; l<iNumLayers; ++l)
		//{
			//FbxAnimLayer* pAnimLayer = pAnimStack->GetSrcObject<FbxAnimLayer>(l);

			///< Recurrent:
			
		//}
	//}
}

///<
void BuildHierarchy(FbxNode* _pFbxNode, AnimationImport::Bone* _pBone, const bool _bIsRoot)
{
	if (_pFbxNode)
	{
		if (_pFbxNode->GetChildCount()>0)
		{
			_pBone->m_childs.Create(_pFbxNode->GetChildCount());
		}

		FbxDouble3 s = _pFbxNode->LclScaling.Get();			
		FbxDouble3 t = _pFbxNode->LclTranslation.Get();
		FbxDouble3 r = _pFbxNode->LclRotation.Get();

		//ERotationOrder e =  _pFbxNode->RotationOrder.Get();
		//fbxDouble1 al = _pFbxNode->AxisLen.Get();
	//	FbxVector4 loalR = _pFbxNode->GetLocalRFromDefault();
		
		bool bIsMaya = true;
		if (bIsMaya)
		{

			FbxDouble3 rPost = _pFbxNode->PostRotation.Get();
			FbxDouble3 rPre = _pFbxNode->PreRotation.Get();
			Vector3f pre = Vector3f((float32)rPre[0], (float32)rPre[1], (float32)rPre[2]);
			Vector3f post = Vector3f((float32)rPost[0], (float32)rPost[1], (float32)rPost[2]);

			Quaternionf qPre = Quaternionf::GenRotation(pre.x(), M::xAxisd)*Quaternionf::GenRotation(pre.y(), M::yAxisd)*Quaternionf::GenRotation(pre.z(), M::zAxisd);
			Quaternionf qPost = Quaternionf::GenRotation(post.x(), M::xAxisd)*Quaternionf::GenRotation(post.y(), M::yAxisd)*Quaternionf::GenRotation(post.z(), M::zAxisd);
			_pBone->m_r = Quaternionf::LogMap(qPost*qPre);
			
			//Quaternionf hack = Quaternionf::GenRotation(-0.5f*M::Pi, M::zAxis);
			//_pBone->m_t = hack.Rotate(Vector3f((float32)t[0], (float32)t[1], (float32)t[2]));
			_pBone->m_t = Vector3f((float32)t[0], (float32)t[1], (float32)t[2]);
		}
		else
		{
			Quaternionf hack = Quaternionf::GenRotation(-0.5f*M::Pi, M::zAxis);
			_pBone->m_t = hack.Rotate(Vector3f((float32)t[0], (float32)t[1], (float32)t[2]));

			///< Rotation
			_pBone->m_r = Vector3f((float32)r[0], (float32)r[1], (float32)r[2]);
			_pBone->m_r = _pBone->m_r*(M::Pi / 180.0f);

			if (_bIsRoot)//(strcmp(_pBone->m_csName,"Root") == 0)
				_pBone->m_r = Vector3f(0);
		}
		

		//M::AffineRotation(_pBone->m_r)

		{
			_pBone->m_l=1.0f;
			FbxSkeleton* pFbxSkeleton = (FbxSkeleton*)_pFbxNode->GetNodeAttribute();
			if (pFbxSkeleton)
				_pBone->m_l = pFbxSkeleton->LimbLength.Get();	

			int z=0;
			z++;
		}
			
			
		for(int32 j=0; j<_pFbxNode->GetChildCount(); ++j)
		{					 		
			M::CopyConstChar<void>(_pFbxNode->GetChild(j)->GetName(), &(_pBone->m_childs[j].m_csName));
			BuildHierarchy(_pFbxNode->GetChild(j), &_pBone->m_childs[j], false);
		}
		
	}
}

///<
void CountBones(AnimationImport::Bone* _pBone, int32& _iCount)
{
	if (_pBone)
	{
		_iCount+=_pBone->m_childs.Size();
		for (uint32 j=0; j<_pBone->m_childs.Size(); ++j)
			CountBones(&_pBone->m_childs[j], _iCount);
	}
}

///<
void PopulateBones(AnimationImport::Bone* _pBone, DVector<AnimationImport::Bone*>& _bones, int32& _index)
{
	if (_pBone)
	{
		for (uint32 i=0; i<_pBone->m_childs.Size(); ++i)
		{
			_pBone->m_childs[i].m_id=_index;
			_bones[_pBone->m_childs[i].m_id] = &_pBone->m_childs[i];
			_index++;
			PopulateBones(&_pBone->m_childs[i], _bones, _index);
		}
	}
}

///<
void BuildArray( AnimationImport& _animData)
{
	int32 boneCount=1;
	CountBones(&_animData.m_root, boneCount);
	if (boneCount>0)
	{
		_animData.m_boneArray.Create(boneCount);

		_animData.m_root.m_id=0;

		_animData.m_boneArray[_animData.m_root.m_id]=&_animData.m_root;
		int32 index=1;
		PopulateBones(_animData.m_boneArray[0], _animData.m_boneArray, index);
	}
}

///<
void FbxImport::IttParseHierarchy(FbxNode* _pFbxNode, MeshImport& _meshData, const char* _csRootName)
{
	if(_pFbxNode->GetNodeAttribute())
	{
		if (_pFbxNode->GetNodeAttribute()->GetAttributeType()==FbxNodeAttribute::eSkeleton)
		{
			AnimationImport& _animData = *_meshData.m_pAnimationImport;
			if (!_meshData.m_bHasBones)
			{
				_meshData.m_bHasBones = true;
				///< 
				//char* lSkeletonTypes[] = {"Root", "Limb", "Limb Node", "Effector"};
				//char* type = lSkeletonTypes[pFbxSkeleton->GetSkeletonType()];
				//	GetChannels(_pAnimLayer, _pNode, &_animData.m_root);

				///< Build Hierarchy !
				const char* strName = _pFbxNode->GetName();
				M::CopyConstChar<void>(strName, &_animData.m_root.m_csName);
				BuildHierarchy(_pFbxNode, &_animData.m_root, true);

				BuildArray(_animData);

			}			
		}
	}


	for (int32 j=0; j<_pFbxNode->GetChildCount(); ++j)
	{
		IttParseHierarchy(_pFbxNode->GetChild(j), _meshData, _csRootName);
	}
	
}


///<
void FbxImport::IttParseWeights(FbxNode* _pNode, MeshImport& _meshData)
{
	const char* csNodeName = _pNode->GetName();

	if(_pNode->GetNodeAttribute())
	{
		if (_pNode->GetNodeAttribute()->GetAttributeType()==FbxNodeAttribute::eMesh)
		{

			const char* meshName = _pNode->GetName();
			FbxMesh* pMesh = (FbxMesh*)_pNode->GetNodeAttribute();
			int32 iCount = pMesh->GetDeformerCount();

			if(iCount>0)
			{
				_meshData.m_NumVertices = pMesh->GetControlPointsCount();
				_meshData.m_pImportedSkinData = new MeshImport::SkinData[_meshData.m_NumVertices ];

				FbxDeformer* pDeformer = pMesh->GetDeformer(0);
				FbxDeformer::EDeformerType defType = pDeformer->GetDeformerType();
				if (FbxDeformer::eSkin==defType)
				{
					FbxSkin *pSkin =  (FbxSkin *)pDeformer; //->GetSrcObject<FbxSkin>();
					//FbxSkin *pSkin = dynamic_cast<FbxSkin*>(pDeformer);
					int32 clusterCount = pSkin->GetClusterCount();	

					const int32 numTest = pSkin->GetControlPointIndicesCount();
					int32*		pIndices = pSkin->GetControlPointIndices();
					float64*	pWeights = pSkin->GetControlPointBlendWeights();

					for (int32 i=0; i<clusterCount; ++i)
					{
						FbxCluster *pCluster = pSkin->GetCluster(i);
						
						const char* csBoneName =  pCluster->GetLink()->GetName();							
						int32 boneIndex = _meshData.m_pAnimationImport->FindBoneIndex(csBoneName);		

						const int32 iNumAffectedVertices = pCluster->GetControlPointIndicesCount();
						for (int32 i=0; i<iNumAffectedVertices; ++i)
						{
							int32*		pIndices = pCluster->GetControlPointIndices();
							float64*	pWeights = pCluster->GetControlPointWeights();

							MeshImport::SkinData& vert = _meshData.m_pImportedSkinData[ pIndices[i] ];
								
							vert.SetBoneIndexWeight(boneIndex, pWeights[i]);
						}
						
					}
				}
			}
		}
	}	

	for(int32 j=0; j<_pNode->GetChildCount(); ++j)
	{
		IttParseWeights(_pNode->GetChild(j), _meshData);
	}
}

///<
void DisplayCurveKeys(FbxAnimCurve *pCurve)
{
	FbxTime   lKeyTime;
	float   lKeyValue;
	char    lTimeString[256];
	FbxString lOutputString;
	int     lCount;

	int lKeyCount = pCurve->KeyGetCount();

	for(lCount = 0; lCount < lKeyCount; lCount++)
	{
		lKeyValue = static_cast<float>(pCurve->KeyGetValue(lCount));
		lKeyTime  = pCurve->KeyGetTime(lCount);

		//ASSERT( (pCurve->KeyGetInterpolation(lCount)&KFCURVE_INTERPOLATION_LINEAR)==KFCURVE_INTERPOLATION_LINEAR, "Only deals with linear interpolation for now.");
	}
}
/*

///<
void CopyAnimCurve( Vector<FbxAnimCurve*,3>& _AnimCurves, DVector<KeyFrame>& _KFs)
{
	KFCurve* pFirstKFCurve = _AnimCurves[0]->GetKFCurve();
	if (pFirstKFCurve)
	{
		int32 iNbKeyFrames = pFirstKFCurve->KeyGetCount();
		_KFs.Create(iNbKeyFrames);

		
		for (int32 kf = 0; kf<iNbKeyFrames; ++kf)
		{
			ASSERT( (pFirstKFCurve->KeyGetInterpolation(kf)&KFCURVE_INTERPOLATION_LINEAR)==KFCURVE_INTERPOLATION_LINEAR, "Only deals with linear interpolation for now.");

			_KFs[kf]._t  = pFirstKFCurve->KeyGetTime(kf).GetSecondDouble();
			for (int32 c=0; c<3;++c)
			{
				_KFs[kf]._x[c] = static_cast<float32>(_AnimCurves[c]->GetKFCurve()->KeyGetValue(kf));
			}
		}
	}	
}*/

///<
void FbxImport::GetChannels(FbxAnimLayer* _pAnimLayer, FbxNode* _pNode, AnimationImport::Bone* _pBone)
{


	{
		Vector< FbxAnimCurve*, 3 > translations;

		translations[0] = _pNode->LclTranslation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		translations[1] = _pNode->LclTranslation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		translations[2] = _pNode->LclTranslation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);

		ASSERT(translations[0]!=NULL && translations[1]!=NULL && translations[2]!=NULL, "");


		KFCurve* pFirstKFCurve = translations[0]->GetKFCurve();
		if (pFirstKFCurve)
		{
			int32 iNbKeyFrames = translations[0]->KeyGetCount();
			//_KFs.Create(iNbKeyFrames);

			for (int32 kf = 0; kf<iNbKeyFrames; ++kf)
			{
				//ASSERT( (pFirstKFCurve->KeyGetInterpolation(kf)&KFCURVE_INTERPOLATION_LINEAR)==KFCURVE_INTERPOLATION_LINEAR, "Only deals with linear interpolation for now.");

				float64 sd  = translations[0]->KeyGetTime(kf).GetSecondDouble();
				Vector3f t;
				for (int32 c=0; c<3;++c)
				{
					t[c] = static_cast<float32>(translations[c]->KeyGetValue(kf));
				}
			}

		}


		/*
		
		lKeyValue = static_cast<float>(pCurve->KeyGetValue(lCount));
        lKeyTime  = pCurve->KeyGetTime(lCount);

        lOutputString = "            Key Time: ";
        lOutputString += lKeyTime.GetTimeString(lTimeString, FbxUShort(256));
        lOutputString += ".... Key Value: ";
        lOutputString += lKeyValue;
        lOutputString += " [ ";
        lOutputString += interpolation[ InterpolationFlagToIndex(pCurve->KeyGetInterpolation(lCount)) ];
        if ((pCurve->KeyGetInterpolation(lCount)&FbxAnimCurveDef::eInterpolationConstant) == FbxAnimCurveDef::eInterpolationConstant)
        {
            lOutputString += " | ";
            lOutputString += constantMode[ ConstantmodeFlagToIndex(pCurve->KeyGetConstantMode(lCount)) ];
        }
        else if ((pCurve->KeyGetInterpolation(lCount)&FbxAnimCurveDef::eInterpolationCubic) == FbxAnimCurveDef::eInterpolationCubic)
        {
            lOutputString += " | ";
            lOutputString += cubicMode[ TangentmodeFlagToIndex(pCurve->KeyGetTangentMode(lCount)) ];
            lOutputString += " | ";
			lOutputString += tangentWVMode[ TangentweightFlagToIndex(pCurve->KeyGet(lCount).GetTangentWeightMode()) ];
            lOutputString += " | ";
			lOutputString += tangentWVMode[ TangentVelocityFlagToIndex(pCurve->KeyGet(lCount).GetTangentVelocityMode()) ];
        }
        lOutputString += " ]";
        lOutputString += "\n";
        FBXSDK_printf (lOutputString);
		
		*/

		//CopyAnimCurve(translations, _pBone->m_Translations);
	}


	{
		Vector< FbxAnimCurve*, 3 > scales;

		scales[0] = _pNode->LclScaling.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		scales[1] = _pNode->LclScaling.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		scales[2] = _pNode->LclScaling.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);


		ASSERT(scales[0]!=NULL && scales[1]!=NULL && scales[2]!=NULL, "");

		KFCurve* pFirstKFCurve = scales[0]->GetKFCurve();
		if (pFirstKFCurve)
		{
			int32 iNbKeyFrames = scales[0]->KeyGetCount();
			//_KFs.Create(iNbKeyFrames);

			for (int32 kf = 0; kf<iNbKeyFrames; ++kf)
			{

				float64 sd  = scales[0]->KeyGetTime(kf).GetSecondDouble();
				Vector3f s;
				for (int32 c=0; c<3;++c)
				{
					s[c] = static_cast<float32>(scales[c]->KeyGetValue(kf));
				}
			}

		}


		//CopyAnimCurve(scales, _pBone->m_Scales);
	}
	

	{
		Vector< FbxAnimCurve*, 3 > rotations;

		rotations[0] = _pNode->LclRotation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		rotations[1] = _pNode->LclRotation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		rotations[2] = _pNode->LclRotation.GetCurve(_pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);

		ASSERT(rotations[0]!=NULL && rotations[1]!=NULL && rotations[2]!=NULL, "");

		KFCurve* pFirstKFCurve = rotations[0]->GetKFCurve();
		if (pFirstKFCurve)
		{
			int32 iNbKeyFrames = rotations[0]->KeyGetCount();
			//_KFs.Create(iNbKeyFrames);

			for (int32 kf = 0; kf<iNbKeyFrames; ++kf)
			{

				float64 sd  = rotations[0]->KeyGetTime(kf).GetSecondDouble();
				Vector3f r;
				for (int32 c=0; c<3;++c)
				{
					r[c] = static_cast<float32>(rotations[c]->KeyGetValue(kf));
				}
			}

		}

	}
}


///<
void FbxImport::ImportMeshes(MeshImport& _meshData)
{	
	FbxNode* pRootNode = m_pScene->GetRootNode();
	for (int32 i=0; i<pRootNode->GetChildCount(); ++i)
	{
		if (pRootNode->GetChild(i))
			IttImportMesh(pRootNode->GetChild(i), _meshData);
	}			
}

///<
bool FbxImport::Init(std::string& _strFbxFileName)
{
	if (!m_pSdkManager)
		Release();

	m_pSdkManager = FbxManager::Create();
	ASSERT(m_pSdkManager!=NULL, "Failed Creating SDK !");

	int32 lSDKMajor,  lSDKMinor,  lSDKRevision;        
	FbxManager::GetFileFormatVersion(lSDKMajor, lSDKMinor, lSDKRevision);
	std::cout << "SDK Version : " << lSDKMajor << ". " << lSDKMinor << ". " << lSDKRevision << ". \n";

	FbxIOSettings* pIos = FbxIOSettings::Create(m_pSdkManager, IOSROOT);
	m_pSdkManager->SetIOSettings(pIos);

	m_pScene = FbxScene::Create(m_pSdkManager,"");

	///< Import Scene
	return ImportScene(m_pSdkManager,m_pScene,_strFbxFileName.c_str());
	
}

void FbxImport::Release()
{
	if (m_pSdkManager) 
	{
		m_pSdkManager->Destroy();
		m_pSdkManager = NULL;
		m_pScene=NULL;
	}
}

///<
bool FbxImport::ImportScene(FbxManager* _pSDKManager, FbxScene* _pScene, const char* _pFileName)
{
	bool bImport=false;
	FbxImporter* pImporter = FbxImporter::Create(_pSDKManager,"");

	if (pImporter)
	{
		const bool bImportStatus = pImporter->Initialize(_pFileName, -1, _pSDKManager->GetIOSettings());
		int32 lFileMajor,  lFileMinor,  lFileRevision;
		pImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);
		std::cout << "File SDK Version : " << lFileMajor << ". " << lFileMinor << ". " << lFileRevision << ". \n";
		ASSERT(bImportStatus, "Failed Initialise with given file ");

		if (!bImportStatus)
		{
			//std::cout << "Call to FbxImporter::Initialize() failed.\n" << "Error returned: " << pImporter->GetGetLastErrorString() << "\n\n";
			return false;
		}

		bImport = pImporter->Import(_pScene);
		ASSERT(bImport, "Error importing Scene !");

		pImporter->Destroy();
		pImporter=NULL;
	}

	return bImport;
}


///< Maybe a collection of meshes?
void FbxImport::IttImportMesh(FbxNode* _pNode, MeshImport& _meshData)
{
	//ASSERT(_pNode->GetNodeAttribute()!=NULL, "null node attribute");

	if(_pNode->GetNodeAttribute())
	{
		FbxNodeAttribute::EType AttributeType = (_pNode->GetNodeAttribute()->GetAttributeType());

		if (AttributeType==FbxNodeAttribute::eMesh)
		{
			FbxUtility::ImportMesh(_pNode, _meshData);
		}

		for (int32 i=0; i<_pNode->GetChildCount(); ++i)
		{
			IttImportMesh(_pNode->GetChild(i), _meshData);
		}
	}	
}






