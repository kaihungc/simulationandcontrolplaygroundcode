#include "MotorOptPlan.h"



MotorOptPlan::MotorOptPlan()
{
	
}


MotorOptPlan::~MotorOptPlan()
{
	
}

void MotorOptPlan::initialize(vector<RMCRobot*>& rmcRobots)
{
	livingConnectors.clear();
	livingMotors.clear();
	movableMotors.clear();
	defaultOrientations.clear();
	rotationAxes.clear();
	livingIndexMap.clear();
	movableIndexMap.clear();

	for (auto rmcRobot : rmcRobots)
	{
		for (int i = 0; i < rmcRobot->getRMCCount(); i++)
		{
			RMC* rmc = rmcRobot->getRMC(i);
			if (rmc->type == LIVING_MOTOR)
			{
				LivingMotor* livingMotor = dynamic_cast<LivingMotor*>(rmc);
				livingIndexMap[livingMotor] = (int)livingMotors.size();
				livingMotors.push_back(livingMotor);

				if (livingMotor->isMovable())
				{
					movableIndexMap[livingMotor] = (int)movableMotors.size();
					movableMotors.push_back(livingMotor);
					defaultOrientations.push_back(livingMotor->state.orientation);
					rotationAxes.push_back(livingMotor->getWorldCoordinates(livingMotor->motorAxis));
				}
			}
			else if (rmc->type == LIVING_CONNECTOR)
			{
				LivingConnector* livingConnector = dynamic_cast<LivingConnector*>(rmc);
				if (livingConnector->isFullyConnected())
					livingConnectors.push_back(livingConnector);
			}
		}
	}

	getParamCountAndStartIndice();
}

void MotorOptPlan::getParamCountAndStartIndice()
{
	int curStartIndex = 0;

	if (optimizeMotorEmbedding)
	{
		motorEmbeddingStartIndex = curStartIndex;
		curStartIndex += (int)movableMotors.size();
	}
	else {
		motorEmbeddingStartIndex = -1;
	}

	if (optimizeMotorBracket)
	{
		bracketParamStartIndex = curStartIndex;
		curStartIndex += (int)livingMotors.size() * 2;
	}
	else {
		bracketParamStartIndex = -1;
	}

	totalParamNum = curStartIndex;
}

void MotorOptPlan::setParamsFromList(const dVector& p)
{
	if (optimizeMotorEmbedding)
	{
		for (uint i = 0; i < movableMotors.size(); i++)
		{
			LivingMotor* livingMotor = movableMotors[i];
			livingMotor->state.orientation = getRotationQuaternion(p[motorEmbeddingStartIndex + i], rotationAxes[i]) * defaultOrientations[i];
		}
	}
	
	if (optimizeMotorBracket)
	{
		for (uint i = 0; i < livingMotors.size(); i++)
		{
			LivingHornBracket* bracket = livingMotors[i]->bracket;
			bracket->bracketInitialAngle = p[bracketParamStartIndex + 2 * i];
			bracket->bracketConnectorAngle = p[bracketParamStartIndex + 2 * i + 1];
			livingMotors[i]->update();
		}
	}
}

void MotorOptPlan::writeParamsToList(dVector& p)
{
	p.resize(totalParamNum);

	if (optimizeMotorEmbedding)
	{
		for (uint i = 0; i < movableMotors.size(); i++)
		{
			LivingMotor* livingMotor = movableMotors[i];
			Quaternion embedQ = livingMotor->state.orientation * defaultOrientations[i].getInverse();
			p[motorEmbeddingStartIndex + i] = embedQ.getRotationAngle(rotationAxes[i]);
		}
	}
	
	if (optimizeMotorBracket)
	{
		for (uint i = 0; i < livingMotors.size(); i++)
		{
			LivingHornBracket* bracket = livingMotors[i]->bracket;
			p[bracketParamStartIndex + 2 * i] = bracket->bracketInitialAngle;
			p[bracketParamStartIndex + 2 * i + 1] = bracket->bracketConnectorAngle;
		}
	}
}
