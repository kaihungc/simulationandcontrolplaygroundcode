#pragma once

#include <GUILib/GLApplication.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLTrackingCamera.h>
#include <RobotDesignerLib/LocomotionEngineManager.h>
#include "MotionGraph.h"

struct AnimationParams
{
	double f;
	int animationCycle;
	bool drawRobotPose;
	bool drawPlanDetails;
	bool drawContactForces;
	bool drawOrientation;
};


class MOPTWindow : public GLWindow3D
{
public:
	bool initialized = false;
	GLApplication* glApp;
	TwBar* mainMenuBar;
	AnimationParams anmParams;

	Robot* robot = NULL;
	ReducedRobotState startState = ReducedRobotState(13);

	FootFallPattern footFallPattern;
	FootFallPattern footFallPatternStart;
	FootFallPattern footFallPatternEnd;
	FootFallPatternViewer* ffpViewer = NULL;
	FootFallPatternViewer* start_ffpViewer = NULL;
	FootFallPatternViewer* end_ffpViewer = NULL;

	LocomotionEngineManager* locomotionManager = NULL;
	LocomotionEngineManager* locomotionManagerStart = NULL;
	LocomotionEngineManager* locomotionManagerEnd = NULL;

	enum OPT_OPTIONS {
		GRF_OPT = 0,
		GRF_OPT_V2,
		GRF_OPT_V3,
		IP_OPT,
		IP_OPT_V2
	};
	int optimizeOption = GRF_OPT_V3;

	int nPoints;
	double motionPlanTime = 0.8;

public:
	MOPTWindow(int x, int y, int w, int h, GLApplication* glApp);
	~MOPTWindow();

	void clear();
	void loadRobot(Robot* robot, ReducedRobotState* startState);
	void loadParametersForMotionPlan();
	void unloadParametersForMotionPlan();

	LocomotionEngineManager* initializeNewMP(bool doWarmStart = true);
	LocomotionEngineManager* initializeNewTransitionMP();
	double runMOPTStep();
	void syncWithMotionGraphNode(MotionGraphNode* node);
	void syncWithMotionGraphEdge(MotionGraphEdge* edge);
	void prepareForTransitionMOPT(MotionGraphNode* startNode, MotionGraphNode* endNode);
	void createTransitionFFP();
	void reset();

	void setAnimationParams(double f, int animationCycle);

	void saveToFile(FILE* fp);
	void loadFromFile(FILE* fp);
	void loadFFPFromFile(const char* fName);

	virtual void drawScene();
	virtual void drawAuxiliarySceneInfo();
	virtual void setupLights();

	virtual bool onMouseMoveEvent(double xPos, double yPos);
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);

	virtual void setViewportParameters(int posX, int posY, int sizeX, int sizeY);
};