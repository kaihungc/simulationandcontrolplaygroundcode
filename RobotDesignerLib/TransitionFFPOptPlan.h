#pragma once
#include "FootFallPattern.h"

using namespace std;

struct ContStepPattern {
	double startIndex = -1.0;
	double endIndex = -1.0;
};

class TransitionFFPOptPlan
{
public:
	FootFallPattern* transFFP = NULL;
	FootFallPattern* startFFP = NULL;
	FootFallPattern* endFFP = NULL;
	int transStartIndex = -1;
	int transEndIndex = -1;

	int totalParamNum = 0;

	vector<ContStepPattern> startStepPatterns;
	vector<ContStepPattern> endStepPatterns;
	vector<ContStepPattern> planStepPatterns;

public:
	TransitionFFPOptPlan(FootFallPattern* transFFP, FootFallPattern* startFFP, FootFallPattern* endFFP,
		int transStartIndex, int transEndIndex);
	~TransitionFFPOptPlan();

	void setParamsFromList(const dVector& p);
	void writeParamsToList(dVector& p);
};

