#include "PossionDesignSampler.h"


PossionDesignSampler::PossionDesignSampler(ModularDesignWindow* modularDesign, LivingBracketParametrizedDesign* livingDesign)
	: LivingDesignSampler(modularDesign, livingDesign)
{

}

PossionDesignSampler::~PossionDesignSampler()
{
}

void PossionDesignSampler::generateDesignSamples(vector<dVector>& designs)
{
	candidateDesigns.clear();
	designQueue = {};

	// push the default design as seed
	candidateDesigns.push_back(livingDesign->defaultParams);
	designQueue.push(livingDesign->defaultParams);

	while (!designQueue.empty() && candidateDesigns.size() <= 100)
	{
		dVector curDesign = designQueue.front();
		designQueue.pop();

		// generateSingleElementVariationSamples(curDesign);
		generateMultipleElementVariationSamples(curDesign, 50);
	}

	designs.insert(designs.end(), candidateDesigns.begin(), candidateDesigns.end());
}

void PossionDesignSampler::generateSingleElementVariationSamples(dVector& curDesign)
{
	auto& mirrorMap = modularDesign->rmcMirrorMap;

	// position based variation
	for (auto rmc : livingDesign->movableRMCs)
	{
		livingDesign->setDesignParams(curDesign);
		if (mirrorMap.count(rmc))
			rmc->state.position += V3D((randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.3;
		else
			rmc->state.position += V3D(0.0, (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.3;
		
		dVector newDesign = livingDesign->getCurrentDesignParams();

		if (checkDesignNovelty(newDesign) && checkDesignValid(newDesign))
		{
			candidateDesigns.push_back(newDesign);
			designQueue.push(newDesign);
		}
	}

	// orientation based variation
	/*for (auto rmc : livingDesign->movableRMCs)
	{
		livingDesign->setDesignParams(curDesign);
		rmc->state.orientation = getRotationQuaternion(PI * randNumberIn01Range(), getRandomUnitVector()) * rmc->state.orientation;

		dVector newDesign = livingDesign->getCurrentDesignParams();

		if (checkDesignNovelty(newDesign) && checkDesignValid(newDesign))
		{
			candidateDesigns.push_back(newDesign);
			designQueue.push(newDesign);
		}
	}*/
}

void PossionDesignSampler::generateMultipleElementVariationSamples(dVector& curDesign, int sampleN)
{
	auto& mirrorMap = modularDesign->rmcMirrorMap;
	auto& fpMirrorMap = modularDesign->bodyFpMirrorMap;

	for (int i = 0; i < sampleN; i++)
	{
		livingDesign->setDesignParams(curDesign);
		for (auto rmc : livingDesign->movableRMCs)
		{
			if (mirrorMap.count(rmc))
				rmc->state.position += V3D((randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.1;
			else
				rmc->state.position += V3D(0.0, (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.1;
		}

		for (auto fp : livingDesign->bodyFeaturePts)
		{
			if (fpMirrorMap.count(fp))
				fp->coords += V3D((randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.1;
			else
				fp->coords += V3D(0.0, (randNumberIn01Range() - 0.5), (randNumberIn01Range() - 0.5)) * 0.1;
		}

		dVector newDesign = livingDesign->getCurrentDesignParams();

		if (checkDesignNovelty(newDesign) && checkDesignValid(newDesign))
		{
			candidateDesigns.push_back(newDesign);
			designQueue.push(newDesign);
		}
	}
}

bool PossionDesignSampler::checkDesignValid(dVector& design)
{
	auto& movableRMCs = livingDesign->movableRMCs;
	auto& connectors = livingDesign->connectors;
	auto& mirrorMap = modularDesign->rmcMirrorMap;

	// symmetry constraint -- stay on one side
	if (livingDesign->useSymmParams) {
		for (auto rmc : movableRMCs)
		{
			if (mirrorMap.count(rmc) && rmc->state.position[0] < 0.02)
				return false;
		}
	}

	// connector no penetration constraint
	for (auto connector : connectors)
	{
		RMCPin* pin1 = connector->pins[0].getConnectedPin();
		RMCPin* pin2 = connector->pins[1].getConnectedPin();
		PinFace& face1 = pin1->face;
		PinFace& face2 = pin2->face;
		RMC* rmc1 = pin1->rmc;
		RMC* rmc2 = pin2->rmc;

		V3D centerVec = (rmc2->getWorldCoordinates(face2.center) - rmc1->getWorldCoordinates(face1.center)).normalized();
		if (rmc2->getWorldCoordinates(face2.normal).dot(centerVec) >= 0
			|| rmc1->getWorldCoordinates(face1.normal).dot(-centerVec) >= 0) {
			return false;
		}	
	}

	return true;
}

bool PossionDesignSampler::checkDesignNovelty(dVector& design)
{
	for (auto& existDesign : candidateDesigns)
	{
		double dist = (existDesign - design).norm();
		if (dist < 0.05) {
			return false;
		}
	}

	return true;
}
