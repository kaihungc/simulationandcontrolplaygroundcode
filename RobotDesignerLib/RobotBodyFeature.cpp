#include "RobotBodyFeature.h"
#include "RobotBodyPart.h"

void BaseRobotBodyFeature::setPosition(const P3D& newPos, bool propagateChangesDownStream) {
	positionWorld = newPos;
	parent->propagateChangesToMirroredBody();
}

void BaseRobotBodyFeature::rotateAboutPivotPoint(const Quaternion& rot, const P3D& pos) {
	V3D offset(pos, positionWorld);
	positionWorld = pos + rot * offset;
	parent->propagateChangesToMirroredBody();
}

RobotJointFeature::RobotJointFeature(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis) {
	this->parent = parent; this->child = child; this->jointAxis = jointAxis;
	this->positionWorld = worldPos;

	motorMeshParent = motorMeshChild = childSupportMesh = parentSupportMesh = NULL;

	motorAngleMin = -90;
	motorAngleMax = 90;
}

V3D RobotJointFeature::getLocalCoordinatesRotationAxis() {
	return V3D(0,0,1);
}

P3D RobotJointFeature::getWorldLocationForPointOnMotorParentSide(const P3D& p) {
	return positionWorld + getOrientation() * V3D(p);
}

V3D RobotJointFeature::getWorldLocationForVectorOnMotorParentSide(const V3D& v) {
	return getOrientation() * v;
}

P3D RobotJointFeature::getWorldLocationForPointOnMotorChildSide(const P3D& p) {
	return positionWorld + getOrientation() * getRotationQuaternion(getMotorChildFrameEmbeddingAngle(), getLocalCoordinatesRotationAxis()) * V3D(p);
}

V3D RobotJointFeature::getWorldLocationForVectorOnMotorChildSide(const V3D& v) {
	return getOrientation() * getRotationQuaternion(getMotorChildFrameEmbeddingAngle(), getLocalCoordinatesRotationAxis()) * v;
}

double RobotJointFeature::getMotorEmbeddingAngle() {
	if (autoGenerateEmbeddingAngle) {
		//all right... we know where this joint is, and we know (more or less) where the features that it will connect to are... Based on this info, figure out the embedding parameters...
		V3D dir(positionWorld, child->getJointAttachmentPointAsParent());
		dir = getRotationAxisThatAlignsVectors(getLocalCoordinatesRotationAxis(), jointAxis).inverseRotate(dir);
		dir.setComponentAlong(getLocalCoordinatesRotationAxis(), 0);

		if (dir.length() > 0)
			embeddingAngle = -dir.angleWith(Vector3d(0, 1, 0), getLocalCoordinatesRotationAxis());
		else
			embeddingAngle = 0;
	}
	return embeddingAngle;
}

//and this one is similar, but changes the "rest" (i.e. zero) angle between the motor and child embedding
double RobotJointFeature::getMotorChildFrameEmbeddingAngle() {
	//to minimize probability of collisions taking place, make sure the motor's child frame is by default right in the middle of its range of motion
	return -(motorAngleMax + motorAngleMin) / 2;
}

void RobotJointFeature::draw() {
	V3D baseColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0);
	if (selected) baseColor = V3D(80 / 255.0, 36 / 255.0, 1 / 255.0);
	if (highlighted) baseColor *= 0.5;

	if (parentSupportMesh) parentSupportMesh->getMaterial().setColor(baseColor[0], baseColor[1], baseColor[2], 1);
	if (childSupportMesh) childSupportMesh->getMaterial().setColor(baseColor[0], baseColor[1], baseColor[2], 1);

	glPushMatrix();
	glTranslated(positionWorld[0], positionWorld[1], positionWorld[2]);
	double rotAngle = 0;
	V3D rotAxis;

	getOrientation().getAxisAngle(rotAxis, rotAngle);
	glRotated(DEG(rotAngle), rotAxis[0], rotAxis[1], rotAxis[2]);

	glPushMatrix();
//	glRotated(DEG(getMotorEmbeddingAngle()), getLocalCoordinatesRotationAxis()[0], getLocalCoordinatesRotationAxis()[1], getLocalCoordinatesRotationAxis()[2]);

//	drawArrow(P3D(), P3D() + getLocalCoordinatesRotationAxis() * 0.1, 0.01);

	glDisable(GL_LIGHTING);

	P3D p = P3D() + getLocalCoordinatesRotationAxis() * 0.05;

	glEnable(GL_LIGHTING);

	if (parentSupportMesh)
		parentSupportMesh->drawMesh();

	if (motorMeshParent)
		motorMeshParent->drawMesh();

	glPushMatrix();

	if (motorMeshChild)
		motorMeshChild->drawMesh();

	glRotated(DEG(getMotorChildFrameEmbeddingAngle()), getLocalCoordinatesRotationAxis()[0], getLocalCoordinatesRotationAxis()[1], getLocalCoordinatesRotationAxis()[2]);

	if (childSupportMesh)
		childSupportMesh->drawMesh();

	glPopMatrix();

	glPopMatrix();
	glPopMatrix();

	glDisable(GL_LIGHTING);
}

Quaternion RobotJointFeature::getOrientation() {
	return getRotationAxisThatAlignsVectors(getLocalCoordinatesRotationAxis(), jointAxis) * getRotationQuaternion(getMotorEmbeddingAngle(), getLocalCoordinatesRotationAxis());
}

void RobotJointFeature::setPosition(const P3D& newPos, bool propagateChangesDownStream) {
	V3D offset(positionWorld, newPos);
	BaseRobotBodyFeature::setPosition(newPos, propagateChangesDownStream);

	if (propagateChangesDownStream) {
		DynamicArray<BaseRobotBodyFeature*> childFeatures;
		child->addBodyFeaturesToList(childFeatures);
		for (uint i = 0; i < childFeatures.size(); i++) {
			if (childFeatures[i] != this)
				childFeatures[i]->setPosition(childFeatures[i]->positionWorld + offset, propagateChangesDownStream);
		}
		for (uint i = 0; i < child->cJoints.size(); i++)
			child->cJoints[i]->setPosition(child->cJoints[i]->positionWorld + offset, propagateChangesDownStream);
	}
	parent->propagateChangesToMirroredBody();
	child->propagateChangesToMirroredBody();
}

void RobotJointFeature::setJointAxis(const V3D& newJointAxis, bool propagateChangesDownStream, bool isFirstJointInChain) {
	Quaternion qOffset = getRotationAxisThatAlignsVectors(jointAxis, newJointAxis);
	jointAxis = newJointAxis;
	if (propagateChangesDownStream) {
		if (isFirstJointInChain)
			rotateAboutPivotPoint(qOffset, positionWorld);
		for (uint i = 0; i < child->cJoints.size(); i++)
			child->cJoints[i]->setJointAxis(qOffset * child->cJoints[i]->jointAxis, propagateChangesDownStream, false);
	}
	parent->propagateChangesToMirroredBody();
	child->propagateChangesToMirroredBody();
}

void RobotJointFeature::rotateAboutPivotPoint(const Quaternion& rot, const P3D& pos) {
	BaseRobotBodyFeature::rotateAboutPivotPoint(rot, pos);
	DynamicArray<BaseRobotBodyFeature*> childFeatures;
	child->addBodyFeaturesToList(childFeatures);
	for (uint i = 0; i < childFeatures.size(); i++) {
		if (childFeatures[i] != this)
			childFeatures[i]->rotateAboutPivotPoint(rot, pos);
	}
	for (uint i = 0; i < child->cJoints.size(); i++)
		child->cJoints[i]->rotateAboutPivotPoint(rot, pos);
}


