#pragma once
#include "RobotDesign.h"
#include "RobotBodyFeature.h"
#include "BodyPartPointFeature.h"
#include "EndEffectorFeature.h"
#include "Motor_MX28.h"
#include <vector>
#include <RBSimLib/CollisionDetectionPrimitive.h>
#include <RBSimLib/RigidBody.h>
#include <MathLib/boundingBox.h>

class StructureFeature;

/**
	RobotBodyPart is used to represent the main body parts of a robot design. Every RobotBodyPart has one center (of mass), several joints 
	(one of which is the parent, except for the root) and several feature points. A RobotBodyPart coresponds to a node in the geometry 
	tree structure.
*/
class RobotBodyPart{
public:
	//we design the robot in an "all zero" pose. Consequently, we do not need to explicitly keep track of positions/orientations. Rather, these are inferred from the various features that belong to each body part.
//	P3D position;
//	Quaternion orientation;

	// optionally, each robot body part can be a mirrored version of another
	RobotBodyPart *mirroredBodyPart = NULL;

	//we need to have access to some parameters here to be able to generate geometry/inertial parameters
	double massDensity = 1500.0; //300;
	//this is the radius of the body part if it's something like a tubuar structure (e.g. leg)...
	double partRadius = 0.025;

	string name;

	bool highlighted = false;
	bool selected = false;

	//define the shape of this body part
	DynamicArray<RobotBodyFeature*> bodyFeatures;
	DynamicArray<RobotJointFeature*> cJoints;
	RobotJointFeature* pJoint = NULL;
	//these features define the end effectors - each body part can have one or more of those
	DynamicArray<RobotEndEffectorFeature*> EEFeatures;

public:
    RobotBodyPart();
    ~RobotBodyPart();

	void addBodyFeaturesToList(DynamicArray<BaseRobotBodyFeature*>& fList);

	//the position of the body part is infered from the various features that the body part has
	P3D getPosition();

	AxisAlignedBoundingBox getBoundingBox();

	void addJointToBodyPart_MX28(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis);
	void addJointToBodyPart_XL320(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis);
	void addJointToBodyPart_X5(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis);
	void addJointToBodyPart_XM430(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis);



	void addFeaturePoint(const P3D& pos, double featureSize = 0.05);
	void addEEFeaturePoint(const P3D& pos, double featureSize = 0.05);

	/**
		We need to impose an ordering on how the structural parts of the body are formed. The assumption is that we connect things like so:
		COMPosition/pJoint -> (bFeature 1 -> ... bFeature n -> cJoint i)/endEffector. The functions below tell us where the connection point is for a joint
		which is either the parent of a body (so we want to know what it will connect to) or the child (so we want to know what connects to it).
	*/
	void getStructureFeatures(DynamicArray<StructureFeature*>& structureFeatures);
	P3D getJointAttachmentPointAsParent();
	P3D getJointAttachmentPointAsChild();

	P3D getLocalCoordinates(const P3D& p);
	V3D getLocalCoordinates(const V3D& v);

    // return true if this body part contains the feature, false otherwise
    bool containsFeature(BaseRobotBodyFeature* queryFeature);

	void drawSymPair();
	void drawBodyParts();
	void drawFeatures();

	void populateRB(RigidBody* rb);

	void propagateChangesToMirroredBody();

	double getMassForSegment(const P3D &a, const P3D &b);
	double getMassForBox(const P3D& minPoint, const P3D& maxPoint);
	Matrix3x3 getMOIForSegment(const P3D &a, const P3D &b);
	Matrix3x3 getMOIForBox(const P3D& minPoint, const P3D& maxPoint);

	//returns the distance from the ray's origin if the ray hits the body part, or -1 otherwise...
	double getDistanceToRayOriginIfHit(const Ray& ray);

};

