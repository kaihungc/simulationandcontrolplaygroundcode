#include "MotorOptEnergyFunction.h"
#include "ConnectorAlignObjective.h"


MotorOptEnergyFunction::MotorOptEnergyFunction(MotorOptPlan* plan)
{
	this->plan = plan;
	regularizer = 1e-6;

	setupSubObjectives();
}


MotorOptEnergyFunction::~MotorOptEnergyFunction()
{
	for (uint i = 0; i < objectives.size(); i++)
		delete objectives[i];
}

double MotorOptEnergyFunction::computeValue(const dVector& p)
{
	plan->setParamsFromList(p);

	double totalVal = 0;

	//add the regularizer contribution
	if (regularizer > 0) {
		if (m_p0.size() != p.size()) m_p0 = p;
		dVector tmpVec = p - m_p0;
		totalVal += 0.5*regularizer*tmpVec.dot(tmpVec);
	}

	for (uint i = 0; i < objectives.size(); i++)
		totalVal += objectives[i]->computeValue(p);

	return totalVal;
}

void MotorOptEnergyFunction::setCurrentBestSolution(const dVector& p)
{
	plan->setParamsFromList(p);
	m_p0 = p;
}

double MotorOptEnergyFunction::printValue(const dVector& p)
{
	plan->setParamsFromList(p);

	Logger::consolePrint("-------------------------------\n");
	Logger::logPrint("-------------------------------\n");

	double totalVal = 0;
	for (uint i = 0; i < objectives.size(); i++) {
		double w = objectives[i]->weight;
		double v = objectives[i]->computeValue(p);
		totalVal += v;
		Logger::consolePrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
		Logger::logPrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
	}

	Logger::consolePrint("=====> total cost: %lf\n", totalVal);
	Logger::logPrint("=====> total cost: %lf\n", totalVal);

	return totalVal;
}

void MotorOptEnergyFunction::setupSubObjectives()
{
	for (uint i = 0; i < objectives.size(); i++)
		delete objectives[i];
	objectives.clear();

	objectives.push_back(new ConnectorAlignObjective(plan, "Connector Align Objective", 1));
}