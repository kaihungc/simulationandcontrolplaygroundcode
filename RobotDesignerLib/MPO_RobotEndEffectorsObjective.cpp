#include "MPO_RobotEndEffectorsObjective.h"
#include <omp.h>

MPO_RobotEndEffectorsObjective::MPO_RobotEndEffectorsObjective(LocomotionEngineMotionPlan* mp, const std::string& objectiveDescription, double weight) {
	theMotionPlan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

MPO_RobotEndEffectorsObjective::~MPO_RobotEndEffectorsObjective(void){
}

double MPO_RobotEndEffectorsObjective::computeValue(const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//	theMotionPlan->setMPParametersFromList(p);

	double retVal = 0;
	int nLimbs = theMotionPlan->endEffectorTrajectories.size();

	//Beichen Li: OpenMP part
#pragma omp parallel for default(shared) reduction(+:retVal)
	for (int j=0;j<theMotionPlan->nSamplePoints;j++){
		dVector q_t;
		double retSubVal = 0.0;

//		int num = omp_get_thread_num();
//#pragma omp critical
//		{
//			Logger::logPrint("Thread: %d\n", num);
//		}

		theMotionPlan->robotStateTrajectory.getQAtTimeIndex(j, q_t);
//		theMotionPlan->robotRepresentation->setQ(q_t);
//		theMotionPlan->robotRepresentation->calculateWorldCoordinatesAtTimeIndex(j);
		for (int i=0;i<nLimbs;i++){
//			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesAtTimeIndexFor(j, i));
			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesFor(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB));
			retSubVal += 0.5 * err.length2();
		}
		retVal += retSubVal;
	}

	return retVal * weight;
}

void MPO_RobotEndEffectorsObjective::addGradientTo(dVector& grad, const dVector& p) {
	//	assume the parameters of the motion plan have been set already by the collection of objective functions class
	//	theMotionPlan->setMPParametersFromList(p);

	int nLimbs = theMotionPlan->endEffectorTrajectories.size();

	//Beichen Li: OpenMP part
#pragma omp parallel for default(shared)
	for (int j=0;j<theMotionPlan->nSamplePoints;j++){
		dVector q_t;
		MatrixNxM dEndEffectordq;
		theMotionPlan->robotStateTrajectory.getQAtTimeIndex(j, q_t);
		int qSize = q_t.size();
//		theMotionPlan->robotRepresentation->setQ(q_t);
//		theMotionPlan->robotRepresentation->calculateWorldCoordinatesAtTimeIndex(j);

		for (int i=0;i<nLimbs;i++){
//			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesAtTimeIndexFor(j, i));
			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesFor(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB));

			//compute the gradient with respect to the feet locations
			if (theMotionPlan->feetPositionsParamsStartIndex >= 0){
				grad[theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 0] += -err[0]*weight;
				grad[theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 1] += -err[2]*weight;
			}

			//and now compute the gradient with respect to the robot q's
			if (theMotionPlan->robotStatesParamsStartIndex >= 0){
				theMotionPlan->robotRepresentation->compute_dpdq(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB, dEndEffectordq);
	
				//dEdee * deedq = dEdq
				for (int k=0;k<3;k++)
					for (int l=0;l<qSize;l++)
						grad[theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + l] += dEndEffectordq(k, l) * err[k] * weight;
			}
		}
	}

}

void MPO_RobotEndEffectorsObjective::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
	//	assume the parameters of the motion plan have been set already by the collection of objective functions class
	//	theMotionPlan->setMPParametersFromList(p);

	int nLimbs = theMotionPlan->endEffectorTrajectories.size();

	//Beichen Li: OpenMP part with atomic access control
#pragma omp parallel for default(shared)
	for (int j=0;j<theMotionPlan->nSamplePoints;j++){
		dVector q_t;
		MatrixNxM dEndEffectordq, ddEndEffectordq_dqi;
		DynamicArray<MTriplet> hessianParts;

		theMotionPlan->robotStateTrajectory.getQAtTimeIndex(j, q_t);
		int qSize = q_t.size();
//		theMotionPlan->robotRepresentation->setQ(q_t);
//		theMotionPlan->robotRepresentation->calculateWorldCoordinatesAtTimeIndex(j);

		for (int i=0;i<nLimbs;i++){
//			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesAtTimeIndexFor(j, i));
			V3D err(theMotionPlan->endEffectorTrajectories[i].EEPos[j], theMotionPlan->robotRepresentation->getWorldCoordinatesFor(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB));

			//compute the gradient with respect to the feet locations
			if (theMotionPlan->feetPositionsParamsStartIndex >= 0) {
				ADD_HES_ELEMENT(hessianParts, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 0, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 0, 1, weight);
				ADD_HES_ELEMENT(hessianParts, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 1, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 1, 1, weight);
			}


			//and now compute the gradient with respect to the robot q's
			if (theMotionPlan->robotStatesParamsStartIndex >= 0){
				theMotionPlan->robotRepresentation->compute_dpdq(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB, dEndEffectordq);

				for (int k=0;k<qSize;k++){
					bool hasNonZeros = theMotionPlan->robotRepresentation->compute_ddpdq_dqi(q_t, theMotionPlan->endEffectorTrajectories[i].endEffectorLocalCoords, theMotionPlan->endEffectorTrajectories[i].endEffectorRB, ddEndEffectordq_dqi, k);
					if (hasNonZeros == false) continue;
					for (int l=k;l<qSize;l++)
						for (int m=0;m<3;m++){
							double val = ddEndEffectordq_dqi(m, l) * err[m];
							ADD_HES_ELEMENT(hessianParts, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + k, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + l, val, weight);
						}
				}

				//now add the outer product of the jacobians...
				for (int k=0;k<qSize;k++){
					for (int l=k;l<qSize;l++){
						double val = 0;
						for (int m=0;m<3;m++)
							val += dEndEffectordq(m, k) * dEndEffectordq(m, l);
						ADD_HES_ELEMENT(hessianParts, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + k, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + l, val, weight);
					}
				}

				//and now the mixed derivatives
				if (theMotionPlan->feetPositionsParamsStartIndex >= 0){
					for (int k=0;k<qSize;k++){
						ADD_HES_ELEMENT(hessianParts, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 0, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + k, -dEndEffectordq(0, k), weight);
						ADD_HES_ELEMENT(hessianParts, theMotionPlan->feetPositionsParamsStartIndex + j * nLimbs * 2 + i * 2 + 1, theMotionPlan->robotStatesParamsStartIndex + j * theMotionPlan->robotStateTrajectory.nStateDim + k, -dEndEffectordq(2, k), weight);
					}
				}
			}		
		}

		#pragma omp critical
		{
			hessianEntries.insert(hessianEntries.end(), hessianParts.begin(), hessianParts.end());
		}
	}

}



