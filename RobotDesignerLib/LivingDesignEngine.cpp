#include "LivingDesignEngine.h"
#include "NaiveDesignSampler.h"
#include "PossionDesignSampler.h"



LivingDesignEngine::LivingDesignEngine(ModularDesignWindow* modularDesign)
{
	this->modularDesign = modularDesign;
	init();
}


LivingDesignEngine::~LivingDesignEngine()
{
	delete livingDesign;
	delete designSampler;
}

void LivingDesignEngine::init()
{
	delete livingDesign;
	delete designSampler;
	designDB.clear();

	livingDesign = new LivingBracketParametrizedDesign(modularDesign);
	// designSampler = new NaiveDesignSampler(modularDesign, livingDesign);
	designSampler = new PossionDesignSampler(modularDesign, livingDesign);
}

void LivingDesignEngine::generateDesignDB()
{
	designDB.clear();
	
	// use sampler to generate design variations
	designSampler->generateDesignSamples(designDB);

	// set back to original design
	livingDesign->setDesignParams(livingDesign->defaultParams);
}

void LivingDesignEngine::visualizeDesign(int index)
{
	if (index >= (int)designDB.size()) return;

	livingDesign->setDesignParams(designDB[index]);
}
