#pragma once
#include "LivingDesignSampler.h"

class NaiveDesignSampler : public LivingDesignSampler 
{
public:
	NaiveDesignSampler(ModularDesignWindow* modularDesign, LivingBracketParametrizedDesign* livingDesign);
	~NaiveDesignSampler();

	void generateDesignSamples(vector<dVector>& designs);

	// naive sample schemes
	void samplePositionBasedDesigns(vector<dVector>& designs);
	void sampleOrientationBasedDesigns(vector<dVector>& designs);
	void sampleFpBasedDesigns(vector<dVector>& designs);
};

