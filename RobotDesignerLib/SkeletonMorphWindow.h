#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <set>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <GUILib/PlotWindow.h>
#include <RobotDesignerLib/MorphologicalRobotDesign.h>
#include <FBXReaderLib/FBXImporter.h>



/**
* Robot Design and Simulation interface
*/
class SkeletonMorphWindow : public GLWindow3D {

public:
	GLApplication* glApp = NULL;
	TwBar* mainMenuBar = NULL;

	Robot* robot = NULL;
	ReducedRobotState* startState = NULL;
	ReducedRobotState* zeroState = NULL;
	AbstractRBEngine* rbEngine = NULL;
	MorphologicalRobotDesign* MRD = NULL;

	// simplified robot
	Robot* robotSimp = NULL;
	AbstractRBEngine* rbEngineSimp = NULL;
	ReducedRobotState* zeroStateSimp = NULL;

	map<RigidBody*, int> RBIndexMap;
	map<int, int> jointSymmetryMap;
	map<RigidBody*, RigidBody*> RBSymmetryMap;

	RotateWidgetV2* rWidget;
	TranslateWidget* tWidget;

	GLMesh* skinMesh = NULL;
	P3D guidingMeshPos;
	Quaternion guidingMeshRot;
	double guidingMeshScale = 1.0;
	bool highlightedGuidingMesh = false;
	bool pickedGuidingMesh = false;
	bool meshDirty = false;

	Ray mouseRay;

	RigidBody* highlightedRB = NULL;
	RigidBody* pickedRB = NULL;

	Joint* highlightedJoint = NULL;
	Joint* pickedJoint = NULL;
	Joint* secondPickedJoint = NULL;

	RigidBody* highlightedEE = NULL;
	RigidBody* pickedEE = NULL;

	Skeleton::Joint* highlightedRefJoint = NULL;
	Skeleton::Joint* pickedRefJoint = NULL;
	P3D highlightedRefJointPos;

	string FBXPath;
	Skeleton refSkeleton;
	double refSkeletonScale = 0.02;

	vector<V3D> idealAxes;
	vector<P3D> idealJPos;
	vector<P3D> idealEEPos;

	set<Joint*> limbRootJoints;
	set<Joint*> limbRootChildJoints;

public:

	// constructor
	SkeletonMorphWindow(int x, int y, int w, int h, GLApplication* glApp);
	// destructor
	virtual ~SkeletonMorphWindow(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual void setupLights();

	void drawRefSkeleton();

	void loadRobot(const char* fName, const char* fNameRS = NULL);
	void loadSimplifiedRobot(const char* fName);
	void syncRobotSimpWithRobot();
	void syncNonDivergeJoint(Joint* simpJoint, Joint* robotJoint, set<Joint*>& divergeJoints);

	virtual void loadFile(const char* fName);
	virtual void saveFile(const char* fName);
	void saveJointMap(const char* fName);

	void loadMesh(const char* fName);
	void buildMaps();

	bool pickRB(double xPos, double yPos);
	bool pickMesh(double xPos, double yPos);
	bool pickJoint(double xPos, double yPos);
	bool pickRefJoint(double xPos, double yPos);
	bool pickEE(double xPos, double yPos);

	void scaleRigidBody(RigidBody* rb, double scale, bool changeBodyLength = false);
	void adjustJointToWorldPos(Joint* joint, const P3D& pos);
	void adjustJointToWorldPosSymm(Joint* joint, const P3D& pos);
	void adjustEEToWorldPos(RigidBody* EE, const P3D& pos);
	void adjustEEToWorldPosSymm(RigidBody* EE, const P3D& pos);
	void adjustRigidBody(RigidBody* rb);

	void getIdealPoseFromState(ReducedRobotState* state);
	void setIdealPoseToZeroState();
	void swapRotationAxesSymm(HingeJoint* joint1, HingeJoint* joint2);

	void saveRobot(const char* fName);
	void saveRobotSimp(const char* fName);
};
