#pragma once
#include "ModularDesignWindow.h"
#include "LivingBracketParametrizedDesign.h"
#include "LivingDesignSampler.h"

class LivingDesignEngine
{
public:
	// design window
	ModularDesignWindow* modularDesign = NULL;
	// living design
	LivingBracketParametrizedDesign* livingDesign = NULL;
	// design database
	vector<dVector> designDB;
	// design sampler
	LivingDesignSampler* designSampler = NULL;

public:
	LivingDesignEngine(ModularDesignWindow* modularDesign);
	~LivingDesignEngine();

	// intialization
	void init();
	// generate designs data base
	void generateDesignDB();
	
	// visualize design;
	void visualizeDesign(int index);

};

