#pragma once
#include "LocomotionEngineMotionPlan.h"


Quaternion getHeadingOffsetFromMotionPlanToRobotState(LocomotionEngineMotionPlan* mp, double mpPhase, Robot* robot);
