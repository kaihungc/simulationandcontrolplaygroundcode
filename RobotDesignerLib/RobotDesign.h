#pragma once

#include "RobotBodyPart.h"
#include "RobotBodyFeature.h"
#include <GUILib/GLApplication.h>
#include <GUILib/GLTrackingCamera.h>
#include <vector>
#include <deque>
#include <RBSimLib/RigidBody.h>
#include <ControlLib/Robot.h>


class RobotDesign{
protected:
public:
	RobotDesign();
	~RobotDesign();
	DynamicArray<RobotBodyPart*> robotBodyParts;

	void clear();

	RobotBodyPart* createRobotBodyPart();
	RobotBodyPart* createRobotBodyPart(RigidBody* rb);
	RobotBodyPart* getRobotBodyPartByName(const std::string& name);
	int getRobotBodyPartIndexByName(const std::string& name);

	void createJointBetween(RobotBodyPart* parent, RobotBodyPart* child, const P3D& pos, const V3D& axis);
	void readRobotFromFile(const char* filename);
	void saveRobotToFile(const char* filename);

	void transferMeshes(const DynamicArray<RigidBody*>& rbList);

	void draw();


/*
	int createOrRemoveSymPair(const std::vector<int> &IDs, bool flag = true);
	int stickToYZPlane(const std::vector<int> &IDs);
    int startMoving();
	int dragWithDeltaAngle(int ID, V3D delta);
	int setParentEmbeddingAngle(int ID, double delta);
	int setChildEmbeddingAngle(int ID, double delta);
	double getParentEmbeddingAngle(int ID);
	double getChildEmbeddingAngle(int ID);
    int setZAxis(int ID, V3D axis);
    V3D getZAxis(int ID);
	bool checkIfUseFeaturePoint(int ID);
	int setUseFeaturePoint(int ID, bool flag);
	RobotBodyPart* getBodyFromName(std::string name);
	bool isCenter(int ID);


	int setMouseOn(int);

	int getMinEmbeddingAngle(int);
	int setMinEmbeddingAngle(int, double);
	int getMaxEmbeddingAngle(int);
	int setMaxEmbeddingAngle(int, double);
	*/
};

