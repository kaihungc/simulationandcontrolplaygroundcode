#include "TransitionFFPOptPlan.h"


TransitionFFPOptPlan::TransitionFFPOptPlan(FootFallPattern* transFFP, FootFallPattern* startFFP, FootFallPattern* endFFP, int transStartIndex, int transEndIndex)
{
	this->transFFP = transFFP;
	this->startFFP = startFFP;
	this->endFFP = endFFP;
	this->transStartIndex = transStartIndex;
	this->transEndIndex = transEndIndex;

	int nSamplePoints = transFFP->strideSamplePoints;
	totalParamNum = 2 * transFFP->stepPatterns.size();

	planStepPatterns.resize(transFFP->stepPatterns.size());
	for (int i = 0; i < (int)transFFP->stepPatterns.size(); i++)
	{
		planStepPatterns[i].startIndex = transFFP->stepPatterns[i].startIndex;
		planStepPatterns[i].endIndex = transFFP->stepPatterns[i].endIndex;
	}

	startStepPatterns.resize(startFFP->stepPatterns.size());
	for (int i = 0; i < (int)startFFP->stepPatterns.size(); i++)
	{
		startStepPatterns[i].startIndex = startFFP->stepPatterns[i].startIndex - transStartIndex;
		startStepPatterns[i].endIndex = startFFP->stepPatterns[i].endIndex - transStartIndex;
		if (startStepPatterns[i].startIndex < 0)
		{
			startStepPatterns[i].startIndex += nSamplePoints;
			startStepPatterns[i].endIndex += nSamplePoints;
		}
	}

	endStepPatterns.resize(endFFP->stepPatterns.size());
	for (int i = 0; i < (int)endFFP->stepPatterns.size(); i++)
	{
		endStepPatterns[i].startIndex = endFFP->stepPatterns[i].startIndex + (nSamplePoints - 2 - transEndIndex);
		endStepPatterns[i].endIndex = endFFP->stepPatterns[i].endIndex + (nSamplePoints - 2 - transEndIndex);
		if (endStepPatterns[i].startIndex >= nSamplePoints)
		{
			endStepPatterns[i].startIndex -= nSamplePoints;
			endStepPatterns[i].endIndex -= nSamplePoints;
		}
	}
}

TransitionFFPOptPlan::~TransitionFFPOptPlan()
{
}

void TransitionFFPOptPlan::setParamsFromList(const dVector& p)
{
	for (int i = 0; i < (int)transFFP->stepPatterns.size(); i++)
	{
		ContStepPattern& sPattern = planStepPatterns[i];
		sPattern.startIndex = p[2 * i];
		sPattern.endIndex = p[2 * i + 1];
	}
}

void TransitionFFPOptPlan::writeParamsToList(dVector& p)
{
	p.resize(totalParamNum);

	for (int i = 0; i < (int)transFFP->stepPatterns.size(); i++)
	{
		ContStepPattern& sPattern = planStepPatterns[i];
		p[2 * i] = sPattern.startIndex;
		p[2 * i + 1] = sPattern.endIndex;
	}
}
