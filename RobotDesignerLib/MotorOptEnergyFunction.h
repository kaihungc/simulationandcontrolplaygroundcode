#pragma once
#include "MotorOptPlan.h"
#include <OptimizationLib/ObjectiveFunction.h>

class MotorOptEnergyFunction : public ObjectiveFunction
{
public:
	MotorOptPlan* plan;
	DynamicArray<ObjectiveFunction*> objectives;

	bool printDebugInfo = false;

	//this is the solution used as a regularizer...
	dVector m_p0;
	double regularizer;

public:
	MotorOptEnergyFunction(MotorOptPlan* plan);
	~MotorOptEnergyFunction();

	virtual double computeValue(const dVector& p);
	virtual void setCurrentBestSolution(const dVector& p);

	double printValue(const dVector& p);
	void setupSubObjectives();
};

