#include "ModularRobotCollisionManager.h"
#include "RMCBulletObject.h"
#include "RMCRobot.h"




void ModularRobotCollisionManager::initializeCollisionHandler() {
	delete bt_collision_world;
	delete bt_broadphase;
	delete bt_dispatcher;
	delete bt_collision_configuration;
	delete bt_DebugDrawer;

	double scene_size = 500;
	unsigned int max_objects = 16000;

	bt_collision_configuration = new btDefaultCollisionConfiguration();
	bt_dispatcher = new btCollisionDispatcher(bt_collision_configuration);

	btScalar sscene_size = (btScalar)scene_size;
	btVector3 worldAabbMin(-sscene_size, -sscene_size, -sscene_size);
	btVector3 worldAabbMax(sscene_size, sscene_size, sscene_size);
	//This is one type of broadphase, bullet has others that might be faster depending on the application
	//	bt_broadphase = new bt32BitAxisSweep3(worldAabbMin, worldAabbMax, max_objects, 0);  // true for disabling raycast accelerator
	bt_broadphase = new btDbvtBroadphase();
	bt_collision_world = new btCollisionWorld(bt_dispatcher, bt_broadphase, bt_collision_configuration);
	bt_DebugDrawer = new GLDebugDrawer();
	bt_collision_world->setDebugDrawer(bt_DebugDrawer);
}

void ModularRobotCollisionManager::clearBulletCollisionWorld() {
	//empty up the list of collision objects
	while (bt_collision_world->getCollisionObjectArray().size() > 0)
		bt_collision_world->removeCollisionObject(bt_collision_world->getCollisionObjectArray()[0]);
}

void ModularRobotCollisionManager::setupBulletCollisionWorld(RMCSearchNode* node) {
	if (bt_collision_world == NULL)
		initializeCollisionHandler();
	clearBulletCollisionWorld();

	//go through all pairs of objects, create collision primitives for all of them and see if they collide...
	DynamicArray<AbstractBulletObject*> bulletCollisionPrimitives;
	for (uint i = 0; i < robotList.size(); i++) {
		if (robotList[i] == searchTree->targetRobot) continue;
		
		robotList[i]->addBulletObjectsToList(bulletCollisionPrimitives);
	}

	searchTree->addPathBulletObjectsToList(node, bulletCollisionPrimitives);

	for (uint i = 0; i < bulletCollisionPrimitives.size(); i++) {
		//Logger::print("%d type: %d\n", i, bulletCollisionPrimitives[i]->type);
		bt_collision_world->addCollisionObject(bulletCollisionPrimitives[i]->getCollisionObject());
	}
}
		


/*
This method performs broadphase collision detection in bullet world
*/
void ModularRobotCollisionManager::performCollisionDetection() {

	collisionDepths.clear();

	//Perform collision detection
	bt_collision_world->performDiscreteCollisionDetection();
	int totalNumberOfCollisions = 0;

	// go through the results and check for pairs of collisions...
	int numManifolds = bt_collision_world->getDispatcher()->getNumManifolds();
	//For each contact manifold
	for (int i = 0; i < numManifolds; i++) {
		btPersistentManifold* contactManifold = bt_collision_world->getDispatcher()->getManifoldByIndexInternal(i);
		btCollisionObject* obA = (btCollisionObject*)(contactManifold->getBody0());
		btCollisionObject* obB = (btCollisionObject*)(contactManifold->getBody1());

		if (isConnected((AbstractBulletObject*)obA->getUserPointer(), (AbstractBulletObject*)obB->getUserPointer()))
		{
			//Logger::print("SOMETHING shouldn't collide.\n");
			continue;
		}


		contactManifold->refreshContactPoints(obA->getWorldTransform(), obB->getWorldTransform());
		int numContacts = contactManifold->getNumContacts();

		//For each contact point in that manifold
		for (int j = 0; j < numContacts; j++) {
			totalNumberOfCollisions++;
			btManifoldPoint& pt = contactManifold->getContactPoint(j);

			//CollisionObject col;
			//col.obj1 = (BaseObject*)obA->getUserPointer();
			//col.obj2 = (BaseObject*)obB->getUserPointer();
			/*if (!obj>shouldCollideWith(col.obj2) && !col.obj2->shouldCollideWith(col.obj1)) {
				break;
			}*/
			//col.obj1->collided = true;
			//col.obj2->collided = true;
			////the coordinate frame of the collision primitive may not be the same as the coordinate frame of the object - so go from the world coordinates of the point to the local coordinates in object space...
			//col.p_LocalO1 = col.obj1->getLocalCoordinates(getP3D(pt.getPositionWorldOnA()));
			//col.p_LocalO2 = col.obj2->getLocalCoordinates(getP3D(pt.getPositionWorldOnB()));
			//col.nWorld = getV3D(pt.m_normalWorldOnB);
			//col.penetrationDepth = pt.getDistance();
			//col.weight = 100;

			//assembly->collisionObjects.push_back(col);

			collisionDepths.push_back(pt.getDistance());
			//Logger::consolePrint("we have a collision!!. Dist: %lf\n", pt.getDistance());
		}
	}
	//Logger::consolePrint("total number of collisions: %d\n", totalNumberOfCollisions);
}

bool ModularRobotCollisionManager::isConnected(AbstractBulletObject* objA, AbstractBulletObject* objB)
{
	if (objA->type == RMC_OBJECT && objB->type == RMC_OBJECT)
	{
		RMC* rmcA = ((RMCBulletObject*)objA)->parent;
		RMC* rmcB = ((RMCBulletObject*)objB)->parent;
		if (rmcA == rmcB)
			return true;
		return rmcA->isConnected(rmcB);
	}
	else if (objA->type == SEARCH_NODE_OBJECT && objB->type == SEARCH_NODE_OBJECT)
	{
		RMCSearchNode* nodeA = ((RMCSearchNodeBulletObject*)objA)->parent;
		RMCSearchNode* nodeB = ((RMCSearchNodeBulletObject*)objB)->parent;

		if (nodeA == nodeB)
			return true;
		return nodeA->isConnected(nodeB);
	}
	else
	{
		RMCSearchNode* node = (RMCSearchNode*) (objA->type == SEARCH_NODE_OBJECT ? ((RMCSearchNodeBulletObject*)objA)->parent : ((RMCSearchNodeBulletObject*)objB)->parent);
		RMC* rmc = (RMC*)(objA->type == RMC_OBJECT ? ((RMCBulletObject*)objA)->parent : ((RMCBulletObject*)objB)->parent);

		return node->parent == searchTree->root && rmc == searchTree->rootRMC;
	}

	return true;

}

