#include "RobotDesignWindow.h"
#include <GUILib/GLUtils.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/Matrix.h>

//CLICK+CTRL: clears selection
//holding SHIFT while moving translate/rotate widget propagates changes downstream. No shift changes only the selected feature.
//'r' to show rotate widget, 't' for translate widget

//TODO: visualize the symmetry plane, show the design box, simplify all mesh operations
//TODO: fine tune editing modes
//TODO: operations on body parts: add new child joint + click
//TODO: stick to xy plane and such...
//TODO: feature points should have a method to propagate changes

void TW_CALL toggleSymBodyPair(void* clientData) {
	((RobotDesignWindow*)clientData)->createOrRemoveSymPair();
}

RobotDesignWindow::RobotDesignWindow(int x, int y, int w, int h, GLApplication* glApp) : AbstractDesignWindow(x, y, w, h){
	
	type = ROBOT_DESIGN;
	this->glApp = glApp;
	robot = new RobotDesign();

	TwAddVarRW(glApp->mainMenuBar, "Structure Features: attachment points", TW_TYPE_BOOLCPP,  &StructureFeature::showAttachmentPoints, "");
	TwAddVarRW(glApp->mainMenuBar, "Structure Features: convex hull", TW_TYPE_BOOLCPP, &StructureFeature::showConvexHull, "");
	TwAddVarRW(glApp->mainMenuBar, "Structure Features: wire frame", TW_TYPE_BOOLCPP, &StructureFeature::showWireFrameConvexHull, "");
	
	TwAddButton(glApp->mainMenuBar, "Toggle Symmetric Body Pairs ", toggleSymBodyPair, this, " label='Symmetric Body Pairs' group='Operation' key='s' ");

	delete camera;
	camera = new GLTrackingCamera(-1.5);
	camera->setCameraTarget(P3D(0, -0.12, 0));

//	TwAddButton(glApp->mainMenuBar, "clearSelection", RobotDesignWindowClearSeletion, this, " label='Clear Selection' group='Operation'");
//	TwAddButton(glApp->mainMenuBar, "split", RobotDesignWindowSplit, this, " label='Split It!' group='Operation'");
//	TwAddButton(glApp->mainMenuBar, "createOrRemoveSymPair", RobotDesignWindowCreateOrRemoveSymPair, this, " label='Create/Remove SymPair' group='Operation'");
//	TwAddButton(glApp->mainMenuBar, "stickToYZPlane", RobotDesignWindowStickToYZPlane, this, " label='Stick To YZ Plane?' group='Operation'");
//	TwAddSeparator(glApp->mainMenuBar, "sep3", "");
//	TwAddButton(glApp->mainMenuBar, "newRobot", RobotDesignWindowNewRobot, this, " label='New' group='File' ");
//	TwAddButton(glApp->mainMenuBar, "save", RobotDesignWindowSave, this, " label='Save' group='File' ");
//	TwAddVarRW(glApp->mainMenuBar, "saveFileName", TW_TYPE_CSSTRING(256), saveFileName, "label='Save File Name' group='File'");
//	showGroundPlane = false;

//	TwAddVarRW(glApp->mainMenuBar, "paramX", TW_TYPE_DOUBLE, &posX, " label='X' step=0.01 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramY", TW_TYPE_DOUBLE, &posY, " label='Y' step=0.01 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramZ", TW_TYPE_DOUBLE, &posZ, " label='Z' step=0.01 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramAngleX", TW_TYPE_DOUBLE, &angleX, " label='Angle X' step=0.5 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramAngleY", TW_TYPE_DOUBLE, &angleY, " label='Angle Y' step=0.5 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramAngleZ", TW_TYPE_DOUBLE, &angleZ, " label='Angle Z' step=0.5 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "parentEmbeddingAngle", TW_TYPE_DOUBLE, &parentEmbeddingAngle, " label='Parent Embedding Angle' step=0.5 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "childEmbeddingAngle", TW_TYPE_DOUBLE, &childEmbeddingAngle, " label='Child Embedding Angle' step=0.5 group='Param'");
//	TwAddVarRW(glApp->mainMenuBar, "paramUseFeaturePoint", TW_TYPE_BOOLCPP, &paramBool[0], " label='Use Feature Point' group='Param'");
}

RobotDesignWindow::~RobotDesignWindow(void) {
}

void RobotDesignWindow::createOrRemoveSymPair() {
	if (selectedBodyParts.size() != 2) {
		Logger::consolePrint("Need two body parts selected in order to create/remove symmetry pair\n");
		return;
	}

	if (selectedBodyParts[0]->mirroredBodyPart == NULL && selectedBodyParts[1]->mirroredBodyPart == NULL) {
		selectedBodyParts[0]->mirroredBodyPart = selectedBodyParts[1];
		selectedBodyParts[1]->mirroredBodyPart = selectedBodyParts[0];
		return;
	}

	if (selectedBodyParts[0]->mirroredBodyPart == selectedBodyParts[1] && selectedBodyParts[1]->mirroredBodyPart == selectedBodyParts[0]) {
		selectedBodyParts[0]->mirroredBodyPart = selectedBodyParts[1]->mirroredBodyPart = NULL;
		return;
	}

	Logger::consolePrint("Could not create SYM pair because at least one of the selected body parts is mirrored to something else\n");

}


void RobotDesignWindow::loadMenuParametersFor(BaseRobotBodyFeature* brbFeature) {
	std::string objName = std::string("") + brbFeature->name;
	TwAddButton(glApp->mainMenuBar, objName.c_str(), NULL, NULL, " ");
	DynamicArray<std::pair<char*, bool*>> params2 = brbFeature->getObjectFlags();
	for (auto it = params2.begin(); it != params2.end(); ++it)
		TwAddVarRW(glApp->mainMenuBar, it->first, TW_TYPE_BOOLCPP, it->second, "");
	DynamicArray<std::pair<char*, double*>> params1 = brbFeature->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it)
		TwAddVarRW(glApp->mainMenuBar, it->first, TW_TYPE_DOUBLE, it->second, "step=.005");
}

void RobotDesignWindow::unloadMenuParametersFor(BaseRobotBodyFeature* brbFeature) {
	std::string objName = std::string("") + brbFeature->name;
	TwRemoveVar(glApp->mainMenuBar, objName.c_str());
	DynamicArray<std::pair<char*, bool*>> params2 = brbFeature->getObjectFlags();
	for (auto it = params2.begin(); it != params2.end(); ++it)
		TwRemoveVar(glApp->mainMenuBar, it->first);

	DynamicArray<std::pair<char*, double*>> params1 = brbFeature->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it)
		TwRemoveVar(glApp->mainMenuBar, it->first);
}

void RobotDesignWindow::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);


}

void RobotDesignWindow::updateBodyPartFeaturesList() {
	featureList.clear();
	for (uint i = 0; i < robot->robotBodyParts.size(); i++)
		robot->robotBodyParts[i]->addBodyFeaturesToList(featureList);

}

RobotBodyPart* RobotDesignWindow::getClosestRobotBodyPartToRay(const Ray& ray, double& dist) {
	updateBodyPartFeaturesList();
	RobotBodyPart* ans = NULL;
	dist = -1;

	for (uint i = 0; i < robot->robotBodyParts.size(); i++) {
		double tmp = robot->robotBodyParts[i]->getDistanceToRayOriginIfHit(ray);
		if (tmp >= 0 && (dist < 0 || dist > tmp)) {
			dist = tmp;
			ans = robot->robotBodyParts[i];
		}
	}

	return ans;
}

BaseRobotBodyFeature* RobotDesignWindow::getClosestBodyFeatureToRay(const Ray& ray, double& dist) {
	updateBodyPartFeaturesList();
	BaseRobotBodyFeature* ans = NULL;
	dist = -1;

	for (uint i = 0; i < featureList.size(); i++){
		double tmp = featureList[i]->getDistanceToRayOriginIfHit(ray);
		if (tmp >= 0 && (dist < 0 || dist > tmp)) {
			dist = tmp;
			ans = featureList[i];
		}
	}

	return ans;
}

//triggered when mouse moves
bool RobotDesignWindow::onMouseMoveEvent(double xPos, double yPos) {
	preDraw();
	Ray clickedRay = getRayFromScreenCoords(xPos, yPos);
	bool processed = false;

	if (!processed && rotateWidget.onMouseMoveEvent(xPos, yPos)) {
		//update the orientation if we've selected a joint type feature
		if (RobotJointFeature* j = dynamic_cast<RobotJointFeature*>(selectedBodyFeatures[0]))
			j->setJointAxis(rotateWidget.getOrientation() * j->getLocalCoordinatesRotationAxis(), glfwGetKey(glApp->glfwWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS);
		processed = true;
	}

	if (!processed && translateWidget.onMouseMoveEvent(xPos, yPos)) {
		//update the position of the selected object and continue...
		if (selectedBodyFeatures.size() == 1)
			selectedBodyFeatures[0]->setPosition(translateWidget.pos, glfwGetKey(glApp->glfwWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS);
		processed = true;
	}

	//check if we're hovering over anything important here...
	if (!processed && GlobalMouseState::lButtonPressed == false && GlobalMouseState::rButtonPressed == false && GlobalMouseState::mButtonPressed == false) {
		//we should only have one thing highlighted at one time - so un-highlight everything...
		updateBodyPartFeaturesList();
		double featureDist;
		for (uint i = 0; i < featureList.size(); i++)
			featureList[i]->highlighted = false;
		BaseRobotBodyFeature* highlightedBodyFeature = getClosestBodyFeatureToRay(clickedRay, featureDist);
		if (highlightedBodyFeature)
			highlightedBodyFeature->highlighted = true;

		double bodyPartDist;
		//check to see if any of the body parts should be highlighted...
		for (uint i = 0; i < robot->robotBodyParts.size(); i++)
			robot->robotBodyParts[i]->highlighted = false;
		RobotBodyPart* highlightedBodyPart = getClosestRobotBodyPartToRay(clickedRay, bodyPartDist);

		if (highlightedBodyPart)
			if (highlightedBodyFeature == NULL || (highlightedBodyFeature->parent != highlightedBodyPart && featureDist > bodyPartDist))
				highlightedBodyPart->highlighted = true;
		if (highlightedBodyPart && highlightedBodyPart->highlighted && highlightedBodyFeature)
			highlightedBodyFeature->highlighted = false;
	}

	postDraw();

	if (processed) return true;

	return GLWindow3D::onMouseMoveEvent(xPos, yPos);
}


void RobotDesignWindow::onMenuMouseButtonProcessedEvent() {
	if (selectedBodyFeatures.size() == 1){
		RobotJointFeature* rjf = dynamic_cast<RobotJointFeature*>(selectedBodyFeatures[0]);
		if (rjf)
			rjf->child->propagateChangesToMirroredBody();
		else
			selectedBodyFeatures[0]->parent->propagateChangesToMirroredBody();
	}
}

//triggered when mouse buttons are pressed
bool RobotDesignWindow::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (translateWidget.onMouseButtonEvent(button, action, mods, xPos, yPos))
		return true;
	if (rotateWidget.onMouseButtonEvent(button, action, mods, xPos, yPos))
		return true;

	//if any feature is being hovered over, and we click the mouse button, then we should invert its selection
	if (GlobalMouseState::lButtonPressed == true) {
		//change the selection of anything that is highlighted
		updateBodyPartFeaturesList();
		selectedBodyFeatures.clear();
		selectedBodyParts.clear();


		for (uint i = 0; i < robot->robotBodyParts.size(); i++) {
			if (mods & GLFW_MOD_CONTROL) 
				robot->robotBodyParts[i]->selected = false;
			if (robot->robotBodyParts[i]->highlighted)
				robot->robotBodyParts[i]->selected = !robot->robotBodyParts[i]->selected;
			if (robot->robotBodyParts[i]->selected) {
				selectedBodyParts.push_back(robot->robotBodyParts[i]);
			}
		}

		bool bodyFeatureHighlighted = false;
		for (uint i = 0; i < featureList.size(); i++)
			if (featureList[i]->highlighted)
				bodyFeatureHighlighted = true;

		for (uint i = 0; i < featureList.size(); i++)
			if (featureList[i]->selected)
				unloadMenuParametersFor(featureList[i]);

		for (uint i = 0; i < featureList.size(); i++) {
//			no need to ever have multiple body features selected at the same time
			if ((mods & GLFW_MOD_CONTROL || bodyFeatureHighlighted) && featureList[i]->highlighted == false)
				featureList[i]->selected = false;
			if (featureList[i]->highlighted)
				featureList[i]->selected = !featureList[i]->selected;
			if (featureList[i]->selected) {
				selectedBodyFeatures.push_back(featureList[i]);
				loadMenuParametersFor(featureList[i]);
			}
		}
		return true;
	}



	return  GLWindow3D::onMouseButtonEvent(button, action, mods, xPos, yPos);
}

//triggered when using the mouse wheel
bool RobotDesignWindow::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLWindow3D::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return true;
}

bool RobotDesignWindow::onKeyEvent(int key, int actionI, int mods) {
	if (key == 't' || key == 'T' && actionI == GLFW_PRESS) {
		showTranslateWidget = !showTranslateWidget;
		if (showTranslateWidget)
			showRotateWidget = false;
	}
	else if (key == 'r' || key == 'R' && actionI == GLFW_PRESS) {
		showRotateWidget = !showRotateWidget;
		if (showRotateWidget)
			showTranslateWidget = false;
	}

	return (GLWindow3D::onKeyEvent(key, actionI, mods));
 
	return false;
}

bool RobotDesignWindow::onCharacterPressedEvent(int key, int mods) {
	if (GLWindow3D::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

void RobotDesignWindow::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);
	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
	delete robot;
	robot = new RobotDesign();
	robot->readRobotFromFile(fName);
}

void RobotDesignWindow::saveFile(const char* fName) {
	robot->saveRobotToFile(fName);
}

// Draw the AppRobotDesigner scene - camera transformations, lighting, shadows, reflections, etc AppRobotDesignerly to everything drawn by this method
void RobotDesignWindow::drawScene() {
	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);
	glPushMatrix();
	glScaled(0.15, 0.15, 0.15);
	drawDesignEnvironmentBox();
	glPopMatrix();

	robot->draw();
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void RobotDesignWindow::drawAuxiliarySceneInfo() {
	preDraw();
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);

	for (uint i = 0; i < robot->robotBodyParts.size(); i++)
		robot->robotBodyParts[i]->drawFeatures();

	//sync the widgets with the selected feature
	if (selectedBodyFeatures.size() == 1) {
		translateWidget.pos = selectedBodyFeatures[0]->positionWorld;
		rotateWidget.pos = selectedBodyFeatures[0]->positionWorld;
		if (RobotJointFeature* j = dynamic_cast<RobotJointFeature*>(selectedBodyFeatures[0]))
			rotateWidget.setOrientation(j->getOrientation());
	}
	translateWidget.visible = showTranslateWidget && selectedBodyFeatures.size() == 1;
	rotateWidget.visible = showRotateWidget && selectedBodyFeatures.size() == 1 && (dynamic_cast<RobotJointFeature*>(selectedBodyFeatures[0]));
	//	translateWidget.visible = showTranslateWidget && key1Down && (!robot->isCenter(selectedID[0]));
	//	rotateWidget.visible = showRotateWidget && key2Down && (!robot->isCenter(selectedID[0]));

	translateWidget.draw();
	rotateWidget.draw();
	postDraw();
}
