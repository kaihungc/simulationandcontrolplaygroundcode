#pragma once

#include "RobotDesignerLib/RobotDesign.h"
#include <string>
#include <map>
#include <algorithm>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <GUILib/GLWindow3D.h>
#include <RobotDesignerLib/AbstractDesignWindow.h>

/**
 * Test AppRobotDesigner
 */
class RobotDesignWindow : public AbstractDesignWindow {
private:
	GLApplication* glApp;
	TranslateWidget translateWidget;
//	RotateWidgetV1 rotateWidget;
	RotateWidgetV2 rotateWidget;
	bool showTranslateWidget = false, showRotateWidget = false;

	DynamicArray<BaseRobotBodyFeature*> featureList;

	const char* startStateFName = NULL;

public:

	RobotDesign* robot;
	// constructor
	RobotDesignWindow(int x, int y, int w, int h, GLApplication* glApp);
	// destructor
	virtual ~RobotDesignWindow(void);

	// Draw the AppRobotDesigner scene - camera transformations, lighting, shadows, reflections, etc AppRobotDesignerly to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();

	//mouse & keyboard event callbacks...

	//all these methods should return true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	BaseRobotBodyFeature* getClosestBodyFeatureToRay(const Ray& ray, double& dist);
	RobotBodyPart* getClosestRobotBodyPartToRay(const Ray& ray, double& dist);
	void updateBodyPartFeaturesList();

	void onMenuMouseButtonProcessedEvent();

	void prepareForSimulation(AbstractRBEngine* rbEngine) {
		robot->transferMeshes(rbEngine->rbs);
	}

	virtual ReducedRobotState getStartState(Robot* robot) {
		ReducedRobotState rs(robot);
		if (startStateFName != NULL)
			rs.readFromFile(startStateFName);
		return rs;
	}

	virtual void setStartStateFName(const char* startStateFName) {
		this->startStateFName = startStateFName;
	}

	DynamicArray<BaseRobotBodyFeature*> selectedBodyFeatures;
	DynamicArray<RobotBodyPart*> selectedBodyParts;

	void setupLights();

	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	void createOrRemoveSymPair();

	void loadMenuParametersFor(BaseRobotBodyFeature* brbFeature);
	void unloadMenuParametersFor(BaseRobotBodyFeature* brbFeature);

};



