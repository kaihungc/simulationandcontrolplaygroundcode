#pragma once

#include <MathLib/V3D.h>
#include <MathLib/P3D.h>
#include <vector>
#include "RobotBodyFeature.h"

class Motor_X5 : public RobotJointFeature {
public:
	Motor_X5(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis = V3D(0, 0, 1));
	~Motor_X5(void);
	
	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints);
};

