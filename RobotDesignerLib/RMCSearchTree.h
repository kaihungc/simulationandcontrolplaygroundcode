#pragma once
#include <RobotDesignerLib/RMC.h>
#include <RobotDesignerLib/RMCRobot.h>
#include <queue>
#include "RMCSearchNodeBulletObject.h"
#include <MathLib/LevelSet.h>
#include <Utils\Timer.h>

class RMCSearchNodeBulletObject;
class ModularRobotCollisionManager;

class RMCSearchNode
{
public:
	RMCSearchNode* parent = NULL;
	vector<RMCSearchNode*> children;
	int id = 0;  // the index in its parent's children

	// current tail RMC's name
	string RMCName;
	// the pin id of parent node's connection to current node
	int parentPinId = -1;
	// the pin id of current node's connection to parent
	int childPinId = -1;
	// relative transformation id
	int relTransId = 0;
	// used pin ids, seems redundant, but root of the tree can have many used pins
	set<int> usedPinIds;
	// total cost
	double cost = 1e10;

	// cost so far and cost to go..
	double pathCost = 0;
	double heuristicCost = 0;
	double aestheticCost = 0;
	int moduleCount = 0;

	RMCType type;

	// position of current tail RMC
	P3D position;
	// orientation of current tail RMC
	Quaternion orientation;

	// list of collision objects
	vector<RMCSearchNodeBulletObject*> bulletObjects;

	RMCSearchNode();
	~RMCSearchNode();

	RMCSearchNode(RMC* rmc);

	bool isConnected(RMCSearchNode* node) {
		if (node->parent == this || parent == node)
			return true;

		return false;
	}
};

// for priority queue
struct NodeCompare
{
	bool operator()(const RMCSearchNode* t1, const RMCSearchNode* t2) const
	{
		return t1->cost > t2->cost;
	}
};

class RMCSearchTree
{
public:
	// RMCs 
	vector<RMC*>& rmcWarehouse;
	// used for rmc lookup, because we will only store the name of RMC in SearchNode
	map<string, RMC*>& rmcNameMap;
	// transformation map, important for connecting pins
	map<string, vector<Transformation>>& transformationMap;

	// the root search node
	RMCSearchNode* root = NULL;
	// the target search node
	RMCSearchNode* target = NULL;

	// these information will help us rebuild the robot from a search path
	// the root robot we start searching from
	RMCRobot* rootRobot = NULL;
	// the root RMC we start searching from
	RMC* rootRMC = NULL;
	// the target RMCRobot, we want to connect to its root
	RMCRobot* targetRobot = NULL;
	// the target RMC
	RMC* targetRMC = NULL;

	ModularRobotCollisionManager* searchTreeCollisionManager = NULL;

	// stores current best node for visualization purpose.
	RMCSearchNode* currentBestNode = NULL;

	double minDist = 1e10;
	// openset is set of nodes that still need to be expanded and searched..
	// implemented as priority queue. It is sorted based on cost (ascending order)
	priority_queue<RMCSearchNode*, std::vector<RMCSearchNode*>, NodeCompare> openSet;

	LevelSet* meshSDF = NULL;

	// weights of different costs
	double heuristicWeight = 1;
	double connectorCostWeight = 0.01;
	//double meshCostWeight = 0.9;
	//double lineCostWeight = 0.5;
	double pathCostWeight = 0;
	double motorOrientationWeight = 0;
	double aestheticCostWeight = 0;

	bool useMeshCost = false;

	// for analysis and results
	int nodesExpanded = 0;
	Timer searchTimer; 

public:
	RMCSearchTree(map<string, vector<Transformation>>& _transformationMap, vector<RMC*>& _rmcWarehouse, map<string, RMC*>& _rmcNameMap, ModularRobotCollisionManager* collisionManager);
	~RMCSearchTree();

	RMCSearchNode* getRoot() {
		return root;
	}

	void setRoot(RMCRobot* _rootRobot, RMC* _rootRMC) {
		rootRobot = _rootRobot;
		rootRMC = _rootRMC;
		root = new RMCSearchNode(rootRMC);
	}

	void setTarget(RMCRobot* _targetRobot, RMC* _targetRMC) {
		delete target;

		targetRobot = _targetRobot;
		targetRMC = _targetRMC;
		target = new RMCSearchNode(targetRMC);
	}

	void setGuidingMeshSDF(LevelSet* _meshSDF) {
		meshSDF = _meshSDF;
	}

	// build the real RMC connections of the path
	RMC* buildPathSolution(RMCSearchNode* node);

	// delete tree
	void deleteTree(RMCSearchNode* node);

	// search from current node for possible configuration, results will be stored in node's children
	void searchFromNode(RMCSearchNode* node);

	// draw all RMCs on the path from the root to this node
	void drawPathRMC(RMCSearchNode* node);

	// draw the tail RMC of the RMCSearchNode
	void drawNodeRMC(RMCSearchNode* node);

	void prepareForSearch();

	bool searchNextNodeInQueue();

	// brute force search -- searches all possible connecting paths from start to goal node
	// assumes root and target nodes are already set by user.
	// with collision cost and priority queue
	RMCSearchNode* bruteForceSearch2();

	// compare RMCSearchNodes -- returns true if both nodes are same (based on heuristic criteria), false otherwise
	bool compareRMCSearchNode(RMCSearchNode* node1, RMCSearchNode* node2);

	//calculate cost
	double calculateHeuristicCost(RMCSearchNode* node);
	double calculateCollisionCost(RMCSearchNode* node);
	int countConnectors(RMCSearchNode* node);
	int countTotalModules(RMCSearchNode* node);
	double calculateMeshSDFCost(RMCSearchNode* node);
	double calculateAestheticCost_usingMesh(RMCSearchNode* node);
	double calculateAestheticCost_usingStLine(RMCSearchNode* node);
	double calculatePathCost_usingModuleDist(RMCSearchNode* node);
	void calculateTotalCost(RMCSearchNode* node);
	void checkHeuristicConsistency(RMCSearchNode* node);
	void checkHeuristicAdmissibility(RMCSearchNode* node);

	// check if node is close to goal
	double compareRMCSearchNodeToGoal(RMCSearchNode* node);

	void addPathBulletObjectsToList(RMCSearchNode* node, DynamicArray<AbstractBulletObject*>& list);
};
