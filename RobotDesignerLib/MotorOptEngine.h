#pragma once

#include "MotorOptPlan.h"
#include "MotorOptEnergyFunction.h"

class MotorOptEngine
{
public:
	MotorOptPlan* plan = NULL;
	MotorOptEnergyFunction* energyFunction = NULL;

public:
	MotorOptEngine();
	~MotorOptEngine();

	void initialize(vector<RMCRobot*>& rmcRobots);
	double optimizePlan(int maxIterNum);
};

