#include <GUILib/GLUtils.h>

#include "SkeletonMorphWindow.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <RobotDesignerLib/KinematicRobotController.h>
#include <RobotDesignerLib/ControlUtil.h>
#include <GUILib/OBJReader.h>

/**
UI code:
*/

SkeletonMorphWindow::SkeletonMorphWindow(int x, int y, int w, int h, GLApplication* glApp) : GLWindow3D(x, y, w, h) {

	this->glApp = glApp;
	mainMenuBar = glApp->mainMenuBar;

	tWidget = new TranslateWidget(AXIS_X | AXIS_Y | AXIS_Z);
	rWidget = new RotateWidgetV2();

	rWidget->visible = false;
	tWidget->visible = false;

	loadRobot("../data/rbs/quadrupedTemplate/quadSpine_v2.rbs",
		"../data/rbs/quadrupedTemplate/quadSpine_v2.rs");
	loadSimplifiedRobot("../data/rbs/quadrupedTemplate/quadSpine_v2_simple.rbs");

	loadFile("../data/fbx/baby rhino.fbx");
}

void SkeletonMorphWindow::loadRobot(const char* fName, const char* fNameRS) {
	delete robot;
	delete startState;
	delete rbEngine;
	delete MRD;

	// *************************** load robot ***************************
	rbEngine = new ODERBEngine();
	rbEngine->loadRBsFromFile(fName);
	robot = new Robot(rbEngine->rbs[0]);
	setupSimpleRobotStructure(robot);

	robot->bFrame->updateStateInformation();

	// remove all meshes
	for (int i = 0; i < robot->getRigidBodyCount(); i++) {
		RigidBody* rb = robot->getRigidBody(i);
		rb->meshes.clear();
		rb->carveMeshes.clear();
		rb->meshTransformations.clear();
	}


	zeroState = new ReducedRobotState(robot);
	startState = new ReducedRobotState(robot);
	if (fNameRS) {
		startState->readFromFile(fNameRS);
	}
	getIdealPoseFromState(startState);
	setIdealPoseToZeroState();

	robot->setState(zeroState);

	MRD = new MorphologicalRobotDesign(robot);

	buildMaps();

	limbRootJoints.clear();
	limbRootChildJoints.clear();

	auto& robotLimbs = robot->getEndEffectorRBs();
	for (uint i = 0; i < robotLimbs.size(); i++)
	{
		RigidBody* robotLimb = robotLimbs[i];
		Joint* rootJoint = robotLimb->pJoints[0];
		for (int j = 0; j < 4; j++)
			rootJoint = rootJoint->parent->pJoints[0];
		Joint* childJoint = rootJoint->child->cJoints[0];

		limbRootJoints.insert(rootJoint);
		limbRootChildJoints.insert(childJoint);
		adjustJointToWorldPos(childJoint, rootJoint->getWorldPosition());
	}

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
		adjustRigidBody(robot->getRigidBody(i));
}

void SkeletonMorphWindow::loadSimplifiedRobot(const char* fName)
{
	delete robotSimp;
	delete rbEngineSimp;
	delete zeroStateSimp;
	rbEngineSimp = new ODERBEngine();
	rbEngineSimp->loadRBsFromFile(fName);
	robotSimp = new Robot(rbEngineSimp->rbs[0]);
	setupSimpleRobotStructure(robotSimp);

	robotSimp->bFrame->updateStateInformation();

	// remove all meshes
	for (int i = 0; i < robotSimp->getRigidBodyCount(); i++) {
		RigidBody* rb = robotSimp->getRigidBody(i);
		rb->meshes.clear();
		rb->carveMeshes.clear();
		rb->meshTransformations.clear();
	}

	zeroStateSimp = new ReducedRobotState(robotSimp);

	for (int i = 0; i < robotSimp->getRigidBodyCount(); i++)
	{
		adjustRigidBody(robotSimp->getRigidBody(i));
	}
}

void SkeletonMorphWindow::syncRobotSimpWithRobot()
{
	set<Joint*> divergeJoints;

	// ******************* legs *******************
	auto& robotLimbs = robot->getEndEffectorRBs();
	auto& simpLimbs = robotSimp->getEndEffectorRBs();
	for (uint i = 0; i < simpLimbs.size(); i++)
	{
		RigidBody* robotLimb = robotLimbs[i];
		RigidBody* simpLimb = simpLimbs[i];

		P3D robotEEPos = robotLimb->getWorldCoordinates(robotLimb->rbProperties.endEffectorPoints[0].coords) + V3D(0, 0.02, 0);
		adjustEEToWorldPos(simpLimb, robotEEPos);

		Joint* rootJointSimp = simpLimb->pJoints[0];
		for (int j = 0; j < 2; j++)
			rootJointSimp = rootJointSimp->parent->pJoints[0];

		Joint* rootJoint = robotLimb->pJoints[0];
		for (int j = 0; j < 4; j++)
			rootJoint = rootJoint->parent->pJoints[0];

		divergeJoints.insert(rootJoint);

		adjustJointToWorldPos(rootJointSimp, rootJoint->getWorldPosition());
		((HingeJoint*)rootJointSimp)->rotationAxis = ((HingeJoint*)rootJoint)->rotationAxis;

		P3D secondJPos = rootJoint->child->cJoints[0]->getWorldPosition();
		adjustJointToWorldPos(rootJointSimp->child->cJoints[0], secondJPos);
		((HingeJoint*)rootJointSimp->child->cJoints[0])->rotationAxis = ((HingeJoint*)rootJoint->child->cJoints[0])->rotationAxis;

		P3D thirdJPos = (robotEEPos + secondJPos) * 0.5 + V3D(0, 0, 1) * (robotEEPos - secondJPos).norm() * 0.5 * tan(RAD(-20));
		adjustJointToWorldPos(simpLimb->pJoints[0], thirdJPos);
		((HingeJoint*)simpLimb->pJoints[0])->rotationAxis = ((HingeJoint*)rootJoint->child->cJoints[0])->rotationAxis;
	}

	// ******************* Non-leg joints *******************
	for (uint i = 0; i < robot->getRoot()->cJoints.size(); i++)
	{
		Joint* robotJoint = robot->getRoot()->cJoints[i];
		Joint* simpJoint = robotSimp->getRoot()->cJoints[i];

		syncNonDivergeJoint(simpJoint, robotJoint, divergeJoints);
	}

	// ******************* fix rigid body *******************
	for (int i = 0; i < robotSimp->getRigidBodyCount(); i++)
	{
		adjustRigidBody(robotSimp->getRigidBody(i));
	}
}

void SkeletonMorphWindow::syncNonDivergeJoint(Joint* simpJoint, Joint* robotJoint, set<Joint*>& divergeJoints)
{
	if (divergeJoints.count(robotJoint)) return;

	adjustJointToWorldPos(simpJoint, robotJoint->getWorldPosition());
	((HingeJoint*)simpJoint)->rotationAxis = ((HingeJoint*)robotJoint)->rotationAxis;

	for (uint i = 0; i < robotJoint->child->cJoints.size(); i++)
	{
		Joint* childRobotJoint = robotJoint->child->cJoints[i];
		Joint* childSimpJoint = simpJoint->child->cJoints[i];

		syncNonDivergeJoint(childSimpJoint, childRobotJoint, divergeJoints);
	}
}

void SkeletonMorphWindow::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("obj") == 0) {
		loadMesh(fName);
		return;
	}

	if (fNameExt.compare("fbx") == 0) {
		FBXPath = fName;
		FBXImporter::loadSkeleton(fName, refSkeleton);
		return;
	}

	if (fNameExt.compare("rbs") == 0) {
		loadRobot(fName);
		return;
	}
}

SkeletonMorphWindow::~SkeletonMorphWindow(void) {

	delete skinMesh;

	delete startState;
	delete robot;
	delete rbEngine;
	delete MRD;

	delete robotSimp;
	delete rbEngineSimp;
	delete zeroStateSimp;
}

// Restart the application.
void SkeletonMorphWindow::restart() {

}


// Run the App tasks
void SkeletonMorphWindow::process() {


}

//triggered when mouse moves
bool SkeletonMorphWindow::onMouseMoveEvent(double xPos, double yPos) {

	highlightedRB = NULL;
	highlightedGuidingMesh = false;

	preDraw();
	if (rWidget->onMouseMoveEvent(xPos, yPos)) {
		if (pickedGuidingMesh)
		{
			guidingMeshRot = rWidget->getOrientation();
			meshDirty = true;
		}
		if (pickedRB)
		{
			zeroState->setOrientation(rWidget->getOrientation());
		}
		postDraw();
		return true;
	}

	if (tWidget->onMouseMoveEvent(xPos, yPos)) {
		if (pickedRB)
		{
			zeroState->setPosition(tWidget->pos);
			rWidget->pos = tWidget->pos;
		}
		if (pickedGuidingMesh)
		{
			guidingMeshPos = rWidget->pos = tWidget->pos;
			meshDirty = true;
		}
		if (pickedJoint)
		{
			adjustJointToWorldPosSymm(pickedJoint, tWidget->pos);
			if (limbRootJoints.count(pickedJoint))
				adjustJointToWorldPosSymm(pickedJoint->child->cJoints[0], tWidget->pos);
			for (int i = 0; i < robot->getRigidBodyCount(); i++)
				adjustRigidBody(robot->getRigidBody(i));
		}
		if (pickedEE)
		{
			adjustEEToWorldPosSymm(pickedEE, tWidget->pos);
			for (int i = 0; i < robot->getRigidBodyCount(); i++)
				adjustRigidBody(robot->getRigidBody(i));
		}
		postDraw();
		return true;
	}

	mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	postDraw();

	bool res = pickJoint(xPos, yPos);
	if (res) return true;

	res = pickEE(xPos, yPos);
	if (res) return true;

	res = pickRefJoint(xPos, yPos);
	if (res) return true;

	res = pickRB(xPos, yPos);
	if (res) return true;

	res = pickMesh(xPos, yPos);
	if (res) return true;

	if (GLWindow3D::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool SkeletonMorphWindow::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {

	if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
	{
		preDraw();
		if (rWidget->onMouseMoveEvent(xPos, yPos) || tWidget->onMouseMoveEvent(xPos, yPos)) {
			postDraw();
			return true;
		}
		postDraw();

		rWidget->visible = tWidget->visible = false;

		if (highlightedRefJoint)
		{
			if (pickedJoint) {
				adjustJointToWorldPosSymm(pickedJoint, highlightedRefJointPos);
				if (limbRootJoints.count(pickedJoint))
					adjustJointToWorldPosSymm(pickedJoint->child->cJoints[0], highlightedRefJointPos);
				for (int i = 0; i < robot->getRigidBodyCount(); i++)
					adjustRigidBody(robot->getRigidBody(i));
			}

			if (pickedEE) {
				adjustEEToWorldPosSymm(pickedEE, highlightedRefJointPos);
				for (int i = 0; i < robot->getRigidBodyCount(); i++)
					adjustRigidBody(robot->getRigidBody(i));
			}
		}

		pickedRB = highlightedRB;
		if (pickedRB)
		{
			Logger::consolePrint("Selected rigid body %s\n", pickedRB->getName().c_str());
			rWidget->visible = false; tWidget->visible = true;
			tWidget->pos = zeroState->getPosition();
		}

		pickedGuidingMesh = highlightedGuidingMesh;
		if (pickedGuidingMesh)
		{
			rWidget->visible = tWidget->visible = true;
			rWidget->pos = tWidget->pos = guidingMeshPos;
			rWidget->setOrientation(guidingMeshRot);
		}

		pickedJoint = highlightedJoint;
		if (pickedJoint)
		{
			Logger::consolePrint("Selected joint %s\n", pickedJoint->name.c_str());
			rWidget->visible = false; tWidget->visible = true;
			tWidget->pos = pickedJoint->getWorldPosition();

			if ((mods & GLFW_MOD_ALT) > 0)
			{
				if (secondPickedJoint)
				{
					swapRotationAxesSymm((HingeJoint*)pickedJoint,
						(HingeJoint*)secondPickedJoint);
					secondPickedJoint = NULL;
				}
				else {
					secondPickedJoint = pickedJoint;
				}
			}
		}

		pickedEE = highlightedEE;
		if (pickedEE)
		{
			rWidget->visible = false; tWidget->visible = true;
			tWidget->pos = pickedEE->getWorldCoordinates(pickedEE->rbProperties.endEffectorPoints[0].coords);
		}
	}


	if (GLWindow3D::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool SkeletonMorphWindow::onMouseWheelScrollEvent(double xOffset, double yOffset) {

	if (pickedGuidingMesh)
	{
		guidingMeshScale *= (1 + yOffset * 0.05);
		meshDirty = true;
		return true;
	}

	if (pickedRB)
	{
		bool changeBodyLength = GetAsyncKeyState(VK_LSHIFT) < 0;
		scaleRigidBody(pickedRB, 1 + yOffset * 0.05, changeBodyLength);
		if (RBSymmetryMap.count(pickedRB))
		{
			scaleRigidBody(RBSymmetryMap[pickedRB], 1 + yOffset * 0.05, changeBodyLength);
		}
		return true;
	}

	if (GetAsyncKeyState(VK_LSHIFT) < 0)
	{
		refSkeletonScale *= (1 + yOffset * 0.05);
		Logger::consolePrint("Ref skeleton scale: %lf\n", refSkeletonScale);
		return true;
	}

	if (GLWindow3D::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool SkeletonMorphWindow::onKeyEvent(int key, int action, int mods) {

	/*
	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
	{
	refSkeleton.m_rootVisDebugRot = getRotationQuaternion(RAD(90), V3D(1, 0, 0)) * refSkeleton.m_rootVisDebugRot;
	}

	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
	{
	refSkeleton.m_rootVisDebugRot = getRotationQuaternion(RAD(-90), V3D(1, 0, 0)) * refSkeleton.m_rootVisDebugRot;
	}
	*/

	if (key == GLFW_KEY_B && action == GLFW_PRESS)
	{
		syncRobotSimpWithRobot();
	}

	if (GLWindow3D::onKeyEvent(key, action, mods))
		return true;

	return false;
}

void SkeletonMorphWindow::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

void SkeletonMorphWindow::saveJointMap(const char* fName)
{
	FILE* fp = fopen(fName, "w+");

	double threshold = 1e-2;
	int numRefJoint = refSkeleton.numJoints();
	refSkeleton.setScaleDanger(refSkeletonScale);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		P3D jPos = joint->getWorldPosition();
		Skeleton::Joint* refJoint = NULL;
		double closestDist = threshold;

		int numJoints = refSkeleton.size();
		for (int j = 0; j < numJoints; ++j)
		{
			Skeleton::Joint* _joint = refSkeleton.FindJoint(j);
			P3D refPos = refSkeleton.X(_joint);

			double dist = (jPos - refPos).norm();
			if (dist < closestDist)
			{
				closestDist = dist;
				refJoint = _joint;
			}
		}

		fprintf(fp, "%s %s\n", joint->name.c_str(), refJoint ? refJoint->m_strName.c_str() : "none");
	}

	fclose(fp);
}

void SkeletonMorphWindow::loadMesh(const char* fName)
{
	skinMesh = OBJReader::loadOBJFile(fName);
	AxisAlignedBoundingBox bbox = skinMesh->getBoundingBox();
	double initScale = 1.0 / bbox.diameter();
	skinMesh->translate(-(bbox.center()));
	skinMesh->scale(initScale, P3D());
	skinMesh->computeNormals();
	skinMesh->calBoundingBox();
}

void SkeletonMorphWindow::buildMaps()
{
	jointSymmetryMap.clear();
	RBIndexMap.clear();
	RBSymmetryMap.clear();

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RBIndexMap[robot->getRigidBody(i)] = i;
	}

	for (int i = 0; i < robot->getJointCount() - 1; i++)
		for (int j = i + 1; j < robot->getJointCount(); j++)
		{
			P3D jPos1 = robot->getJoint(i)->getWorldPosition();
			P3D jPos2 = robot->getJoint(j)->getWorldPosition();
			jPos1[0] *= -1;

			if ((jPos1 - jPos2).norm() < 1e-3)
			{
				jointSymmetryMap[i] = j;
				jointSymmetryMap[j] = i;
				RBSymmetryMap[robot->getJoint(i)->child] = robot->getJoint(j)->child;
				RBSymmetryMap[robot->getJoint(j)->child] = robot->getJoint(i)->child;
			}
		}
}

bool SkeletonMorphWindow::pickRB(double xPos, double yPos)
{
	RigidBody* tmpPick = NULL;
	P3D pLocal;
	double tMin = DBL_MAX;
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
		if (robot->getRigidBody(i)->getRayIntersectionPointTo(mouseRay, &pLocal)) {
			double tVal = mouseRay.getRayParameterFor(robot->getRigidBody(i)->getWorldCoordinates(pLocal));
			if (tVal < tMin) {
				tMin = tVal;
				tmpPick = robot->getRigidBody(i);
			}
		}

	highlightedRB = tmpPick;
	if (highlightedRB == NULL) {
		return false;
	}

	return true;
}

bool SkeletonMorphWindow::pickMesh(double xPos, double yPos)
{
	if (skinMesh) {
		Transformation invTrans = Transformation(guidingMeshRot.getRotationMatrix(), guidingMeshPos).inverse();
		Ray newRay(invTrans.transform(mouseRay.origin) / guidingMeshScale, invTrans.transform(mouseRay.direction) / guidingMeshScale);
		highlightedGuidingMesh = skinMesh->getDistanceToRayOriginIfHit(newRay);
	}

	return highlightedGuidingMesh;
}

bool SkeletonMorphWindow::pickJoint(double xPos, double yPos)
{
	Joint* tmpPick = NULL;
	double closestDist = 0.01;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		if (limbRootChildJoints.count(joint)) continue;

		P3D jPos = joint->getWorldPosition();
		double dist = mouseRay.getDistanceToPoint(jPos);
		if (dist < closestDist)
		{
			tmpPick = joint;
			closestDist = dist;
		}
	}

	highlightedJoint = tmpPick;

	if (!highlightedJoint)
		return false;

	return true;
}

bool SkeletonMorphWindow::pickRefJoint(double xPos, double yPos)
{
	Skeleton::Joint* pTmpPick = NULL;
	double closestDist = 0.01;

	int numJoints = refSkeleton.size();
	for (int i = 0; i < numJoints; ++i)
	{
		Skeleton::Joint* pJoint = refSkeleton.FindJoint(i);
		P3D jPos = refSkeleton.X(i);

		double dist = mouseRay.getDistanceToPoint(jPos);
		if (dist < closestDist)
		{
			pTmpPick = pJoint;
			closestDist = dist;
			highlightedRefJointPos = jPos;
		}
	}

	highlightedRefJoint = pTmpPick;
	if (!highlightedRefJoint)
		return false;

	return true;
}

bool SkeletonMorphWindow::pickEE(double xPos, double yPos)
{
	RigidBody* tmpPick = NULL;
	double closestDist = 0.005;

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		if (rb->rbProperties.endEffectorPoints.empty()) continue;

		P3D eePos = rb->getWorldCoordinates(rb->rbProperties.endEffectorPoints[0].coords);
		double dist = mouseRay.getDistanceToPoint(eePos);
		if (dist < closestDist)
		{
			tmpPick = rb;
			closestDist = dist;
		}
	}

	highlightedEE = tmpPick;

	if (!highlightedEE)
		return false;

	return true;
}

void SkeletonMorphWindow::scaleRigidBody(RigidBody* rb, double scale, bool changeBodyLength)
{
	vector<double> params;
	MRD->getCurrentSetOfParameters(params);
	int id = RBIndexMap[rb];

	if (id > 0)
	{
		params[id + 1] *= scale;
	}
	else {
		int index = changeBodyLength ? 1 : 0;
		params[index] *= scale;
	}

	MRD->setParameters(params);
}

void SkeletonMorphWindow::adjustJointToWorldPos(Joint* joint, const P3D& pos)
{
	joint->pJPos = joint->parent->getLocalCoordinates(pos);
	joint->cJPos = joint->child->getLocalCoordinates(pos);
}

void SkeletonMorphWindow::adjustJointToWorldPosSymm(Joint* joint, const P3D& pos)
{
	adjustJointToWorldPos(joint, pos);
	if (jointSymmetryMap.count(joint->jIndex))
	{
		Joint* symJoint = robot->getJoint(jointSymmetryMap[joint->jIndex]);
		P3D symPos = pos;
		symPos[0] = zeroState->getPosition()[0] * 2 - symPos[0];
		adjustJointToWorldPos(symJoint, symPos);
	}
}

void SkeletonMorphWindow::adjustEEToWorldPos(RigidBody* EE, const P3D& pos)
{
	EE->rbProperties.endEffectorPoints[0].coords =
		EE->getLocalCoordinates(pos);
}

void SkeletonMorphWindow::adjustEEToWorldPosSymm(RigidBody* EE, const P3D& pos)
{
	adjustEEToWorldPos(EE, pos);
	if (RBSymmetryMap.count(EE)) {
		P3D symPos = pos;
		symPos[0] = zeroState->getPosition()[0] * 2 - symPos[0];
		adjustEEToWorldPos(RBSymmetryMap[EE], symPos);
	}
}

void SkeletonMorphWindow::adjustRigidBody(RigidBody* rb)
{
	int num = 0;
	P3D center;

	for (auto joint : rb->pJoints)
	{
		center += joint->cJPos;
		num++;
	}

	for (auto joint : rb->cJoints)
	{
		center += joint->pJPos;
		num++;
	}

	for (auto& ee : rb->rbProperties.endEffectorPoints)
	{
		center += ee.coords;
		num++;
	}

	center /= num;

	for (auto joint : rb->pJoints)
		joint->cJPos -= center;

	for (auto joint : rb->cJoints)
		joint->pJPos -= center;

	for (auto& ee : rb->rbProperties.endEffectorPoints)
		ee.coords -= center;

	if (robot && rb == robot->getRoot()) {
		zeroState->setPosition(zeroState->getPosition() + center);
	}

	if (robotSimp && rb == robotSimp->getRoot()) {
		zeroStateSimp->setPosition(zeroStateSimp->getPosition() + center);
	}

	if (rb->rbProperties.endEffectorPoints.size() > 0) {
		for (auto cdp : rb->cdps)
			delete cdp;
		rb->cdps.clear();
		rb->cdps.push_back(new SphereCDP(rb->rbProperties.endEffectorPoints[0].coords, 0.01));
	}
}


void SkeletonMorphWindow::getIdealPoseFromState(ReducedRobotState* state)
{
	robot->setState(state);
	idealAxes.clear();
	idealJPos.clear();
	idealEEPos.clear();

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		HingeJoint* joint = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		V3D jAxis = joint->parent->getWorldCoordinates(joint->rotationAxis);
		idealAxes.push_back(jAxis);
		idealJPos.push_back(joint->getWorldPosition());
	}

	auto& limbs = robot->getEndEffectorRBs();
	for (int i = 0; i < (int)limbs.size(); i++)
	{
		RigidBody* limb = limbs[i];
		idealEEPos.push_back(limb->getWorldCoordinates(limb->rbProperties.endEffectorPoints[0].coords));
	}
}

void SkeletonMorphWindow::setIdealPoseToZeroState()
{
	robot->setState(zeroState);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		HingeJoint* joint = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		joint->rotationAxis = joint->parent->getLocalCoordinates(idealAxes[i]);
		adjustJointToWorldPos(joint, idealJPos[i]);
	}

	auto& limbs = robot->getEndEffectorRBs();
	for (int i = 0; i < (int)limbs.size(); i++)
	{
		RigidBody* limb = limbs[i];
		adjustEEToWorldPos(limb, idealEEPos[i]);
	}

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
		adjustRigidBody(robot->getRigidBody(i));
}

void SkeletonMorphWindow::swapRotationAxesSymm(HingeJoint* joint1, HingeJoint* joint2)
{
	swap(joint1->rotationAxis, joint2->rotationAxis);

	if (jointSymmetryMap.count(joint1->jIndex) && jointSymmetryMap.count(joint2->jIndex))
	{
		HingeJoint* symJoint1 = (HingeJoint*)robot->getJoint(jointSymmetryMap[joint1->jIndex]);
		HingeJoint* symJoint2 = (HingeJoint*)robot->getJoint(jointSymmetryMap[joint2->jIndex]);
		swap(symJoint1->rotationAxis, symJoint2->rotationAxis);
	}
}

///<
void SkeletonMorphWindow::saveRobot(const char* fName)
{
	if (robot) {
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
		{
			adjustRigidBody(robot->getRigidBody(i));
		}
		robot->saveRBSToFile((char*)fName);
		saveJointMap("../out/tmpJointMap.jm");
	}
}

void SkeletonMorphWindow::saveRobotSimp(const char* fName)
{
	if (robotSimp) {
		syncRobotSimpWithRobot();
		robotSimp->setState(zeroStateSimp);
		robotSimp->saveRBSToFile((char*)fName);
	}
}

///< Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void SkeletonMorphWindow::drawScene()
{
	robot->setState(zeroState);
	robotSimp->setState(zeroStateSimp);

	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		if (rb == highlightedRB || rb == pickedRB)
		{
			rb->selected = true;
		}
		else {
			rb->selected = false;
		}
	}

	int flags = SHOW_ABSTRACT_VIEW;// SHOW_MESH | SHOW_MATERIALS; SHOW_CD_PRIMITIVES
	glEnable(GL_LIGHTING);
	rbEngine->drawRBs(flags);
	// rbEngineSimp->drawRBs(flags);

	if (skinMesh)
	{
		glEnable(GL_NORMALIZE);
		glPushMatrix();
		glTranslated(guidingMeshPos[0], guidingMeshPos[1], guidingMeshPos[2]);
		V3D rotAxis; double rotAngle;
		guidingMeshRot.getAxisAngle(rotAxis, rotAngle);
		glRotated(DEG(rotAngle), rotAxis[0], rotAxis[1], rotAxis[2]);
		glScaled(guidingMeshScale, guidingMeshScale, guidingMeshScale);

		if (pickedGuidingMesh || highlightedGuidingMesh)
			skinMesh->getMaterial().setColor(1.0, 0.5, 0.0, 0.6);
		else
			skinMesh->getMaterial().setColor(0.8, 1.0, 1.0, 0.6);

		skinMesh->drawMesh();

		glPopMatrix();
		glDisable(GL_NORMALIZE);
	}

	if (highlightedJoint || pickedJoint)
	{
		P3D jPos = highlightedJoint ? highlightedJoint->getWorldPosition() : pickedJoint->getWorldPosition();
		glColor3d(1.0, 0.0, 0.0);
		drawSphere(jPos, 0.01, 12);
	}

	if (highlightedEE || pickedEE)
	{
		RigidBody* ee = highlightedEE ? highlightedEE : pickedEE;
		P3D eePos = ee->getWorldCoordinates(ee->rbProperties.endEffectorPoints[0].coords);
		glColor3d(1.0, 0.0, 0.0);
		drawSphere(eePos, 0.01, 12);
	}

	drawRefSkeleton();

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		HingeJoint* joint = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		P3D jPos = joint->getWorldPosition();
		V3D jAxis = joint->parent->getWorldCoordinates(joint->rotationAxis);
		glColor3d(0.0, 1.0, 0.0);
		drawArrow(jPos, jPos + jAxis * 0.05, 0.005, 12);
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void SkeletonMorphWindow::drawAuxiliarySceneInfo() {

	preDraw();
	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_VIEWPORT_BIT | GL_SCISSOR_BIT | GL_POINT_BIT | GL_LINE_BIT | GL_TRANSFORM_BIT);

	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);
	tWidget->draw();
	rWidget->draw();
	glPopAttrib();

	postDraw();
}

void SkeletonMorphWindow::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}

///<
void SkeletonMorphWindow::drawRefSkeleton()
{
	refSkeleton.setScaleDanger(refSkeletonScale);

	double linksScale = 3.0;
	double jointsScale = 6.0;
	refSkeleton.drawLinksAndJointSpheres(linksScale, jointsScale);

	/*
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glPushMatrix();
	glScaled(refSkeletonScale, refSkeletonScale, refSkeletonScale);

	int numJoints = refSkeleton.size();
	for (int i = 0; i < numJoints; ++i)
	{
	Skeleton::Joint* _joint = refSkeleton.FindJoint(i);
	//P3D() + m_rootVisDebugRot.rotate(X(i)) + m_rootVisDebugOffset;
	P3D _jpos = P3D() + refSkeleton.m_rootVisDebugRot.rotate(refSkeleton.X(i)) + refSkeleton.m_rootVisDebugOffset;

	if (i != 0) {
	P3D _jposprev = P3D() + refSkeleton.m_rootVisDebugRot.rotate(refSkeleton.X(_joint->m_pPrevious->m_id)) + refSkeleton.m_rootVisDebugOffset;
	glColor3d(1.0, 1.0, 1.0);
	drawCylinder(_jpos, _jposprev, 0.002 / refSkeletonScale, 12);
	}
	if (_joint == highlightedRefJoint)
	glColor3d(1.0, 0.0, 0.0);
	else
	glColor3d(1.0, 1.0, 0.0);
	drawSphere(_jpos, 0.007 / refSkeletonScale, 12);
	}
	glPopMatrix();
	*/
}