#include "RobotBodyPart.h"
#include "GUILib/GLContentManager.h"
#include "GUILib/OBJReader.h"
#include <iostream>
#include "direct.h"
#include "windows.h"
#include <MathLib/boundingBox.h>
#include "Motor_XL320.h"
#include "Motor_XM430.h"
#include "Motor_X5.h"
#include "Motor_X5_M.h"

#include "StructureFeature.h"

//why is the mirrored body part of the main body the main body?

using namespace std;

P3D mirror(const P3D& p) {
	return P3D(-p[0], p[1], p[2]);
}

V3D mirror(const V3D& v) {
	return V3D(-v[0], v[1], v[2]);
}

RobotBodyPart::RobotBodyPart(){
    selected = false;
	highlighted = false;
}

RobotBodyPart::~RobotBodyPart(){
}

void RobotBodyPart::addJointToBodyPart_XL320(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis) {
	if (child->pJoint == NULL) {
		RobotJointFeature* robotJointFeature = new Motor_XL320(this, child, pos, rotAxis);
		cJoints.push_back(robotJointFeature);
		child->pJoint = robotJointFeature;
//		Logger::consolePrint("Succesfully added a joint!\n");
	}
	else {
		Logger::consolePrint("trying to add a joint to a body part that already has a parent!\n");
	}
}

void RobotBodyPart::addJointToBodyPart_XM430(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis) {
	if (child->pJoint == NULL) {
		RobotJointFeature* robotJointFeature = new Motor_XM430(this, child, pos, rotAxis);
		cJoints.push_back(robotJointFeature);
		child->pJoint = robotJointFeature;
//		Logger::consolePrint("Succesfully added a joint!\n");
	}
	else {
		Logger::consolePrint("trying to add a joint to a body part that already has a parent!\n");
	}
}

void RobotBodyPart::addJointToBodyPart_X5(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis) {
	if (child->pJoint == NULL) {
		RobotJointFeature* robotJointFeature;
		if (pos.x() > 0)
			robotJointFeature = new Motor_X5(this, child, pos, rotAxis);
		else
			robotJointFeature = new Motor_X5_M(this, child, pos, rotAxis);

		cJoints.push_back(robotJointFeature);
		child->pJoint = robotJointFeature;
//		Logger::consolePrint("Succesfully added a joint!\n");
	}
	else {
		Logger::consolePrint("trying to add a joint to a body part that already has a parent!\n");
	}
}


void RobotBodyPart::addJointToBodyPart_MX28(RobotBodyPart* child, const P3D& pos, const V3D& rotAxis) {
	if (child->pJoint == NULL) {
		RobotJointFeature* robotJointFeature = new Motor_MX28(this, child, pos, rotAxis);
		cJoints.push_back(robotJointFeature);
		child->pJoint = robotJointFeature;
//		Logger::consolePrint("Succesfully added a joint!\n");
	} else {
		Logger::consolePrint("trying to add a joint to a body part that already has a parent!\n");
	}
}

void RobotBodyPart::addFeaturePoint(const P3D& pos, double featureSize){
	bodyFeatures.push_back(new RobotBodyFeature(this, pos, featureSize));
}

void RobotBodyPart::addEEFeaturePoint(const P3D& pos, double featureSize) {
	EEFeatures.push_back(new RobotEndEffectorFeature(this, pos, featureSize));
}

bool RobotBodyPart::containsFeature(BaseRobotBodyFeature* queryFeature){
	if (pJoint == queryFeature) return true;
	for (uint i = 0; i < cJoints.size(); i++)
		if (queryFeature == cJoints[i]) return true;
	for (uint i = 0; i < bodyFeatures.size(); i++)
		if (queryFeature == bodyFeatures[i]) return true;
	for (uint i = 0; i < EEFeatures.size(); i++)
		if (queryFeature == EEFeatures[i]) return true;

	return false;
}

/**
	We need to impose an ordering on how the structural parts of the body are formed. The assumption is that we connect things like so:
	COMPosition/pJoint -> (bFeature 1 -> ... bFeature n -> cJoint i)/endEffector. The functions below tell us where the connection point is for a joint
	which is either the parent of a body (so we want to know what it will connect to) or the child (so we want to know what connects to it).
*/
void RobotBodyPart::getStructureFeatures(DynamicArray<StructureFeature*>& structureFeatures) {
	//don't know what to do if there is no parent AND no body feature point...
	if (pJoint == NULL && bodyFeatures.size() == 0 && cJoints.size() <= 1)
		return;

	if (pJoint == NULL/* && bodyFeatures.size() == 0*/) {
		DynamicArray<AttachmentPoint> aps0;
		DynamicArray<AttachmentPoint> aps;
		cJoints[0]->getListOfPossibleAttachmentPoints(this, aps0);
		for (uint i = 1; i < cJoints.size(); i++)
			cJoints[i]->getListOfPossibleAttachmentPoints(this, aps);
		for (uint i = 0; i < bodyFeatures.size(); i++)
			bodyFeatures[i]->getListOfPossibleAttachmentPoints(this, aps);
		structureFeatures.push_back(new StructureFeature(aps0, aps));
		return;
	}

	if (bodyFeatures.size() > 0 && pJoint)
		structureFeatures.push_back(new StructureFeature(pJoint, bodyFeatures[0], this));
	for (uint i = 1; i < bodyFeatures.size();i++)
		structureFeatures.push_back(new StructureFeature(bodyFeatures[i-1], bodyFeatures[i], this));
	BaseRobotBodyFeature* lastFeature = pJoint;
	if (bodyFeatures.size() > 0)
		lastFeature = bodyFeatures[bodyFeatures.size() - 1];

	for (uint i = 0; i < cJoints.size(); i++)
		structureFeatures.push_back(new StructureFeature(lastFeature, cJoints[i], this));

	if (EEFeatures.size() > 0 && cJoints.size() == 0){
		DynamicArray<AttachmentPoint> parentaps;
		lastFeature->getListOfPossibleAttachmentPoints(this, parentaps);
		DynamicArray<AttachmentPoint> eeaps;
		for (uint i = 0; i < EEFeatures.size(); i++)
			EEFeatures[i]->getListOfPossibleAttachmentPoints(this, eeaps);
		structureFeatures.push_back(new StructureFeature(parentaps, eeaps));
	}
}

void RobotBodyPart::drawBodyParts(){
	//and now draw the various mechanical structures needed for this body part, remembering the order in which we generate structures...
	DynamicArray<StructureFeature*> structureFeatures;
	getStructureFeatures(structureFeatures);

	V3D baseColor(0.8 * 243/255.0, 0.8 * 245/255.0, 0.8 * 221/255.0);
	if (selected) baseColor = V3D(80 / 255.0, 36 / 255.0, 1 / 255.0);
	if (highlighted) baseColor *= 0.5;

	for (uint i = 0; i < structureFeatures.size(); i++) {
		structureFeatures[i]->draw(baseColor[0], baseColor[1], baseColor[2]);
		delete structureFeatures[i];
	}

	//draw all the various features
	if (pJoint) pJoint->draw();

	drawSymPair();
}

void RobotBodyPart::propagateChangesToMirroredBody() {
	if (mirroredBodyPart == NULL)
		return;

	if (pJoint && mirroredBodyPart->pJoint){
		mirroredBodyPart->pJoint->positionWorld = mirror(pJoint->positionWorld);
		mirroredBodyPart->pJoint->jointAxis = mirror(pJoint->jointAxis);

		//propagate also the embedding angle info...
		mirroredBodyPart->pJoint->autoGenerateEmbeddingAngle = pJoint->autoGenerateEmbeddingAngle;
		mirroredBodyPart->pJoint->embeddingAngle = -pJoint->embeddingAngle;
	}

	if (cJoints.size() == mirroredBodyPart->cJoints.size()) {
		for (uint i = 0; i < cJoints.size(); i++) {
			mirroredBodyPart->cJoints[i]->positionWorld = mirror(cJoints[i]->positionWorld);
			mirroredBodyPart->cJoints[i]->jointAxis = mirror(cJoints[i]->jointAxis);

			//propagate also the embedding angle info...
			mirroredBodyPart->cJoints[i]->autoGenerateEmbeddingAngle = cJoints[i]->autoGenerateEmbeddingAngle;
			mirroredBodyPart->cJoints[i]->embeddingAngle = -cJoints[i]->embeddingAngle;
		}
	}
	else {
		Logger::consolePrint("Number of child joints between mirrored body parts does not match up...\n");
	}

	if (bodyFeatures.size() == mirroredBodyPart->bodyFeatures.size()) {
		for (uint i = 0; i < bodyFeatures.size(); i++) {
			mirroredBodyPart->bodyFeatures[i]->positionWorld = mirror(bodyFeatures[i]->positionWorld);
			mirroredBodyPart->bodyFeatures[i]->featureSize = bodyFeatures[i]->featureSize;
		}
	}
	else {
		Logger::consolePrint("Number of body features between mirrored body parts does not match up...\n");
	}

	if (EEFeatures.size() == mirroredBodyPart->EEFeatures.size()) {
		for (uint i = 0; i < EEFeatures.size(); i++) {
			mirroredBodyPart->EEFeatures[i]->positionWorld = mirror(EEFeatures[i]->positionWorld);
			mirroredBodyPart->EEFeatures[i]->featureSize = EEFeatures[i]->featureSize;
		}
	}
	else {
		Logger::consolePrint("Number of end effector features between mirrored body parts does not match up...\n");
	}

	if (cJoints.size() == mirroredBodyPart->cJoints.size()) {
		for (uint i = 0; i < cJoints.size(); i++) {
			mirroredBodyPart->cJoints[i]->positionWorld = mirror(cJoints[i]->positionWorld);
			mirroredBodyPart->cJoints[i]->jointAxis = mirror(cJoints[i]->jointAxis);
		}
	}
	else {
		Logger::consolePrint("Number of child joints between mirrored body parts does not match up...\n");
	}

}


void RobotBodyPart::drawSymPair() {
	if (mirroredBodyPart != NULL) {
		glColor3d(1, 0.7, 0.7);
		glLineStipple(1, 0x0101);
		glEnable(GL_LINE_STIPPLE);
		glBegin(GL_LINES);
		P3D pi = getPosition();
		P3D pj = mirroredBodyPart->getPosition();
		glVertex3d(pi[0], pi[1], pi[2]);
		glVertex3d(pj[0], pj[1], pj[2]);
		glEnd();
		glDisable(GL_LINE_STIPPLE);
	}
}

void RobotBodyPart::drawFeatures() {
//	glDisable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	//	glCullFace(GL_FRONT);
	//	drawMeshElements();
	glCullFace(GL_BACK);
	//	drawMeshElements();
	for (uint i = 0; i < bodyFeatures.size(); i++)
		bodyFeatures[i]->draw();
	for (uint i = 0; i < EEFeatures.size(); i++)
		EEFeatures[i]->draw();

	glDisable(GL_CULL_FACE);
//	glEnable(GL_DEPTH_TEST);
}

void RobotBodyPart::addBodyFeaturesToList(DynamicArray<BaseRobotBodyFeature*>& fList) {
	//to avoid joints from being added to the list twice, we only add the parent here, and the child joints will be added to the list when their children get processed...
	if (pJoint)
		fList.push_back(pJoint);
	for (uint i = 0; i < bodyFeatures.size(); i++)
		fList.push_back(bodyFeatures[i]);
	for (uint i = 0; i < EEFeatures.size(); i++)
		fList.push_back(EEFeatures[i]);
}

AxisAlignedBoundingBox RobotBodyPart::getBoundingBox() {
	AxisAlignedBoundingBox bBox; bBox.empty();
	if (pJoint) bBox.addPoint(pJoint->positionWorld);
	for (uint i = 0; i < cJoints.size(); i++)
		bBox.addPoint(cJoints[i]->positionWorld);
	for (uint i = 0; i < bodyFeatures.size(); i++)
		bBox.addPoint(bodyFeatures[i]->positionWorld);
	if (cJoints.size() == 0)
		for (uint i = 0; i < EEFeatures.size(); i++)
			bBox.addPoint(EEFeatures[i]->positionWorld);
	return bBox;
}

//the position of the body part is infered from the various features that the body part has
P3D RobotBodyPart::getPosition() {
	//if this RB has nothing, what can we do...
	if (pJoint == NULL && cJoints.size() == 0 && bodyFeatures.size() == 0 && EEFeatures.size() == 0)
		return P3D();

	return getBoundingBox().center();
}

Matrix3x3 RobotBodyPart::getMOIForBox(const P3D& minPoint, const P3D& maxPoint){
	double w, h, d, m = getMassForBox(minPoint, maxPoint);

	V3D diff = maxPoint - minPoint;

	w = diff.x();
	h = diff.y();
	d = diff.z();

	double oneOver12 = 1.0 / 12.0;
	Matrix3x3 moi; moi.setZero();
	moi(0, 0) = m * (h*h + d*d) / 12.0;
	moi(1, 1) = m * (w*w + d*d) / 12.0;
	moi(2, 2) = m * (h*h + w*w) / 12.0;
	return moi;
}

double RobotBodyPart::getMassForBox(const P3D& minPoint, const P3D& maxPoint){
	double w, h, d;

	V3D diff = maxPoint - minPoint;

	w = diff.x();
	h = diff.y();
	d = diff.z();
	return w*h*d*massDensity;
}

Matrix3x3 RobotBodyPart::getMOIForSegment(const P3D &a, const P3D &b) {
	//first find the principal moments of inertia...
	double h = MAX(V3D(a, b).length(), 0.001), r = partRadius, m = getMassForSegment(a, b);

	Matrix3x3 paMoI; paMoI.setZero();
	paMoI(0, 0) = paMoI(1, 1) = m*(3 * r*r + h*h) / 12.0;
	paMoI(2, 2) = 0.5*m*r*r;

	//now, find the rotation that aligns the y axis to the vector between a and b
	V3D v1 = V3D(a, b).unit(), v2 = V3D(0.0, 1.0, 0.0), v3;
	if (v1.length() < 0.5) v1 = V3D(1, 0, 0);
	double dotP = v1.dot(v2);
	if (abs(dotP) < 1.0) {
		v2 = v2 - v1*dotP;
		v2.normalize();
		v3 = v1.cross(v2);
	}
	else {
		v2 = V3D(1.0, 0.0, 0.0)*dotP;
		v3 = v1.cross(v2);
	}
	Matrix3x3 rot;
	rot(0, 0) = v3.x(); rot(1, 0) = v3.y(); rot(2, 0) = v3.z();
	rot(0, 1) = v2.x(); rot(1, 1) = v2.y(); rot(2, 1) = v2.z();
	rot(0, 2) = v1.x();	rot(1, 2) = v1.y();	rot(2, 2) = v1.z();

	return rot * paMoI * rot.transpose();
}

double RobotBodyPart::getMassForSegment(const P3D &a, const P3D &b) {
	double A = PI * partRadius * partRadius;
	double h = V3D(a, b).length();
	h = MAX(h, 0.001);

	return A*h*massDensity;
}

P3D RobotBodyPart::getLocalCoordinates(const P3D& p) {
	return P3D() + (p - getPosition());
}

V3D RobotBodyPart::getLocalCoordinates(const V3D& v) {
	return v;
}

void RobotBodyPart::populateRB(RigidBody* rb){
	rb->name = this->name;
	for (uint i = 0; i < rb->cdps.size(); i++)
		delete rb->cdps[i];
	rb->cdps.clear();
	rb->rbProperties.frictionCoeff = 0.8;
	rb->rbProperties.restitutionCoeff = 0.2;

	rb->state = RBState();
	rb->state.position = getPosition();

/** compute CDPs, mass and moment of inertia */	
	
	//we have two cases to handle here... 
	if (pJoint && (cJoints.size() == 1 || (cJoints.size() == 0 && EEFeatures.size() > 0))) {
		//the body part has one parent and one child or one end effector - best approximate it by a tubular segment
		P3D p1 = pJoint->positionWorld;
		P3D p2;
		if (cJoints.size() == 1)
			p2 = cJoints[0]->positionWorld;
		else
			for (uint i = 0; i < EEFeatures.size(); i++)
				p2 += EEFeatures[i]->positionWorld / EEFeatures.size();

		//p1 and p2 are in world coordinates now, so switch these to local coordinates...
		p1 = getLocalCoordinates(p1);
		p2 = getLocalCoordinates(p2);

		rb->rbProperties.mass = getMassForSegment(p1, p2);
		rb->rbProperties.MOI_local = getMOIForSegment(p1, p2);

		//due to the size of the limb, the CDP could touch the ground at the wrong place... so adjust it a bit. 
		if (cJoints.size() == 0)
			p2 -= V3D(p1, p2).unit() * partRadius;

		rb->cdps.push_back(new CapsuleCDP(p1 + V3D(p1, p2).unit() * partRadius, p2 + V3D(p1, p2).unit() * -partRadius, partRadius));

		//now, handle the end effectors...
		if (cJoints.size() == 0) {
			for (uint i = 0; i < EEFeatures.size(); i++) {
				rb->rbProperties.addEndEffectorPoint(getLocalCoordinates(EEFeatures[i]->positionWorld), EEFeatures[i]->featureSize);
				rb->cdps.push_back(new SphereCDP(getLocalCoordinates(EEFeatures[i]->positionWorld), EEFeatures[i]->featureSize));
			}
		}
	}
	else {
		//lump all joints and body features into a box, and use that...
		AxisAlignedBoundingBox bBox = getBoundingBox();
		P3D halfSides = bBox.halfSides();
		if (halfSides[0] < 0.05) halfSides[0] = 0.05;
		if (halfSides[1] < 0.05) halfSides[1] = 0.05;
		if (halfSides[2] < 0.05) halfSides[2] = 0.05;

		P3D p1 = getLocalCoordinates(bBox.center() + halfSides*-1);
		P3D p2 = getLocalCoordinates(bBox.center() + halfSides);

		rb->rbProperties.mass = getMassForBox(p1, p2);
		rb->rbProperties.MOI_local = getMOIForBox(p1, p2);
		rb->cdps.push_back(new BoxCDP(p1, p2));
	}

	//now handle the body features...
	for (uint i = 0; i < bodyFeatures.size(); i++)
		rb->rbProperties.bodyPointFeatures.push_back(RBFeaturePoint(getLocalCoordinates(bodyFeatures[i]->positionWorld), bodyFeatures[i]->featureSize));
}

/**
	We need to impose an ordering on how the structural parts of the body are formed. The assumption is that we connect things like so:
	COMPosition/pJoint -> (bFeature 1 -> ... bFeature n -> cJoint i)/endEffector. The functions below tell us where the connection point is for a joint
	which is either the parent of a body (so we want to know what it will connect to) or the child (so we want to know what connects to it).
*/
P3D RobotBodyPart::getJointAttachmentPointAsParent() {
	//what will the pJoint of this body part connect to?

	//iff there are no joints, try to connect it to the end effectors
	if (cJoints.size() == 0) {
		if (EEFeatures.size() > 0){
			P3D p;
			for (uint i = 0; i < EEFeatures.size(); i++)
				p += EEFeatures[i]->positionWorld / EEFeatures.size();
			return p;
		}
	}

	//if there are body features, connect it to the first one
	if (bodyFeatures.size() > 0)
		return bodyFeatures[0]->positionWorld;

	if (cJoints.size() > 0) {
		P3D p;
		for (uint i = 0; i < cJoints.size(); i++)
			p += cJoints[i]->positionWorld / cJoints.size();
		return p;
	}

	return getPosition();
}

P3D RobotBodyPart::getJointAttachmentPointAsChild() {
	//for the child joints, walk backwards...
	//if there are body features, connect to the last one
	if (bodyFeatures.size() > 0)
		return bodyFeatures[bodyFeatures.size()-1]->positionWorld;

	if (pJoint)
		return pJoint->positionWorld;

	return getPosition();
}

//returns the distance from the ray's origin if the ray hits the body part, or -1 otherwise...
double RobotBodyPart::getDistanceToRayOriginIfHit(const Ray& ray) {
	double minDistance = -1;

	DynamicArray<StructureFeature*> structureFeatures;
	getStructureFeatures(structureFeatures);


	for (uint i = 0; i < structureFeatures.size(); i++) {
		double dist = structureFeatures[i]->getDistanceToRayOriginIfHit(ray);
		if (dist > 0 && (dist < minDistance || minDistance == -1))
			minDistance = dist;
		delete structureFeatures[i];
	}

	return minDistance;
}
