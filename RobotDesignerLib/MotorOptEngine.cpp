#include "MotorOptEngine.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <OptimizationLib/BFGSFunctionMinimizer.h>


MotorOptEngine::MotorOptEngine()
{
	plan = new MotorOptPlan();
	energyFunction = new MotorOptEnergyFunction(plan);
}


MotorOptEngine::~MotorOptEngine()
{
	delete energyFunction;
	delete plan;
}

void MotorOptEngine::initialize(vector<RMCRobot*>& rmcRobots)
{
	plan->initialize(rmcRobots);
}

double MotorOptEngine::optimizePlan(int maxIterNum) {
	// NewtonFunctionMinimizer minimizer(maxIterNum);
	BFGSFunctionMinimizer minimizer(maxIterNum);

	energyFunction->printDebugInfo = true;
	dVector params;
	this->plan->writeParamsToList(params);

	//energyFunction->testGradientWithFD(params);
	//energyFunction->testHessianWithFD(params);

	double val = 0;
	minimizer.minimize(energyFunction, params, val);
	
	energyFunction->printValue(params);

	return val;
}

