#include "ConnectorAlignObjective.h"


ConnectorAlignObjective::ConnectorAlignObjective(MotorOptPlan* plan, const std::string& objectiveDescription, double weight)
{
	this->plan = plan;
	this->description = objectiveDescription;
	this->weight = weight;
}


ConnectorAlignObjective::~ConnectorAlignObjective()
{
}

double ConnectorAlignObjective::computeValue(const dVector& p)
{
	double cost = 0;

	for (auto connector : plan->livingConnectors)
	{
		RMCPin* pin1 = connector->pins[0].getConnectedPin();
		RMCPin* pin2 = connector->pins[1].getConnectedPin();
		PinFace& face1 = pin1->face;
		PinFace& face2 = pin2->face;
		RMC* rmc1 = pin1->rmc;
		RMC* rmc2 = pin2->rmc;

		V3D centerVec = (rmc2->getWorldCoordinates(face2.center) - rmc1->getWorldCoordinates(face1.center)).normalized();
		
		cost += rmc2->getWorldCoordinates(face2.normal).dot(centerVec) - rmc1->getWorldCoordinates(face1.normal).dot(centerVec);
	}

	return cost * weight;
}
