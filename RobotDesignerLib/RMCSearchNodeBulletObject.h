#pragma once

#include <BulletCollision/btBulletCollisionCommon.h>
#include "RMCBulletObject.h"
#include "RMCSearchTree.h"

class RMCSearchNode;

class RMCSearchNodeBulletObject : public AbstractBulletObject {

public:
	RMCSearchNode* parent;

	BT_DECLARE_ALIGNED_ALLOCATOR();

	// constructor
	RMCSearchNodeBulletObject(RMCSearchNode* p, AbstractBulletObject* rmcBulletObject);

	RMCSearchNodeBulletObject* clone() {
		return NULL;
	}

	void loadFromFile(FILE* fp);
	btCollisionObject* getCollisionObject();

};
