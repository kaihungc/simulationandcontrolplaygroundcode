#pragma once

#include "LocomotionEngineEnergyFunction.h"
#include "LocomotionEngineMotionPlan.h"
#include "LocomotionEngineConstraints.h"
#include "OptimizationLib/ConstrainedObjectiveFunction.h"
#include <OptimizationLib/SQPFunctionMinimizer.h>
#include <OptimizationLib/SQPFunctionMinimizer_BFGS.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <OptimizationLib/BFGSFunctionMinimizer.h>
#include "OptimizationLib/LBFGS.h"

/**
	This is a locomotion engine that works for arbitrary robot types
*/
class LocomotionEngine{

public:
	LocomotionEngine(LocomotionEngineMotionPlan* motionPlan);
	virtual ~LocomotionEngine(void);

	double optimizePlan(int maxIterations = 1);
	double optimizePlan_BFGS(int maxIterations = 1);
public:
	LocomotionEngine_EnergyFunction* energyFunction;
	LocomotionEngine_Constraints* constraints;
	LocomotionEngineMotionPlan* motionPlan;

	ConstrainedObjectiveFunction* constrainedObjectiveFunction;
	
	SQPFunctionMinimizer_BFGS* BFGSSQPminimizer = NULL;
	BFGSFunctionMinimizer* BFGSminimizer = NULL;
	//LBFGSFunctionMinimizer* LBFGSminimizer = NULL;

	LBFGSpp::LBFGSParam<double> param;
	LBFGSpp::LBFGSSolver<double> solver;

	bool converged = false;

	//Beichen Li: This parameter determines whether hard constraints are added to the energy function or not
	bool useObjectivesOnly = true;

	//Beichen Li: This parameter controls the use of LBFGS external library
	bool useLBFGS = true;
	int maxIterations = 100;

	//Beichen Li: Old function value is recorded for regularizer update
	double oldFunctionValue = 0.0;

};



