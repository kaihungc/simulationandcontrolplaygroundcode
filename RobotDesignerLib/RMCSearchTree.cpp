#include "RMCSearchTree.h"
#include "ModularRobotCollisionManager.h"
#include "MathLib\Segment.h"

RMCSearchNode::RMCSearchNode() {}

RMCSearchNode::~RMCSearchNode() {
	for (uint i = 0; i < bulletObjects.size(); i++)
		delete bulletObjects[i];
}

RMCSearchNode::RMCSearchNode(RMC* rmc) {
	RMCName = rmc->name;
	position = rmc->state.position;
	orientation = rmc->state.orientation;
	type = rmc->type;

	for (int i = 0; i < (int)rmc->pins.size(); i++)
	{
		if (!rmc->pins[i].idle)
			usedPinIds.insert(i);
	}

	for (uint m = 0; m < rmc->bulletCollisionObjects.size(); m++)
	{
		bulletObjects.push_back(new RMCSearchNodeBulletObject(this, rmc->bulletCollisionObjects[m]));
	}
}



RMCSearchTree::RMCSearchTree(map<string, vector<Transformation>>& _transformationMap, vector<RMC *>& _rmcWarehouse, map<string, RMC *>& _rmcNameMap, ModularRobotCollisionManager* collisionManager)
	: transformationMap(_transformationMap), rmcWarehouse(_rmcWarehouse), rmcNameMap(_rmcNameMap)
{
	searchTreeCollisionManager = collisionManager;
}


RMCSearchTree::~RMCSearchTree()
{
	searchTreeCollisionManager->clearBulletCollisionWorld();
	deleteTree(root);
	delete target;
}

void RMCSearchTree::deleteTree(RMCSearchNode* node)
{
	for (uint i = 0; i < node->children.size(); i++)
		deleteTree(node->children[i]);

	delete node;
}

void RMCSearchTree::searchFromNode(RMCSearchNode* node)
{
	if (!node->children.empty()) return;

	RMC* nodeRmc = rmcNameMap[node->RMCName];

	bool needHornConnection = false;

	if (node == root)
	{
		// 0 is the pin id for horn connection
		if (node->usedPinIds.count(0) == 0)
		{
			needHornConnection = true;
		}
	}

	for (uint k = 0; k < nodeRmc->pins.size(); k++)
	{
		// if the pin is used then continue
		if (node->usedPinIds.count((int)k)) continue;

		RMCPin* nodePin = &nodeRmc->pins[k];
		for (uint i = 0; i < rmcWarehouse.size(); i++)
		{
			RMC* childRMC = rmcWarehouse[i];
			if (needHornConnection && childRMC->type != HORN_BRACKET_RMC) continue;
			if (childRMC->type == PLATE_RMC || childRMC->type == EE_RMC || childRMC->type == EE_HORN_RMC) continue;
		
			for (uint j = 0; j < childRMC->pins.size(); j++)
			{
				RMCPin* childPin = &childRMC->pins[j];
				if (!nodePin->isCompatible(childPin)) continue;

				// pins are compatible
				string key = nodePin->name + '+' + childPin->name;
				vector<Transformation> transformations;

				if (transformationMap.count(key)) {
					transformations = transformationMap[key];
				}
				else {
					Logger::print("Error in transformation map info!");
					return;
				}

				for (uint p = 0; p < transformations.size(); p++)
				{
					RMCSearchNode* childNode = new RMCSearchNode();
					Transformation trans = nodePin->transformation * transformations[p] * childPin->transformation.inverse();

					childNode->id = (int)node->children.size();
					childNode->parent = node;
					childNode->parentPinId = nodePin->id;
					childNode->childPinId = childPin->id;
					childNode->usedPinIds.insert(childPin->id);
					childNode->RMCName = childRMC->name;
					childNode->position = node->position + node->orientation.rotate(trans.T);
					childNode->orientation.setRotationFrom(node->orientation.getRotationMatrix() * trans.R);
					childNode->type = childRMC->type;
					childNode->relTransId = (int)p;

					for (uint m = 0; m < childRMC->bulletCollisionObjects.size(); m++)
					{
						childNode->bulletObjects.push_back(new RMCSearchNodeBulletObject(childNode, childRMC->bulletCollisionObjects[m]));
					}


					node->children.push_back(childNode);
				}
			}
		}
	}
}

void RMCSearchTree::drawPathRMC(RMCSearchNode* node)
{
	if (!node) return;

	drawNodeRMC(node);
	drawPathRMC(node->parent);
}

void RMCSearchTree::drawNodeRMC(RMCSearchNode* node)
{
	RMC* nodeRmc = rmcNameMap[node->RMCName];
	RBState origState = nodeRmc->state;

	nodeRmc->state.position = node->position;
	nodeRmc->state.orientation = node->orientation;

	nodeRmc->draw(SHOW_MESH, Vector4d(0, 0, 1, 1));

	nodeRmc->state = origState;
}

RMC* RMCSearchTree::buildPathSolution(RMCSearchNode* node) 
{
	if (node == root)
		return rootRMC;

	RMC* tailRMC = buildPathSolution(node->parent);
	if (!tailRMC) return NULL;

	if (node->RMCName != target->RMCName){
		RMC* nodeRMC = rmcNameMap[node->RMCName]->clone();
		rootRobot->connectRMC(nodeRMC, &tailRMC->pins[node->parentPinId], &nodeRMC->pins[node->childPinId], node->relTransId);
		return nodeRMC;
	}
	else {
		rootRobot->connectRMCRobot(targetRobot, &tailRMC->pins[node->parentPinId], &targetRMC->pins[node->childPinId], node->relTransId);
		return NULL;
	}

	return NULL;
}

void RMCSearchTree::addPathBulletObjectsToList(RMCSearchNode* node, DynamicArray<AbstractBulletObject*>& list)
{
	while (node != root)
	{
		for (uint i = 0; i < node->bulletObjects.size(); i++)
		{
			list.push_back(node->bulletObjects[i]);
		}
		node = node->parent;
	}
}

int RMCSearchTree::countConnectors(RMCSearchNode* node) {

	if (node == root)
		return 0;

	return countConnectors(node->parent) + (int)(node->type == CONNECTOR_RMC);
}

int RMCSearchTree::countTotalModules(RMCSearchNode* node) {

	if (node == root)
		return 0;

	if (node->type == MOTOR_RMC)
		return node->parent->moduleCount;
	else
		return node->parent->moduleCount + 1;
}

double RMCSearchTree::calculatePathCost_usingModuleDist(RMCSearchNode* node) {
	if (node == root) {
		return 0;
	}

	//double totalPathDistance = calculatePathCost_usingModuleDist(node->parent) + V3D(node->position,node->parent->position).length();
	double totalPathDistance = node->parent->pathCost + V3D(node->position, node->parent->position).length();
	return totalPathDistance;

}


double RMCSearchTree::calculateAestheticCost_usingMesh(RMCSearchNode* node) {

	if (node == root)
		return calculateMeshSDFCost(node);

	double meshCost = calculateMeshSDFCost(node);
	return node->parent->aestheticCost + meshCost;
}

double RMCSearchTree::calculateAestheticCost_usingStLine(RMCSearchNode* node) {

	if (node == root)
		return 0;

	double dist = 0;
	Segment StLine = Segment();
	StLine.a = root->position;
	if (target) {
		StLine.b = target->position;
		dist = V3D(StLine.getClosestPointTo(node->position), node->position).length();
	}
	return node->parent->aestheticCost + dist;
}

void RMCSearchTree::calculateTotalCost(RMCSearchNode* node) {
	node->heuristicCost = calculateHeuristicCost(node);
	if(pathCostWeight>0)
		node->pathCost = calculatePathCost_usingModuleDist(node);
	if(connectorCostWeight>0)
		node->moduleCount = countTotalModules(node);

	if (aestheticCostWeight > 0) {
		if (useMeshCost)
			node->aestheticCost = calculateAestheticCost_usingMesh(node);
		else
			node->aestheticCost = calculateAestheticCost_usingStLine(node);
	}

	//node->cost = heuristicWeight*node->heuristicCost + pathCostWeight*node->pathCost + connectorCostWeight*countConnectors(node);
	node->cost = heuristicWeight*node->heuristicCost + pathCostWeight*node->pathCost + connectorCostWeight*node->moduleCount +
		aestheticCostWeight* node->aestheticCost;

	// DO NOT NEED TO CHECK THIS FOR TREE SERACH...
	//if(node!=root)
		//checkHeuristicConsistency(node);
}

void RMCSearchTree::checkHeuristicConsistency(RMCSearchNode* node) {
	// DO NOT NEED TO CHECK THIS FOR TREE SERACH...
	//h(m) - h(n) <= g(m, n) (cost to go from m to n)
	
	double diffH = node->heuristicCost*heuristicWeight - node->parent->heuristicCost*heuristicWeight;
	// everything other than heuristic cost is path cost..
	double diffPathCost = (node->cost - node->heuristicCost*heuristicWeight) - (node->parent->cost -
		node->parent->heuristicCost*heuristicWeight);

	if (diffH > diffPathCost)
		Logger::consolePrint("inconsistent heuristics, delP:%lf, delH:%lf!\n", diffPathCost, diffH);

}

void RMCSearchTree::checkHeuristicAdmissibility(RMCSearchNode* node) {
	// this function is tested for node = proposed target/best found node
	//heuristic should be optimistic and underestimate the actual cost to go to the target..

	double H = heuristicWeight* V3D(root->position - target->position).length();// estimated heuristics to reach target from root
	double PathCost = pathCostWeight * (node->pathCost) + connectorCostWeight * countTotalModules(node)
		+ aestheticCostWeight * (node->aestheticCost); // cost to go --  cost to reach target from root..

	// we want estimated cost from the root to the target (H) to be less than the actual cost to go or path cost..
	Logger::logPrint("path cost wt:%lf, aesthetic wt:%lf, connectorWt: %lf\n", pathCostWeight, aestheticCostWeight, connectorCostWeight);
	Logger::logPrint("path cost:%lf, aesthetic cost:%lf, connector cost: %d\n", node->pathCost, node->aestheticCost,
		countTotalModules(node));

	if (H > PathCost)
		Logger::logPrint("inadmissible heuristics: g:%lf, h:%lf, numModules:%d !\n", PathCost, H, countTotalModules(node));
	else
		Logger::logPrint("Yaay! admissible heuristics: g:%lf, h:%lf, numModules:%d !\n", PathCost, H, countTotalModules(node));
}

void RMCSearchTree::prepareForSearch() {

	searchTimer.restart();
	minDist = 1e10;
	openSet = priority_queue<RMCSearchNode*, std::vector<RMCSearchNode*>, NodeCompare>();

	// add root node to openset
	calculateTotalCost(root);
	openSet.push(root);

	currentBestNode = NULL;
}

bool RMCSearchTree::searchNextNodeInQueue() {

	if (openSet.empty() || openSet.size() > 30000 || searchTimer.timeEllapsed()>10) return true;

	RMCSearchNode* currentNode = openSet.top();
	openSet.pop();
	nodesExpanded = nodesExpanded + 1;

	searchFromNode(currentNode);
	//Logger::consolePrint("Expanded current node (children:%d, cost:%lf), openSet size: %d \n", currentNode->children.size(),
		//currentNode->cost, openSet.size());

	for (uint i = 0; i < currentNode->children.size(); i++) {
		RMCSearchNode* childNode = currentNode->children[i];
		if (calculateCollisionCost(childNode) > 0) continue;
		
		// put it here instead of calculateNodeCost, because we may want to prune this branch.
		calculateTotalCost(childNode);
		//Logger::consolePrint("path cost:%lf, Hcost:%lf, total:%lf \n", childNode->pathCost,
			//childNode->heuristicCost, childNode->cost);

		if (childNode->type == MOTOR_RMC)
		{
			//double dist = childNode->cost;
			double dist = childNode->heuristicCost;
			if (dist < minDist)
			{
				minDist = dist;
				currentBestNode = childNode;
				//Logger::logPrint("Selected current motor node as best, distFromGoal: %lf \n", dist);
				//checkHeuristicAdmissibility(childNode);
			}
			//else if(dist == minDist && (childNode->aestheticCost < currentBestNode->aestheticCost ||
			//	childNode->moduleCount < currentBestNode->moduleCount))
			// accept only if the node is more desirable..
			else if (dist == minDist && ((connectorCostWeight*childNode->moduleCount +
				aestheticCostWeight* childNode->aestheticCost) <= (connectorCostWeight*currentBestNode->moduleCount +
					aestheticCostWeight* currentBestNode->aestheticCost)))
			{
				minDist = dist;
				currentBestNode = childNode;
				//Logger::logPrint("Selected current motor node as best, distFromGoal: %lf \n", dist);
				//checkHeuristicAdmissibility(childNode);
			}
		}
		else {
			openSet.push(childNode);
		}
	}
	//Logger::consolePrint("Openset size now: %d,\n", openSet.size());

	return false;
}

RMCSearchNode* RMCSearchTree::bruteForceSearch2() {
	Logger::consolePrint("Brute force search\n");

	prepareForSearch();

	while (!openSet.empty()) {
		bool res = searchNextNodeInQueue();
		if (res) break;
	}

	//Logger::consolePrint("Brute force search ends, no solution found! \n");
	if (currentBestNode == NULL)
	{
		Logger::print("brute force failed!");
	}
	Logger::consolePrint("ENDING brute force, returning best node with cost: %lf \n", minDist);
	return currentBestNode;
}

bool RMCSearchTree::compareRMCSearchNode(RMCSearchNode* node1, RMCSearchNode* node2) {
	bool sameNodes = true;

	// first check if these nodes correspond to same type of RMC
	if (strcmp(node1->RMCName.c_str(), node2->RMCName.c_str()) != 0) {
		sameNodes = false;
		return sameNodes;
	}

	// compare position and orientation
	/*if (abs(node1->orientation.dot(node2->orientation)) - 1 != 0) {
	sameNodes = false;
	return sameNodes;
	}*/
	if (V3D(node1->position - node2->position).length() > 0.05) {
		sameNodes = false;
		return sameNodes;
	}

	double differenceQuatScalar = abs((node1->orientation.getInverse()*node2->orientation).s);
	double angleThres = 10 * PI / (double)180;
	Logger::consolePrint("orientation compare angle (rad): %lf, threshold: %lf\n", 2 * acos(differenceQuatScalar), angleThres);
	if (2 * acos(differenceQuatScalar)>angleThres) {
		sameNodes = false;
		return sameNodes;
	}

	return sameNodes;
}


double RMCSearchTree::calculateHeuristicCost(RMCSearchNode* node) {

	double dist = 0;
	//double alpha = 1;
	//double beta = 1e-2;

	dist += heuristicWeight* V3D(node->position - target->position).length();
	//dist += beta * countConnectors(node);

	if (node->type == MOTOR_RMC)
	{		
		//dist += beta * countConnectors(node);

		V3D nodeAxis = node->orientation.rotate(rmcNameMap[node->RMCName]->motorAxis).normalized();
		V3D targetAxis = target->orientation.rotate(targetRMC->motorAxis).normalized();
		double dotProd = abs(nodeAxis.dot(targetAxis));
		dist += heuristicWeight * (1 - dotProd);
		double differenceQuatScalar = abs((node->orientation.getInverse()*target->orientation).s);
		dist += motorOrientationWeight * acos(differenceQuatScalar);
	}

	return dist;

	/*if (rmcNameMap[node->RMCName]->type == MOTOR_RMC) {
		double differenceQuatScalar = abs((node->orientation.getInverse()*target->orientation).s);
		dist += V3D(node->position - target->position).length();
		dist += alpha * acos(differenceQuatScalar);

		return dist;
	}
	else {
		searchFromNode(node);
		double count = 0;
		for (uint i = 0; i < node->children.size(); i++) {
			if (rmcNameMap[node->children[i]->RMCName]->type == MOTOR_RMC) {
				double differenceQuatScalar = abs((node->children[i]->orientation.getInverse()*target->orientation).s);
				dist += V3D(node->children[i]->position - target->position).length();
				dist += alpha * acos(differenceQuatScalar);
				count = count + 1;
			}
		}
		if(count>0)
			return dist / count;
		else {
			double differenceQuatScalar = abs((node->orientation.getInverse()*target->orientation).s);
			dist += V3D(node->position - target->position).length();
			dist += alpha * acos(differenceQuatScalar);

			return dist;
		}

	}*/

}

double RMCSearchTree::calculateCollisionCost(RMCSearchNode* node) {

	double weight = 1e5;
	double cost = 0;

	// add all the nodes along the path and check if the configuration is collision free
	searchTreeCollisionManager->setupBulletCollisionWorld(node);
	searchTreeCollisionManager->performCollisionDetection();

	for (unsigned int i = 0; i < searchTreeCollisionManager->collisionDepths.size(); i++)
	{
		cost += searchTreeCollisionManager->collisionDepths[i] * searchTreeCollisionManager->collisionDepths[i];
	}

	return weight*cost;
}

double RMCSearchTree::compareRMCSearchNodeToGoal(RMCSearchNode* node)
{
	double dist = 0;
	double alpha = 1e-2;

	double differenceQuatScalar = abs((node->orientation.getInverse()*target->orientation).s);
	dist += V3D(node->position - target->position).length();
	dist += alpha * acos(differenceQuatScalar);

	return dist;
}

double RMCSearchTree::calculateMeshSDFCost(RMCSearchNode* node)
{
	if (!meshSDF) return 0;

	P3D pos = node->position;
	double SDFVal;

	if (meshSDF->isInside(pos[0], pos[1], pos[2]))
		SDFVal = meshSDF->interpolateData(pos[0], pos[1], pos[2]);
	else
		SDFVal = DBL_MAX;

	return max(SDFVal, 0.0);
}