#pragma once
#include "StructureFeature.h"
#include <MathLib\V3D.h>
#include <vector>

#include <MathLib/mathLib.h>
#include <MathLib/Quaternion.h>

#include <ControlLib/Robot.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>

using namespace std;

class JointParameters {
public:
	P3D pJPos, cJPos;
	V3D rotationAxis;

	JointParameters(Joint* j) {
		pJPos = j->pJPos;
		cJPos = j->cJPos;
		rotationAxis = static_cast<HingeJoint *>(j)->rotationAxis;
	}

};

class EndEffectorParameters {
public:
	P3D coords;
	double featureSize;
	int jIndex, CPIndex;

	EndEffectorParameters(RBEndEffector *ee, int jIndex, int CPIndex = 0) {
		coords = ee->coords;
		featureSize = ee->featureSize;
		this->jIndex = jIndex;
		this->CPIndex = CPIndex;
	}

	EndEffectorParameters(const RBEndEffector& ee, int jIndex, int CPIndex = 0) {
		coords = ee.coords;
		featureSize = ee.featureSize;
		this->jIndex = jIndex;
		this->CPIndex = CPIndex;
	}
};

enum ParameterType {
	PARAMETER_RB,
	PARAMETER_JOINT_Z,
	PARAMETER_JOINT_Y
};

class DesignParameters {
public:
	double value;
	int objIndex;
	ParameterType type;

	DesignParameters(double value, int objIndex, ParameterType type = PARAMETER_RB) {
		this->value = value;
		this->objIndex = objIndex;
		this->type = type;
	}
};

/**
Given a set of parameters, instances of this class will output new robot morphologies.
*/

class ParameterizedRobotDesign {
public:
	Robot* robot;
	LocomotionEngineMotionPlan* motionPlan;

	//the morphology of the design is given by the parameterization of each joint (e.g. position in child and parent coords, rotation axis, etc).
	//we will store the initial morphology here
	DynamicArray<JointParameters> initialMorphology;
	DynamicArray<EndEffectorParameters> initialEndEffectors;

	ParameterizedRobotDesign(Robot* robot, LocomotionEngineMotionPlan* motionPlan = NULL);
	~ParameterizedRobotDesign();

	virtual void getCurrentSetOfParameters(DynamicArray<double>& params);
	virtual void setParameters(const DynamicArray<double>& params);


};

#define TESTPARAMS		11

class TestParameterizedRobotDesign : public ParameterizedRobotDesign {
public:
	DynamicArray<double> currentParams;

	//Beichen Li: inital params for quick initialization
	const double initParams[TESTPARAMS] = {1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};

	TestParameterizedRobotDesign(Robot* robot) : ParameterizedRobotDesign(robot, NULL) {
		//should probably check that this really is the morphology we expect...
		currentParams.resize(TESTPARAMS, 0);
		
		//Beichen Li: initialize user parameters
		for (int i = 0; i < TESTPARAMS; ++i)
			currentParams[i] = initParams[i];
	}

	~TestParameterizedRobotDesign() {
	}

	void getCurrentSetOfParameters(DynamicArray<double>& params) {
		params = currentParams;
	}

	void setParameters(const DynamicArray<double>& params) {
		currentParams = params;

		//offset the positions of the hip joints, symetrically...

		int end = currentParams.size();

		//Beichen Li: each parameter is defined by the following section
		//param 0-1 adjust the upper joints with respect to root
		for (Joint *j : robot->root->cJoints) {
			j->pJPos.x() = initialMorphology[j->jIndex].pJPos.x() * params[0];
			j->pJPos.z() = initialMorphology[j->jIndex].pJPos.z() * params[1];
		}

		//param 2-4 adjust the length of limbs at different layers
		for (int i = 2; i < 5; i++)
			for (int j = 0; j < 4; j++) {
				int thisIndex = (i - 2) << 2 | j;
				Joint *thisJoint = robot->getJoint(thisIndex);
				thisJoint->cJPos = initialMorphology[thisIndex].cJPos * params[i];

				if (thisJoint->child != NULL) {
					for (Joint *cJoint : thisJoint->child->cJoints)
						cJoint->pJPos = initialMorphology[cJoint->jIndex].pJPos * params[i];

					//Beichen Li: for now every end effector can be adjusted
					int cEECount = thisJoint->child->rbProperties.getEndEffectorPointCount();
					for (int k = 0; k < cEECount; k++) {
						P3D eeCoords = initialEndEffectors[thisJoint->child->rbProperties.getEndEffectorIndex(k)].coords;
						thisJoint->child->rbProperties.setEndEffectorPoint(0, eeCoords * params[i]);
					}
				}
			}

		//param 5-7 rotate the joint axes (around Z axis) at different layers
		//param 8-10 rotate the joint axes (around Y axis) at different layers
		for (int i = 5; i < 8; i++)
			for (int j = 0; j < 4; j++) {
				int thisIndex = (i - 5) << 2 | j;
				HingeJoint *thisJoint = static_cast<HingeJoint *>(robot->getJoint(thisIndex));
				thisJoint->rotationAxis = initialMorphology[thisJoint->jIndex].rotationAxis.
					rotate(params[i] * M_PI, V3D(0, 0, 1)).
					rotate(params[i + 3] * M_PI, V3D(0, 1, 0));
			}

		/*robot->getJoint(0)->pJPos.x() = initialMorphology[0].pJPos.x() + params[0];
		robot->getJoint(1)->pJPos.x() = initialMorphology[1].pJPos.x() - params[0];
		robot->getJoint(2)->pJPos.x() = initialMorphology[2].pJPos.x() + params[0];
		robot->getJoint(3)->pJPos.x() = initialMorphology[3].pJPos.x() - params[0];

		robot->getJoint(0)->pJPos.z() = initialMorphology[0].pJPos.z() + params[1];
		robot->getJoint(1)->pJPos.z() = initialMorphology[1].pJPos.z() + params[1];
		robot->getJoint(2)->pJPos.z() = initialMorphology[2].pJPos.z() - params[1];
		robot->getJoint(3)->pJPos.z() = initialMorphology[3].pJPos.z() - params[1];

		robot->getJoint(0)->pJPos.y() = initialMorphology[0].pJPos.y() + params[2];
		robot->getJoint(1)->pJPos.y() = initialMorphology[1].pJPos.y() + params[2];
		robot->getJoint(2)->pJPos.y() = initialMorphology[2].pJPos.y() + params[3];
		robot->getJoint(3)->pJPos.y() = initialMorphology[3].pJPos.y() + params[3];

		robot->getJoint(4)->pJPos.x() = initialMorphology[4].pJPos.x() + params[4];
		robot->getJoint(5)->pJPos.x() = initialMorphology[5].pJPos.x() - params[4];
		robot->getJoint(6)->pJPos.x() = initialMorphology[6].pJPos.x() + params[4];
		robot->getJoint(7)->pJPos.x() = initialMorphology[7].pJPos.x() - params[4];

		robot->getJoint(4)->pJPos.z() = initialMorphology[4].pJPos.z() + params[5];
		robot->getJoint(5)->pJPos.z() = initialMorphology[5].pJPos.z() + params[5];
		robot->getJoint(6)->pJPos.z() = initialMorphology[6].pJPos.z() - params[5];
		robot->getJoint(7)->pJPos.z() = initialMorphology[7].pJPos.z() - params[5];

		robot->getJoint(4)->pJPos.y() = initialMorphology[4].pJPos.y() + params[6];
		robot->getJoint(5)->pJPos.y() = initialMorphology[5].pJPos.y() + params[6];
		robot->getJoint(6)->pJPos.y() = initialMorphology[6].pJPos.y() + params[7];
		robot->getJoint(7)->pJPos.y() = initialMorphology[7].pJPos.y() + params[7];

		robot->getJoint(8)->pJPos.x() = initialMorphology[8].pJPos.x() + params[8];
		robot->getJoint(9)->pJPos.x() = initialMorphology[9].pJPos.x() - params[8];
		robot->getJoint(10)->pJPos.x() = initialMorphology[10].pJPos.x() + params[8];
		robot->getJoint(11)->pJPos.x() = initialMorphology[11].pJPos.x() - params[8];

		robot->getJoint(8)->pJPos.z() = initialMorphology[8].pJPos.z() + params[9];
		robot->getJoint(9)->pJPos.z() = initialMorphology[9].pJPos.z() + params[9];
		robot->getJoint(10)->pJPos.z() = initialMorphology[10].pJPos.z() - params[9];
		robot->getJoint(11)->pJPos.z() = initialMorphology[11].pJPos.z() - params[9];

		robot->getJoint(8)->pJPos.y() = initialMorphology[8].pJPos.y() + params[10];
		robot->getJoint(9)->pJPos.y() = initialMorphology[9].pJPos.y() + params[10];
		robot->getJoint(10)->pJPos.y() = initialMorphology[10].pJPos.y() + params[11];
		robot->getJoint(11)->pJPos.y() = initialMorphology[11].pJPos.y() + params[11];

		robot->getJoint(8)->cJPos.y() = initialMorphology[8].cJPos.y() + params[12];
		robot->getJoint(9)->cJPos.y() = initialMorphology[9].cJPos.y() + params[12];
		robot->getJoint(10)->cJPos.y() = initialMorphology[10].cJPos.y() + params[13];
		robot->getJoint(11)->cJPos.y() = initialMorphology[11].cJPos.y() + params[13];*/


	}

	//Bechen Li: get end effector position based on joint index
	P3D getInitEndEffectorPointByIndex(int jIndex, int CPIndex = 0) {
		for (EndEffectorParameters eeParam : initialEndEffectors) {
			if (eeParam.jIndex == jIndex && eeParam.CPIndex == CPIndex)
				return eeParam.coords;
		}
		return P3D();
	}

};

class AutoParameterizedRobotDesign : public ParameterizedRobotDesign {
public:
	DynamicArray<DesignParameters> currentParams;
	DynamicArray<V3D> axisZ;
	DynamicArray<V3D> axisY;

	bool enableLimbSize = true;
	bool enableMotorAxis = false;

	AutoParameterizedRobotDesign(Robot *robot): ParameterizedRobotDesign(robot, NULL) {
		detectParameters(robot);
	}

	void detectParameters(Robot *robot) {
		currentParams.clear();
		int rbCount = robot->getRigidBodyCount();
		int jCount = robot->getJointCount();
		
		//Beichen Li: rigid body length parameters
		if (enableLimbSize) {
			for (int i = 0; i < rbCount; i++)
				currentParams.push_back(DesignParameters(1.0, i, PARAMETER_RB));
		}

		//Beichen Li: joint rotation axis parameters
		if (enableMotorAxis) {
			for (int i = 0; i < jCount; i++) {
				V3D rotationAxis = static_cast<HingeJoint *>(robot->getJoint(i))->rotationAxis.unit();

				//Beichen Li: obtain the rotation axes as the design parameters of motor rotation axis
				V3D axis1, axis2;
				rotationAxis.getOrthogonalVectors(axis1, axis2);
				axisZ.push_back(axis1);
				axisY.push_back(axis2);

				//Beichen Li: it should be noticed that Z and Y here does not refer to axes in world coordinates
				//They are calculated so that the real motor axis could be changed as we expect
				currentParams.push_back(DesignParameters(0.0, i, PARAMETER_JOINT_Z));
				currentParams.push_back(DesignParameters(0.0, i, PARAMETER_JOINT_Y));

				//Logger::logPrint("Motor %d:\n", i);
				//Logger::logPrint("  R Axis: %.6lf %.6lf %.6lf\n", rotationAxis.x(), rotationAxis.y(), rotationAxis.z());
				//Logger::logPrint("  Z Axis: %.6lf %.6lf %.6lf\n", axis1.x(), axis1.y(), axis1.z());
				//Logger::logPrint("  Y Axis: %.6lf %.6lf %.6lf\n", axis2.x(), axis2.y(), axis2.z());
			}
		}
	}

	void applyParameterToRobot(int i, Robot *robot) {
//		if (i < 0 || i >= (int)currentParams.size())
//			return;

		RigidBody *rb = NULL;
		HingeJoint *j = NULL;
		int objIndex = currentParams[i].objIndex;
		double value = currentParams[i].value;

		//Now apply the parameter to robot according to its type

		switch (currentParams[i].type) {
			//Rigid body parameter for the scale
			case PARAMETER_RB:
				rb = robot->getRigidBody(objIndex);
				if (objIndex > 0) {
					j = static_cast<HingeJoint *>(robot->getJoint(objIndex - 1));
					j->cJPos = initialMorphology[j->jIndex].cJPos * value;
				}
				for (Joint *cj : rb->cJoints)
					cj->pJPos = initialMorphology[cj->jIndex].pJPos * value;
				
				for (int k = 0; k < rb->rbProperties.getEndEffectorPointCount(); k++) {
					int eeIndex = rb->rbProperties.getEndEffectorIndex(k);
					rb->rbProperties.setEndEffectorPoint(k, initialEndEffectors[eeIndex].coords * value);
				}
				break;

			//Joint parameter for the Z component of rotation axis
			//for each joint its Z and Y parameter should be processed in order (Z -> Y)
			case PARAMETER_JOINT_Z:
				j = static_cast<HingeJoint *>(robot->getJoint(objIndex));
				j->rotationAxis = initialMorphology[j->jIndex].rotationAxis.rotate(value * M_PI, axisZ[objIndex]);
				break;

			//Joint parameter for the Y component of rotation axis
			case PARAMETER_JOINT_Y:
				j = static_cast<HingeJoint *>(robot->getJoint(objIndex));
				j->rotationAxis = j->rotationAxis.rotate(value * M_PI, axisY[objIndex]);
				break;
		}
	}

	void getCurrentSetOfParameters(DynamicArray<double>& params) {
		int end = currentParams.size();
		params.resize(end);

		for (int i = 0; i < end; i++)
			params[i] = currentParams[i].value;
	}

	//Beichen Li: added the same implementation as DynamicArray version for dVector
	void getCurrentSetOfParameters(dVector& params) {
		int end = currentParams.size();
		params.resize(end);

		for (int i = 0; i < end; i++)
			params[i] = currentParams[i].value;
	}

	void setParameters(const DynamicArray<double>& params) {
		int end = currentParams.size();

		for (int i = 0; i < end; i++) {
			currentParams[i].value = params[i];
			applyParameterToRobot(i, this->robot);
		}
	}

	//Beichen Li: added the same implementation for DynamicArray version for dVector
	void setParameters(const dVector& params) {
		int end = currentParams.size();

		for (int i = 0; i < end; i++) {
			currentParams[i].value = params[i];
			applyParameterToRobot(i, this->robot);
		}
	}
};
