#pragma once
#include "direct.h"
#include <MathLib/Ray.h>
#include <GUILib/GLUtils.h>
#include <GLFW/glfw3.h>
#include <MathLib/MathLib.h>
#include <MathLib\V3D.h>
#include <GUILib/GLMesh.h>
#include <vector>
#include <MathLib/Quaternion.h>
#include "RBSimLib/HingeJoint.h"

using namespace std;

struct AttachmentPoint {
    P3D point;
    V3D normal;
	bool shouldPrune;
    bool isActive;
    AttachmentPoint(const P3D& p, const V3D& n) {
        point = p;
        normal = n;
        isActive = true;
		shouldPrune = true;
    }

	AttachmentPoint(const P3D& p) {
		point = p;
		isActive = true;
		shouldPrune = false;
	}

};

class RobotBodyPart;
class RobotDesign;

/**
	RobotFeature is a base class for representing various types of features - joints, COMs, end effectors, etc.
*/
class BaseRobotBodyFeature {
public:
	//every body feature belongs to a body part
	RobotBodyPart* parent = NULL;

	char *name = "unnamed body feature";

	//for collision detection purposes...
	double featureSize = 0.03;
public:
	bool selected = false;
	bool highlighted = false;
	bool stickToYZPlane = false;

	//for each feature, we will work (== edit) its world coordinates position. Since all orientations are zero, this should be intuitive.
	P3D positionWorld;
public:
	BaseRobotBodyFeature() {}
    virtual ~BaseRobotBodyFeature() {}

	//returns the distance from the ray's origin if the ray hits the body part, or -1 otherwise...
	double getDistanceToRayOriginIfHit(const Ray& ray) {
		P3D p;
		double dist = ray.getDistanceToPoint(positionWorld, &p);
		if (dist < featureSize)
			return ray.getRayParameterFor(p);
		return -1;
	}
	
	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints) {}

	// draw this feature
	virtual void draw() {}

	virtual void setPosition(const P3D& newPos, bool propagateChangesDownStream = false);

	virtual void rotateAboutPivotPoint(const Quaternion& rot, const P3D& pos);

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() { return DynamicArray<std::pair<char*, double*>>(); }
	virtual DynamicArray<std::pair<char*, bool*>> getObjectFlags() { return DynamicArray<std::pair<char*, bool*>>(); }

};

//but it will always corresponds to (0,0,0) in the local coordinate frame of the body
class RobotBodyFeature : public BaseRobotBodyFeature {
protected:

public:
	RobotBodyFeature(RobotBodyPart* parent, const P3D& pos, double featureSize = 0.05) { this->parent = parent;  this->positionWorld = pos; this->featureSize = featureSize; this->name = "body feature";
	}

	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints) {
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D( 1, 1, 1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D( 1, 1,-1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D( 1,-1, 1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D( 1,-1,-1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, 1, 1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, 1,-1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1,-1, 1) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1,-1,-1) * featureSize));
	}

	void draw() {
		V3D baseColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0);
		if (selected) baseColor = V3D(80 / 255.0, 36 / 255.0, 1 / 255.0);
		if (highlighted) baseColor *= 0.5;

		glColor4d(baseColor[0], baseColor[1], baseColor[2], 0.5);
		drawSphere(positionWorld, featureSize);
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> params;
		params.push_back(std::pair<char*, double*>("featureSize", &featureSize));
		return params;
	}

};

class RobotJointFeature : public BaseRobotBodyFeature {
public:
	RobotBodyPart* child = NULL;
	// rotation axis, expressed in world coords == child coords == parent coords, since we are editing the structure in its "zero pose"
	V3D jointAxis;
	double motorAngleMin;
	double motorAngleMax;

	DXL_Properites dynamixelProperties;

	GLMesh *motorMeshParent, *motorMeshChild;
	GLMesh *parentSupportMesh, *childSupportMesh;

	bool autoGenerateEmbeddingAngle = true;
	double embeddingAngle = 0;

	std::string originalJointName;

public:
	RobotJointFeature(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis = V3D(0, 0, 1));

	V3D getLocalCoordinatesRotationAxis();

	P3D getWorldLocationForPointOnMotorParentSide(const P3D& p);
	V3D getWorldLocationForVectorOnMotorParentSide(const V3D& v);

	P3D getWorldLocationForPointOnMotorChildSide(const P3D& p);
	V3D getWorldLocationForVectorOnMotorChildSide(const V3D& v);

	//the motor embedding angle allows us to change the orientation of the motor as it is embedded within the joint
	double getMotorEmbeddingAngle();
	//and this one is similar, but changes the "rest" (i.e. zero) angle between the motor and child embedding
	double getMotorChildFrameEmbeddingAngle();

	void draw();

	Quaternion getOrientation();

	virtual void setPosition(const P3D& newPos, bool propagateChangesDownStream = false);
	virtual void setJointAxis(const V3D& newJointAxis, bool propagateChangesDownStream = false, bool isFirstJointInChain = true);
	virtual void rotateAboutPivotPoint(const Quaternion& rot, const P3D& pos);


	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() { 
		DynamicArray<std::pair<char*, double*>> params; 
		params.push_back(std::pair<char*, double*>("emebdding angle", &embeddingAngle));
		return params;
	}

	virtual DynamicArray<std::pair<char*, bool*>> getObjectFlags() {
		DynamicArray<std::pair<char*, bool*>> params;
		params.push_back(std::pair<char*, bool*>("autogen emebdding angle", &autoGenerateEmbeddingAngle));
		return params;
	}

};

class RobotEndEffectorFeature : public BaseRobotBodyFeature {
public:
	RobotEndEffectorFeature(RobotBodyPart* parent, const P3D& pos, double featureSize = 0.02) { this->parent = parent; this->positionWorld = pos; this->featureSize = featureSize; this->name = "end effector feature"; }

	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints) {
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(1, 1, 1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(1, 1, -1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(1, -1, 1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(1, -1, -1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, 1, 1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, 1, -1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, -1, 1) * featureSize + V3D(0, 1, 0) * featureSize));
		attachmentPoints.push_back(AttachmentPoint(positionWorld + V3D(-1, -1, -1) * featureSize + V3D(0, 1, 0) * featureSize));
	}

	void draw() {
		return;

		V3D baseColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0);
		if (selected) baseColor = V3D(80 / 255.0, 36 / 255.0, 1 / 255.0);
		if (highlighted) baseColor *= 0.5;

		glColor4d(baseColor[0], baseColor[1], baseColor[2], 0.5);
		drawSphere(positionWorld, featureSize);
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> params;
		params.push_back(std::pair<char*, double*>("featureSize", &featureSize));
		return params;
	}

};
