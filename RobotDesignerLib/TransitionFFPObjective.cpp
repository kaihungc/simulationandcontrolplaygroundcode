#include "TransitionFFPObjective.h"


TransitionFFPObjective::TransitionFFPObjective(TransitionFFPOptPlan* plan, const std::string& objectiveDescription, double weight)
{
	this->plan = plan;
	this->description = objectiveDescription;
	this->weight = weight;

	regularizer = 1e-6;
}


TransitionFFPObjective::~TransitionFFPObjective()
{
}

void TransitionFFPObjective::setCurrentBestSolution(const dVector& p)
{
	plan->setParamsFromList(p);
	m_p0 = p;
}

double TransitionFFPObjective::computeValue(const dVector& p)
{
	plan->setParamsFromList(p);

	double cost = 0;

	if (regularizer > 0)
	{
		if (m_p0.size() != p.size()) m_p0 = p;
		dVector tmpVec = p - m_p0;
		cost += 0.5*regularizer*tmpVec.dot(tmpVec);
	}

	int nSamplePoints = plan->transFFP->strideSamplePoints;

	for (int i = 0; i < (int)plan->planStepPatterns.size(); i++)
	{
		auto& planSP = plan->planStepPatterns[i];
		auto& startSP = plan->startStepPatterns[i];
		auto& endSP = plan->endStepPatterns[i];

	}

	return cost * weight;
}
