#include "BodyPartPointFeature.h"
#include "GUILib/GLContentManager.h"

BodyPartPointFeature::BodyPartPointFeature(const P3D& localCoords, P3D _position, double size, RobotBodyPart* _parent) :StructureFeature(RobotNodeType::FEATURE, _position, _parent){
	this->localCoords = localCoords;
	this->size = size;
}

void BodyPartPointFeature::draw(){
	glEnable(GL_LIGHTING);
	//glColor4d(0.9,0.9,0.9,1);
    double deltColor = 0.0;
    if (mouseOn)
        deltColor = 0.3;
    if (!selected)
    {
        if (colorType == COLORTYPE::ORIGINAL_COLOR)
        {
            glColor4d(MAX(0.0, 152.0 / 256.0 - deltColor), MAX(0.0, 204.0 / 256.0 - deltColor), MAX(0.0, 235.0 / 256.0 - deltColor), 1.0);
        }
        else if (colorType == COLORTYPE::FAILED_COLOR)
        {
            glColor4d(MAX(0.0, 0.8 - deltColor), 0, 0, 1.0);
        }
        else if (colorType == COLORTYPE::SUCCESSFUL_COLOR)
        {
            glColor4d(0.0, MAX(0.0, 0.9 - deltColor), 0, 1.0);
        }
    }
    else
        glColor4d(MAX(0.0, 104.0 / 256.0 - deltColor), MAX(0.0, 52.0 / 256.0 - deltColor), MAX(0.0, 21.0 / 256.0 - deltColor), 1.0);
	drawSphere(position, size*0.8, 15);
	glDisable(GL_LIGHTING);
    if (symPairID != -1 && symPairNode->exist) {
        glColor3d(1, 0.7, 0.7);
        glLineStipple(1, 0x0101);
        glEnable(GL_LINE_STIPPLE);
        glBegin(GL_LINES);
        P3D pi = position;
        P3D pj = symPairNode->getPosition();
        glVertex3d(pi[0], pi[1], pi[2]);
        glVertex3d(pj[0], pj[1], pj[2]);
        glEnd();
        glDisable(GL_LINE_STIPPLE);
    }
}

void BodyPartPointFeature::getCompleteListOfPossibleAttachmentPoints(bool isStart, vector<AttachmentPoint>& attachmentPoints){
	attachmentPoints.clear();
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,1)*size/2), orientation * V3D(1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,1)*size/2), orientation * V3D(0,1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,1)*size/2), orientation * V3D(0,0,1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,-1)*size/2), orientation * V3D(1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,-1)*size/2), orientation * V3D(0,1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,1,-1)*size/2), orientation * V3D(0,0,-1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,1)*size/2), orientation * V3D(1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,1)*size/2), orientation * V3D(0,-1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,1)*size/2), orientation * V3D(0,0,1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,1)*size/2), orientation * V3D(-1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,1)*size/2), orientation * V3D(0,1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,1)*size/2), orientation * V3D(0,0,1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,1)*size/2), orientation * V3D(-1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,1)*size/2), orientation * V3D(0,-1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,1)*size/2), orientation * V3D(0,0,1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,-1)*size/2), orientation * V3D(-1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,-1)*size/2), orientation * V3D(0,1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,1,-1)*size/2), orientation * V3D(0,0,-1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,-1)*size/2), orientation * V3D(1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,-1)*size/2), orientation * V3D(0,-1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(1,-1,-1)*size/2), orientation * V3D(0,0,-1)));

	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,-1)*size/2), orientation * V3D(-1,0,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,-1)*size/2), orientation * V3D(0,-1,0)));
	attachmentPoints.push_back(AttachmentPoint(position + orientation.rotate(V3D(-1,-1,-1)*size/2), orientation * V3D(0,0,-1)));

}
// create an sphere
string BodyPartPointFeature::generateObj()
{
    GLMesh* objMesh = GLContentManager::getGLMesh("resource/featurePoint-2.obj")->clone();
    char buffer[256];
    string filename;
    sprintf(buffer, "tmp\\%d.obj", (int)this);
    filename = buffer;
    FILE* fp = fopen(filename.c_str(), "w");
    objMesh->scale(size / 13.0, P3D(0, 0, 0));
    objMesh->translate(position);
    objMesh->renderToObjFile(fp, 0, getRotationQuaternion(0, V3D(0, 0, 1)), P3D(0, 0, 0));
    fclose(fp);

    return filename;
}

