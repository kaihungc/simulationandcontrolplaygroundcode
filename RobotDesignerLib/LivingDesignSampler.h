#pragma once
#include "LivingBracketParametrizedDesign.h"

class LivingDesignSampler
{
public:
	ModularDesignWindow* modularDesign = NULL;
	LivingBracketParametrizedDesign* livingDesign = NULL;

public:
	LivingDesignSampler(ModularDesignWindow* modularDesign, LivingBracketParametrizedDesign* livingDesign);
	~LivingDesignSampler();

	virtual void generateDesignSamples(vector<dVector>& designs) = 0;
};

