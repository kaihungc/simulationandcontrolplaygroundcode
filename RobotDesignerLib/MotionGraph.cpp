#include "MotionGraph.h"
#include "LocomotionEngineManagerGRF.h"
#include "LocomotionEngineManagerTransition.h"

/************************************************************************/
/* MotionGraphNode													    */
/************************************************************************/

MotionGraphNode::MotionGraphNode(LocomotionEngineManager* manager)
{
	this->manager = manager;
	this->motionPlan = manager->motionPlan;
}

MotionGraphNode::~MotionGraphNode()
{
	Logger::print("FreeNode\n");
	delete manager;
}

/************************************************************************/
/* MotionGraphEdge													    */
/************************************************************************/

MotionGraphEdge::MotionGraphEdge(LocomotionEngineManager* manager, MotionGraphNode* source, MotionGraphNode* sink)
{
	this->manager = manager;
	this->motionPlan = manager->motionPlan;

	this->source = source;
	this->sink = sink;
}

MotionGraphEdge::~MotionGraphEdge()
{
	Logger::print("FreeEdge\n");
	delete manager;
}


/************************************************************************/
/* MotionGraph   													    */
/************************************************************************/



MotionGraph::MotionGraph()
{
	
}


MotionGraph::~MotionGraph()
{
	while (!nodeSet.empty())
	{
		removeNode(nodeSet.back());
	}
}

int MotionGraph::getNodeIndex(MotionGraphNode* node)
{
	for (int i = 0; i < (int)nodeSet.size(); i++)
	{
		if (nodeSet[i] == node)
		{
			return i;
		}
	}

	return -1;
}

int MotionGraph::getEdgeIndex(MotionGraphEdge* edge)
{
	for (int i = 0; i < (int)edgeSet.size(); i++)
	{
		if (edgeSet[i] == edge)
		{
			return i;
		}
	}

	return -1;
}

void MotionGraph::addNode(MotionGraphNode* node)
{
	nodeSet.push_back(node);
}

void MotionGraph::addEdge(MotionGraphEdge* edge)
{
	edgeSet.push_back(edge);
	edge->source->outEdges.insert(edge);
	edge->sink->inEdges.insert(edge);
}

void MotionGraph::removeNode(MotionGraphNode* node)
{
	for (auto e : node->inEdges)
	{
		removeEdge(e);
	}

	for (auto e : node->outEdges)
	{
		removeEdge(e);
	}
	nodeSet.erase(std::remove(nodeSet.begin(), nodeSet.end(), node), nodeSet.end());

	delete node;
}

void MotionGraph::removeEdge(MotionGraphEdge* edge)
{
	edge->source->outEdges.erase(edge);
	edge->sink->inEdges.erase(edge);
	
	edgeSet.erase(std::remove(edgeSet.begin(), edgeSet.end(), edge), edgeSet.end());

	delete edge;
}

MotionGraphEdge* MotionGraph::findEdge(MotionGraphNode* source, MotionGraphNode* sink)
{
	for (auto e : edgeSet)
	{
		if (e->source == source && e->sink == sink)
		{
			return e;
		}
	}

	return NULL;
}

void MotionGraph::saveToFile(FILE* fp)
{
	fprintf(fp, "MotionGraph\n\n");

	for (int i = 0; i < (int)nodeSet.size(); i++)
	{
		nodeSet[i]->index = i;
		fprintf(fp, "Node %d\n\n", i);
		nodeSet[i]->motionPlan->writeParamsToFile(fp);
	}

	for (int i = 0; i < (int)edgeSet.size(); i++)
	{
		MotionGraphEdge* edge = edgeSet[i];
		fprintf(fp, "Edge %d %d\n\n", edge->source->index, edge->sink->index);
		edge->motionPlan->writeParamsToFile(fp);
	}

	fprintf(fp, "EndMotionGraph\n\n");
}

void MotionGraph::loadFromFile(FILE* fp, Robot* robot)
{
	char buffer[200];
	char keyword[50];

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "Node") == 0)
		{
			LocomotionEngineManager* manager = new LocomotionEngineManagerGRFv2(robot, NULL, 10);
			manager->motionPlan->readParamsFromFile(fp);
			manager->locked = true;
			addNode(new MotionGraphNode(manager));
		}
		else if (strcmp(keyword, "Edge") == 0)
		{
			int sourceIndex, sinkIndex;
			int num = sscanf(line + strlen(keyword), "%d %d", &sourceIndex, &sinkIndex);
			LocomotionEngineManager* manager = new LocomotionEngineManagerTransition(robot, 10);
			manager->motionPlan->readParamsFromFile(fp);
			manager->locked = true;
			addEdge(new MotionGraphEdge(manager, nodeSet[sourceIndex], nodeSet[sinkIndex]));
		}
		else if (strcmp(keyword, "EndMotionGraph") == 0)
		{
			break;
		}
	}
}
