#pragma once
#include <OptimizationLib/ObjectiveFunction.h>
#include "TransitionFFPOptPlan.h"

class TransitionFFPObjective : public ObjectiveFunction
{
public:
	TransitionFFPOptPlan* plan;

	dVector m_p0;
	double regularizer;
public:
	TransitionFFPObjective(TransitionFFPOptPlan* plan, const std::string& objectiveDescription, double weight);
	~TransitionFFPObjective();

	void setCurrentBestSolution(const dVector& p);
	virtual double computeValue(const dVector& p);
};

