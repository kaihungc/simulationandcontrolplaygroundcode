#pragma once
#include "RMC.h"

class LivingMotor : public RMC
{
public:
	LivingBracketMotor_XM430* motor = NULL;
	LivingHornBracket* bracket = NULL;
	vector<RMCPin> candidatePins;
	int activeBodyPinID = -1;

public:
	LivingMotor(LivingHornBracket* lbh = NULL);
	~LivingMotor();

	virtual LivingMotor* clone();
	virtual bool pickMesh(Ray& ray, double* closestDist = NULL);
	virtual void draw(int flags, const Vector4d& color = Vector4d(0, 0, 0, 0));
	virtual void update();

	virtual void generatePins();

	void exportMeshes(const char* dirName, int index, bool mergeMeshes = false);
	void syncSymmParameters(LivingMotor* refMotor);
	void switchToBestBodyPin();
};

