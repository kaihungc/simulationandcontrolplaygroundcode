#pragma once
#include "LivingDesignSampler.h"

class PossionDesignSampler : public LivingDesignSampler 
{
public:
	vector<dVector> candidateDesigns;
	queue<dVector> designQueue;

public:
	PossionDesignSampler(ModularDesignWindow* modularDesign, LivingBracketParametrizedDesign* livingDesign);
	~PossionDesignSampler();

	void generateDesignSamples(vector<dVector>& designs);
	void generateSingleElementVariationSamples(dVector& curDesign);
	void generateMultipleElementVariationSamples(dVector& curDesign, int sampleN);

	bool checkDesignValid(dVector& design);
	bool checkDesignNovelty(dVector& design);
};

