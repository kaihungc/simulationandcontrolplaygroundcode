#include "RMCSearchNodeBulletObject.h"

RMCSearchNodeBulletObject::RMCSearchNodeBulletObject(RMCSearchNode* p, AbstractBulletObject* rmcBulletObject) {
	parent = p;
	colShape = new btBoxShape(*((btBoxShape*)rmcBulletObject->colShape));
	geometryCenterLocalOffset = rmcBulletObject->geometryCenterLocalOffset;
	type = SEARCH_NODE_OBJECT;
}

btCollisionObject* RMCSearchNodeBulletObject::getCollisionObject() {
	P3D	position =  parent->position + parent->orientation.rotate(geometryCenterLocalOffset);
	Quaternion rotation = parent->orientation;
	collisionObject.getWorldTransform().setOrigin(btVector3((btScalar)position[0], (btScalar)position[1], (btScalar)position[2]));
	collisionObject.getWorldTransform().setRotation(btQuaternion((btScalar)rotation.v[0], (btScalar)rotation.v[1], (btScalar)rotation.v[2], (btScalar)rotation.s));
	colShape->setMargin(0.0005f);
	collisionObject.setCollisionShape(colShape);
	collisionObject.setUserPointer(this);
	collisionObject.setCollisionFlags(0);

	return &collisionObject;
}

void RMCSearchNodeBulletObject::loadFromFile(FILE* fp) {

}