#include "RobotBodyPart.h"
#include "EndEffectorFeature.h"


EndEffectorFeature::EndEffectorFeature(){

}

void EndEffectorFeature::draw(){
    if (son != NULL)
        son->draw();
	glEnable(GL_LIGHTING);
    double deltColor = 0.0;
    if (mouseOn)
        deltColor = 0.3;
	glColor4d(1 - deltColor,0,0,1);
	if (eeCP.size() == 0)
		drawSphere(getLocalCoordsForLocalFeaturePoint(P3D()), 0.02, 15);
	else
		for (uint i=0;i<eeCP.size();i++)
			drawSphere(getLocalCoordsForLocalFeaturePoint(eeCP[i]), 0.02, 15);

	glColor4d(0,1,0,1);
	if (eeCP.size() > 1)
		for (uint i=0;i<eeCP.size();i++){
			if (i==eeCP.size()-1)
				drawCylinder(getLocalCoordsForLocalFeaturePoint(eeCP[i]), getLocalCoordsForLocalFeaturePoint(eeCP[0]), 0.005);
			else
				drawCylinder(getLocalCoordsForLocalFeaturePoint(eeCP[i]), getLocalCoordsForLocalFeaturePoint(eeCP[i+1]), 0.005);
		}
	glDisable(GL_LIGHTING);
}

void EndEffectorFeature::getCompleteListOfPossibleAttachmentPoints(bool isStart, vector<AttachmentPoint>& attachmentPoints){
	attachmentPoints.clear();

	if (eeCP.size() == 0)
		attachmentPoints.push_back(AttachmentPoint(getLocalCoordsForLocalFeaturePoint(P3D()), getLocalCoordsForLocalFeatureVector(V3D(0,1,0))));
	else
		for (uint i=0;i<eeCP.size();i++)
			attachmentPoints.push_back(AttachmentPoint(getLocalCoordsForLocalFeaturePoint(eeCP[i]), getLocalCoordsForLocalFeatureVector(V3D(0,1,0))));
}

// create an sphere
string EndEffectorFeature::generateObj()
{
    GLMesh* objMesh = GLContentManager::getGLMesh("resource/featurePoint-2.obj")->clone();
    char buffer[256];
    string filename;
    sprintf(buffer, "tmp\\%d.obj", (int)this);
    filename = buffer;
    FILE* fp = fopen(filename.c_str(), "w");
    objMesh->scale(size / 13.0, P3D(0, 0, 0));
    objMesh->translate(position);
    objMesh->renderToObjFile(fp, 0, getRotationQuaternion(0, V3D(0, 0, 1)), P3D(0, 0, 0));
    fclose(fp);

    return filename;
}