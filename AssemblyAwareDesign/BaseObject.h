#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include <OptimizationLib\ObjectiveFunction.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "ObjectivesAndConstraints.h"
#include "ParameterizedDisassemblyPath.h"
#include <MathLib/ConvexHull3D.h>
#include "ParameterizedDisassemblyPath.h"

# define ASSEMBLY_ANGLE 0x01

class Fastener;
class BulletCollisionObject;
class RigidlyAttachedSupport;

class BaseObject {
public:
	//The state of each object is defined by its position and orientation. The orientation is parameterized through Euler angles.
	P3D position;																	//position of component in world coordinates
	double alpha = 0, beta = 0, gamma = 0;											//internal rotation angles
	V3D n_alpha = V3D(0, 0, 1), n_beta = V3D(0, 1, 0), n_gamma = V3D(1, 0, 0);		//internal rotation axes. They change near gimbal lock orientations..

public:
	V3D geometryCenterLocalOffset = V3D(0, 0, 0);
	double collideColor = 1.0;

public:
	// constructor
	BaseObject() {

	}

	// destructor
	virtual ~BaseObject();

	BT_DECLARE_ALIGNED_ALLOCATOR();

	// name of the object
	std::string name = "BaseObject";

	//flags that affect visualization of this object
	bool picked = false;
	bool collided = false;
	bool frozen = false;

	//returns scaling coefficients to make sihouettes rougly equaly along all dimensions
	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) = 0;

	//methods for rendering the object
	virtual void drawSilhouette(double scale, double r, double g, double b);
	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) = 0;
	virtual void draw();

	void setupGLTransformationMatrices();
	
	virtual bool shouldCollideWith(BaseObject* obj) = 0;

	virtual P3D getPosition() {
		return position;
	}

	virtual void setPosition(const P3D& p) {
		position = p;
	}

	virtual Quaternion getOrientation() {
		return getRotationQuaternion(gamma, n_gamma) * getRotationQuaternion(beta, n_beta) * getRotationQuaternion(alpha, n_alpha);
	}

	virtual void setOrientation(const Quaternion& Q) {
		computeEulerAnglesFromQuaternion(Q, n_alpha, n_beta, n_gamma, alpha, beta, gamma);
	}

	virtual void reparameterizeRotations();

	P3D getWorldCoordinates(const P3D& p);

	V3D getWorldCoordinates(const V3D& v);

	P3D getLocalCoordinates(const P3D& p);

	V3D getLocalCoordinates(const V3D& v);

	virtual void onComponentSelect();

	virtual void onComponentDeselect();

	void toggleFreeze();

	void setWidgetsFromObjectState(TranslateWidget* tWidget, RotateWidget* rWidget);
	void setObjectStateFromWidgets(TranslateWidget* tWidget, RotateWidget* rWidget);

	// for collision objects
	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {}

	//for the GUI...
	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() { return DynamicArray<std::pair<char*, double*>>(); }

	//pushes the object's state parameters to list, starting at the given index, given the set of flags that are passed in as parameters
	void pushStateParametersToList(DynamicArray<double>&paramList, bool addX, bool addY, bool addZ, bool addAlpha, bool addBeta, bool addGamma) {
		if (addX) paramList.push_back(position[0]);
		if (addY) paramList.push_back(position[1]);
		if (addZ) paramList.push_back(position[2]);
		if (addAlpha) paramList.push_back(alpha);
		if (addBeta) paramList.push_back(beta);
		if (addGamma) paramList.push_back(gamma);
	}

	void readStateParametersFromList(DynamicArray<double>&paramList, int& pIndex, bool addX, bool addY, bool addZ, bool addAlpha, bool addBeta, bool addGamma) {
		if (addX) position[0] = paramList[pIndex++];
		if (addY) position[1] = paramList[pIndex++];
		if (addZ) position[2] = paramList[pIndex++];
		if (addAlpha) alpha = paramList[pIndex++];
		if (addBeta) beta = paramList[pIndex++];
		if (addGamma) gamma = paramList[pIndex++];
	}

	//copying properties like position, orientation etc
	virtual void copyObjectPropetriesInto(BaseObject* obj) {
		obj->setPosition(this->getPosition());
		//obj->setOrientation(this->getOrientation());
		obj->n_alpha = this->n_alpha;
		obj->n_gamma = this->n_gamma;
		obj->n_beta = this -> n_beta;
		obj->beta = this->beta;
		obj->gamma = this->gamma;
		obj->alpha = this->alpha;
		obj->frozen = this->frozen;
	}

	/**
	writes to txt file
	*/
	virtual void writeToFile(FILE* fp) {};

	/**
	reads from txt file
	*/
	virtual void loadFromFile(FILE* fp) {};
};


// denote walls of the chassis in the assembly
class WallObject : public BaseObject {
public:
	V3D normal = V3D(0, 1, 0);
	P3D wallCenter = P3D(0, 0, 0);
	P3D bounds = P3D(0, 0, 0);
	// same as the range in collision object (epsilon)
	double epsilon = 0.001;

	//btStaticPlaneShape colShape = btStaticPlaneShape(bulletVector(normal), btScalar(-normal.dot(wallCenter)));
	btStaticPlaneShape colShape = btStaticPlaneShape(bulletVector(normal), btScalar(0));
	btCollisionObject colObject;

	// constructor
	WallObject(P3D p, V3D n, P3D halfExtent) {
		name = "Wall";
		normal = n;
		wallCenter = p;
		bounds = halfExtent;
		setPosition(wallCenter + normal*epsilon);
		colShape = btStaticPlaneShape(bulletVector(normal), btScalar(0));
	}
	WallObject() {
		name = "Wall";
	}
	// destructor
	virtual ~WallObject() {}

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) {
		if (abs(normal.dot(V3D(0, 0, 1))) == 1 || abs(normal.dot(V3D(1, 0, 0))) == 1 || abs(normal.dot(V3D(0, 1, 0))) == 1) {
			P3D p = bounds + -normal*normal.dot(bounds);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_LIGHTING);
			glColor3f(0.2f, 0.2f, 0.2f);
			drawBox(-p, p);
			glEnable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}

	bool checkPointOutOfBounds(P3D p) {
		P3D localp = this->getLocalCoordinates(p);
		V3D distV = V3D(this->getLocalCoordinates(wallCenter), localp);

		if (distV.length() > V3D(this->getLocalCoordinates(wallCenter), bounds).length())
			return true;
		else
			return false;

		//if (abs(p.dot(V3D(1, 0, 0)) > this->bounds[0]))
		//	return true;
		//else if (abs(p.dot(V3D(0, 1, 0)) > this->bounds[1]))
		//	return true;
		//else if (abs(p.dot(V3D(0, 0, 1)) > this->bounds[2]))
		//	return true;
		//else
		//	return false;
	}

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimZ = 0;
		dimX = 0;
		dimY = 0;
	}

	virtual bool shouldCollideWith(BaseObject* obj) {
		return true;
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs);

};

//electro mechanical components, fastners (others?) are assemblable, walls and supports are not...
class AssemblableComponent : public BaseObject {
public:
	DynamicArray<P3D> objectCoordsConvexHullVertices;
	

	ParameterizedDisassemblyPath* disassemblyPath = NULL;

	//if t is 0, then the position returned is the 'default' position of the component. If it is 1, then the returned position will correspond to the location of the component after disassembly 
	virtual P3D getPositionOnAssemblyPath(double t) {
		if (!disassemblyPath)
			return getPosition();
		return disassemblyPath->getComponentPositionAlongPathAt(t);
//		return getPosition() + disassemblyPath->getPositionAlongPathAt(t);
	}

	//same for orientations...
	virtual Quaternion getOrientationOnAssemblyPath(double t) {
		if (!disassemblyPath)
			return getOrientation();
		return disassemblyPath->getComponentOrientationAlongPathAt(t);
//		else
//			return disassemblyPath->getOrientationAlongPathAt(t) * getOrientation();
	}

	// constructor
	AssemblableComponent() {}
	// destructor
	virtual ~AssemblableComponent() {
		delete disassemblyPath;
		disassemblyPath = NULL;
	}

	void drawOnAssemblyPath(double assemblyPhase) {
		Quaternion qOriginal = getOrientation();
		P3D pos = getPosition();

		setOrientation(getOrientationOnAssemblyPath(assemblyPhase));
		setPosition(getPositionOnAssemblyPath(assemblyPhase));

		draw();

		setOrientation(qOriginal);
		setPosition(pos);

	}

	virtual void drawSweptConvexHull() {
		GLMesh* cvxHullMesh = getWorldCoordsSweptObjectMesh();
		GLShaderMaterial tmpMat;
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.1;
		cvxHullMesh->setMaterial(tmpMat);
		cvxHullMesh->drawMesh();
	}

	void addBulletCollisionForObjectOnPath(DynamicArray<btCollisionObject*>& bulletCollisionObjs, double assemblyPhase) {
		Quaternion qOriginal = getOrientation();
		P3D pos = getPosition();

		setOrientation(getOrientationOnAssemblyPath(assemblyPhase));
		setPosition(getPositionOnAssemblyPath(assemblyPhase));

		addBulletCollisionObjectsToList(bulletCollisionObjs);

		setOrientation(qOriginal);
		setPosition(pos);
	}

	GLMesh* getWorldCoordsSweptObjectMesh();

	void addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects);
};

/*
	Class for main electromechanical components
*/
class EMComponent : public AssemblableComponent {
public:

	//this is the index at which parameters for this object appear in a global list of parameters
	int pIndexStart = -1;
	// flags denoting which rotations can be optimized and which ones are locked by the user. This preference is given in world co-ordinates (Since that is what the user can see via the widget.
	bool optimizeAlpha = false, optimizeBeta = false, optimizeGamma = false, optimizeAdditionalRotAxis = true;
	// flags denoting which positions can be optimized. This will allow restricting some components to be in a desired plane (such as ultrasonic sensor should always be the the front face plane of the robot)
	bool optimizeXPos = true, optimizeYPos = true, optimizeZPos = true;

	//if we want the object to rotate about an arbitrary axis, then use this...
	V3D additionalRotAxis = V3D(0, 1, 0); // local rotation axis
	double additionalRotAngle = 0; // rotation angle about arbitrary rotation axis above...

	// Objective function used to fix the state of an object...
	TargetStateObjective targetStateObj;

public:
	//this parameter is used to keep track of the relative (dis)assembly ordering - set externally...
	int disassemblyIndex = -1;
	int assemblyIndex = -1;

public:
	// constructor
	EMComponent() : targetStateObj(this) {}
	// destructor
	virtual ~EMComponent() {}

	virtual void resetTargetStateValues() {
		targetStateObj.setTargets(getPosition(), getOrientation());
	}

	virtual Quaternion getOrientation() {

		return BaseObject::getOrientation() * getRotationQuaternion(additionalRotAngle, additionalRotAxis);

	}

	virtual void setOrientation(const Quaternion& Q) {
		// want to keep contribution from optimization angle intact... so alpha, beta and gamma will be set according to "everything else"...
		Quaternion Qopt;
		Qopt.setRotationFrom(additionalRotAngle, additionalRotAxis);

		BaseObject::setOrientation(Q*Qopt.getInverse());
	}

	virtual EMComponent* clone(bool cloneTexture = true) = 0;

	virtual bool shouldCollideWith(BaseObject* obj);

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() { return DynamicArray<std::pair<char*, double*>>(); }

	virtual DynamicArray<std::pair<char*, bool*>> getListOfRotationDOFs() {
		DynamicArray<std::pair<char*, bool*>> result;
		result.push_back(std::pair<char*, bool*>("additional rot axis", &optimizeAdditionalRotAxis));
		result.push_back(std::pair<char*, bool*>("X axis", &optimizeAlpha));
		result.push_back(std::pair<char*, bool*>("Y axis", &optimizeBeta));
		result.push_back(std::pair<char*, bool*>("Z axis", &optimizeGamma));
		return result;
	}

	virtual DynamicArray<std::pair<char*, bool*>> getListOfPositionDOFs() {
		DynamicArray<std::pair<char*, bool*>> result;
		result.push_back(std::pair<char*, bool*>("X-axis", &optimizeXPos));
		result.push_back(std::pair<char*, bool*>("Y-axis", &optimizeYPos));
		result.push_back(std::pair<char*, bool*>("Z-axis", &optimizeZPos));
		return result;
	}

	virtual DynamicArray<std::pair<char*, int*>> getOrder(int maxIndex) {
		DynamicArray<std::pair<char*, int*>> result;
		//setAssemblyIndex(maxIndex);
		result.push_back(std::pair<char*, int*>("Assembly order", &assemblyIndex));
		setDisAssemblyIndex(maxIndex);
		return result;
	}

	void  setAssemblyIndex(int maxIndex) {
		this->assemblyIndex = maxIndex - this->disassemblyIndex;
	}

	void setDisAssemblyIndex(int maxIndex) {
		this->disassemblyIndex = maxIndex - this->assemblyIndex;
	}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList, int optimizationFlag);
	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex, int optimizationFlag);

	void setParameterStartIndex(int pStartIndex) {
		this->pIndexStart = pStartIndex;
	}

	int getParameterStartIndex() {
		return pIndexStart;
	}

	virtual void addSupportsToList(DynamicArray<RigidlyAttachedSupport*>& supportObjs) {};
	virtual void addFastnersToList(DynamicArray<Fastener*>& attachmentObjs) {};

	void writeBasePropertiesToFile(FILE* fp);
	void readBasePropertiesFromFile(FILE* fp);

	virtual void onComponentSelect();

	virtual void onComponentDeselect();

	virtual void addObjectives(DynamicArray<ObjectiveFunction*>& objArray);

	bool getValidRandomPos(P3D& randomPos, P3D bounds);
	bool getValidRandomAngle(double& randomAngle, int angleType = 0x00);

	virtual void copyObjectPropetriesInto(BaseObject* obj) {
		BaseObject::copyObjectPropetriesInto(obj);
		if (dynamic_cast<EMComponent*>(obj)) {
			dynamic_cast<EMComponent*>(obj)->disassemblyIndex = this->disassemblyIndex;
			dynamic_cast<EMComponent*>(obj)->optimizeXPos = this->optimizeXPos;
			dynamic_cast<EMComponent*>(obj)->optimizeYPos = this->optimizeYPos;
			dynamic_cast<EMComponent*>(obj)->optimizeZPos = this->optimizeZPos;
			dynamic_cast<EMComponent*>(obj)->optimizeAlpha = this->optimizeAlpha;
			dynamic_cast<EMComponent*>(obj)->optimizeBeta = this->optimizeBeta;
			dynamic_cast<EMComponent*>(obj)->optimizeGamma = this->optimizeGamma;
			dynamic_cast<EMComponent*>(obj)->optimizeAdditionalRotAxis = this->optimizeAdditionalRotAxis;
			dynamic_cast<EMComponent*>(obj)->additionalRotAngle = this->additionalRotAngle;
			this->disassemblyPath->copyObjectPropetriesInto(dynamic_cast<EMComponent*>(obj)->disassemblyPath);
		}
	}
};

inline void addBTColShapeVerticesToPointList(const btBoxShape& box, DynamicArray<P3D>& pointList, V3D colShapeOffset = V3D(0,0,0)) {
	btVector3 tmpP;
	btTransform T;
	T.setIdentity();
	T.setOrigin(bulletVector(colShapeOffset));

	for (int i = 0; i < box.getNumVertices(); i++) {
		box.getVertex(i, tmpP);
		pointList.push_back(getP3D(T*tmpP));
	}

}

