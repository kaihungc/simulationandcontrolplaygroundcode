#include <include/glew.h>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include "BaseObject.h"
#include <GUILib/GLContentManager.h>
#include <GUILib/GLUtils.h>
#include <random>
#include "BulletCollisionObject.h"
#include <MathLib/ConvexHull3D.h>

BaseObject::~BaseObject() {
}

bool EMComponent::shouldCollideWith(BaseObject* obj) {
	return true;
}

P3D BaseObject::getWorldCoordinates(const P3D& p) {
	return getPosition() + getOrientation() * V3D(p);
}

V3D BaseObject::getWorldCoordinates(const V3D& v) {
	return getOrientation() * v;
}

P3D BaseObject::getLocalCoordinates(const P3D& p) {
	return P3D() + getLocalCoordinates(V3D(getPosition(), p));
}

V3D BaseObject::getLocalCoordinates(const V3D& v) {
	return getOrientation().inverseRotate(v);
}

void BaseObject::toggleFreeze() {
	frozen = !frozen;
}

void BaseObject::onComponentSelect() {

}

void BaseObject::onComponentDeselect() {
}

void BaseObject::setWidgetsFromObjectState(TranslateWidget* tWidget, RotateWidget* rWidget) {
	tWidget->pos = getPosition();
	rWidget->setOrientation(getOrientation());
}

//when the second angle gets too close to PI/2, we get close to a gimbal lock. The effect of this is that it
//confuses twist with rotation about the last axis. If we need twist specifically for any purpose, this is a problem. We will therefore
//change the last two rotation axes, but in a way that keeps the same rotation overall, of course
void BaseObject::reparameterizeRotations() {
	Quaternion q = getRotationQuaternion(gamma, n_gamma) * getRotationQuaternion(beta, n_beta) * getRotationQuaternion(alpha, n_alpha);

	//extract the twist from the rotation - this will give us two rotation axes that we can start from...
	Quaternion qTwist = decomposeRotation(q, n_alpha);

	//keeping the rotation axis for alpha fixed, we'll try to explain as much of the overall rotation with two orthogonal axes
	Quaternion qBeforeTwist = q * qTwist.getComplexConjugate();
	if (!IS_ZERO(qBeforeTwist.v.length())) {
		n_gamma = qBeforeTwist.v.unit();
		n_beta = n_gamma.cross(n_alpha).unit();
		n_gamma = n_alpha.cross(n_beta).unit();
	}

	computeEulerAnglesFromQuaternion(q, n_alpha, n_beta, n_gamma, alpha, beta, gamma);
	//Logger::consolePrint("after: (%lf %lf %lf) %lf , %lf , %lf . Error: %lf\n", n_gamma[0], n_gamma[1], n_gamma[2], gamma, beta, alpha, fabs((q.getComplexConjugate() * getOrientation()).s) - 1.0);
}

void BaseObject::setObjectStateFromWidgets(TranslateWidget* tWidget, RotateWidget* rWidget) {
	setPosition(tWidget->pos);
	setOrientation(rWidget->getOrientation());
	reparameterizeRotations();
}

void BaseObject::drawSilhouette(double scale, double r, double g, double b) {
	//could first draw the original object in stencil buffer, and then draw silhouete where the stencil wasn't... to get rid of internal lines...

	// render the silhouette of the object...
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_CULL_FACE); // enable culling
	glCullFace(GL_FRONT); // enable culling of front faces
	GLShaderMaterial tmpMat;
	tmpMat.setShaderProgram(GLContentManager::getShaderProgram("silhouette"));
	tmpMat.setFloatParam("u_offset1", 0.0f);
	tmpMat.setFloatParam("u_color1", (float)r, (float)g, (float)b);
	double bbX, bbY, bbZ;
	getSilhouetteScalingDimensions(bbX, bbY, bbZ);
	double maxDim = MAX(bbX, bbY); maxDim = MAX(maxDim, bbZ);
	glPushMatrix();
//	V3D geomOffset = getGeometryCenterLocalOffset();
	V3D geomOffset;
	glTranslated(geomOffset[0], geomOffset[1], geomOffset[2]);
	glScaled((bbX / maxDim + scale / maxDim) / (bbX / maxDim), (bbY / maxDim + scale / maxDim) / (bbY / maxDim), (bbZ / maxDim + scale / maxDim) / (bbZ / maxDim));
	glTranslated(-geomOffset[0], -geomOffset[1], -geomOffset[2]);
	drawObjectGeometry(&tmpMat);
	glPopMatrix();
	glDisable(GL_CULL_FACE);
}

void BaseObject::setupGLTransformationMatrices() {
	P3D p = getPosition();
	glTranslated(p[0], p[1], p[2]);
	V3D rotAxis; double rotAngle;
	getOrientation().getAxisAngle(rotAxis, rotAngle);
	glRotated(DEG(rotAngle), rotAxis[0], rotAxis[1], rotAxis[2]);
}

void BaseObject::draw() {
	checkOGLErrors();
	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);

	glStencilFunc(GL_ALWAYS, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	setupGLTransformationMatrices();

	// render the model to indicate places where the silhouette should not appear
	drawObjectGeometry();

	glStencilFunc(GL_NOTEQUAL, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	if (frozen)
		drawSilhouette(0.0005, 0, 1.0, 0);

	if (picked)
		drawSilhouette(0.001, 1, 0.5, 0);

	if (collided)
		drawSilhouette(0.002, 1, 0, 0);
		//drawSilhouette(0.002, MIN(0.5+collideColor,1), 0, 0);

	glDisable(GL_STENCIL_TEST);

	// render the normal model
	drawObjectGeometry();
	glPopMatrix();
}

void WallObject::addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
	bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShape, &colObject));
}

GLMesh* AssemblableComponent::getWorldCoordsSweptObjectMesh() {
	return disassemblyPath->getWorldCoordinatesSweptObjectMesh();
}

void AssemblableComponent::addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects) {
	disassemblyPath->addSweptConvexHullObjectsToList(sweptConvexHullObjects);
}

void EMComponent::onComponentSelect() {
	resetTargetStateValues();
	targetStateObj.weight = 10;
}

void EMComponent::onComponentDeselect() {
	targetStateObj.weight = 0;
}

void EMComponent::addObjectives(DynamicArray<ObjectiveFunction*>& objArray) {
	objArray.push_back(&targetStateObj);
}

//modifies parameterList by pushing its own parameters.
void EMComponent::pushParametersToList(DynamicArray<double>& parameterList, int optimizationFlag) {
	if (optimizationFlag & OPTIMIZE_STATE_PARAMETERS && !frozen){
		pushStateParametersToList(parameterList, optimizeXPos, optimizeYPos, optimizeZPos, optimizeAlpha, optimizeBeta, optimizeGamma);
		if (optimizeAdditionalRotAxis) parameterList.push_back(additionalRotAngle);
	}

	if (disassemblyPath && optimizationFlag & OPTIMIZE_ASSEMBLY_PARAMETERS)
		disassemblyPath->pushParametersToList(parameterList);
}

void EMComponent::readParametersFromList(DynamicArray<double>& parameterList, int& pIndex, int optimizationFlag) {
	if (optimizationFlag & OPTIMIZE_STATE_PARAMETERS && !frozen) {
		readStateParametersFromList(parameterList, pIndex, optimizeXPos, optimizeYPos, optimizeZPos, optimizeAlpha, optimizeBeta, optimizeGamma);
		if (optimizeAdditionalRotAxis)
			additionalRotAngle = parameterList[pIndex++];
	}

	if (disassemblyPath && optimizationFlag & OPTIMIZE_ASSEMBLY_PARAMETERS)
		disassemblyPath->readParametersFromList(parameterList, pIndex);
	
}

bool EMComponent::getValidRandomPos(P3D& randomPos, P3D bounds) {

	// truncate to max/min allowable value if needed
	bool truncationNeeded = false;
	if (fabs(randomPos[0]) > fabs(bounds[0])) {
		randomPos[0] = bounds[0] * SGN(randomPos[0]);
		truncationNeeded = true;
	}
	if (fabs(randomPos[1]) > fabs(bounds[1])) {
		randomPos[1] = bounds[1] * SGN(randomPos[1]);
		truncationNeeded = true;
	}
	if (fabs(randomPos[2]) > fabs(bounds[2])) {
		randomPos[2] = bounds[2] * SGN(randomPos[2]);
		truncationNeeded = true;
	}

	// check if we can change the position
	if (!this->frozen) {
		if (!optimizeXPos)
			randomPos[0] = this->getPosition()[0];
		if (!optimizeYPos)
			randomPos[1] = this->getPosition()[1];
		if (!optimizeZPos)
			randomPos[2] = this->getPosition()[2];
	}
	else {
		randomPos = this->getPosition();
		truncationNeeded = false;
	}

	return truncationNeeded;
}

bool EMComponent::getValidRandomAngle(double& randomAngle, int angleType) {
	/***Checking Angles***/
	// truncate to max/min allowable value if needed
	bool truncationNeeded = false;

	if (fabs(randomAngle) > fabs(PI / 2)) {
		randomAngle = PI / 2 * SGN(randomAngle);
		truncationNeeded = true;
	}

	if (!(angleType & ASSEMBLY_ANGLE)) {
		// check which orientations can we change
		if (!this->frozen) {
			if (!optimizeAdditionalRotAxis) {
			//if (!optimizeAlpha && !optimizeBeta && !optimizeGamma) {
				randomAngle = additionalRotAngle;
				truncationNeeded = false;
			}
		}
		else {
			randomAngle = additionalRotAngle;
			truncationNeeded = false;
		}
	}

	return truncationNeeded;
}


void EMComponent::writeBasePropertiesToFile(FILE* fp) {
	fprintf(fp, "\t%s %s\n", getASString(OBJ_NAME), this->name.c_str());

	P3D objPos = getPosition();
	fprintf(fp, "\t%s %10.10lf %10.10lf %10.10lf\n", getASString(OBJ_POSITION), objPos[0], objPos[1], objPos[2]);

	fprintf(fp, "\t%s %d\n", getASString(DISASS_IND), this->disassemblyIndex);

	fprintf(fp, "\t%s ", getASString(ASSEM_PARAMS));
	disassemblyPath->writePropertiesToFile(fp);
	fprintf(fp, "\n");

	fprintf(fp, "\t%s %d %d %d\n", getASString(OBJ_ALLOW_POS), optimizeXPos, optimizeYPos, optimizeZPos);

	bool allowableRotArray[4] = { optimizeAlpha, optimizeBeta, optimizeGamma, optimizeAdditionalRotAxis};
	fprintf(fp, "\t%s %d %d %d %d\n", getASString(OBJ_ALLOW_ROT), allowableRotArray[0], allowableRotArray[1], allowableRotArray[2],
		allowableRotArray[3]);

	double additionalRot[4] = { additionalRotAngle, additionalRotAxis[0], additionalRotAxis[1], additionalRotAxis[2] };
	fprintf(fp, "\t%s %10.10lf %lf %lf %lf\n", getASString(OBJ_ADDITIONALROTATION), additionalRot[0], additionalRot[1], additionalRot[2],
		additionalRot[3]);

	Quaternion objRot = getOrientation();
	fprintf(fp, "\t%s %10.10lf %10.10lf %10.10lf %10.10lf\n", getASString(OBJ_ORIENTATION), objRot.s, objRot.v[0], objRot.v[1], objRot.v[2]);

	if (frozen)
		fprintf(fp, "\t%s %d\n", getASString(OBJ_IS_FROZEN), 1);
	else
		fprintf(fp, "\t%s %d\n", getASString(OBJ_IS_FROZEN), 0);
}

void EMComponent::readBasePropertiesFromFile(FILE* fp) {

	// return end of baseobj properties
	long endBase = 0;

	//have a temporary buffer used to read the file line by line...
	char buffer[200];

	//this is where it happens.
	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, 200, fp);
		char *line = lTrim(buffer);
		int lineType = getASLineType(line);
		switch (lineType) {

		case OBJ_NAME: {
			this->name = std::string() + trim(line);
		}
					   break;

		case OBJ_POSITION: {
			double t1 = 0, t2 = 0, t3 = 0;
			if (sscanf(line, "%lf %lf %lf", &t1, &t2, &t3) != 3)
				throwError("Incorrect rigid body input file - 3 arguments are required to specify the world coordinates position of an object\n", line);
			setPosition(P3D(t1, t2, t3));
		}
						   break;

		case OBJ_ORIENTATION: {
			double t1 = 0, t2 = 0, t3 = 0, t4 = 0;
			if (sscanf(line, "%lf %lf %lf %lf ", &t1, &t2, &t3, &t4) != 4)
				throwError("Incorrect assembly input file - 4 arguments are required to specify the world coordinates orientation of an object\n", line);
			Quaternion Q = Quaternion(t1, V3D(t2, t3, t4));
			setOrientation(Q);
		}
							  break;

		case DISASS_IND: {
			int ind = -1;
			sscanf(line, "%d", &ind);
			this->disassemblyIndex = ind;
		}
						 break;

		case ASSEM_PARAMS: {
			this->disassemblyPath->readPropertiesFromString(line);
		}
						   break;

		case OBJ_ALLOW_POS: {
			int ind1 = -1, ind2 = -1, ind3 = -1;
			sscanf(line, "%d %d %d", &ind1, &ind2, &ind3);
			if (ind1 == 0)
				this->optimizeXPos = false;
			if (ind2 == 0)
				this->optimizeYPos = false;
			if (ind3 == 0)
				this->optimizeZPos = false;
		}
							break;

		case OBJ_ALLOW_ROT: {
			int ind1 = -1, ind2 = -1, ind3 = -1, ind4 = -1;
			sscanf(line, "%d %d %d %d", &ind1, &ind2, &ind3, &ind4);
			if (ind1 == 1)
				this->optimizeAlpha = true;
			else
				this->optimizeAlpha = false;
			if (ind2 == 1)
				this->optimizeBeta = true;
			else
				this->optimizeBeta = false;
			if (ind3 == 1)
				this->optimizeGamma = true;
			else
				this->optimizeGamma = false;
			if (ind4 == 1)
				this->optimizeAdditionalRotAxis = true;
			else
				this->optimizeAdditionalRotAxis = false;
		}
							break;

		case OBJ_ADDITIONALROTATION: {
			double x = -1, y = -1, z = -1;
			sscanf(line, "%lf %lf %lf %lf\n", &additionalRotAngle, &x, &y, &z);
			additionalRotAxis = V3D(x, y, z);
		}
							break;

		case OBJ_IS_FROZEN: {
			int ind = -1;
			sscanf(line, "%d", &ind);
			if (ind == 1)
				this->frozen = true;
		}
							return;
		//TODO: not clean. It is assumed that anything before frozen is in base, everything after frozen is specific to the object
		break;
		}

	}

}