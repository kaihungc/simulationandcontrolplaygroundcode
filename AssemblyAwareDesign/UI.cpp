#include "UI.h"
#include <GUILib/GLContentManager.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLMeshDisplayWindow.h>
#include <GUILib/GLWindowContainer.h>
#include <iostream>
#include <queue>
#include "ObjectDisplayWindow.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <GUILib\GLTrackingCamera.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <GUILib/GLUtils.h>
#include <GUILib/PlotWindow.h>
#include <MathLib/Segment.h>
#include "BoxComponent.h"
#include "SphereComponent.h"
#include "MakeBlockComponents.h"
#include "MotorComponents.h"
#include "BatteryComponents.h"
#include <GUILib/GLUtils.h>
#include "Support.h"
#include "ElectronicsLibrary.h"


//TODO:
// should visualize collisions/closest points to walls...
//- delete components...
//- walls, supports, fastners...
//- constraints manager...
//- generate supports
//- see fastners
//- see collision detection and perform collisions
//- get objective function based on collisions...

UI::UI() {
	camera = new GLTrackingCamera();

	((GLTrackingCamera*)camera)->camDistance = -1;

	setWindowTitle("Assembly Aware Design - Test UI");
	bgColor[0] = bgColor[1] = bgColor[2] = 1;

	showGroundPlane = false;
	showDesignEnvironmentBox = true;
	designEnvironmentScale = 1.0;

	windowArray = new GLWindowContainer(4, 4, (int)(getGLAppInstance()->getMainWindowWidth() * 0.75), (getGLAppInstance()->getMainWindowHeight() - 20 - (getGLAppInstance()->getMainWindowHeight() / 2)),
		(int)(getGLAppInstance()->getMainWindowWidth() * 0.25), getGLAppInstance()->getMainWindowHeight() / 2);

	TwDefine(" MainMenuBar color='80 200 200' text=dark ");
	//TwAddSeparator(mainMenuBar, "sep2", "");
	TwAddSeparator(mainMenuBar, "sep3", "");
	TwAddButton(mainMenuBar, "Save and load assembly", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "Save to file", UIButtonEventSave, "save", "");
	TwAddButton(mainMenuBar, "Load from file", UIButtonEventLoad, "load", "");
	//TwAddButton(mainMenuBar, "Export .obj", UIButtonEventExport, "export", "");
	//TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddSeparator(mainMenuBar, "sep5", "");
	TwAddButton(mainMenuBar, "Assembly functions", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "Delete selected", UIButtonEventDelete, "delete", "");
	TwAddButton(mainMenuBar, "Get assembly fill ratio", UIButtonEventAssemblyFillRatio, "fillRatio", "");
	TwAddButton(mainMenuBar, "Set assembly order", UIButtonEventSetAssemblyOrder, "order", "");
	TwAddButton(mainMenuBar, "Swap assembly order", UIButtonEventSwapOrder, "swaporder", "");
	TwAddButton(mainMenuBar, "Create supports on chassis", UIButtonEventCreateSupport, "support", "");
	TwAddSeparator(mainMenuBar, "sep6", "");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Design");
	runOptionList.push_back("\\Assembly Animation");
	runOptionList.push_back("\\Assembly Animation With Swept Paths");
	runOptionList.push_back("\\Gradient-based Optimization");
	runOptionList.push_back("\\MC-based Optimization");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddButton(mainMenuBar, "Incremental design", UIButtonIncrementalDesign, "incremental", "");
	TwAddButton(mainMenuBar, "Incremental Alternate", UIButtonIncrementalAlternateDesign, "incrementalAlternate", "");

	TwAddVarRW(mainMenuBar, "Animation Speed", TW_TYPE_DOUBLE, &animStep, " min=0 max=0.1 step=0.005 group = 'Animation' ");
	TwAddVarRW(mainMenuBar, "Animation Phase", TW_TYPE_DOUBLE, &animPhase, "min=0 max=1 step=0.005 group = 'Animation' ");
	TwAddButton(mainMenuBar, "Reset Animation", UIButtonEventResetAnim, "reset", "group = 'Animation'");
	TwAddSeparator(mainMenuBar, "sep7", "");

	TwAddSeparator(mainMenuBar, "sep7", "");
	TwAddButton(mainMenuBar, "Create virtual assembly", UIButtonEventBenchMark, "benchmark", "group = 'Benchmark (only fillratio)'");
	TwAddButton(mainMenuBar, "Create virtual assembly (boxes)", UIButtonEventBenchMarkWithBoxes, "benchmarkboxes", "group = 'Benchmark (only fillratio)'");
	TwAddVarRW(mainMenuBar, "Fill ratio", TW_TYPE_DOUBLE, &fillRatio, " min=0 max=0.9 step=0.01 group = 'Benchmark (only fillratio)' ");
	TwAddVarRW(mainMenuBar, "Num components", TW_TYPE_INT16, &numComponentsVirtual, " min=0 max=100 step=1 group = 'Benchmark' ");
	TwAddButton(mainMenuBar, "Create virtual assembly (boxes) fixed numComp", UIButtonEventBenchMarkWithBoxes2, "benchmarkboxes2", "group = 'Benchmark'");

	TwAddButton(mainMenuBar, "Benchmark fill ratio metric", UIButtonVirtualConstantMetricComponents, "virtualtestfillratio", "");
	TwAddButton(mainMenuBar, "Benchmark fill ratio metric (INCR)", UIButtonEventTestIncrementalVirtual, "testincrementalvirtual", "");
	//TwAddButton(mainMenuBar, "Benchmark for virtual design", UIButtonEventVirtualBatchTest, "virtualtest", "");
	TwAddButton(mainMenuBar, "Benchmark for real design (mixed)", UIButtonEventBatchTest, "test", "");
	TwAddButton(mainMenuBar, "Benchmark for real design (INCR)", UIButtonEventBatchTestIncremental, "testincremental", "");
	TwAddButton(mainMenuBar, "Benchmark for real design (INCR-ALT)", UIButtonEventBatchTestIncrementalAlternate, "testincrementalalternate", "");
	//TwAddButton(mainMenuBar, "Benchmark for real design (one-by-one)", UIButtonEventBatchTestStd, "teststd", "");
	//TwAddButton(mainMenuBar, "Benchmark for real design (gradient only)", UIButtonEventBatchTestGradient, "testgrad", "");

	TwAddVarRW(mainMenuBar, "Bullet CPs", TW_TYPE_BOOLCPP, &drawBulletCPs, " label='Draw Bullet Debug Info' group='Viz' key='b'");
	TwAddVarRW(mainMenuBar, "Bullet Closest Point Pairs", TW_TYPE_BOOLCPP, &drawClosestPointPairs, " label='Draw Closest Point Pairs Info' group='Viz' key='p'");

	TwAddSeparator(mainMenuBar, "sep10", "");
	TwAddButton(mainMenuBar, "Scale chassis", UIButtonScaleChassis, "scalechassis","");
	TwAddVarRW(mainMenuBar, "scale", TW_TYPE_DOUBLE, &chassisScale, " min=0.1 max=10 step=0.1 ");
	
	//	TwAddVarRW(mainMenuBar, "Cost function plots", TW_TYPE_BOOLCPP, &drawPlots, " label='Draw cost plots' group='Viz'");

	tWidget = new TranslateWidget(AXIS_X | AXIS_Y | AXIS_Z);
	rWidget = new RotateWidgetV2();
	rWidgetDir = new RotateWidgetV1();
	rWidgetDir->axis->localAxis = V3D(0, 1, 0);

	//objectMenuList.push_back(new Adafruit());
	//objectMenuList.push_back(new Sensor());
	//objectMenuList.push_back(new PCB());
	//objectMenuList.push_back(new Board());
	//objectMenuList.push_back(new Breakout());
	//objectMenuList.push_back(new MakeBlockGeneric());

	//objectMenuList.push_back(new BoxComponent(0.03, 0.02, 0.015, true));
	//objectMenuList.push_back(new SphereComponent(0.02, false));
	objectMenuList.push_back(new MakeBlockMotorDriver());
	objectMenuList.push_back(new MakeBlockOrionBoard());
	objectMenuList.push_back(new MakeBlockUltrasonic());
	objectMenuList.push_back(new MakeBlockBluetooth());
	//objectMenuList.push_back(new MakeBlockEncoderMotorDriver());
	objectMenuList.push_back(new MakeBlockAccelerometer());
	objectMenuList.push_back(new DCMotor());
	objectMenuList.push_back(new ServoMotor());
	objectMenuList.push_back(new MicroServoMotor());
	objectMenuList.push_back(new Battery9v());
	objectMenuList.push_back(new Battery12vHolder());
	objectMenuList.push_back(new MakeBlockSoundSensor());
	objectMenuList.push_back(new MakeBlockLED());
	objectMenuList.push_back(new MakeBlockButton());
	objectMenuList.push_back(new MakeBlockMegaPi());

	for (uint i = 0; i < objectMenuList.size(); i++) {
		windowArray->addSubWindow(new ObjectDisplayWindow(objectMenuList[i]));
	}

	planner = new MCMCPlanners(&mainAssembly);


	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
	plotWindow->createData("optValues", 1, 0, 0);
	plotWindow->createData("bestValue", 0, 1, 0);
	//plotWindow->createData("best10", 0, 0, 1);


	// user study enclosure and motors for each example:
	//string wallE = "siggraph17_results/userStudies/owl.txt";
	//mainAssembly.loadAssembly(wallE.c_str());
}

UI::~UI(void) {
}

//triggered when mouse moves
bool UI::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (rWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (rWidgetDir->onMouseMoveEvent(xPos, yPos) == true) return true;

	if (menuSelectedObject) {
		unloadMenuNameFor(menuSelectedObject);
		menuSelectedObject = NULL;
	}

	if (windowArray->isActive() == true) {
		if (selectedObject) {
			unloadMenuParametersFor(selectedObject);
			selectedObject->picked = false;
			selectedObject->onComponentDeselect();
			selectedObject = NULL;
		}

		for (size_t i = 0; i < windowArray->subWindows.size(); i++)
		{
			if (windowArray->subWindows[i]->isActive()) {
				menuSelectedObject = objectMenuList[i];
				loadMenuNameFor(objectMenuList[i]);
			}
		}

	}

	if (windowArray->onMouseMoveEvent(xPos, yPos) == true)
		return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true)
		return true;

	return false;
}

void UI::loadMenuNameFor(BaseObject* objPrimitive) {
	//obj name
	std::string objName = objPrimitive->name;
	TwAddButton(mainMenuBar, objName.c_str(), NULL, NULL, " ");
}

void UI::unloadMenuNameFor(BaseObject* objPrimitive) {
	std::string objName = objPrimitive->name;
	TwRemoveVar(mainMenuBar, objName.c_str());
}

void UI::loadMenuParametersFor(BaseObject* objPrimitive) {
	//obj name
	std::string objName = objPrimitive->name;
	TwAddButton(mainMenuBar, objName.c_str(), NULL, NULL, " ");
	//obj assembly-dir properties
	DynamicArray<std::pair<char*, double*>> params1 = objPrimitive->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_DOUBLE, it->second, "min=-1 max=1 step=0.1");
	}

	EMComponent* emComponent = dynamic_cast<EMComponent*>(objPrimitive);
	if (!emComponent) return;

	//obj rotations
	TwAddButton(mainMenuBar, "Allow optimization of rotations about", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "..(select 1)..", NULL, NULL, " ");
	DynamicArray<std::pair<char*, bool*>> paramsRot = emComponent->getListOfRotationDOFs();
	for (auto it = paramsRot.begin(); it != paramsRot.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_BOOLCPP, it->second, "");
	}

	//obj positions
	TwAddButton(mainMenuBar, "Allow optimization of positions along", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "(in-plane restriction)", NULL, NULL, " ");
	DynamicArray<std::pair<char*, bool*>> paramsPos = emComponent->getListOfPositionDOFs();
	for (auto it = paramsPos.begin(); it != paramsPos.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_BOOLCPP, it->second, "");
	}

	// obj disassembly index
	TwAddButton(mainMenuBar, "Assembly plan order", NULL, NULL, " ");
	int maxIndex = mainAssembly.electroMechanicalComponents.size() - 1;
	string range = "min=0 max=" + to_string(maxIndex) + " step=1";
	emComponent->setAssemblyIndex(maxIndex);
	DynamicArray<std::pair<char*, int*>> paramsAssem = emComponent->getOrder(maxIndex);
	for (auto it = paramsAssem.begin(); it != paramsAssem.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_INT32, it->second, range.c_str());
	}
}

void UI::unloadMenuParametersFor(BaseObject* objPrimitive) {
	std::string objName = objPrimitive->name;
	TwRemoveVar(mainMenuBar, objName.c_str());
	DynamicArray<std::pair<char*, double*>> params1 = objPrimitive->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it)
		TwRemoveVar(mainMenuBar, it->first);

	EMComponent* emComponent = dynamic_cast<EMComponent*>(objPrimitive);
	if (!emComponent) return;

	//obj rotations
	TwRemoveVar(mainMenuBar, "Allow optimization of rotations about");
	TwRemoveVar(mainMenuBar, "..(select 1)..");
	DynamicArray<std::pair<char*, bool*>> params0 = emComponent->getListOfRotationDOFs();
	for (auto it = params0.begin(); it != params0.end(); ++it) {
		TwRemoveVar(mainMenuBar, it->first);
	}

	//obj positions
	TwRemoveVar(mainMenuBar, "Allow optimization of positions along");
	TwRemoveVar(mainMenuBar, "(in-plane restriction)");
	DynamicArray<std::pair<char*, bool*>> params00 = emComponent->getListOfPositionDOFs();
	for (auto it = params00.begin(); it != params00.end(); ++it) {
		TwRemoveVar(mainMenuBar, it->first);
	}

	int maxIndex = mainAssembly.electroMechanicalComponents.size() - 1;
	TwRemoveVar(mainMenuBar, "Assembly plan order");
	DynamicArray<std::pair<char*, int*>> params3 = emComponent->getOrder(maxIndex);
	for (auto it = params3.begin(); it != params3.end(); ++it) {
		TwRemoveVar(mainMenuBar, it->first);
	}
}

//triggered when using the mouse wheel
bool UI::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (windowArray->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

void UI::loadFile(const char* fName) {
	// LOADING CHASSIS IN FUTURE AS MESH
	//std::string fileName;
	//fileName.assign(fName);
	//std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	//if (fNameExt == "obj")
	//{
	//	
	//}
	mainAssembly.loadAssembly(fName);
}

void UI::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

bool UI::onKeyEvent(int key, int action, int mods) {
	if (key >= '1' && key <= '5'){
		runOption = key - '1';

		if (action == GLFW_PRESS) {
			//this->appIsRunning = true;
			if (runOption == GRADIENT_BASED_OPTIMIZATION) {
				Logger::consolePrint("Starting gradient based optimization.....\n");
				gradItr = 0;
			}
			else if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS)
				Logger::consolePrint("Animating assembly process!\n");
			else if (runOption == MC_BASED_OPTIMIZATION)
				Logger::consolePrint("Starting Mixed MCMC Optimization.....\n");
		}

		return true;
	}

	if (windowArray->onKeyEvent(key, action, mods)) return true;

	// preventing app killing on esc press
	if (key == GLFW_KEY_ESCAPE) {
		return true;
	}

	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool UI::onCharacterPressedEvent(int key, int mods) {
	if (key == 'r') {
		translateRotateWidget = !translateRotateWidget;
		return true;
	}

	if (key == 'f') {
		if (selectedObject)
			selectedObject->toggleFreeze();
	}

	if (key == 'a') {
		rWidgetDir->visible = !rWidgetDir->visible;
		return true;
	}

	if (windowArray->onCharacterPressedEvent(key, mods)) return true;
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool UI::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (tWidget->isPicked() || rWidgetDir->isPicked() || rWidget->isPicked()) return true;
	Ray clickedRay = getRayFromScreenCoords(xPos, yPos);

	//if (rWidget->isPicked() && (mods & GLFW_MOD_SHIFT)) {
	//	auto rAxes = dynamic_cast<RotateWidgetV2*> (rWidget)->rAxes;
	//	for (auto it = rAxes.begin(); it != rAxes.end(); ++it)
	//		if (it->isPicked) {

	//			double newTorusAngle = it->getAngleOnTorusForRay(clickedRay);
	//			double ang;
	//			Quaternion q = rWidget->getOrientation();
	//			ang = q.getRotationAngle(it->rotationAxis);
	//			double angleOffset = abs(ang);// -it->lastPickedAngle;
	//			if (angleOffset > 2 * PI) angleOffset -= 2 * PI;
	//			if (angleOffset < -2 * PI) angleOffset += -2 * PI;

	//			Logger::consolePrint("offset:%lf, ang:%lf\n\n", angleOffset, newTorusAngle, it->lastPickedAngle,ang);
	//			if (abs(angleOffset) > 3 * PI / 2 + PI / 4 && abs(angleOffset) <= PI / 4) angleOffset = 0;
	//			if (abs(angleOffset) > PI / 4 && abs(angleOffset) <= PI / 2 + PI / 4) angleOffset = PI / 2 * SGN(ang);
	//			if (abs(angleOffset) >  PI/2 + PI / 4 && abs(angleOffset) <= PI + PI/4) angleOffset = PI*SGN(ang);
	//			if (abs(angleOffset) > PI + PI/4 && abs(angleOffset) <= 3*PI/2 + PI/4) angleOffset = 3*PI/2 * SGN(ang);

	//			//note: the twist angle is not bounded on either side, so it can increase arbitrarily...
	//			Quaternion orientation;
	//			orientation = getRotationQuaternion(angleOffset, it->rotationAxis)*q;
	//			//it->lastPickedAngle = angleOffset;
	//			rWidget->setOrientation(orientation);
	//			return true;
	//		}
	//}
	//else if (rWidget->isPicked())
		//return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (windowArray->isActive() == false && button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		// check to see which menu item is selected and clone it
		for (uint i = 0; i < windowArray->subWindows.size(); i++) {
			if (windowArray->subWindows[i]->isSelected() || (i == 0 && mods & GLFW_MOD_ALT)) {

				//unselect any previously selected objects
				if (selectedObject) {
					unloadMenuParametersFor(selectedObject);
					selectedObject->picked = false;
					selectedObject->onComponentDeselect();
					selectedObject = NULL;
				}

				P3D clickedPoint;
				clickedRay.getDistanceToPoint(P3D(), &clickedPoint);
				tWidget->pos = clickedPoint;
				mainAssembly.electroMechanicalComponents.push_back(objectMenuList[i]->clone());
				mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = mainAssembly.electroMechanicalComponents.size() - 1;
				mainAssembly.electroMechanicalComponents.back()->setPosition(clickedPoint);
				windowArray->subWindows[i]->selected = false;

				planner->plannerState->initialized = false;
				planner->plannerState->converged = false;
				planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;
				return true;		
			}
		}

		// check if object in main window is selected

		//first of all, if the shift key is not held down, then unpick everything...
		if (!(mods & GLFW_MOD_SHIFT)) {
			for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++)
				mainAssembly.electroMechanicalComponents[i]->picked = false;
		}

		mainAssembly.collisionManager->setupCollisionPrimitives(0, CM_EMCOMPONENTS);
		BaseObject* selectionObject = mainAssembly.collisionManager->getFirstObjectHitByRay(clickedRay);

		if (selectionObject)
			selectionObject->picked = !selectionObject->picked;

		//now, if only one object is picked, then it is selected. And selected objects have some special meaning...
		BaseObject* newObjectSelection = NULL;
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
			if (mainAssembly.electroMechanicalComponents[i]->picked) {
				if (newObjectSelection == NULL)
					newObjectSelection = mainAssembly.electroMechanicalComponents[i];
				else {
					newObjectSelection = NULL;
					break;
				}
			}
		}

		//now, if we have a new selection, then deselect the old one, and select the new one
		if (newObjectSelection != selectedObject) {
			if (selectedObject != NULL) {
				unloadMenuParametersFor(selectedObject);
				selectedObject->onComponentDeselect();
			}
			if (newObjectSelection != NULL) {
				loadMenuParametersFor(newObjectSelection);
				newObjectSelection->setWidgetsFromObjectState(tWidget, rWidget);
				if (dynamic_cast<EMComponent*> (newObjectSelection)) {
					V3D v= dynamic_cast<EMComponent*> (newObjectSelection)->disassemblyPath->getWorldCoordinatesAssemblyPath();
					Quaternion Q = getRotationAxisThatAlignsVectors(V3D(0, 1, 0),v);
					rWidgetDir->setOrientation(Q);
				}
				newObjectSelection->onComponentSelect();
			}
			selectedObject = newObjectSelection;
		}
	}

	if (windowArray->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}


// Run the App tasks
void UI::process() {
	// do the work here...
	if (selectedObject) {
		selectedObject->setObjectStateFromWidgets(tWidget, rWidget);
	}

	if (runOption == DEFAULT) {
		animPhase = 0.0;
		this->appIsRunning = false;
	}

	if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
		animPhase += animStep / (MAX(mainAssembly.getSortedListOfAssemblableComponents().size(), 1));
		//if (animPhase > 1) animPhase -= 1;
		if (animPhase > 1) {
			animPhase = 1;
			runOption = DEFAULT;
		}
	}
	
	if (runOption == GRADIENT_BASED_OPTIMIZATION) {
		mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
		bool converged = false;
		mainAssembly.minSupportVolume = true;
		//mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
		//mainAssembly.constraintsManager->constraintsCollection->printDebugInfo = true;
		double val = mainAssembly.constraintsManager->fixConstraints(converged);
		Logger::consolePrint("paramSize:%d \n", mainAssembly.constraintsManager->params.size());
		if (converged) {
			Logger::consolePrint("gradient converged at Itr:%d!\n", gradItr + 1);
		}
		else {
			gradItr = gradItr + 1;
			if (gradItr % 5 == 0)
				Logger::consolePrint("Assembly cost:%lf, Gradient Iterations:%d\n", val, gradItr);
		}
		planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;
		planner->plannerState->initialized = false; 

	}
	if (runOption == MC_BASED_OPTIMIZATION) {
		//int statesEvaluated = 0;
		//planner->searchOrderAndPlacementWithParallelTemperedMH_adaptive(7, statesEvaluated,
			//600, 10, 0.001);
		if (!planner->plannerState->converged) {
			if (!planner->plannerState->initialized) {
				planner->setUpPlanner();
			}
			Timer planTimer;
			planner->interactiveSearchPTMH_adaptive();
			//Logger::consolePrint("plan-step time:%lf\n", planTimer.timeEllapsed());
			if (planner->plannerState->converged) {
				this->appIsRunning = false;
				Logger::consolePrint("time for MCMC: %lf\n", this->processTimer.timeEllapsed());
			}
		}
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void UI::drawScene() {
	//sync with the widgets...
	rWidget->pos = tWidget->pos;
	rWidgetDir->pos = tWidget->pos;

	if (selectedObject) {
		tWidget->visible = translateRotateWidget;
		rWidget->visible = !translateRotateWidget;
		selectedObject->setObjectStateFromWidgets(tWidget, rWidget);
		if (dynamic_cast<EMComponent*> (selectedObject)) {
			Quaternion Q = rWidgetDir->getOrientation();
			dynamic_cast<EMComponent*> (selectedObject)->disassemblyPath->setWorldCoordinatesAssemblyPath(Q);
		}
		selectedObject->onComponentSelect();
	}
	else {
		tWidget->visible = rWidget->visible = false;
		rWidgetDir->visible = false;
	}

	//updating support at each step right now..
	mainAssembly.snapSupportsToWalls();

	//draw all the support structures...
	DynamicArray<RigidlyAttachedSupport*> supportObjs;
	for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
		supportObjs.clear();
		mainAssembly.electroMechanicalComponents[i]->addSupportsToList(supportObjs);
		for (uint j = 0; j < supportObjs.size(); j++) {
			supportObjs[j]->draw();
			//supportObjs[j]->drawWallExtension();
		}

		// visualize wire meshes
		//if (dynamic_cast<MakeBlockComponents*>(mainAssembly.electroMechanicalComponents[i]) &&
			//!dynamic_cast<Battery9v*>(mainAssembly.electroMechanicalComponents[i]))
			//dynamic_cast<MakeBlockComponents*>(mainAssembly.electroMechanicalComponents[i])->getWireGeometryMesh()->drawMesh();
	}


	//draw collision primitives...
	if (mainAssembly.collisionManager && drawBulletCPs) {
		//draw them once in their original, pickable position...
		
		//and now in their animated position...
		if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS){
			if (runOption == DISASSEMBLY_VISUALIZATION)
				mainAssembly.collisionManager->setupCollisionPrimitives(animPhase);
			else
				mainAssembly.collisionManager->setupCollisionPrimitives(animPhase, CM_EMCOMPONENTS, COL_SWEPT_CVX_HULL);
			mainAssembly.collisionManager->drawCollisionPrimitives();
			mainAssembly.collisionManager->processCollisions();
			for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
				glColor3d(0, 1, 0);
				drawSphere(mainAssembly.collisionObjects[i].p_WorldOnO1, 0.001);
				glColor3d(0, 0, 1);
				drawSphere(mainAssembly.collisionObjects[i].p_WorldOnO2, 0.001);
			}
		}
		else {
			mainAssembly.collisionManager->setupCollisionPrimitives();
			mainAssembly.collisionManager->drawCollisionPrimitives();
		}
	}

	glEnable(GL_LIGHTING);

	if (mainAssembly.collisionManager && drawClosestPointPairs){
		mainAssembly.collisionManager->performCollisionDetectionForEntireAssemblyProcess();
		//NOTE: at some point may want to be much more conservative with when we even check collisions between objects i and j... perhaps through an overall convex hull that includes swept object, supports, fastners?
		/*for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++){
			double panicLevel = mapTo01Range(V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(), 0, 0.05);
			glColor3d(1, panicLevel, panicLevel);
			drawCylinder(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2, 0.002);
		}*/
		if (runOption != DISASSEMBLY_VISUALIZATION && runOption != DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
			for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
				if (mainAssembly.collisionObjects[i].penetrationDepth < 0) {
					//if ((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//	(dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//	mainAssembly.chassis->collided = true;

					//	if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//		(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//		continue;
					//	}
					//	else {
					//		mainAssembly.chassis->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//	}
					//}
					////else {
					//	//double panicLevel = mapTo01Range(V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(), 0, 0.09);
					//	mainAssembly.collisionObjects[i].obj1->collided = true;
					//	mainAssembly.collisionObjects[i].obj2->collided = true;

					//	if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//		(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//		continue;
					//	}
					//	else {
					//		mainAssembly.collisionObjects[i].obj2->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//		mainAssembly.collisionObjects[i].obj1->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//	}
					////}

					/*Logger::consolePrint(" len:%lf, PD:%lf, obj1Name:%s, obj2Name:%s, collideColor: %lf \n", V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(),
						mainAssembly.collisionObjects[i].penetrationDepth, mainAssembly.collisionObjects[i].obj1->name.c_str(),
						mainAssembly.collisionObjects[i].obj2->name.c_str(), 0.5 + abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10);*/
					////plotWindow->addDataPoint(0, V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length());
					////plotWindow->addDataPoint(1, abs(mainAssembly.collisionObjects[i].penetrationDepth));

					if ((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) ||
						(dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))) {
						mainAssembly.chassis->collided = true;
					}
					if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)))
						(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1))->parent->collided = true;
					if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2)))
						(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))->parent->collided = true;
					mainAssembly.collisionObjects[i].obj1->collided = true;
					mainAssembly.collisionObjects[i].obj2->collided = true;
				}
			}
		}
	}

	// draw objects and fasteners in the assembly...
	if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS){
		//first off, see how many items there are to be disassembled... EMcomponents, fastners, etc. And we assume each one of these is disassemblable, so there are as many unique disassembly indices as objects...
		//DynamicArray<AssemblableComponent*> assemblableComponents = mainAssembly.getSortedListOfAssemblableComponents();
		DynamicArray<AssemblableComponent*> assemblableComponents = mainAssembly.getSortedListReverse();

		double range = 1.0 / assemblableComponents.size();
		int activeObjIndex = (int)(animPhase / range);
		double activeObjPhase = (animPhase - activeObjIndex * range) / range;
		//NOTE: we are assuming that all these assemblable components are sorted by disassembly index, with dependents (e.g. fastners) following their electromechanical component parents
		for (uint i = assemblableComponents.size(); i-- > 0;) {
		//for (uint i = 0; i < assemblableComponents.size(); i++) {
			if ((int)i < activeObjIndex)
				assemblableComponents[i]->drawOnAssemblyPath(0);
			else if ((int)i == activeObjIndex) {
				mainAssembly.collisionManager->highlightComponentsCollidingWith(assemblableComponents[i]);
				assemblableComponents[i]->drawOnAssemblyPath(1.00 - activeObjPhase);
				//assemblableComponents[i]->drawOnAssemblyPath(activeObjPhase);
			}
			//else
			//	assemblableComponents[i]->drawOnAssemblyPath(1);
		}
		if (runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
			 assemblableComponents[activeObjIndex]->drawSweptConvexHull();
		}
	}
	else {
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++)
			mainAssembly.electroMechanicalComponents[i]->draw();
		//draw all the fasteners...
		DynamicArray<Fastener*> fastenerObjs;
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
			fastenerObjs.clear();
			mainAssembly.electroMechanicalComponents[i]->addFastnersToList(fastenerObjs);
			for (uint j = 0; j < fastenerObjs.size(); j++) {
				fastenerObjs[j]->draw();
			}
		}
	}

	// chassis and walls..
	if(mainAssembly.chassis)
		mainAssembly.chassis->draw();
	for (size_t i = 0; i < mainAssembly.chassis->walls.size(); i++){
		mainAssembly.chassis->walls[i].draw();
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void UI::drawAuxiliarySceneInfo() {
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);

	// draw the widgets
	tWidget->draw();
	rWidget->draw();
	rWidgetDir->draw();
	
	// plots of cost function
	if (drawPlots)
		plotWindow->draw();

	// subwindow container window drawn
	windowArray->draw();

	// displaying closest points for debugging
	////if (runOption != DISASSEMBLY_VISUALIZATION && runOption != DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
	//	for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
	//		if (mainAssembly.collisionObjects[i].penetrationDepth < 0) {
	//			P3D p1 = mainAssembly.collisionObjects[i].p_WorldOnO1;
	//			P3D p2 = mainAssembly.collisionObjects[i].p_WorldOnO2;
	//			glColor3d(1, 0, 0);
	//			drawSphere(p1, 0.002);
	//			glColor3d(0, 1, 0);
	//			drawSphere(p2, 0.002);

	//			//if (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) {
	//			//	glColor3d(0, 0, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->wallCenter, p1, 0.001);
	//			//	glColor3d(0, 1, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->wallCenter, (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->bounds, 0.001);
	//			//}

	//			//if (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2)) {
	//			//	glColor3d(0, 0, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->wallCenter, p2, 0.001);
	//			//	glColor3d(0, 1, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->wallCenter, (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->bounds, 0.001);
	//			//}

	//			}
	//		}
	//	//}

	// wall normals
	//for (size_t i = 0; i < mainAssembly.chassis->walls.size(); i++)
	//{
	//	glColor3d(1, 0, 0);
	//	drawArrow(mainAssembly.chassis->walls[i].wallCenter, mainAssembly.chassis->walls[i].wallCenter + mainAssembly.chassis->walls[i].normal*
	//		0.05, 0.001);
	//	glColor3d(0, 1, 0);
	//	drawSphere(mainAssembly.chassis->walls[i].wallCenter, 0.005);
	//}
}

// Restart the application.
void UI::restart() {
	mainAssembly.collisionManager->clearCollisionPrimitives();
	if (selectedObject) {
		unloadMenuParametersFor(selectedObject);
		selectedObject->onComponentDeselect();
		selectedObject = NULL;
	}

	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
	planner->plannerState->initialized = false;
	planner->plannerState->converged = false;
	planner->plannerState->mode = NEW_ASSEMBLY_MODE;
	// for balbot..
	//planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;

	runOption = DEFAULT;
	animPhase = 0.0;

	// reloading user study file:
	//string wallE = "siggraph17_results/userStudies/owl.txt";
	//mainAssembly.loadAssembly(wallE.c_str());
}

bool UI::processCommandLine(const std::string& cmdLine) {

	if (strcmp(cmdLine.c_str(), "delete") == 0) {
		onDelete();
		return true;
	}

	if (strcmp(cmdLine.c_str(), "save") == 0) {
		mainAssembly.saveAssembly();
		return true;
	}

	if (strcmp(cmdLine.c_str(), "load") == 0) {
		mainAssembly.loadAssembly("assembly.txt");
		return true;
	}

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}


void UI::onDelete() {

	// resetting run option
	runOption = DEFAULT;

	// reset planner after deleting
	planner->plannerState->initialized = false;
	planner->plannerState->converged = false;
	planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;

	mainAssembly.collisionManager->clearCollisionPrimitives();
	if (selectedObject) {
		unloadMenuParametersFor(selectedObject);
		selectedObject->onComponentDeselect();
		selectedObject = NULL;
	}

	for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size();) {
		if (mainAssembly.electroMechanicalComponents[i]->picked) {
			delete mainAssembly.electroMechanicalComponents[i];
			mainAssembly.electroMechanicalComponents.erase(mainAssembly.electroMechanicalComponents.begin() + i);
		}
		else
			++i;
	}

	planner->setTopDownAssemblyOrder(&mainAssembly);

	/*for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
		mainAssembly.electroMechanicalComponents[i]->disassemblyIndex = i;
	}*/

	mainAssembly.collisionObjects.clear();
	mainAssembly.collisionManager->clearCollisionPrimitives();


}

double UI::createVirtualAssemblyUsingComponents(double seed, double targetFillRatio) {
	//delete all the objects in the GUI
	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	double chassisvolume = 0;
	if (mainAssembly.chassis) {
		double l, h, b;
		mainAssembly.chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8 * l*b*h;
	}


	double totalvolume = 0;
	double fillratio = 0;
	int num_mainboard = 0;
	int count = 0;

	//push back random objects until the filling ratio is bigger than given value
	while (fillratio < targetFillRatio) {
		std::uniform_int_distribution<int> uniftype(1, 7);
		std::default_random_engine re(seed + count);
		int type = uniftype(re);
		switch (type) {
		case 1: mainAssembly.electroMechanicalComponents.push_back(new PCB());  break;
		case 2: mainAssembly.electroMechanicalComponents.push_back(new Sensor()); break;
		case 3: if (num_mainboard < 1) {//only one maim board will be added
			mainAssembly.electroMechanicalComponents.push_back(new MakeBlockGeneric());
			num_mainboard++;
		}	break;
		case 4: mainAssembly.electroMechanicalComponents.push_back(new Breakout());  break;
		case 5: mainAssembly.electroMechanicalComponents.push_back(new Adafruit());  break;
		case 6: mainAssembly.electroMechanicalComponents.push_back(new Board()); break;
		case 7: mainAssembly.electroMechanicalComponents.push_back(new Battery9v()); break;
		}

		double volume = 0;
		DynamicArray<btCollisionObject*> newCollisionObjs;
		mainAssembly.electroMechanicalComponents.back()->addBulletCollisionObjectsToList(newCollisionObjs);
		for (uint t = 0; t < newCollisionObjs.size(); t++) {
			btTransform T;
			T = newCollisionObjs[t]->getWorldTransform();
			//bounding box diagonal points
			btVector3 bmin, bmax;
			newCollisionObjs[t]->getCollisionShape()->getAabb(T, bmin, bmax);
			btVector3 size;
			size[0] = abs((bmax[0] - bmin[0]));
			size[1] = abs((bmax[1] - bmin[1]));
			size[2] = abs((bmax[2] - bmin[2]));
			volume += size[0] * size[1] * size[2];
		}

		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = count;
		count++;

		//calculate the actual filing ratio by accoutning for supports..
		DynamicArray<RigidlyAttachedSupport*> supports;//store th elist of supports to give it to the collidearea function
		mainAssembly.electroMechanicalComponents.back()->addSupportsToList(supports);
		for (uint j = 0; j < supports.size(); j++) {
			if (supports[j]->closestWallId == -1) {
				mainAssembly.findClosestChassisWallToSupport(supports[j]);
				supports[j]->updateSupportSize();
			}
			double sxsup1, sysup1, szsup1;
			supports[j]->getSilhouetteScalingDimensions(sxsup1, sysup1, szsup1);//size of support
			volume += abs(sxsup1*szsup1*sysup1) * 8;
			//volume += abs(sxsup1*szsup1*(mainAssembly.chassis->yMax/2 - 0.005)) * 4; //0.005 is chassis bottom wall thickness
		}

		totalvolume += volume;
		fillratio = totalvolume / chassisvolume;
		newCollisionObjs.clear();
	}

	return fillratio;
}

double UI::createVirtualAssemblyUsingBoxComponents(double seed, double targetFillRatio) {
	//delete all the objects in the GUI
	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	double chassisvolume = 0;
	if (mainAssembly.chassis) {
		double l, h, b;
		mainAssembly.chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8*l*b*h;
	}


	double totalvolume = 0;
	double fillratio = 0;
	int num_mainboard = 0;
	int count = 0;

	//push back random objects until the filling ratio is bigger than given value
	while (fillratio < targetFillRatio) {

		//std::uniform_real_distribution<double> sizeDist(0.02, 0.05);
		std::uniform_real_distribution<double> sizeDist(0.01, 0.03);
		std::default_random_engine re(seed + count);

		//mainAssembly.electroMechanicalComponents.push_back(new BoxComponent(0.03, 0.015, 0.03, true));
		mainAssembly.electroMechanicalComponents.push_back(new BoxComponent(sizeDist(re), sizeDist(re), sizeDist(re), true));
		double volume = 0;
		DynamicArray<btCollisionObject*> newCollisionObjs;
		mainAssembly.electroMechanicalComponents.back()->addBulletCollisionObjectsToList(newCollisionObjs);
		for (uint t = 0; t < newCollisionObjs.size(); t++) {
			btTransform T;
			T = newCollisionObjs[t]->getWorldTransform();
			//bounding box diagonal points
			btVector3 bmin, bmax;
			newCollisionObjs[t]->getCollisionShape()->getAabb(T, bmin, bmax);
			btVector3 size;
			size[0] = abs((bmax[0] - bmin[0]));
			size[1] = abs((bmax[1] - bmin[1]));
			size[2] = abs((bmax[2] - bmin[2]));
			volume += size[0] * size[1] * size[2];
		}

		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = count;
		count++;

		//calculate the actual filing ratio by accoutning for supports..
		DynamicArray<RigidlyAttachedSupport*> supports;//store th elist of supports to give it to the collidearea function
		mainAssembly.electroMechanicalComponents.back()->addSupportsToList(supports);
		for (uint j = 0; j < supports.size(); j++) {
			if (supports[j]->closestWallId == -1) {
				mainAssembly.findClosestChassisWallToSupport(supports[j]);
				supports[j]->updateSupportSize();
			}
			double sxsup1, sysup1, szsup1;
			supports[j]->getSilhouetteScalingDimensions(sxsup1, sysup1, szsup1);//size of support
			volume += abs(sxsup1*szsup1*sysup1) * 8;
			//volume += abs(sxsup1*szsup1*(mainAssembly.chassis->yMax/2 - 0.005)) * 4; //0.005 is chassis bottom wall thickness
		}

		totalvolume += volume;
		fillratio = totalvolume / chassisvolume;
		newCollisionObjs.clear();
	}

	return fillratio;
}

double UI::createVirtualAssemblyUsingBoxComponents(double seed, double targetFillRatio, int numComponents) {
	//delete all the objects in the GUI
	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	double totalvolume = 0;
	double fillratio = 0;

	std::uniform_real_distribution<double> sizeDist(0.01, 0.04); //0.05
	for (size_t i = 0; i < numComponents; i++)
	{
		std::default_random_engine re(seed + i);
		double compVolume = 0;
		EMComponent* compToAdd = new BoxComponent(sizeDist(re), sizeDist(re), sizeDist(re), true);
		DynamicArray<btCollisionObject*> newCollisionObjs;
		compToAdd->addBulletCollisionObjectsToList(newCollisionObjs);

		for (uint t = 0; t < newCollisionObjs.size(); t++) {
			btTransform T;
			T = newCollisionObjs[t]->getWorldTransform();
			//bounding box diagonal points
			btVector3 bmin, bmax;
			newCollisionObjs[t]->getCollisionShape()->getAabb(T, bmin, bmax);
			btVector3 size;
			size[0] = abs((bmax[0] - bmin[0]));
			size[1] = abs((bmax[1] - bmin[1]));
			size[2] = abs((bmax[2] - bmin[2]));
			compVolume += size[0] * size[1] * size[2];
		}

		//calculate the actual filing ratio by accoutning for supports..
		DynamicArray<RigidlyAttachedSupport*> supports;//store the list of supports to give it to the collidearea function
		compToAdd->addSupportsToList(supports);
		for (uint j = 0; j < supports.size(); j++) {
			if (supports[j]->closestWallId == -1) {
				mainAssembly.findClosestChassisWallToSupport(supports[j]);
				supports[j]->updateSupportSize();
			}
			double sxsup1, sysup1, szsup1;
			supports[j]->getSilhouetteScalingDimensions(sxsup1, sysup1, szsup1);//size of support
			compVolume += abs(sxsup1*szsup1*sysup1) * 8;
		}

		totalvolume = totalvolume + compVolume;
		mainAssembly.electroMechanicalComponents.push_back(compToAdd);
		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = i;

		newCollisionObjs.clear();
	}

	double reqdChassisVolume = totalvolume / targetFillRatio;
	double reqdChassisLen = pow(reqdChassisVolume / (sqrt(2)), 1.0/3.0);
	//Logger::consolePrint("reqdChVol: %lf, reqdCLen:%lf\n", reqdChassisVolume, reqdChassisLen);
	double chassisvolume = 0;
	if (mainAssembly.chassis) {
		if (dynamic_cast<VirtualChassis*>(mainAssembly.chassis))
			dynamic_cast<VirtualChassis*>(mainAssembly.chassis)->updateSizeChassis(reqdChassisLen);

		// crosscheck
		double l, h, b;
		mainAssembly.chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8 * l*b*h;

		//Logger::consolePrint("l,b,h: %lf %lf %lf\n", l,b,h);
	}

	fillratio = totalvolume / chassisvolume;
	return fillratio;
}

void UI::createVirtualAssemblyForDataSet(int numObjs) {
	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	int count = 0;
	for (int i = 0; i < numObjs/3; i++)
	{	
		// add 3 types of objs
		mainAssembly.electroMechanicalComponents.push_back(new BoxComponent(0.02, 0.02, 0.02, false));
		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = count;
		count++;

		mainAssembly.electroMechanicalComponents.push_back(new SphereComponent(0.015, false));
		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = count;
		count++;

		mainAssembly.electroMechanicalComponents.push_back(new BoxComponent(0.03, 0.02, 0.035, false));
		mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = count;
		count++;
	}
}

void TW_CALL UIButtonEventCreateSupport(void* clientData) {
	if (strcmp((char*)clientData, "support") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app)
			app->mainAssembly.snapSupportsToWalls();
	}
}


void TW_CALL UIButtonEventLoad(void* clientData) {
	if (strcmp((char*)clientData, "load") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			app->mainAssembly.collisionManager->clearCollisionPrimitives();
			if (app->selectedObject) {
				app->unloadMenuParametersFor(app->selectedObject);
				app->selectedObject->onComponentDeselect();
				app->selectedObject = NULL;
			}

			while (!app->mainAssembly.electroMechanicalComponents.empty()) {
				delete app->mainAssembly.electroMechanicalComponents.back();
				app->mainAssembly.electroMechanicalComponents.pop_back();
			}

			app->mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
			app->mainAssembly.loadAssembly("assembly.txt");
		}
	}
}

void TW_CALL UIButtonEventDelete(void* clientData) {
	if (strcmp((char*)clientData, "delete") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app)
			app->onDelete();
	}
}

void TW_CALL UIButtonEventSave(void* clientData) {
	if (strcmp((char*)clientData, "save") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app)
			app->mainAssembly.saveAssembly();
	}
}

void TW_CALL UIButtonEventAssemblyFillRatio(void* clientData) {
	if (strcmp((char*)clientData, "fillRatio") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			double ratio = app->mainAssembly.calculateFillRatio();
			double compRatio = app->mainAssembly.calculateComponentFillRatio();
			double supportRatio = app->mainAssembly.calculateSupportFillRatio();
			Logger::consolePrint("Assembly fill ratio: %lf, comp ratio :%lf, support ratio: %lf, net:%lf\n", ratio, compRatio, supportRatio,
				supportRatio+compRatio);
			//double cost = app->planner->getAssemblyCost(&app->mainAssembly);
			//Logger::consolePrint("Assembly cost: %lf\n", cost);
		}
	}
}

void TW_CALL UIButtonEventSetAssemblyOrder(void* clientData) {
	if (strcmp((char*)clientData, "order") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			app->planner->setTopDownAssemblyOrder(&app->mainAssembly);
			Logger::consolePrint(" set order based on height\n");
		}
	}
}

void TW_CALL UIButtonEventSwapOrder(void* clientData) {
	if (strcmp((char*)clientData, "swaporder") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			DynamicArray<EMComponent*> toSwapComponents;
			for (size_t i = 0; i < app->mainAssembly.electroMechanicalComponents.size(); i++)
			{
				if (app->mainAssembly.electroMechanicalComponents[i]->picked) {
					toSwapComponents.push_back(app->mainAssembly.electroMechanicalComponents[i]);
				}
			}

			if (toSwapComponents.size() > 2)
				Logger::consolePrint("Select only 2 components for swap\n");
			else {
				std::swap(toSwapComponents[0]->disassemblyIndex, toSwapComponents[1]->disassemblyIndex);
				Logger::consolePrint("Swapped assembly order of selected components\n");
			}

			toSwapComponents.clear();
		}
	}
}

void TW_CALL UIButtonEventResetAnim(void* clientData) {
	if (strcmp((char*)clientData, "reset") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			app->animPhase = 0.00;
		}
	}

}

void TW_CALL UIButtonEventBenchMark(void* clientData) {
	if (strcmp((char*)clientData, "benchmark") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			double outputFill = app->createVirtualAssemblyUsingComponents(0, app->fillRatio);
			Logger::consolePrint("Virtual assembly created with arbitrary components with fill ratio:%lf, num components:%d\n",
				outputFill, app->mainAssembly.electroMechanicalComponents.size());
			app->planner->plannerState->mode = NEW_ASSEMBLY_MODE;
			app->planner->setUpPlanner();
		}
	}
}

void TW_CALL UIButtonEventBenchMarkWithBoxes(void* clientData) {
	if (strcmp((char*)clientData, "benchmarkboxes") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			double outputFill = app->createVirtualAssemblyUsingBoxComponents(0, app->fillRatio);
			Logger::consolePrint("Virtual assembly created with arbitrary boxes with fill ratio:%lf, num components:%d\n",
				outputFill, app->mainAssembly.electroMechanicalComponents.size());
			app->planner->plannerState->mode = NEW_ASSEMBLY_MODE;
			app->planner->setUpPlanner();
		}
	}
}

void TW_CALL UIButtonEventBenchMarkWithBoxes2(void* clientData) {
	if (strcmp((char*)clientData, "benchmarkboxes2") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			double outputFill = app->createVirtualAssemblyUsingBoxComponents(0, app->fillRatio, app->numComponentsVirtual);
			Logger::consolePrint("Virtual assembly created with arbitrary boxes with fill ratio:%lf, num components:%d\n",
				outputFill, app->mainAssembly.electroMechanicalComponents.size());
			app->planner->plannerState->mode = NEW_ASSEMBLY_MODE;
			app->planner->setUpPlanner();
		}
	}
}

void TW_CALL UIButtonEventExport(void* clientData) {
	if (strcmp((char*)clientData, "export") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			app->mainAssembly.saveCAD();
			Logger::consolePrint("Exported support and fastener geomerty for external boolean operations\n");
		}
	}
}

void TW_CALL UIButtonEventBatchTest(void* clientData) {
	if (strcmp((char*)clientData, "test") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "avgDesignStatsClumsy1000s_10runsSeed99Moves08_AssemOptimInGradExitValid.csv", "w");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "collision free assembly after PTMC,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(99);

			Timer planTimer0;
			for (int j = 0; j < 10; j++) {
				int seed = rand()%100;
				app->restart();
				app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;
				app->mainAssembly.loadAssembly("clumsyTestAssembly_wAllowedRot.txt");
				app->planner->setUpPlanner(seed);
				Timer planTimer1;
				double plannerCost = 0;
				while (!app->planner->plannerState->converged) {
					plannerCost = app->planner->interactiveSearchPTMH_adaptive();
					if (planTimer1.timeEllapsed() > 1000) {
						break;
					}
				}
				//Logger::consolePrint("total time ellapsed for planner: %lfs\n", planTimer1.timeEllapsed());
				fprintf(fp, "%lf,", planTimer1.timeEllapsed());
				fprintf(fp, "%d,", app->planner->plannerState->converged);
				fprintf(fp, "%d,", app->planner->plannerState->iterations);
				fprintf(fp, "%lf,", plannerCost);
				//Logger::consolePrint("planner cost: %lfs\n",plannerCost);

				bool validPlannerAssem = app->mainAssembly.checkCollisionFreeValidness();
				fprintf(fp, "%d,", validPlannerAssem);

				Timer planTimer2;
				bool gradConverged = false;
				int gradIterations = 1;
				double gradCost = 0;
				if (!validPlannerAssem) {
					app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
					//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
					gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
					while (!gradConverged) {
						gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
						gradIterations = gradIterations + 1;
						if (planTimer2.timeEllapsed() > 100) {
							break;
						}
					}
				}
				double finalFillRatio = app->mainAssembly.calculateFillRatio();
				bool valid = app->mainAssembly.checkCollisionFreeValidness();

				//Logger::consolePrint("total time ellapsed for grad: %lfs\n", planTimer2.timeEllapsed());
				//Logger::consolePrint("grad cost: %lf, converge:%d\n", gradCost, gradConverged);

				fprintf(fp, "%lf,", planTimer2.timeEllapsed());
				fprintf(fp, "%d,", gradConverged);
				fprintf(fp, "%d,", gradIterations);
				fprintf(fp, "%lf,", gradCost);
				fprintf(fp, "%lf,", finalFillRatio);
				fprintf(fp, "%d,", valid);
				fprintf(fp, "\n");

				Logger::consolePrint("clumsyTestAssembly 0-8 moves no supportMin, j: %d, seed:%d, validAssemAfterPlanner: %d, validAssemAfterGrad: %d ", j, seed, validPlannerAssem, valid);
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonEventBatchTestStd(void* clientData) {
	if (strcmp((char*)clientData, "teststd") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			Logger::consolePrint("Starting Std. Tests!\n");
			FILE *fp;
			fopen_s(&fp, "averageDesignStatsStdTests.csv", "w");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "valid assembly after planner,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "valid assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(38);

			Timer planTimer0;
			for (int j = 0; j < 10; j++) {
				int seed = rand() % 100;
				app->restart();
				//app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;
				app->mainAssembly.loadAssembly("assembly.txt");
				app->planner->setUpPlanner(seed);
				Logger::consolePrint("j: %d, seed:%d ", j, seed);
				Timer planTimer1;
				double plannerCost = 0;
				while (!app->planner->plannerState->converged) {
					plannerCost = app->planner->interactiveSearchPTMH_adaptive();
					if (planTimer1.timeEllapsed() > 500) {
						break;
					}
				}
				//Logger::consolePrint("total time ellapsed for planner: %lfs\n", planTimer1.timeEllapsed());
				fprintf(fp, "%lf,", planTimer1.timeEllapsed());
				fprintf(fp, "%d,", app->planner->plannerState->converged);
				fprintf(fp, "%d,", app->planner->plannerState->iterations);
				fprintf(fp, "%lf,", plannerCost);
				//Logger::consolePrint("planner cost: %lfs\n",plannerCost);
				bool validPlanner = app->mainAssembly.checkCollisionFreeValidness();
				fprintf(fp, "%d,", validPlanner);

				Timer planTimer2;
				bool gradConverged = false;
				//app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
				//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
				double gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
				int gradIterations = 1;
				while (!gradConverged) {
					gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
					gradIterations = gradIterations + 1;
					if (planTimer2.timeEllapsed() > 100) {
						break;
					}
				}
				double finalFillRatio = app->mainAssembly.calculateFillRatio();
				bool valid = app->mainAssembly.checkCollisionFreeValidness();

				//Logger::consolePrint("total time ellapsed for grad: %lfs\n", planTimer2.timeEllapsed());
				//Logger::consolePrint("grad cost: %lf, converge:%d\n", gradCost, gradConverged);

				fprintf(fp, "%lf,", planTimer2.timeEllapsed());
				fprintf(fp, "%d,", gradConverged);
				fprintf(fp, "%d,", gradIterations);
				fprintf(fp, "%lf,", gradCost);
				fprintf(fp, "%lf,", finalFillRatio);
				fprintf(fp, "%d,", valid);
				fprintf(fp, "\n");
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonEventBatchTestGradient(void* clientData) {
	if (strcmp((char*)clientData, "testgrad") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			Logger::consolePrint("Starting Gradient only Tests!\n");
			FILE *fp;
			fopen_s(&fp, "averageDesignStatsGradTests.csv", "w");
			fprintf(fp, "time,");
			fprintf(fp, "best cost,");
			fprintf(fp, "best gradient converged,");
			fprintf(fp, "best gradient itr,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "valid assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(99);

			DynamicArray<int> itrs;
			DynamicArray<bool>convergeSet;
			DynamicArray<double>allCost;
			DynamicArray<double>allTime;
			DynamicArray<double>allFillRatios;
			DynamicArray<bool>allValidChecks;

			Timer planTimer0;
			for (int j = 0; j < 10; j++) {
				int seed = rand() % 100;
				app->restart();
				//app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;
				app->mainAssembly.loadAssembly("assembly.txt");
				app->planner->setUpPlanner(seed);
				Logger::consolePrint("j: %d, seed:%d ", j, seed);

				for (int i = 0; i < app->planner->plannerState->numberOfChains; i++)
				{
					Timer planTimer2;
					int itr = 0;
					bool converged = false;
					double val = 0;
					app->planner->copyAssemblyStateToInputAssembly(&app->mainAssembly, app->planner->plannerState->currentChainStates[i]);
					app->mainAssembly.constraintsManager->minimizer.bfgsApproximator.reset();
					while (!converged) {
						val = app->mainAssembly.constraintsManager->fixConstraints(converged,1);
							itr = itr + 1;
							if (planTimer2.timeEllapsed() > 100) {
								break;
							}
					}
					itrs.push_back(itr);
					convergeSet.push_back(converged);
					allCost.push_back(val);
					allTime.push_back(planTimer2.timeEllapsed());
					double finalFillRatio = app->mainAssembly.calculateFillRatio();
					bool valid = app->mainAssembly.checkCollisionFreeValidness();

					allValidChecks.push_back(valid);
					allFillRatios.push_back(finalFillRatio);
					Logger::consolePrint("chainid:%d, converged:%d, val:%lf\n", i, converged, val);
				}

				auto minCost = DBL_MAX;
				bool bestConverge = false;
				int bestItr = -1;
				double bestTime = -1;
				double bestFill = -1;
				bool bestValid = false;

				for (size_t k = 0; k < allCost.size(); k++)
				{
					if (allCost[k] < minCost) {
						minCost = allCost[k];
						bestConverge = convergeSet[k];
						bestItr = itrs[k];
						bestTime = allTime[k];
						bestValid = allValidChecks[k];
						bestFill = allFillRatios[k];
					}
				}

				//int itr = 0;
				//while (!app->planner->plannerState->converged) {
				//	plannerCost = app->planner->gradientTest();
				//	itr = itr + 1;
				//	if (planTimer1.timeEllapsed() > 20) {
				//		break;
				//	}
				//	Logger::consolePrint("itr:%d, cost:%lf\n", itr, plannerCost);
				//}

				fprintf(fp, "%lf,", bestTime);
				fprintf(fp, "%lf,", minCost);
				fprintf(fp, "%d,", bestConverge);
				fprintf(fp, "%d,", bestItr);
				fprintf(fp, "%lf,", bestFill);
				fprintf(fp, "%d,", bestValid);
				fprintf(fp, "\n");
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonEventVirtualBatchTest(void* clientData) {
	if (strcmp((char*)clientData, "virtualtest") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "averageVirtualDesignStats.csv", "w");
			fprintf(fp, "initial fill ratio,");
			fprintf(fp, "number of components,");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "param size,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(10);

			Timer planTimer0;
			//double fillRatioForTest[5] = { 0.30, 0.40, 0.50, 0.60, 0.70};
			double fillRatioForTest[1] = { 0.60 };
			for (size_t i = 0; i < 1; i++) {
				for (int j = 0; j < 1; j++) {
					int seed = rand() % 100;
					app->restart();
					double outputFill = app->createVirtualAssemblyUsingBoxComponents(0, fillRatioForTest[i]);
					fprintf(fp, "%lf,", outputFill);
					fprintf(fp, "%d,", app->mainAssembly.electroMechanicalComponents.size());
					app->planner->setUpPlanner(seed);
					Logger::consolePrint("j: %d, seed:%d ", j, seed);
					Timer planTimer1;
					double plannerCost = 0;
					while (!app->planner->plannerState->converged) {
						plannerCost = app->planner->interactiveSearchPTMH_adaptive();
						if (planTimer1.timeEllapsed() > 500) {
							break;
						}
					}
					//Logger::consolePrint("total time ellapsed for planner: %lfs\n", planTimer1.timeEllapsed());
					fprintf(fp, "%lf,", planTimer1.timeEllapsed());
					fprintf(fp, "%d,", app->planner->plannerState->converged);
					fprintf(fp, "%d,", app->planner->plannerState->iterations);
					fprintf(fp, "%lf,", plannerCost);
					//Logger::consolePrint("planner cost: %lfs\n",plannerCost);

					Timer planTimer2;
					bool gradConverged = false;
					//app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
					//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
					double gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
					int gradIterations = 1;
					while (!gradConverged) {
						gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
						gradIterations = gradIterations + 1;
						if (planTimer2.timeEllapsed() > 100) {
							break;
						}
					}
					double finalFillRatio = app->mainAssembly.calculateFillRatio();
					bool valid = app->mainAssembly.checkCollisionFreeValidness();

					//Logger::consolePrint("total time ellapsed for grad: %lfs\n", planTimer2.timeEllapsed());
					//Logger::consolePrint("grad cost: %lf, converge:%d\n", gradCost, gradConverged);

					fprintf(fp, "%lf,", planTimer2.timeEllapsed());
					fprintf(fp, "%d,", gradConverged);
					fprintf(fp, "%d,", gradIterations);
					fprintf(fp, "%lf,", gradCost);
					fprintf(fp, "%d,", app->mainAssembly.constraintsManager->params.size());
					fprintf(fp, "%lf,", finalFillRatio);
					fprintf(fp, "%d,", valid);
					fprintf(fp, "\n");
				}
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonVirtualConstantMetricComponents(void* clientData) {
	if (strcmp((char*)clientData, "virtualtestfillratio") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "averageVirtualDesignStatsTestFillRatio.csv", "w");
			fprintf(fp, "seed,");
			fprintf(fp, "initial component fill ratio,");
			fprintf(fp, "initial net fill ratio,");
			fprintf(fp, "number of components,");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "collision free assembly after PTMC,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "param size,");
			fprintf(fp, "final component fill ratio,");
			fprintf(fp, "final support fill ratio,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(10);

			int numComponentsRange[5] = {5, 10, 15, 30, 50};// 24, 32, 40};//, 50, 100

			Timer planTimer0;
			// averaging results by running exp 'n' times -- running each experiment for 30 min now..
			for (size_t k = 0; k < 9; k++)
			{
				int seed = rand() % 1000;
				for (size_t i = 0; i < sizeof(numComponentsRange) / sizeof(numComponentsRange[0]); i++) {
					app->restart();
					app->numComponentsVirtual = numComponentsRange[i];
					double startFill = app->createVirtualAssemblyUsingBoxComponents(i, 0.10, app->numComponentsVirtual);
					int j = 1;
					bool lastSuccess = true;
					//int jEnd = 8;
					//if (i > 3)
					//	jEnd = 4;
					/*for (int j = 1; j <= jEnd; j++) {*/
					while(lastSuccess){
						if (dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)) {
							dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)->scaleChassis(j);
						}
						//double outputFill = app->mainAssembly.calculateComponentFillRatio();
						fprintf(fp, "%d,", seed);
						fprintf(fp, "%lf,", app->mainAssembly.calculateComponentFillRatio());
						fprintf(fp, "%lf,", startFill);
						fprintf(fp, "%d,", app->mainAssembly.electroMechanicalComponents.size());
						app->planner->plannerState->converged = false;
						app->planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;
						app->planner->setUpPlanner(seed);

						Logger::consolePrint("j: %d, seed:%d ", j, seed);
						Timer planTimer1;
						double plannerCost = 0;
						while (!app->planner->plannerState->converged) {
							plannerCost = app->planner->interactiveSearchPTMH_adaptive();
							if (planTimer1.timeEllapsed() > 1700*2) {
								break;
							}
						}
						//Logger::consolePrint("total time ellapsed for planner: %lfs\n", planTimer1.timeEllapsed());
						fprintf(fp, "%lf,", planTimer1.timeEllapsed());
						fprintf(fp, "%d,", app->planner->plannerState->converged);
						fprintf(fp, "%d,", app->planner->plannerState->iterations);
						fprintf(fp, "%lf,", plannerCost);
						//Logger::consolePrint("planner cost: %lfs\n",plannerCost);

						bool validAssemPlanner = app->mainAssembly.checkCollisionFreeValidness();
						fprintf(fp, "%d,", validAssemPlanner);

						Timer planTimer2;
						bool gradConverged = false;
						double gradCost = plannerCost;
						int gradIterations = 0;
						if (!validAssemPlanner) {
							//app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
							//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
							gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
							while (!gradConverged) {
								gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
								gradIterations = gradIterations + 1;
								if (planTimer2.timeEllapsed() > 100*2) {
									break;
								}
							}
						}
						else {
							app->mainAssembly.constraintsManager->assembly->setupConstraints();
							app->mainAssembly.constraintsManager->assembly->writeCurrentParametersToList(app->mainAssembly.constraintsManager->params);
						}
						double finalCompFillRatio = app->mainAssembly.calculateComponentFillRatio();
						double finalSupportFillRatio = app->mainAssembly.calculateSupportFillRatio();
						bool valid = app->mainAssembly.checkCollisionFreeValidness();

						//Logger::consolePrint("total time ellapsed for grad: %lfs\n", planTimer2.timeEllapsed());
						//Logger::consolePrint("grad cost: %lf, converge:%d\n", gradCost, gradConverged);

						fprintf(fp, "%lf,", planTimer2.timeEllapsed());
						fprintf(fp, "%d,", gradConverged);
						fprintf(fp, "%d,", gradIterations);
						fprintf(fp, "%lf,", gradCost);
						fprintf(fp, "%d,", app->mainAssembly.constraintsManager->params.size());
						fprintf(fp, "%lf,", finalCompFillRatio);
						fprintf(fp, "%lf,", finalSupportFillRatio);
						fprintf(fp, "%lf,", finalCompFillRatio+finalSupportFillRatio);
						fprintf(fp, "%d,", valid);
						fprintf(fp, "\n");

						j = j + 1;
						lastSuccess = validAssemPlanner || valid;
					}
					fprintf(fp, "\n");
				}
				fprintf(fp, "\n");
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonScaleChassis(void* clientData) {
	if (strcmp((char*)clientData, "scalechassis") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			if (dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)) {
				dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)->scaleChassis(app->chassisScale);
				//app->planner->plannerState->initialized = false;
				app->planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;
				app->planner->setUpPlanner();
			}
		}
	}
}


void TW_CALL UIButtonIncrementalAlternateDesign(void* clientData) {
	if (strcmp((char*)clientData, "incrementalAlternate") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {

			// sort components based on size/volume (but only the non fixed ones)
			DynamicArray<EMComponent*> sortTheseComponents, sortedComponents;
			sortTheseComponents = app->mainAssembly.getListOfNonFixedComponents();
			sortedComponents = app->mainAssembly.getSortedComponentsBasedOnVolume(sortTheseComponents);

			Timer planTimer1;
			DynamicArray<AssemblyState> prevChainStates;
			for (size_t i = 0; i < sortedComponents.size(); i++)
			{
				DynamicArray<EMComponent*> subAssemblyComponents = app->mainAssembly.getListOfFixedComponents();

				for (size_t j = 0; j <= i; j++)
				{
					subAssemblyComponents.push_back(sortedComponents[j]);
				}

				// create sub assembly
				Assembly* subAssembly = app->mainAssembly.clone(subAssemblyComponents);


				Logger::consolePrint("Prev chain state size:%d\n", prevChainStates.size());
				// add new component info in assembly state info that we got from prev planning
				if (prevChainStates.size() > 0) {
					for (size_t m = 0; m < prevChainStates.size(); m++)
					{
						Logger::consolePrint("stateId: %d, numComp prevSate:%d ", m, prevChainStates[m].objStates.size());
						prevChainStates[m].appendComponentState(subAssemblyComponents.back());
						Logger::consolePrint("after append -- numComp prevSate:%d \n", prevChainStates[m].objStates.size());
					}
				}

				//  and solve for the subassembly
				MCMCPlanners* subAssemblyPlanner = new MCMCPlanners(subAssembly, prevChainStates);
				Logger::consolePrint("initial subAssemb cost: %lf\n", app->planner->getAssemblyCost(subAssembly));

				subAssemblyPlanner->plannerState->converged = false;
				subAssemblyPlanner->plannerState->mode = UPDATE_ASSEMBLY_PREVSTATE;
				DynamicArray<int> newComponentID;
				newComponentID.push_back(i);
				subAssemblyPlanner->plannerState->incrementalPertubComponentsID = newComponentID;
				subAssemblyPlanner->setUpPlanner(71);
				double plannerCost = 0;
				//Timer planTimer2;
				while (!subAssemblyPlanner->plannerState->converged) {
					plannerCost = subAssemblyPlanner->interactiveSearchPTMH_adaptive();
					if (planTimer1.timeEllapsed() > 1000) {
						break;
					}
				}

				prevChainStates = subAssemblyPlanner->plannerState->currentChainBestStates;
				// copy the optimized values from the planner back
				for (size_t k = 0; k < subAssemblyComponents.size(); k++)
				{
					subAssembly->electroMechanicalComponents[k]->copyObjectPropetriesInto(subAssemblyComponents[k]);
				}

				//app->mainAssembly = *subAssembly;
				// crosscheck main assembly cost
				double cost = app->planner->getAssemblyCost(&app->mainAssembly);
				Logger::consolePrint("incremental j:%d, subAssemblySize: %d, plannerCost: %lf, mainAssemCost: %lf\n", i,
					subAssembly->electroMechanicalComponents.size(), plannerCost, cost);

				// we don't need the subassembly anymore
				delete subAssembly;
				delete subAssemblyPlanner;

			}

			Logger::consolePrint("time: %lf \n", planTimer1.timeEllapsed());
		}
	}
}



void TW_CALL UIButtonIncrementalDesign(void* clientData) {
	if (strcmp((char*)clientData, "incremental") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {

			// sort components based on size/volume (but only the non fixed ones)
			DynamicArray<EMComponent*> sortTheseComponents, sortedComponents;
			sortTheseComponents = app->mainAssembly.getListOfNonFixedComponents();
			sortedComponents = app->mainAssembly.getSortedComponentsBasedOnVolume(sortTheseComponents);

			Timer planTimer1;
			for (size_t i = 0; i < sortedComponents.size(); i++)
			{
				DynamicArray<EMComponent*> subAssemblyComponents = app->mainAssembly.getListOfFixedComponents();

				for (size_t j = 0; j <= i; j++)
				{
					subAssemblyComponents.push_back(sortedComponents[j]);
				}

				// create sub assembly
				Assembly* subAssembly = app->mainAssembly.clone(subAssemblyComponents);

				//  and solve for the subassembly
				MCMCPlanners* subAssemblyPlanner = new MCMCPlanners(subAssembly);
				subAssemblyPlanner->plannerState->converged = false;
				subAssemblyPlanner->plannerState->mode = UPDATE_ASSEMBLY_INCREXPLORE_5050_BEST;
				DynamicArray<int> newComponentID;
				newComponentID.push_back(i);
				subAssemblyPlanner->plannerState->incrementalPertubComponentsID = newComponentID;
				subAssemblyPlanner->setUpPlanner(10);
				double plannerCost = 0;
				Timer planTimer2;
				while (!subAssemblyPlanner->plannerState->converged) {
					plannerCost = subAssemblyPlanner->interactiveSearchPTMH_adaptive();
					if (planTimer2.timeEllapsed() > 10000) {
						break;
					}
				}

				// copy the optimized values from the planner back
				for (size_t k = 0; k < subAssemblyComponents.size(); k++)
				{
					subAssembly->electroMechanicalComponents[k]->copyObjectPropetriesInto(subAssemblyComponents[k]);
				}

				//app->mainAssembly = *subAssembly;
				// crosscheck main assembly cost
				double cost = app->planner->getAssemblyCost(&app->mainAssembly);
				Logger::consolePrint("incremental j:%d, subAssemblySize: %d, plannerCost: %lf, mainAssemCost: %lf, time:%lf\n", i, 
					subAssembly->electroMechanicalComponents.size(), plannerCost, cost, planTimer2.timeEllapsed());

				// we don't need the subassembly anymore
				delete subAssembly;
				delete subAssemblyPlanner;

			}

			Logger::consolePrint("time: %lf \n", planTimer1.timeEllapsed());
		}
	}
}


void TW_CALL UIButtonEventBatchTestIncremental(void* clientData) {
	if (strcmp((char*)clientData, "testincremental") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "avgDesignStatsIncrementalClumsy1000s_10runsSeed99Moves08_AssemOptimInGrad_INCREXPLOREBest50ExitValidNoSupMin.csv", "w");
			//fopen_s(&fp, "test.csv", "w");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "collision free assembly after PTMC,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(99);

			Timer planTimer0;
			for (int t = 0; t < 10; t++) {
				int seed = rand() % 100;
				app->restart();
				app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;
				app->mainAssembly.loadAssembly("clumsyTestAssembly_wAllowedRotLimited.txt"); //clumsyTestAssembly_wAllowedRotLimited.txt

				// sort components based on size/volume (but only the non fixed ones)
				DynamicArray<EMComponent*> sortTheseComponents, sortedComponents;
				sortTheseComponents = app->mainAssembly.getListOfNonFixedComponents();
				sortedComponents = app->mainAssembly.getSortedComponentsBasedOnVolume(sortTheseComponents);

				Timer planTimer1;
				bool plannerConverged = false;
				int plannerIterations = 0;
				for (size_t i = 0; i < sortedComponents.size(); i++)
				{
					DynamicArray<EMComponent*> subAssemblyComponents = app->mainAssembly.getListOfFixedComponents();
					for (size_t j = 0; j <= i; j++)
					{
						subAssemblyComponents.push_back(sortedComponents[j]);
					}

					// PRE SUP MIN OFF
					app->mainAssembly.minSupportVolume = false;
					// create sub assembly
					Assembly* subAssembly = app->mainAssembly.clone(subAssemblyComponents);
					//  and solve for the subassembly
					MCMCPlanners* subAssemblyPlanner = new MCMCPlanners(subAssembly);
					subAssemblyPlanner->plannerState->converged = false;
					subAssemblyPlanner->plannerState->mode = UPDATE_ASSEMBLY_INCREXPLORE_5050_BEST;// UPDATE_ASSEMBLY_RANDOM; UPDATED_ASSEMBLY_MODE; UPDATE_ASSEMBLY_INCREXPLORE_5050
					// you need the below 3 lines for INCREXPLORE_5050, updatedModeRandom, updatedRandomBest, and INCR mode..
					DynamicArray<int> newComponentID;
					newComponentID.push_back(i);
					subAssemblyPlanner->plannerState->incrementalPertubComponentsID = newComponentID;
					subAssemblyPlanner->setUpPlanner(seed);
					double plannerCost = 0;
					//Timer planTimer3;
					while (!subAssemblyPlanner->plannerState->converged) {
						//Timer planTimer3;
						plannerCost = subAssemblyPlanner->interactiveSearchPTMH_adaptive();
						if (planTimer1.timeEllapsed() > 1000) { //400
							break;
						}
					}

					// copy the optimized values from the planner back
					for (size_t k = 0; k < subAssemblyComponents.size(); k++)
					{
						subAssembly->electroMechanicalComponents[k]->copyObjectPropetriesInto(subAssemblyComponents[k]);
					}
					plannerConverged = subAssemblyPlanner->plannerState->converged;
					plannerIterations = plannerIterations + subAssemblyPlanner->plannerState->iterations;
					// we don't need the subassembly anymore
					delete subAssembly;
					delete subAssemblyPlanner;

					////POST SUP MIN
					//app->mainAssembly.minSupportVolume = true;
					//bool midgradConverged = false;
					//Timer midGradient;
					//double midgradCost = app->mainAssembly.constraintsManager->fixConstraints(midgradConverged);
					//while (!midgradConverged) {
					//	midgradCost = app->mainAssembly.constraintsManager->fixConstraints(midgradConverged);
					//	if (midGradient.timeEllapsed() > 30) {
					//		break;
					//	}
					//}
				}

				fprintf(fp, "%lf,", planTimer1.timeEllapsed());
				fprintf(fp, "%d,", plannerConverged);
				fprintf(fp, "%d,", plannerIterations);
				fprintf(fp, "%lf,", app->planner->getAssemblyCost(&app->mainAssembly));

				bool validPlannerAssem = app->mainAssembly.checkCollisionFreeValidness();
				fprintf(fp, "%d,", validPlannerAssem);

				Timer planTimer2;
				bool gradConverged = false;
				int gradIterations = 1;
				double gradCost = 0;
				if (!validPlannerAssem) {
					app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
					app->mainAssembly.minSupportVolume = true;
					//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
					gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
					while (!gradConverged) {
						gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
						gradIterations = gradIterations + 1;
						if (planTimer2.timeEllapsed() > 100) {
							break;
						}
					}
				}
				double finalFillRatio = app->mainAssembly.calculateFillRatio();
				bool valid = app->mainAssembly.checkCollisionFreeValidness();

				fprintf(fp, "%lf,", planTimer2.timeEllapsed());
				fprintf(fp, "%d,", gradConverged);
				fprintf(fp, "%d,", gradIterations);
				fprintf(fp, "%lf,", gradCost);
				fprintf(fp, "%lf,", finalFillRatio);
				fprintf(fp, "%d,", valid);
				fprintf(fp, "\n");

				Logger::consolePrint("owlTestAssembly 0-8 moves supportMin, j: %d, seed:%d, validAssemAfterPlanner: %d, validAssemAfterGrad: %d, time: %lf\n", t, seed, validPlannerAssem, valid,
					planTimer1.timeEllapsed());
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonEventBatchTestIncrementalAlternate(void* clientData) {
	if (strcmp((char*)clientData, "testincrementalalternate") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "avgDesignStatsIncrementalCrusher1000s_10runsSeed0Moves08_AssemOptimInGrad_PREVSTATEBest50ExitValidNoSupMin.csv", "w");
			//fopen_s(&fp, "test.csv", "w");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "collision free assembly after PTMC,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(0);

			Timer planTimer0;
			bool plannerConverged = false;
			int plannerIterations = 0;
			for (int t = 0; t < 10; t++) {
				int seed = rand() % 100;
				app->restart();
				app->mainAssembly.minSupportVolume = false;
				app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;
				app->mainAssembly.loadAssembly("wallETestAssembly.txt"); //clumsyTestAssembly_wAllowedRotLimited.txt

				// sort components based on size/volume (but only the non fixed ones)
				DynamicArray<EMComponent*> sortTheseComponents, sortedComponents;
				sortTheseComponents = app->mainAssembly.getListOfNonFixedComponents();
				sortedComponents = app->mainAssembly.getSortedComponentsBasedOnVolume(sortTheseComponents);

				Timer planTimer1;
				DynamicArray<AssemblyState> prevChainStates;
				for (size_t i = 0; i < sortedComponents.size(); i++)
				{
					DynamicArray<EMComponent*> subAssemblyComponents = app->mainAssembly.getListOfFixedComponents();

					for (size_t j = 0; j <= i; j++)
					{
						subAssemblyComponents.push_back(sortedComponents[j]);
					}

					// create sub assembly
					Assembly* subAssembly = app->mainAssembly.clone(subAssemblyComponents);

					// add new component info in assembly state info that we got from prev planning
					if (prevChainStates.size() > 0) {
						for (size_t m = 0; m < prevChainStates.size(); m++)
						{
							prevChainStates[m].appendComponentState(subAssemblyComponents.back());
						}
					}

					//  and solve for the subassembly
					MCMCPlanners* subAssemblyPlanner = new MCMCPlanners(subAssembly, prevChainStates);

					subAssemblyPlanner->plannerState->converged = false;
					subAssemblyPlanner->plannerState->mode = UPDATE_ASSEMBLY_PREVSTATE_BEST;
					DynamicArray<int> newComponentID;
					newComponentID.push_back(i);
					subAssemblyPlanner->plannerState->incrementalPertubComponentsID = newComponentID;
					subAssemblyPlanner->setUpPlanner(seed);
					double plannerCost = 0;
					//Timer planTimer2;
					while (!subAssemblyPlanner->plannerState->converged) {
						plannerCost = subAssemblyPlanner->interactiveSearchPTMH_adaptive();
						if (planTimer1.timeEllapsed() > 1000) {
							break;
						}
					}

					prevChainStates = subAssemblyPlanner->plannerState->currentChainBestStates;
					// copy the optimized values from the planner back
					for (size_t k = 0; k < subAssemblyComponents.size(); k++)
					{
						subAssembly->electroMechanicalComponents[k]->copyObjectPropetriesInto(subAssemblyComponents[k]);
					}

					//app->mainAssembly = *subAssembly;
					// crosscheck main assembly cost
					double cost = app->planner->getAssemblyCost(&app->mainAssembly);
					Logger::consolePrint("incremental j:%d, subAssemblySize: %d, plannerCost: %lf, mainAssemCost: %lf\n", i,
						subAssembly->electroMechanicalComponents.size(), plannerCost, cost);

					plannerConverged = subAssemblyPlanner->plannerState->converged;
					plannerIterations = plannerIterations + subAssemblyPlanner->plannerState->iterations;
					// we don't need the subassembly anymore
					delete subAssembly;
					delete subAssemblyPlanner;
				}

				fprintf(fp, "%lf,", planTimer1.timeEllapsed());
				fprintf(fp, "%d,", plannerConverged);
				fprintf(fp, "%d,", plannerIterations);
				fprintf(fp, "%lf,", app->planner->getAssemblyCost(&app->mainAssembly));

				bool validPlannerAssem = app->mainAssembly.checkCollisionFreeValidness();
				fprintf(fp, "%d,", validPlannerAssem);

				Timer planTimer2;
				bool gradConverged = false;
				int gradIterations = 1;
				double gradCost = 0;
				if (!validPlannerAssem) {
					app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
					app->mainAssembly.minSupportVolume = true;
					//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
					gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
					while (!gradConverged) {
						gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
						gradIterations = gradIterations + 1;
						if (planTimer2.timeEllapsed() > 100) {
							break;
						}
					}
				}
				double finalFillRatio = app->mainAssembly.calculateFillRatio();
				bool valid = app->mainAssembly.checkCollisionFreeValidness();

				fprintf(fp, "%lf,", planTimer2.timeEllapsed());
				fprintf(fp, "%d,", gradConverged);
				fprintf(fp, "%d,", gradIterations);
				fprintf(fp, "%lf,", gradCost);
				fprintf(fp, "%lf,", finalFillRatio);
				fprintf(fp, "%d,", valid);
				fprintf(fp, "\n");

				Logger::consolePrint("j: %d, seed:%d, validAssemAfterPlanner: %d, validAssemAfterGrad: %d, time: %lf\n", t, seed, validPlannerAssem, valid,
					planTimer1.timeEllapsed());
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}
}

void TW_CALL UIButtonEventTestIncrementalVirtual(void* clientData) {
	if (strcmp((char*)clientData, "testincrementalvirtual") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			FILE *fp;
			fopen_s(&fp, "TEST30Seed10_wsuppUpdate.csv", "w");
			//fopen_s(&fp, "averageVirtualDesignStats_IncrementalTestFillRatio_1hr_Seed50.csv", "w");
			fprintf(fp, "seed,");
			fprintf(fp, "initial component fill ratio,");
			fprintf(fp, "initial net fill ratio,");
			fprintf(fp, "number of components,");
			fprintf(fp, "time of planner,");
			fprintf(fp, "planner converged,");
			fprintf(fp, "planner iterations,");
			fprintf(fp, "cost after planner,");
			fprintf(fp, "collision free assembly after PTMC,");
			fprintf(fp, "time of gradient,");
			fprintf(fp, "gradient converged,");
			fprintf(fp, "gradient iterations,");
			fprintf(fp, "cost after gradient,");
			fprintf(fp, "param size,");
			fprintf(fp, "final component fill ratio,");
			fprintf(fp, "final support fill ratio,");
			fprintf(fp, "final fill ratio,");
			fprintf(fp, "collision free assembly,");
			fprintf(fp, "\n");
			// random seeds for testing..
			std::srand(10);

			int numComponentsRange[1] = { 30 };// 24, 32, 40};//, 50, 100
			//int numComponentsRange[6] = { 5, 10, 15, 30, 50, 100 };// 24, 32, 40};//, 50, 100

			Timer planTimer0;
			// averaging results by running exp 'n' times -- running each experiment for 30 min now..
			for (size_t k = 0; k < 1; k++){
				int seed =  rand() % 1000;
				for (size_t i = 0; i < sizeof(numComponentsRange) / sizeof(numComponentsRange[0]); i++) {
					string fName = "Assembly_IncrementalTestFillRatio_1hr_Seed10suppUp_" + std::to_string(numComponentsRange[i]) + ".txt";
					app->restart();
					app->numComponentsVirtual = numComponentsRange[i];
					double startFill = app->createVirtualAssemblyUsingBoxComponents(i + seed, 0.10, app->numComponentsVirtual);
					double j = 3;
					bool lastSuccess = true;
					//int jEnd = 8;
					//if (i > 3)
					//	jEnd = 4;
					/*for (int j = 1; j <= jEnd; j++) {*/
					while (lastSuccess) {
						if (dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)) {
							dynamic_cast<VirtualChassis*>(app->mainAssembly.chassis)->scaleChassis(j);
						}
						double outputFill = app->mainAssembly.calculateFillRatio();
						fprintf(fp, "%d,", seed);
						fprintf(fp, "%lf,", app->mainAssembly.calculateComponentFillRatio());
						fprintf(fp, "%lf,", outputFill);
						fprintf(fp, "%d,", app->mainAssembly.electroMechanicalComponents.size());

						// INCREMENTAL PLANNER SETUP
						app->mainAssembly.minSupportVolume = false;
						// sort components based on size/volume (but only the non fixed ones)
						DynamicArray<EMComponent*> sortTheseComponents, sortedComponents;
						sortTheseComponents = app->mainAssembly.getListOfNonFixedComponents();
						sortedComponents = app->mainAssembly.getSortedComponentsBasedOnVolume(sortTheseComponents);

						Timer planTimer1;
						bool plannerConverged = false;
						int plannerIterations = 0;
						// ACTUAL PLANNER LOOP
						for (size_t t = 0; t < sortedComponents.size(); t++)
						{
							DynamicArray<EMComponent*> subAssemblyComponents = app->mainAssembly.getListOfFixedComponents();
							for (size_t m = 0; m <= t; m++)
							{
								subAssemblyComponents.push_back(sortedComponents[m]);
							}
							// create sub assembly
							Assembly* subAssembly = app->mainAssembly.clone(subAssemblyComponents);
							//  and solve for the subassembly
							MCMCPlanners* subAssemblyPlanner = new MCMCPlanners(subAssembly);
							subAssemblyPlanner->plannerState->converged = false;
							subAssemblyPlanner->plannerState->mode = UPDATE_ASSEMBLY_INCREXPLORE_5050_BEST;// UPDATE_ASSEMBLY_RANDOM; UPDATED_ASSEMBLY_MODE; UPDATE_ASSEMBLY_INCREXPLORE_5050
							// you need the below 3 lines for INCREXPLORE_5050, updatedModeRandom, updatedRandomBest, and INCR mode..
							DynamicArray<int> newComponentID;
							newComponentID.push_back(t);
							subAssemblyPlanner->plannerState->incrementalPertubComponentsID = newComponentID;
							subAssemblyPlanner->setUpPlanner(seed);
							double plannerCost = 0;
							while (!subAssemblyPlanner->plannerState->converged) {
								plannerCost = subAssemblyPlanner->interactiveSearchPTMH_adaptive();
								if (planTimer1.timeEllapsed() > 1700*2) { //400
									break;
								}
							}
							// copy the optimized values from the planner back
							for (size_t q = 0; q < subAssemblyComponents.size(); q++)
							{
								subAssembly->electroMechanicalComponents[q]->copyObjectPropetriesInto(subAssemblyComponents[q]);
							}
							plannerConverged = subAssemblyPlanner->plannerState->converged;
							plannerIterations = plannerIterations + subAssemblyPlanner->plannerState->iterations;
							// we don't need the subassembly anymore
							delete subAssembly;
							delete subAssemblyPlanner;
						}
						// END OF PLANNER LOOP
						// PLANNER STATS --
						fprintf(fp, "%lf,", planTimer1.timeEllapsed());
						fprintf(fp, "%d,", plannerConverged);
						fprintf(fp, "%d,", plannerIterations);
						fprintf(fp, "%lf,", app->planner->getAssemblyCost(&app->mainAssembly));
						bool validPlannerAssem = app->mainAssembly.checkCollisionFreeValidness();
						fprintf(fp, "%d,", validPlannerAssem);

						Logger::consolePrint("numComponents: %d, startFill: %lf, seed:%d, plannerTime: %lf, validAssemPlanner: %d\n ", 
							numComponentsRange[i], outputFill, seed, planTimer1.timeEllapsed(), validPlannerAssem);

						// POST GRADIENT WITH SUPPORT MIN LOOP
						Timer planTimer2;
						bool gradConverged = false;
						int gradIterations = 1;
						double gradCost = 0;
						if (!validPlannerAssem) {
							app->mainAssembly.optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
							app->mainAssembly.minSupportVolume = true;
							//app->mainAssembly.constraintsManager->constraintsCollection->printProgress = true;
							gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
							while (!gradConverged) {
								gradCost = app->mainAssembly.constraintsManager->fixConstraints(gradConverged);
								gradIterations = gradIterations + 1;
								if (planTimer2.timeEllapsed() > 100*2) {
									break;
								}
							}
						}
						// END OF GRADIENT LOOP
						// GRADIENT STATS
						//double finalFillRatio = app->mainAssembly.calculateFillRatio();
						double finalCompFillRatio = app->mainAssembly.calculateComponentFillRatio();
						double finalSupportFillRatio = app->mainAssembly.calculateSupportFillRatio();
						bool valid = app->mainAssembly.checkCollisionFreeValidness();
						fprintf(fp, "%lf,", planTimer2.timeEllapsed());
						fprintf(fp, "%d,", gradConverged);
						fprintf(fp, "%d,", gradIterations);
						fprintf(fp, "%lf,", gradCost);
						fprintf(fp, "%d,", app->mainAssembly.constraintsManager->params.size());
						fprintf(fp, "%lf,", finalCompFillRatio);
						fprintf(fp, "%lf,", finalSupportFillRatio);
						fprintf(fp, "%lf,", finalCompFillRatio + finalSupportFillRatio);
						fprintf(fp, "%d,", valid);
						fprintf(fp, "\n");

						lastSuccess = validPlannerAssem || valid;
						if (lastSuccess) {
							app->mainAssembly.saveAssembly(fName.c_str());
						}

						j = j + 0.5;
					}
					fprintf(fp, "\n");
				}
			}
			fclose(fp);
			Logger::consolePrint("total time for stat collection: %lfs\n", planTimer0.timeEllapsed());
		}
	}

}



//void TW_CALL UIButtonEventTest(void* clientData) {
//	if (strcmp((char*)clientData, "test") == 0) {
//		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
//		if (app) {
//			// create a virtual assembly (needs multiple of 3 objects for now..)
//			app->createVirtualAssemblyForDataSet(9);
//			app->planner->setUpPlanner();
//
//			// decide number of data points
//			FILE *fp;
//			fopen_s(&fp, "dataset.csv", "w");
//
//			Timer dataSampleTimer;
//			int numDataPoints = 1;
//			for (int i = 0; i < numDataPoints; i++)
//			{
//				// random configuration
//				app->planner->setInitialRandomAssemblyPlacement(app->planner->inputAssembly, i);
//				app->planner->setInitialRandomAssemblyOrientations(app->planner->inputAssembly, i);
//				app->planner->inputAssembly->snapSupportsToWalls();
//				app->planner->setTopDownAssemblyOrder(app->planner->inputAssembly);
//
//				// get params
//				DynamicArray<double> params;
//				for (size_t j = 0; j < app->planner->inputAssembly->electroMechanicalComponents.size(); j++)
//				{
//					P3D pos = app->planner->inputAssembly->electroMechanicalComponents[j]->getPosition();
//					params.push_back(pos[0]);
//					params.push_back(pos[1]);
//					params.push_back(pos[2]);
//					params.push_back(app->planner->inputAssembly->electroMechanicalComponents[j]->additionalRotAngle);
//				}
//				for (size_t j = 0; j < params.size(); j++)
//				{
//					fprintf(fp, "%lf,", params[j]);
//				}
//				// get cost
//				double cost = app->planner->getAssemblyCost(app->planner->inputAssembly);
//				//Logger::consolePrint("cost:%lf\n", cost);
//				fprintf(fp, "%lf,",cost);
//				fprintf(fp, "\n");
//			}
//			//Logger::consolePrint("Time:%lf\n", dataSampleTimer.timeEllapsed());
//			fclose(fp);
//		}
//	}
//}
