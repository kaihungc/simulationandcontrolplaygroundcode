#pragma once
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <GUILib/GLWindow3D.h>
#include <Utils/Logger.h>
#include <Utils/Timer.h>
#include <GUILib/GLCamera.h>
#include <GUILib/GLMesh.h>
#include "BaseObject.h"
#include <GUILib/GLTrackingCamera.h>

/**
* Used for any window showing cylinder that needs to be displayed in the OpenGL window
*/

class ObjectDisplayWindow : public GLWindow3D {
protected:
public:

	GLTrackingCamera* trackingCamera;
	EMComponent* ob = NULL;

	// constructors
	ObjectDisplayWindow(EMComponent* ob, int x, int y, int w, int h);
	ObjectDisplayWindow(EMComponent* ob);
	ObjectDisplayWindow();

	// destructor
	virtual ~ObjectDisplayWindow() {};

	virtual void drawScene();

};

