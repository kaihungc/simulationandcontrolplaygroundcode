#pragma once
#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <GUILib/TranslateWidget.h>
#include "BaseObject.h"
#include "AssemblyUtils.h"
#include "BulletCollisionObject.h"

/*
Box objects -- virtual objects for synthetic assemblies in benchmarking algorithms.
*/

class SphereComponent : public EMComponent {
private:
	double radius = 0.1;
	btBoxShape colShape = btBoxShape(btVector3(btScalar(0.1), btScalar(0.1), btScalar(0.1)));
	btCollisionObject colObject;
	RigidlyAttachedSupport* boxSupport = NULL;

public:
	bool hasSupport = false;

	// constructor
	SphereComponent();
	SphereComponent(double r, bool support = false);

	void initGeometry();

	// destructor
	virtual ~SphereComponent();

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimX = dimY = dimZ = radius;
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> result;
		result.push_back(std::pair<char*, double*>("Sphere-radius", &radius));
		return result;
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
		bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShape, &colObject));
	}

	virtual EMComponent* clone(bool cloneTexture = true);

	virtual void addSupportsToList(DynamicArray<RigidlyAttachedSupport*>& supportObjs) {
		if (boxSupport)
			supportObjs.push_back(boxSupport);
	}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList, int optimizationFlag);
	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex, int optimizationFlag);

	/**
	writes to file
	*/
	virtual void writeToFile(FILE* fp);

	/**
	reads from file
	*/
	virtual void loadFromFile(FILE* fp);

};

