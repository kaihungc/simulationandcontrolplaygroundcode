#include <GUILib/GLApplication.h>

#include "ObjectDisplayWindow.h"
#include <Utils/Utils.h>
#include <GUILib/GLUtils.h>

#include <GUILib/GLContentManager.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLCamera.h>

/**
3D Window with a generic object type
*/
ObjectDisplayWindow::ObjectDisplayWindow(EMComponent* obj, int posX, int posY, int sizeX, int sizeY) : GLWindow3D(posX, posY, sizeX, sizeY) {
	camera = new GLTrackingCamera();
	trackingCamera = (GLTrackingCamera*)camera;
	trackingCamera->camDistance = -0.2;
	trackingCamera->rotAboutUpAxis = PI / 4;

	this->ob = obj;
}

ObjectDisplayWindow::ObjectDisplayWindow(EMComponent* obj) : GLWindow3D() {
	camera = new GLTrackingCamera();
	trackingCamera = (GLTrackingCamera*)camera;
	trackingCamera->camDistance = -0.2;
	trackingCamera->rotAboutUpAxis = PI / 4;

	this->ob = obj;
}

ObjectDisplayWindow::ObjectDisplayWindow() : GLWindow3D() {
	camera = new GLTrackingCamera();
	trackingCamera = (GLTrackingCamera*)camera;
	trackingCamera->camDistance = 0.2;
	trackingCamera->rotAboutUpAxis = PI / 4;

	this->ob = NULL;
}

void ObjectDisplayWindow::drawScene() {
	if (ob)
		ob->draw();
}

