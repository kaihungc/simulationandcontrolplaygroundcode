#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include "BaseObject.h"

#include <OptimizationLib\ObjectiveFunction.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "CollisionManager.h"

class AssemblableComponent;

/*
	An assembly is a collection of objects and connections.
*/
class ParameterizedDisassemblyPath {
public:
	AssemblableComponent* theComponent;

	GLMesh worldCoordinatesSweptObjectMesh;
	double pathLength = 0.05 * 2;//0.2;

public:
	// constructor
	ParameterizedDisassemblyPath(AssemblableComponent* component);

	// destructor
	~ParameterizedDisassemblyPath();

	BT_DECLARE_ALIGNED_ALLOCATOR();

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList) = 0;
	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex) = 0;

	//t==0 will always return the current position of the component. t==1 represents its final location
	virtual P3D getComponentPositionAlongPathAt(double t) = 0;
	//t==0 will always return the current orientation of the component. t==1 represents its final orientation
	virtual Quaternion getComponentOrientationAlongPathAt(double t) = 0;

	virtual void copyObjectPropetriesInto(ParameterizedDisassemblyPath* path) {};

	virtual GLMesh* getWorldCoordinatesSweptObjectMesh() { return &worldCoordinatesSweptObjectMesh; }

	void pushComponentConvexShapeVertsAtTimeTToList(DynamicArray<P3D>& verts, double t);

	virtual void addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects) {}

	virtual void writePropertiesToFile(FILE* fp) {}

	virtual void readPropertiesFromString(const char* line) {}

	virtual V3D getWorldCoordinatesAssemblyPath() { return V3D(); };
	
	virtual void setWorldCoordinatesAssemblyPath(Quaternion v) {};
};

class FixedDirectionDissassemblyPath : public ParameterizedDisassemblyPath {
public:
	V3D disassemblyLocalDefaultPath = V3D(0, 1, 0);

	FixedDirectionDissassemblyPath(AssemblableComponent* component, V3D axis) : ParameterizedDisassemblyPath(component) {
		disassemblyLocalDefaultPath = axis;
	}

	//virtual V3D getWorldCoordinatesAssemblyPath() {
	//	return  theComponent->getOrientation()*disassemblyLocalDefaultPath;
	//}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList) {}

	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex){}

	//t==0 will always return the current position of the component. t==1 represents its final location
	virtual P3D getComponentPositionAlongPathAt(double t);

	//t==0 will always return the current orientation of the component. t==1 represents its final orientation
	virtual Quaternion getComponentOrientationAlongPathAt(double t);

	virtual void writePropertiesToFile(FILE* fp) {
		fprintf(fp, "%10.10lf %lf %lf %lf", pathLength,
			disassemblyLocalDefaultPath[0], disassemblyLocalDefaultPath[1], disassemblyLocalDefaultPath[2]);
	}

	virtual void readPropertiesFromString(const char* line) {
		double x = -1, y = -1, z = -1;
		if (sscanf(line, "%lf %lf %lf %lf", &pathLength, &x, &y, &z) != 4)
			throwError("Incorrect assembly input file - 6 arguments are required to specify the assembly path of an object\n", line);
		disassemblyLocalDefaultPath = V3D(x, y, z);
	}
};

/**
	Note: direction of dissassembly is represented in world coords (e.g. rotating the component does not change the disassembly path)
*/
class ArbitraryDirectionDissassemblyPath : public ParameterizedDisassemblyPath {
public:
	double assemblyAngleTheta = 0; //[rad]
	double assemblyAnglePhi = 0; //[rad]
	V3D rotAxisTheta = V3D(1, 0, 0), rotAxisPhi = V3D(0, 0, 1);
	V3D disassemblyDefaultPath = V3D(0, 1, 0);
	btConvexHullShape btSweptColShape;
	btCollisionObject colObject;


	ArbitraryDirectionDissassemblyPath(AssemblableComponent* component) : ParameterizedDisassemblyPath(component) {}

	virtual void resetInternalAxis() {
		disassemblyDefaultPath.getOrthogonalVectors(rotAxisTheta, rotAxisPhi);
	}

	virtual V3D getWorldCoordinatesAssemblyPath() {
		return getRotationQuaternion(assemblyAngleTheta, rotAxisTheta) * getRotationQuaternion(assemblyAnglePhi, rotAxisPhi) * disassemblyDefaultPath;
	}

	virtual void setWorldCoordinatesAssemblyPath(Quaternion v) {
		disassemblyDefaultPath = v*V3D(0, 1, 0);
		resetInternalAxis();
	}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList);

	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex);

	//t==0 will always return the current position of the component. t==1 represents its final location
	virtual P3D getComponentPositionAlongPathAt(double t);

	//t==0 will always return the current orientation of the component. t==1 represents its final orientation
	virtual Quaternion getComponentOrientationAlongPathAt(double t);

	//StelianQ: when are these copy functions invoked? Shouldn't it be sufficient to copy parameters?
	virtual void copyObjectPropetriesInto(ParameterizedDisassemblyPath* path);

	GLMesh* getWorldCoordinatesSweptObjectMesh();

	virtual void addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects);

	virtual void writePropertiesToFile(FILE* fp) {
		fprintf(fp, "%10.10lf %10.10lf %10.10lf %lf %lf %lf", assemblyAnglePhi, assemblyAngleTheta, pathLength,
			disassemblyDefaultPath[0], disassemblyDefaultPath[1], disassemblyDefaultPath[2]);
	}

	virtual void readPropertiesFromString(const char* line) {
		double x = -1, y = -1, z = -1;
		if (sscanf(line, "%lf %lf %lf %lf %lf %lf",  &assemblyAnglePhi, &assemblyAngleTheta, &pathLength, &x, &y, &z) != 6)
			throwError("Incorrect assembly input file - 6 arguments are required to specify the assembly path of an object\n", line);
		disassemblyDefaultPath = V3D(x, y, z);
		resetInternalAxis();
	}
};



/**
Note: 
	phase1: component will move along some axis specified in coordinate frame of the component (this path will change with rotation of component)
	phase2: direction of dissassembly is represented in world coords (e.g. rotating the component does not change the disassembly path)
*/
class TwoPhaseArbitraryDirectionDissassemblyPath : public ParameterizedDisassemblyPath {
public:
	V3D phase1LocalCoordsDirection = V3D(0,0,-1);
	double phase1PathLength = 0.015;
	double assemblyAngleTheta = 0; //[rad]
	double assemblyAnglePhi = 0; //[rad]
	V3D rotAxisTheta = V3D(1, 0, 0), rotAxisPhi = V3D(0, 0, 1);
	V3D disassemblyDefaultPath = V3D(0, 1, 0);
	btConvexHullShape btSweptColShapePhase1, btSweptColShapePhase2;
	btCollisionObject colObjectPhase1, colObjectPhase2;


	TwoPhaseArbitraryDirectionDissassemblyPath(AssemblableComponent* component) : ParameterizedDisassemblyPath(component) {}

	virtual void resetInternalAxis() {
		disassemblyDefaultPath.getOrthogonalVectors(rotAxisTheta, rotAxisPhi);
	}

	virtual V3D getWorldCoordinatesAssemblyPath() {
		return getRotationQuaternion(assemblyAngleTheta, rotAxisTheta) * getRotationQuaternion(assemblyAnglePhi, rotAxisPhi) * disassemblyDefaultPath;
	}

	virtual void setWorldCoordinatesAssemblyPath(Quaternion v) {
		disassemblyDefaultPath = v*V3D(0, 1, 0);
		resetInternalAxis();
	}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList);
	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex);

	//t==0 will always return the current position of the component. t==1 represents its final location
	virtual P3D getComponentPositionAlongPathAt(double t);

	//t==0 will always return the current orientation of the component. t==1 represents its final orientation
	virtual Quaternion getComponentOrientationAlongPathAt(double t);

	virtual void copyObjectPropetriesInto(ParameterizedDisassemblyPath* path);

	GLMesh* getWorldCoordinatesSweptObjectMesh();

	virtual void addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects);

	virtual void writePropertiesToFile(FILE* fp) {
		fprintf(fp, "%10.10lf %10.10lf %10.10lf %10.10lf %lf %lf %lf %lf %lf %lf", assemblyAnglePhi, assemblyAngleTheta, pathLength, phase1PathLength,
			phase1LocalCoordsDirection[0], phase1LocalCoordsDirection[1], phase1LocalCoordsDirection[2], 
			disassemblyDefaultPath[0], disassemblyDefaultPath[1], disassemblyDefaultPath[2]);
	}

	virtual void readPropertiesFromString(const char* line) {
		double x = -1, y = -1, z = -1, x1 = -1, y1 = -1, z1 = -1;
		if (sscanf(line, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &assemblyAnglePhi, &assemblyAngleTheta, &pathLength,
			&phase1PathLength, &x, &y, &z, &x1, &y1, &z1) != 10)
			throwError("Incorrect assembly input file - 10 arguments are required to specify the assembly path of an object\n", line);
		phase1LocalCoordsDirection = V3D(x, y, z);
		disassemblyDefaultPath = V3D(x1, y1, z1);
	}
};

