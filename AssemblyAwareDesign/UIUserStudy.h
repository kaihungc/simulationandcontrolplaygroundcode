#pragma once

#include <GUILib/GLApplication.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindowContainer.h>
#include <GUILib/GLMesh.h>
#include "BaseObject.h"
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include <GUILib\RotateWidgetV1.h>

#include <OptimizationLib\ObjectiveFunction.h>

#include "assembly.h"
#include "AssemblyPlanner.h"
# include "MCMCPlanners.h"

/**
* Test UI for Assembly-aware design project
*/
class UIUserStudy : public GLApplication {
public:
	// constructor
	UIUserStudy();
	// destructor
	virtual ~UIUserStudy(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	// sub windows
	GLWindowContainer* windowArray;

	bool drawBulletCPs = false;
	bool drawClosestPointPairs = true;

	// plots for cost function
	bool drawPlots = false;
	PlotWindow* plotWindow = NULL;

	// assembly that is being built in the scene
	Assembly mainAssembly;
	// planner
	MCMCPlanners* planner = NULL;

	// objects in sub windows and objects in main window
	DynamicArray<EMComponent*> objectMenuList;
	BaseObject* selectedObject = NULL;
	BaseObject* menuSelectedObject = NULL;

	// Translate Widget
	TranslateWidget* tWidget;
	RotateWidget* rWidget;
	RotateWidgetV1* rWidgetDir;

	//true means translate widget. false means rotate widget
	bool translateRotateWidget = true;

	enum RUN_OPTIONS {
		DEFAULT = 0,
		DISASSEMBLY_VISUALIZATION,
		DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS,
	};
	int runOption = DEFAULT;
	int gradItr = 0;

	//phase for animation
	double animPhase = 0;
	double animStep = 0.05;

	// virtual assembly default fill ratio
	double fillRatio = 0.05;
	double chassisScale = 1.0;
	int numComponentsVirtual = 8;

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);

	void loadMenuParametersFor(BaseObject* objPrimitive);
	void unloadMenuParametersFor(BaseObject* objPrimitive);

	void loadMenuNameFor(BaseObject* objPrimitive);
	void unloadMenuNameFor(BaseObject* objPrimitive);

	// should be used for loading chassis?
	void loadFile(const char* fName);
	void saveFile(const char* fName);

	void onDelete();

};

void TW_CALL UIButtonEventDelete2(void* clientData);
void TW_CALL UIButtonEventSave2(void* clientData);
void TW_CALL UIButtonEventLoad2(void* clientData);
void TW_CALL UIButtonEventCreateSupport2(void* clientData);
void TW_CALL UIButtonEventAssemblyFillRatio2(void* clientData);
void TW_CALL UIButtonEventSetAssemblyOrder2(void* clientData);
void TW_CALL UIButtonEventExport2(void* clientData);
void TW_CALL UIButtonEventResetAnim2(void* clientData);
void TW_CALL UIButtonEventSwapOrder2(void* clientData);
