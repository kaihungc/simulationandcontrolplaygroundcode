#include "FastenerCollisionObject.h"
#include "Fastener.h"

void FastenerCollisionObject::initGeometry() {

	colShape = btCapsuleShape((btScalar)parent->r, (btScalar)parent->disassemblyPath->pathLength);
}

P3D FastenerCollisionObject::getPosition() {
	BaseObject::setPosition(parent->parent->getWorldCoordinates(grandParentLocalSupportPos));
	return BaseObject::getPosition();
}

Quaternion FastenerCollisionObject::getOrientation() {
	BaseObject::setOrientation(parent->getOrientation());
	return BaseObject::getOrientation();
}

bool FastenerCollisionObject::shouldCollideWith(BaseObject* obj) {
	// support shouldn't collide with its parent
	if (dynamic_cast<EMComponent*>(obj)) {
		if (this->parent->parent == dynamic_cast<EMComponent*>(obj))
			return false;
	}
	return true;
}

void FastenerCollisionObject::addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
	bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShape, &colObject,
		parent->pathAxis.unit()*parent->disassemblyPath->pathLength / 2.0));
}