#pragma once
#include <BulletCollision/btBulletCollisionCommon.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include<MathLib/Quaternion.h>
#include <Utils/Logger.h>

//e.g. position and orientation of an object
#define OPTIMIZE_STATE_PARAMETERS 1
//e.g. location of a pulley on a shaft
#define OPTIMIZE_MECHANICAL_PARAMETERS 2
//e.g. parameters that define end points of a shaft and direction of support structures
#define OPTIMIZE_STRUCTURE_PARAMETERS 4
//e.g. parameters that define assembly operations, such as the path along which objects need to be removed and/or inserted
#define OPTIMIZE_ASSEMBLY_PARAMETERS 8

// dimension of the assembly system
#define SIM_1D 1
#define SIM_2D 2
#define SIM_3D 4

enum ASSEMBLY_KEYWORDS {
	AS_NOT_IMPORTANT = -1,
	CHASSIS_NAME,
	CHASSIS_SCALE,
	OBJ_OBJ,
	OBJ_END_OBJ,
	OBJ_NAME,
	OBJ_SUPPORT,
	DISASS_IND,
	ASSEM_PARAMS,
	OBJ_POSITION,
	OBJ_ORIENTATION,
	OBJ_IS_FROZEN,
	OBJ_ALLOW_POS,
	OBJ_ALLOW_ROT,
	OBJ_ADDITIONALROTATION,
	OBJ_DIMS
};

// determine the type of a line that was used in the input file for an assembly
int getASLineType(char* &buffer);

// returns the string associated with the given token
char* getASString(int token);

// conversions between bullet and our interface
btVector3 bulletVector(const P3D& p);
btVector3 bulletVector(const V3D& v);
btQuaternion bulletQuaternion(const Quaternion& q);

V3D getV3D(const btVector3& v);
P3D getP3D(const btVector3& v);
Quaternion getQuaternionFromBullet(const btQuaternion& q);

inline void pushElementIntoVector(dVector& v, int index, double val) {
	if (index >= v.size()) {
//		Logger::consolePrint("need to resize!\n");
		v.conservativeResize(index + 1);
	}

	v[index] = val;
}

inline void roundNearest(double& i, int roundUptoDigitsAfterDecimal) {
	double constant = pow(10, roundUptoDigitsAfterDecimal);
	//Logger::consolePrint("input i:%10.10lf, power:%lf\n", i, constant);
	i =	roundl(i * constant) / constant;
	//Logger::consolePrint(" i after:%10.10lf\n", i);
}

inline void roundNearest(P3D& i, int roundUptoDigitsAfterDecimal) {
	roundNearest(i[0], roundUptoDigitsAfterDecimal);
	roundNearest(i[1], roundUptoDigitsAfterDecimal);
	roundNearest(i[2], roundUptoDigitsAfterDecimal);
}

void saveLogDataToTxtFile(std::string saveFileName, std::string saveFilePath);