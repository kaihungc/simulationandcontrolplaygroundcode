# include "Chassis.h"

void Chassis::drawObjectGeometry(GLShaderMaterial* material) {
	GLShaderMaterial tmpMat;
	GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

	if (material == NULL) {
		tmpMat.r = 1; tmpMat.g = 1; tmpMat.b = 1; tmpMat.a = 0.4;
		//tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
		//tmpMat.setTextureParam(".. / data / textures / matcap / MatCap_0005.bmp", GLContentManager::getTexture("../data/textures/matcap/MatCap_0005.bmp"));
	}

	chassisMesh->setMaterial(*matToUse);
	chassisMesh->drawMesh();
}

