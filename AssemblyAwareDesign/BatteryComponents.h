#pragma once

#include "MakeBlockComponents.h"

class Battery9v : public MakeBlockComponents {
public:
	// constructor
	Battery9v(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~Battery9v() {};

	virtual void initGeometry() {
		name = "Battery9v";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/battery9V.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/battery9V_2.obj"));
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/battery9V_1.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.013 * 2;
		yMax = 0.0081 * 2;
		zMax = 0.0241 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, 0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2 - 0.0025, 0), AxisAlignedBoundingBox(P3D(-xMax/ 2, -0.0025, -zMax / 2), P3D(xMax / 2, 0.0025, zMax / 2))));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Battery9v* newObject = new Battery9v(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}
};

class Battery12vHolder : public MakeBlockComponents {
public:
	// constructor
	Battery12vHolder(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~Battery12vHolder() {};

	virtual void initGeometry() {
		name = "Battery12v";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/battery12VWHolder.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/battery12V_bats.obj"));
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/blue_matte.bmp", GLContentManager::getTexture("../data/textures/matcap/blue_matte.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/battery12V_box.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.09;
		yMax = 0.017;
		zMax = 0.06;

		// adding some offset in y to account for battery out of box.
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0 + 0.0015), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, 0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0.029, -yMax / 2, 0), AxisAlignedBoundingBox(P3D(-0.005, -0.0025, -0.015), P3D(0.005, 0.0025, 0.015))));
		supports.push_back(new RigidlyAttachedSupport(this, P3D(-0.029, -yMax / 2, 0), AxisAlignedBoundingBox(P3D(-0.005, -0.0025, -0.015), P3D(0.005, 0.0025, 0.015))));

		fasteners.push_back(new Fastener(this, P3D(0.029, 0, 0), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(-0.029, 0, 0), V3D(0, 1, 0), 0.00205));

		beta = -PI / 2.0;
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Battery12vHolder* newObject = new Battery12vHolder(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}
};