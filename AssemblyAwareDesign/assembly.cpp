#include <include/glew.h>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <GUILib/GLContentManager.h>
#include <GUILib/GLUtils.h>
#include <MathLib/Segment.h>
#include "assembly.h"
#include "AssemblyUtils.h"
#include "Support.h"
#include "BoxComponent.h"
#include "MotorComponents.h"
#include "MakeBlockComponents.h"
#include "BatteryComponents.h"
#include "Chassis.h"
#include <Mathlib/MeshBoolean.h>
#include "ElectronicsLibrary.h"

#define CREATE_OBJECT_IF_APPROPRIATE(name, TYPE)					\
			if (strcmp(tmp.c_str(), name) == 0) {					\
				TYPE* newObj = new TYPE();							\
				newObj->loadFromFile(fp);							\
				electroMechanicalComponents.push_back(newObj);		\
			}

#define CREATE_CHASSIS_IF_APPROPRIATE(name, TYPE)					\
			if (strcmp(tmp.c_str(), name) == 0) {					\
				TYPE* newObj = new TYPE();							\
				chassis = newObj;		\
			}

Assembly::Assembly(Chassis* chas){
	constraintsManager = new ConstraintsManager(this);
	collisionManager = new CollisionManager(this);

	std::string tmp = chas->name.c_str();
	DynamicArray<std::string> chassisNames = { "WallEChassis", "OwlChassis", "BalanceBotChassis", "VirtualChassis" , "BirdieChassis", "TrapezoidChassis"
	, "TrapezoidFlippedChassis", "HexagonChassis", "BunnyChassis", "TrainingChassis"};

	if (strcmp(tmp.c_str(), chassisNames[0].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("WallEChassis", WallEChassis);
	if (strcmp(tmp.c_str(), chassisNames[1].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("OwlChassis", OwlChassis);
	if (strcmp(tmp.c_str(), chassisNames[2].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("BalanceBotChassis", BalanceBotChassis);
	if (strcmp(tmp.c_str(), chassisNames[3].c_str()) == 0) {
		CREATE_CHASSIS_IF_APPROPRIATE("VirtualChassis", VirtualChassis);
		if (dynamic_cast<VirtualChassis*>(chas)) {
			dynamic_cast<VirtualChassis*>(this->chassis)->updateSizeChassis(dynamic_cast<VirtualChassis*>(chas)->lengthScale);
			dynamic_cast<VirtualChassis*>(this->chassis)->scaleChassis(dynamic_cast<VirtualChassis*>(chas)->currentScaleFactor);
		}
	}
	if (strcmp(tmp.c_str(), chassisNames[4].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("BirdieChassis", BirdieChassis);
	if (strcmp(tmp.c_str(), chassisNames[5].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("TrapezoidChassis", TrapezoidChassis);
	if (strcmp(tmp.c_str(), chassisNames[6].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("TrapezoidFlippedChassis", TrapezoidFlippedChassis);
	if (strcmp(tmp.c_str(), chassisNames[7].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("HexagonChassis", HexagonChassis);
	if (strcmp(tmp.c_str(), chassisNames[8].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("BunnyChassis", BunnyChassis);
	if (strcmp(tmp.c_str(), chassisNames[9].c_str()) == 0)
		CREATE_CHASSIS_IF_APPROPRIATE("TrainingChassis", TrainingChassis);

}

//a struct to store the object index and a parameter value(could be volume, or y position)
struct componentParamterLog {
	int objectIndex;
	double value;
};

//compare function for struct objectParamterLog
bool compareComponentsUsingLoggedParam(componentParamterLog* a, componentParamterLog* b) {
	return a->value > b->value;
};

double Assembly::calculateSupportFillRatio() {

	double volume = 0;
	double chassisvolume = 0;
	double fillratio = 0;

	if (chassis) {
		double l, h, b;
		chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8 * l*b*h;
	}

	DynamicArray<RigidlyAttachedSupport*> supports;

	for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
		electroMechanicalComponents[i]->addSupportsToList(supports);
	}

	// Support volume
	for (uint j = 0; j < supports.size(); j++) {
		double sxsup1, sysup1, szsup1;
		supports[j]->getSilhouetteScalingDimensions(sxsup1, sysup1, szsup1);//size of support
		volume += abs(sxsup1*szsup1*sysup1 * 8);
	}

	if (chassisvolume>0)
		fillratio = volume / chassisvolume;

	return fillratio;
}

double Assembly::calculateComponentFillRatio() {
	double volume = 0;
	double chassisvolume = 0;
	double fillratio = 0;

	if (chassis) {
		double l, h, b;
		chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8 * l*b*h;
	}

	DynamicArray<btCollisionObject*> componentCollisionObjects;

	for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
		electroMechanicalComponents[i]->addBulletCollisionObjectsToList(componentCollisionObjects);
	}

	// Volume of all components
	for (uint t = 0; t < componentCollisionObjects.size(); t++) {
		btTransform T;
		T.setIdentity();
		//bounding box diagonal points
		btVector3 bmin, bmax;
		componentCollisionObjects[t]->getCollisionShape()->getAabb(T, bmin, bmax);
		btVector3 size;
		size[0] = abs((bmax[0] - bmin[0]));
		size[1] = abs((bmax[1] - bmin[1]));
		size[2] = abs((bmax[2] - bmin[2]));
		volume += size[0] * size[1] * size[2];
	}

	if (chassisvolume>0)
		fillratio = volume / chassisvolume;

	return fillratio;
}

double Assembly::calculateFillRatio() {
	double volume = 0;
	double chassisvolume = 0;
	double fillratio = 0;

	if (chassis) {
		double l, h, b;
		chassis->getSilhouetteScalingDimensions(l, h, b);
		chassisvolume = 8*l*b*h;
	}

	DynamicArray<RigidlyAttachedSupport*> supports;
	DynamicArray<btCollisionObject*> componentCollisionObjects;

	for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
		electroMechanicalComponents[i]->addBulletCollisionObjectsToList(componentCollisionObjects);
		electroMechanicalComponents[i]->addSupportsToList(supports);
	}

	// Volume of all components
	for (uint t = 0; t < componentCollisionObjects.size(); t++) {
		btTransform T;
		T.setIdentity();
		//bounding box diagonal points
		btVector3 bmin, bmax;
		componentCollisionObjects[t]->getCollisionShape()->getAabb(T, bmin, bmax);
		btVector3 size;
		size[0] = abs((bmax[0] - bmin[0]));
		size[1] = abs((bmax[1] - bmin[1]));
		size[2] = abs((bmax[2] - bmin[2]));
		volume += size[0] * size[1] * size[2];
	}

	// Support volume
	for (uint j = 0; j < supports.size(); j++) {
		double sxsup1, sysup1, szsup1;
		supports[j]->getSilhouetteScalingDimensions(sxsup1, sysup1, szsup1);//size of support
		volume += abs(sxsup1*szsup1*sysup1 * 8);
	}

	if (chassisvolume>0)
		fillratio = volume / chassisvolume;

	return fillratio;
}

void Assembly::setupConstraints() {
	constraintsManager->constraintsCollection->softConstraints.clear();
	DynamicArray<RigidlyAttachedSupport*> allSupports;

	for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
		//reset target state values to update for user interactions...
		electroMechanicalComponents[i]->resetTargetStateValues();
		electroMechanicalComponents[i]->addSupportsToList(allSupports);
	}
	// collision constraints 
	for (uint i = 0; i < collisionObjects.size(); i++)
		constraintsManager->constraintsCollection->softConstraints.push_back(&collisionObjects[i]);

	// support volume costs
	for (size_t i = 0; i < allSupports.size(); i++)
	{
		constraintsManager->constraintsCollection->softConstraints.push_back(allSupports[i]->supportVolCostObj);
		if (!minSupportVolume)
			constraintsManager->constraintsCollection->softConstraints.back()->weight = 0;
		else
			constraintsManager->constraintsCollection->softConstraints.back()->weight = 10;
	}
}

void Assembly::randomizeAssemblyOrder() {
	// random disassembly indices
	DynamicArray<int> disassemblyIndices;
	for (uint i = 0; i<electroMechanicalComponents.size(); i++) 
		disassemblyIndices.push_back(electroMechanicalComponents[i]->disassemblyIndex);

	// generate a random assembly sequence
	std::random_shuffle(disassemblyIndices.begin(), disassemblyIndices.end());
	int dissassemInd = 0;
	for (uint j = 0; j < electroMechanicalComponents.size(); j++) {
		electroMechanicalComponents[j]->disassemblyIndex = disassemblyIndices[j];
	}
}

Assembly* Assembly::clone() {
	Assembly* newAssembly = new Assembly(chassis);

	// clone objects and create an object list in the clone assembly
	for (uint j = 0; j < electroMechanicalComponents.size(); j++) {
		newAssembly->electroMechanicalComponents.push_back(electroMechanicalComponents[j]->clone(false));
		electroMechanicalComponents[j]->copyObjectPropetriesInto(newAssembly->electroMechanicalComponents.back());
	}

	//supports 
	//newAssembly->completeAssembly();
	//newAssembly->snapSupportsToWall();

	// we are done! Return.. 
	return newAssembly;
}

Assembly* Assembly::clone(DynamicArray<EMComponent*> subAssemblyComponents) {
	Assembly* newAssembly = new Assembly(chassis);

	// clone objects and create an object list in the clone assembly
	//for (uint j = 0; j < electroMechanicalComponents.size(); j++) {
		for (size_t i = 0; i < subAssemblyComponents.size(); i++)
		{
			//if (electroMechanicalComponents[j] == subAssemblyComponents[i]) {
				newAssembly->electroMechanicalComponents.push_back(subAssemblyComponents[i]->clone(true));
				subAssemblyComponents[i]->copyObjectPropetriesInto(newAssembly->electroMechanicalComponents.back());
			//}
		}
	//}

	//supports 
	//newAssembly->completeAssembly();
	//newAssembly->snapSupportsToWall();

	// we are done! Return.. 
	return newAssembly;
}

void Assembly::findClosestChassisWallToSupport(RigidlyAttachedSupport* support) {

	double closestWallDistance = DBL_MAX;
	for (size_t i = 0; i < chassis->walls.size(); i++)
	{
		if (chassis->isWallReal[i]) {
			Plane wallPlane = Plane(chassis->walls[i].wallCenter, chassis->walls[i].normal);
			double d1 = wallPlane.getSignedDistanceToPoint(support->getWorldCoordinates(support->bBox.bmax()));
			double d2 = wallPlane.getSignedDistanceToPoint(support->getWorldCoordinates(support->bBox.bmin()));
			if (support->validateResizeDirection(-wallPlane.n)) {
				if (MIN(d1, d2) < closestWallDistance) {
					closestWallDistance = MIN(d1, d2);
					support->closestWallPlane = wallPlane;
					support->closestWallId = i;
				}
			}
		}
	}
}

Plane Assembly::findClosestChassisWallToComponent(EMComponent* component) {
	double closestWallDistance = DBL_MAX;
	Plane closestWallPlane = Plane();
	for (size_t i = 0; i < chassis->walls.size(); i++)
	{
		if (!chassis->isWallReal[i]) {
			Plane wallPlane = Plane(chassis->walls[i].wallCenter, chassis->walls[i].normal);
			double d = wallPlane.getSignedDistanceToPoint(component->getPosition());
			if (d< closestWallDistance) {
				closestWallDistance = d;
				closestWallPlane = wallPlane;
			}
		}
	}
	return closestWallPlane;
}

void Assembly::snapSupportsToWalls(bool updateNearestWall, bool updateSupportSize) {
	// collect all supports
	DynamicArray<RigidlyAttachedSupport*> allSupports;
	for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
		electroMechanicalComponents[i]->addSupportsToList(allSupports);
	}
	for (size_t j = 0; j < allSupports.size(); j++)
	{
		if (updateNearestWall || allSupports[j]->closestWallId == -1) {
			//updateSupportPosition(allSupports[j]);
			findClosestChassisWallToSupport(allSupports[j]);
		}
		if(updateSupportSize)
			allSupports[j]->updateSupportSize();
	}
}

void Assembly::updateSupportPosition(RigidlyAttachedSupport* support) {
	if (support->parent->disassemblyPath->getWorldCoordinatesAssemblyPath().angleWith(V3D(support->parent->getPosition(),
		support->parent->getWorldCoordinates(support->parentLocalSupportPos))) < PI / 2)
		//support->parentLocalSupportPos = -support->parentLocalSupportPos;
		support->parent->alpha = PI;
}

void Assembly::saveAssembly() {
	FILE* fp = fopen("assembly.txt", "w");
	fprintf(fp, "%s %s\n", getASString(CHASSIS_NAME), this->chassis->name.c_str());
	if (strcmp(this->chassis->name.c_str(), "VirtualChassis") == 0) {
		fprintf(fp, "%s %lf %lf\n", getASString(CHASSIS_SCALE), dynamic_cast<VirtualChassis*>(this->chassis)->lengthScale,
			dynamic_cast<VirtualChassis*>(this->chassis)->currentScaleFactor);
	}

	for (uint i = 0; i<electroMechanicalComponents.size(); i++)
		electroMechanicalComponents[i]->writeToFile(fp);

	fclose(fp);
}

void Assembly::saveAssembly(const char* fName) {
	FILE* fp = fopen(fName, "w");
	fprintf(fp, "%s %s\n", getASString(CHASSIS_NAME), this->chassis->name.c_str());
	if (strcmp(this->chassis->name.c_str(), "VirtualChassis") == 0) {
		fprintf(fp, "%s %lf %lf\n", getASString(CHASSIS_SCALE), dynamic_cast<VirtualChassis*>(this->chassis)->lengthScale,
			dynamic_cast<VirtualChassis*>(this->chassis)->currentScaleFactor);
	}

	for (uint i = 0; i<electroMechanicalComponents.size(); i++)
		electroMechanicalComponents[i]->writeToFile(fp);

	fclose(fp);
}

void Assembly::loadAssembly(const char* fName) {

	// in order to preserve connections and objects being loaded from the file,
	// the current objects in the scene need to be updated. So, make their temporary copy,
	// and wait till objects from file are loaded to make these updates.
	bool objectsInScene = false;
	DynamicArray<EMComponent*> temporaryObjectList;

	if (electroMechanicalComponents.size() > 0) {
		objectsInScene = true;
		temporaryObjectList = electroMechanicalComponents;
		electroMechanicalComponents.clear();
	}

	// open file and start loading objects and connections from the file
	FILE* fp = fopen(fName, "r");

	if (fp == NULL)
		throwError("Invalid file pointer.");

	//have a temporary buffer used to read the file line by line...
	char buffer[200];

	//this is where it happens.
	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, 200, fp);
		char *line = lTrim(buffer);
		int lineType = getASLineType(line);
		switch (lineType) {
		case CHASSIS_NAME: {
			std::string tmp;
			tmp = std::string() + trim(line);
			if (strcmp(tmp.c_str(), this->chassis->name.c_str()) == 0) continue;
			else {
				delete chassis;
				CREATE_CHASSIS_IF_APPROPRIATE("WallEChassis", WallEChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("OwlChassis", OwlChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("BalanceBotChassis", BalanceBotChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("VirtualChassis", VirtualChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("BirdieChassis", BirdieChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("TrapezoidChassis", TrapezoidChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("TrapezoidFlippedChassis", TrapezoidFlippedChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("HexagonChassis", HexagonChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("BunnyChassis", BunnyChassis);
				CREATE_CHASSIS_IF_APPROPRIATE("TrainingChassis", TrainingChassis);
			}
		}
						   break;

		case CHASSIS_SCALE: {
			double ind1, ind2 = -1;
			sscanf(line, "%lf %lf", &ind1, &ind2);

			if (dynamic_cast<VirtualChassis*>(this->chassis)) {
				dynamic_cast<VirtualChassis*>(this->chassis)->updateSizeChassis(ind1);
				dynamic_cast<VirtualChassis*>(this->chassis)->scaleChassis(ind2);
			}
			break;
		}
		case OBJ_NAME: {
			std::string tmp;
			tmp = std::string() + trim(line);

			//Box component
			CREATE_OBJECT_IF_APPROPRIATE("BoxyComponent", BoxComponent);

			// Motors
			CREATE_OBJECT_IF_APPROPRIATE("DCMotor", DCMotor);
			CREATE_OBJECT_IF_APPROPRIATE("ServoMotor", ServoMotor);
			CREATE_OBJECT_IF_APPROPRIATE("MicroServoMotor", MicroServoMotor);

			// Batteries
			CREATE_OBJECT_IF_APPROPRIATE("Battery9v", Battery9v);
			CREATE_OBJECT_IF_APPROPRIATE("Battery12v", Battery12vHolder);

			// for !WallE
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockOrionBoard", MakeBlockOrionBoard);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockMotorDriver", MakeBlockMotorDriver);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockUltrasonic", MakeBlockUltrasonic);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockBluetooth", MakeBlockBluetooth);

			// Owl shaped crib toy
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockMegaPi", MakeBlockMegaPi);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockAccelerometer", MakeBlockAccelerometer);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockEncoderMotorDriver", MakeBlockEncoderMotorDriver);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockSoundSensor", MakeBlockSoundSensor);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockLED", MakeBlockLED);
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockButton", MakeBlockButton);

			// generic components used for benchmarking examples from the electronics library class
			CREATE_OBJECT_IF_APPROPRIATE("MakeBlockGeneric", MakeBlockGeneric);
			CREATE_OBJECT_IF_APPROPRIATE("Adafruit", Adafruit);
			CREATE_OBJECT_IF_APPROPRIATE("Board", Board);
			CREATE_OBJECT_IF_APPROPRIATE("PCB", PCB);
			CREATE_OBJECT_IF_APPROPRIATE("Sensor", Sensor);
			CREATE_OBJECT_IF_APPROPRIATE("Breakout", Breakout);

		}

					   break;
		}
	}

	fclose(fp);

	if (objectsInScene) {
		for (uint i = 0; i < temporaryObjectList.size(); i++) {
			electroMechanicalComponents.push_back(temporaryObjectList[i]);
		}
		temporaryObjectList.clear();
	}
}

bool Assembly::checkCollisionFreeValidness(bool computeCollisionObjects) {
	if(computeCollisionObjects)
		collisionManager->performCollisionDetectionForEntireAssemblyProcess();
	for (uint i = 0; i < collisionObjects.size(); i++) {
		if (collisionObjects[i].penetrationDepth < 0) {
			collisionObjects[i].obj1->collided = true;
			collisionObjects[i].obj2->collided = true;
		}
	}
	DynamicArray<RigidlyAttachedSupport*> allSupports;
	for (size_t i = 0; i < electroMechanicalComponents.size(); i++)
	{
		electroMechanicalComponents[i]->addSupportsToList(allSupports);
		if (electroMechanicalComponents[i]->collided)
			return false;
	}

	for (size_t i = 0; i < allSupports.size(); i++)
	{
		if (allSupports[i]->collided)
			return false;
	}

	return true;
}

DynamicArray<EMComponent*> Assembly::getSortedComponentsBasedOnVolume(DynamicArray<EMComponent*> inputArrayOfComponents) {

	DynamicArray<EMComponent*> sortedComponents;
	DynamicArray<componentParamterLog*> componentVolumeIndexArray;//an array to store the object index and their corresponding y value

	for (uint i = 0; i < inputArrayOfComponents.size(); i++) {
		double volume = 0;
		DynamicArray<btCollisionObject*> componentCollisionObjects;
		inputArrayOfComponents[i]->addBulletCollisionObjectsToList(componentCollisionObjects);

		// Volume of component
		for (uint t = 0; t < componentCollisionObjects.size(); t++) {
			btTransform T;
			T.setIdentity();
			//bounding box diagonal points
			btVector3 bmin, bmax;
			componentCollisionObjects[t]->getCollisionShape()->getAabb(T, bmin, bmax);
			btVector3 size;
			size[0] = abs((bmax[0] - bmin[0]));
			size[1] = abs((bmax[1] - bmin[1]));
			size[2] = abs((bmax[2] - bmin[2]));
			volume += size[0] * size[1] * size[2];
		}

		// store this into another array for sorting
		componentParamterLog* t = new componentParamterLog();
		t->objectIndex = i;
		t->value = volume;
		componentVolumeIndexArray.push_back(t);
	}


	sort(componentVolumeIndexArray.begin(), componentVolumeIndexArray.end(), compareComponentsUsingLoggedParam);

	for (uint i = 0; i < inputArrayOfComponents.size(); i++) {
		sortedComponents.push_back(inputArrayOfComponents[componentVolumeIndexArray[i]->objectIndex]);
	}

	//deleting the array used for sorting..
	for (uint i = 0; i < componentVolumeIndexArray.size(); i++) {
		delete componentVolumeIndexArray[i];
	}

	return sortedComponents;
}

void Assembly::saveCAD() {

	DynamicArray<RigidlyAttachedSupport*> allSupports;
	DynamicArray<Fastener*> allFasteners;
	FILE* fp = fopen("../out/wires.obj", "w");
	int retVal = 0;
	for (size_t i = 0; i < electroMechanicalComponents.size(); i++)
	{
		electroMechanicalComponents[i]->addSupportsToList(allSupports);
		electroMechanicalComponents[i]->addFastnersToList(allFasteners);
		if (dynamic_cast<MakeBlockComponents*>(electroMechanicalComponents[i])
			&& dynamic_cast<MakeBlockComponents*>(electroMechanicalComponents[i])->getWireGeometryMesh())
			retVal += dynamic_cast<MakeBlockComponents*>(electroMechanicalComponents[i])->getWireGeometryMesh()->renderToObjFile
			(fp, retVal, Quaternion(), P3D());
	}
	fclose(fp);

	// write supports to .obj file
	FILE* fp1 = fopen("../out/supports.obj", "w");
	int retVal1 = 0;
	for (size_t i = 0; i < allSupports.size(); i++)
	{
		// ensuring watertightness between support and chassis
		allSupports[i]->closestWallPlane = 	Plane(allSupports[i]->closestWallPlane.p + allSupports[i]->closestWallPlane.n*-0.004, 
			allSupports[i]->closestWallPlane.n);
		allSupports[i]->updateSupportSize();
		retVal1 += allSupports[i]->globalCoordinatesWallExtensionMesh.renderToObjFile(fp1, retVal1, Quaternion(), P3D());
	}
	fclose(fp1);

	// write fasteners to .obj file
	FILE* fp2 = fopen("../out/fasteners.obj", "w");
	int retVal2 = 0;
	for (size_t i = 0; i < allFasteners.size(); i++)
	{
		retVal2 += allFasteners[i]->getFastenerMesh()->renderToObjFile(fp2, retVal2, Quaternion(), P3D());
	}
	fclose(fp2);
}

