#include "BoxComponent.h"
#include "AssemblyUtils.h"
#include <GUILib/GLUtils.h>
#include <iostream>
#include <GUILib/GLContentManager.h>
#include "BulletCollisionObject.h"
#include "Support.h"

/**
	Default constructor
*/
BoxComponent::BoxComponent() : EMComponent() {
	name = "BoxyComponent";
	disassemblyPath = new ArbitraryDirectionDissassemblyPath(this);
	//	disassemblyPath = new TwoPhaseArbitraryDirectionDissassemblyPath(this);
	//initGeometry();
	//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
}

BoxComponent::~BoxComponent() {
	if (boxSupport)
		delete boxSupport;
}

BoxComponent::BoxComponent(double x, double y, double z, bool support) : EMComponent() {
	name = "BoxyComponent";
	l = x, b = z, h = y;
	hasSupport = support;
	disassemblyPath = new ArbitraryDirectionDissassemblyPath(this);
	//	disassemblyPath = new TwoPhaseArbitraryDirectionDissassemblyPath(this);
	//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
	initGeometry();
}

void BoxComponent::initGeometry() {
	colShape = btBoxShape(btVector3(btScalar(l / 2), btScalar(h / 2), btScalar(b / 2)));

	DynamicArray<P3D> colShapePoints;
	//this will work for any union of box collision shapes (or other shapes that we can access the vertices of)
	addBTColShapeVerticesToPointList(colShape, colShapePoints);

	DynamicArray<ConvexHull_Face> faces;
	ConvexHull3D::computeConvexHullFromSetOfPoints(colShapePoints, objectCoordsConvexHullVertices, faces);

	if (hasSupport)
		boxSupport = new RigidlyAttachedSupport(this, P3D(0, -h / 2 - 0.0025, 0), AxisAlignedBoundingBox(P3D(-l / 2, -0.0025, -b / 2), P3D(l / 2, 0.0025, b / 2)));
}


EMComponent* BoxComponent::clone(bool cloneTexture) {
	//TODO: might want to copy other properties, if they are important
	return new BoxComponent(l, h, b, hasSupport);
}

void BoxComponent::drawObjectGeometry(GLShaderMaterial* material) {
	GLShaderMaterial tmpMat;
	GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

	if (material == NULL) {
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.7;
	}

	matToUse->apply();
	drawBox(P3D(-l / 2, -h / 2, -b / 2), P3D(l / 2, h / 2, b / 2));
	matToUse->end();
}

//modifies parameterList by pushing its own parameters. 
void BoxComponent::pushParametersToList(DynamicArray<double>& parameterList, int optimizationFlag) {
	EMComponent::pushParametersToList(parameterList, optimizationFlag);
	//push component specific parameters here...
}

//reads parameters from the list starting at pIndex
void BoxComponent::readParametersFromList(DynamicArray<double>& parameterList, int& pIndex, int optimizationFlag) {
	EMComponent::readParametersFromList(parameterList, pIndex, optimizationFlag);
	//read component specific parameters here...
}

void BoxComponent::writeToFile(FILE* fp) {

	char* str;

	fprintf(fp, "%s\n", getASString(OBJ_OBJ));

	EMComponent::writeBasePropertiesToFile(fp);

	fprintf(fp, "\t%s %lf %lf %lf\n", getASString(OBJ_DIMS), this->l, this->b, this->h);

	if (hasSupport)
		fprintf(fp, "\t%s %d\n", getASString(OBJ_SUPPORT), 1);

	str = getASString(OBJ_END_OBJ);
	fprintf(fp, "%s\n\n\n", str);

}

/**
This method loads all the pertinent information regarding the object from a file.
*/
void BoxComponent::loadFromFile(FILE* fp) {
	if (fp == NULL)
		throwError("Invalid file pointer.");

	// read base object properties and go to the file part where object specific properties start
	readBasePropertiesFromFile(fp);

	//have a temporary buffer used to read the file line by line...
	char buffer[200];

	//this is where it happens.
	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, 200, fp);
		char *line = lTrim(buffer);
		int lineType = getASLineType(line);
		switch (lineType) {

		case OBJ_DIMS: {
			sscanf(line, "%lf %lf %lf", &this->l, &this->b, &this->h);
		}
					   break;

		case OBJ_SUPPORT: {
			int ind = -1;
			sscanf(line, "%d", &ind);
			if (ind == 1) {
				this->hasSupport = true;
			}
		}
						break;

		case AS_NOT_IMPORTANT:
			if (strlen(line) != 0 && line[0] != '#')
				Logger::consolePrint("Ignoring input line: \'%s\'\n", line);
			break;

		case OBJ_END_OBJ:
			initGeometry();
			return;//and... done
			break;
		default:
			throwError("Incorrect assembly input file: \'%s\' - unexpected line.", buffer);
		}
	}
	throwError("Incorrect assembly input file! No /End found");
}
