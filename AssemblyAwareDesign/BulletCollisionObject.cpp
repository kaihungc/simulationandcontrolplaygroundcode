#include "BulletCollisionObject.h"

btCollisionObject* getBulletCollisionObjectFromLocalCoordsShape(BaseObject* p, btCollisionShape* cShape, btCollisionObject* colObject, const V3D& geometryCenterLocalOffset) {
	P3D	position = p->getWorldCoordinates(P3D() + geometryCenterLocalOffset);
	Quaternion rotation = p->getOrientation();
	colObject->getWorldTransform().setIdentity();
	colObject->getWorldTransform().setOrigin(btVector3((btScalar)position[0], (btScalar)position[1], (btScalar)position[2]));
	colObject->getWorldTransform().setRotation(btQuaternion((btScalar)rotation.v[0], (btScalar)rotation.v[1], (btScalar)rotation.v[2], (btScalar)rotation.s));
	cShape->setMargin(0.0000f);

	cShape->setUserPointer(p);
	colObject->setCollisionShape(cShape);
	colObject->setUserPointer(p);
	colObject->setCollisionFlags(0);

	return colObject;
}

btCollisionObject* getBulletCollisionObjectFromWorldCoordsShape(BaseObject* p, btCollisionShape* cShape, btCollisionObject* colObject) {
	cShape->setMargin(0.0000f);

	colObject->getWorldTransform().setIdentity();
	colObject->setCollisionShape(cShape);
	colObject->setUserPointer(p);
	colObject->setCollisionFlags(0);

	return colObject;
}

