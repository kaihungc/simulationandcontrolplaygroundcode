#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <GUILib/TranslateWidget.h>
#include "BaseObject.h"
#include "AssemblyUtils.h"
#include "BulletCollisionObject.h"

/*
	Box objects -- virtual objects for synthetic assemblies in benchmarking algorithms.
*/

class BoxComponent : public EMComponent {
private:
	double l = 0.1, b = 0.1, h = 0.1;
	btBoxShape colShape = btBoxShape(btVector3(btScalar(l / 2), btScalar(h / 2), btScalar(b / 2)));
	btCollisionObject colObject;
	RigidlyAttachedSupport* boxSupport = NULL;

public:
	bool hasSupport = false;

	// constructor
	BoxComponent();
	BoxComponent(double l, double b, double h, bool support = false);

	void initGeometry();

	// destructor
	virtual ~BoxComponent();

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimX = l/2; dimY = h/2;  dimZ = b/2;
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> result;
		result.push_back(std::pair<char*, double*>("disassembly dir-x", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[0]));
		result.push_back(std::pair<char*, double*>("disassembly dir-y", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[1]));
		result.push_back(std::pair<char*, double*>("disassembly dir-z", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[2]));
		dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath.toUnit();
		result.push_back(std::pair<char*, double*>("Cuboid-height", &h));
		result.push_back(std::pair<char*, double*>("Cuboid-l", &l));
		result.push_back(std::pair<char*, double*>("Cuboid-b", &b));
		return result;
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
		bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShape, &colObject));
	}

	virtual EMComponent* clone(bool cloneTexture = true);

	virtual void addSupportsToList(DynamicArray<RigidlyAttachedSupport*>& supportObjs) {
		if(boxSupport)
			supportObjs.push_back(boxSupport);
	}

	//modifies parameterList by pushing its own parameters. 
	virtual void pushParametersToList(DynamicArray<double>& parameterList, int optimizationFlag);
	//reads parameters from the list starting at pIndex
	virtual void readParametersFromList(DynamicArray<double>& parameterList, int& pIndex, int optimizationFlag);

	/**
	writes to file
	*/
	virtual void writeToFile(FILE* fp);

	/**
	reads from file
	*/
	virtual void loadFromFile(FILE* fp);

};

