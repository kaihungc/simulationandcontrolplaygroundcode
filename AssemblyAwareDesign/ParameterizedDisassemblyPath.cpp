
#include <GUILib/GLUtils.h>
#include "BaseObject.h"
#include "ParameterizedDisassemblyPath.h"
#include <MathLib/ConvexHull3D.h>

// constructor
ParameterizedDisassemblyPath::ParameterizedDisassemblyPath(AssemblableComponent* component) {
	theComponent = component;
}

// destructor
ParameterizedDisassemblyPath::~ParameterizedDisassemblyPath() {
};

void ParameterizedDisassemblyPath::pushComponentConvexShapeVertsAtTimeTToList(DynamicArray<P3D>& verts, double t) {
	// store original
	Quaternion qOriginal = theComponent->getOrientation();
	P3D posOriginal = theComponent->getPosition();

	//collect the extreme points along the assembly path, in world coordinates
	theComponent->setOrientation(theComponent->getOrientationOnAssemblyPath(t));
	theComponent->setPosition(theComponent->getPositionOnAssemblyPath(t));

	for (size_t i = 0; i < theComponent->objectCoordsConvexHullVertices.size(); i++)
		verts.push_back(theComponent->getWorldCoordinates(theComponent->objectCoordsConvexHullVertices[i]));

	theComponent->setOrientation(qOriginal);
	theComponent->setPosition(posOriginal);
}

P3D FixedDirectionDissassemblyPath::getComponentPositionAlongPathAt(double t) {
	boundToRange(&t, 0, 1);
	return theComponent->getPosition() + theComponent->getOrientation()*disassemblyLocalDefaultPath * t * pathLength;
}

Quaternion FixedDirectionDissassemblyPath::getComponentOrientationAlongPathAt(double t) {
	return theComponent->getOrientation();
}

//modifies parameterList by pushing its own parameters. 
void ArbitraryDirectionDissassemblyPath::pushParametersToList(DynamicArray<double>& parameterList) {
	parameterList.push_back(assemblyAngleTheta);
	parameterList.push_back(assemblyAnglePhi);
}
//reads parameters from the list starting at pIndex
void ArbitraryDirectionDissassemblyPath::readParametersFromList(DynamicArray<double>& parameterList, int& pIndex) {
	assemblyAngleTheta = parameterList[pIndex++];
	assemblyAnglePhi = parameterList[pIndex++];
}

P3D ArbitraryDirectionDissassemblyPath::getComponentPositionAlongPathAt(double t) {
	boundToRange(&t, 0, 1);
	return theComponent->getPosition() + getRotationQuaternion(assemblyAngleTheta, rotAxisTheta) * getRotationQuaternion(assemblyAnglePhi, rotAxisPhi) * disassemblyDefaultPath * t * pathLength;
}

Quaternion ArbitraryDirectionDissassemblyPath::getComponentOrientationAlongPathAt(double t) {
	return theComponent->getOrientation();
}

GLMesh* ArbitraryDirectionDissassemblyPath::getWorldCoordinatesSweptObjectMesh() {
	DynamicArray<P3D> vertList;
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 1);
	worldCoordinatesSweptObjectMesh.clear();
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);
	return &worldCoordinatesSweptObjectMesh;
}

//StelianQ: when are these copy functions invoked? Shouldn't it be sufficient to copy parameters?
void ArbitraryDirectionDissassemblyPath::copyObjectPropetriesInto(ParameterizedDisassemblyPath* path) {
	ParameterizedDisassemblyPath::copyObjectPropetriesInto(path);
	if (ArbitraryDirectionDissassemblyPath* tmpPath = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(path)) {
		tmpPath->assemblyAnglePhi = this->assemblyAnglePhi;
		tmpPath->assemblyAngleTheta = this->assemblyAngleTheta;
		tmpPath->pathLength = this->pathLength;
		tmpPath->rotAxisTheta = this->rotAxisTheta;
		tmpPath->rotAxisPhi = this->rotAxisPhi;
		tmpPath->disassemblyDefaultPath = this->disassemblyDefaultPath;
	}
}


void ArbitraryDirectionDissassemblyPath::addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects) {
	btSweptColShape = btConvexHullShape();

	DynamicArray<P3D> vertList;
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0.5);
	worldCoordinatesSweptObjectMesh.clear();
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);
	for (int i = 0; i < worldCoordinatesSweptObjectMesh.getVertexCount(); i++)
		btSweptColShape.addPoint(bulletVector(worldCoordinatesSweptObjectMesh.getVertex(i)));
	sweptConvexHullObjects.push_back(getBulletCollisionObjectFromWorldCoordsShape(theComponent, &btSweptColShape, &colObject));
}

//modifies parameterList by pushing its own parameters. 
void TwoPhaseArbitraryDirectionDissassemblyPath::pushParametersToList(DynamicArray<double>& parameterList) {
	parameterList.push_back(assemblyAngleTheta);
	parameterList.push_back(assemblyAnglePhi);
}
//reads parameters from the list starting at pIndex
void TwoPhaseArbitraryDirectionDissassemblyPath::readParametersFromList(DynamicArray<double>& parameterList, int& pIndex) {
	assemblyAngleTheta = parameterList[pIndex++];
	assemblyAnglePhi = parameterList[pIndex++];
}

P3D TwoPhaseArbitraryDirectionDissassemblyPath::getComponentPositionAlongPathAt(double t) {
	boundToRange(&t, 0, 1);

	if (t <= 0.5)
		return theComponent->getPosition() + theComponent->getWorldCoordinates(phase1LocalCoordsDirection) * (t / 0.5) * phase1PathLength;

	P3D startPos = theComponent->getPosition() + theComponent->getWorldCoordinates(phase1LocalCoordsDirection) * phase1PathLength;

	return startPos + getRotationQuaternion(assemblyAngleTheta, rotAxisTheta) * getRotationQuaternion(assemblyAnglePhi, rotAxisPhi) * disassemblyDefaultPath * ((t - 0.5) / 0.5) * pathLength;
}

Quaternion TwoPhaseArbitraryDirectionDissassemblyPath::getComponentOrientationAlongPathAt(double t) {
	return theComponent->getOrientation();
}

void TwoPhaseArbitraryDirectionDissassemblyPath::copyObjectPropetriesInto(ParameterizedDisassemblyPath* path) {
	ParameterizedDisassemblyPath::copyObjectPropetriesInto(path);
	if (TwoPhaseArbitraryDirectionDissassemblyPath* tmpPath = dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(path)) {
		tmpPath->assemblyAnglePhi = this->assemblyAnglePhi;
		tmpPath->assemblyAngleTheta = this->assemblyAngleTheta;
		tmpPath->pathLength = this->pathLength;
		tmpPath->rotAxisTheta = this->rotAxisTheta;
		tmpPath->rotAxisPhi = this->rotAxisPhi;
		tmpPath->disassemblyDefaultPath = this->disassemblyDefaultPath;
		tmpPath->phase1LocalCoordsDirection = this->phase1LocalCoordsDirection;
		tmpPath->phase1PathLength = this->phase1PathLength;
	}
}

GLMesh* TwoPhaseArbitraryDirectionDissassemblyPath::getWorldCoordinatesSweptObjectMesh() {
	DynamicArray<P3D> vertList;
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0.5);
	worldCoordinatesSweptObjectMesh.clear();
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);

	vertList.clear();
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0.5);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 1.0);
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);

	return &worldCoordinatesSweptObjectMesh;
}

void TwoPhaseArbitraryDirectionDissassemblyPath::addSweptConvexHullObjectsToList(DynamicArray<btCollisionObject*>& sweptConvexHullObjects) {
	btSweptColShapePhase1 = btConvexHullShape();
	btSweptColShapePhase2 = btConvexHullShape();

	DynamicArray<P3D> vertList;
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0.5);
	worldCoordinatesSweptObjectMesh.clear();
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);
	for (int i = 0; i < worldCoordinatesSweptObjectMesh.getVertexCount(); i++)
		btSweptColShapePhase1.addPoint(bulletVector(worldCoordinatesSweptObjectMesh.getVertex(i)));
	sweptConvexHullObjects.push_back(getBulletCollisionObjectFromWorldCoordsShape(theComponent, &btSweptColShapePhase1, &colObjectPhase1));

	vertList.clear();
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 0.5);
	pushComponentConvexShapeVertsAtTimeTToList(vertList, 1.0);
	worldCoordinatesSweptObjectMesh.clear();
	ConvexHull3D::computeConvexHullFromSetOfPoints(vertList, &worldCoordinatesSweptObjectMesh);
	for (int i = 0; i < worldCoordinatesSweptObjectMesh.getVertexCount(); i++)
		btSweptColShapePhase2.addPoint(bulletVector(worldCoordinatesSweptObjectMesh.getVertex(i)));
	sweptConvexHullObjects.push_back(getBulletCollisionObjectFromWorldCoordsShape(theComponent, &btSweptColShapePhase2, &colObjectPhase2));
}
