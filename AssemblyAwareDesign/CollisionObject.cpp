#include "CollisionObject.h"
#include <MathLib\Segment.h>

CollisionObject::CollisionObject() {

	a = 1000;//1000;//100 //10
	// need to change this value in wallObject too..
	epsilon = 0.001;// 0.001;//0.001;//0.005;//0.01; 0.002; 0;
}

double CollisionObject::computeValue(const dVector& p) {
	SoftUnilateralConstraint uc(0, a, epsilon);

	//we want the distance between the two objects along the normal (should we keep the normal fixed?) to be zero-ish (or perhaps maximized?)...
	V3D error = p_WorldOnO2 - p_WorldOnO1;
	double d = abs(error.dot(nWorld))*SGN(penetrationDepth);

	if (dynamic_cast<WallObject*>(obj1) || dynamic_cast<WallObject*>(obj2)) {
		WallObject* wall = dynamic_cast<WallObject*>(obj1);
		if(!wall)
			wall = dynamic_cast<WallObject*>(obj2);
		if (penetrationDepth < 0)
			d = d - wall->epsilon;
		else
			d = d + wall->epsilon;
	}

	double errorVal = uc.computeValue(d);

	description = "CollisionObjective " + obj1->name + " " + obj2->name + ", dist: " + std::to_string(d);
	//Logger::consolePrint("pen dist:%10.10lf, error dist: %10.10lf ", penetrationDepth, error.dot(nWorld));
	//Logger::consolePrint("errorVal:%10.10lf\n", errorVal);

	return errorVal;

	//double errorVal = error.dot(nWorld);
	//return 0.5*errorVal*errorVal;
}

