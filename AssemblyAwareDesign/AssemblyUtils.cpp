#include "AssemblyUtils.h"
#include <Utils/Utils.h>

KeyWord AssemblyKeywords[] = {
	{"Chassis", CHASSIS_NAME},
	{"ChassisScale", CHASSIS_SCALE},
	{ "Object", OBJ_OBJ },
	{ "/ObjEnd", OBJ_END_OBJ },
	{ "obj name", OBJ_NAME },
	{ "obj disassem index", DISASS_IND },
	{ "obj assem params", ASSEM_PARAMS },
	{ "obj position", OBJ_POSITION },
	{ "obj orientation", OBJ_ORIENTATION },
	{ "obj support", OBJ_SUPPORT },
	{ "obj frozen", OBJ_IS_FROZEN },
	{ "obj allowedPos", OBJ_ALLOW_POS },
	{ "obj allowedRot", OBJ_ALLOW_ROT },
	{ "obj additionalRot", OBJ_ADDITIONALROTATION},
	{ "obj dimensions", OBJ_DIMS}
};

// determine the type of a line that was used in the input file for an assembly
int getASLineType(char* &buffer) {
	return getLineType(buffer, AssemblyKeywords, sizeof(AssemblyKeywords) / sizeof(AssemblyKeywords[0]));
}

// returns the string associated with the given token
char* getASString(int token) {
	return getKeyword(token, AssemblyKeywords, sizeof(AssemblyKeywords) / sizeof(AssemblyKeywords[0]));
}

btVector3 bulletVector(const V3D& v) {
	return btVector3((btScalar)v[0], (btScalar)v[1], (btScalar)v[2]);
}

btVector3 bulletVector(const P3D& p) {
	return btVector3((btScalar)p[0], (btScalar)p[1], (btScalar)p[2]);
}

btQuaternion bulletQuaternion(const Quaternion& q) {
	V3D axis = q.v;
	return btQuaternion(btScalar(axis[0]), btScalar(axis[1]), btScalar(axis[2]), btScalar(q.s));
}

P3D getP3D(const btVector3& v) {
	return P3D(v[0], v[1], v[2]);
}

V3D getV3D(const btVector3& v) {
	return V3D(v[0], v[1], v[2]);
}

Quaternion getQuaternionFromBullet(const btQuaternion& q) {
	return Quaternion(q[3], q[0], q[1], q[2]);
}

void saveLogDataToTxtFile(std::string saveFileName, std::string saveFilePath) {
	std::string filePath = "../out/";

	// go to the right folder
	std::string systemCall1 = "cd " + filePath;
	system(systemCall1.c_str());
	//copy log file into a file of given name in this folder.
	std::string systemCall = "copy log.txt " + saveFilePath + saveFileName + ".txt";

	std::string combinedCall = systemCall1 + " && " + systemCall;
	system(combinedCall.c_str());
}