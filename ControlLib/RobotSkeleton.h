
#pragma once


#include <Utils\Utils.h>
#include <GUILib\GLUtils.h>

#include <RBSimLib\RigidBody.h>
#include <RBSimLib\BallAndSocketJoint.h>

#include <ControlLib\Robot.h>
#include <ControlLib\GeneralizedCoordinatesRobotRepresentation.h>

#include <FBXReaderLib\Skeleton.hpp>
#include <FBXReaderLib\IKSkeleton.hpp>

class RobotSkeleton
{

	P3D m_xRoot0;

	static void UpdateChildren		(RigidBody* _pChildRigidBody, Skeleton::Joint* _pParentJoint);
	static void PopulateChildren	(RigidBody* _pChild, Skeleton::Joint* _pJoint, int& _id);

	Skeleton::Joint* RobotSkeleton::FindRigidBodyParentJoint(RigidBody* _pRB);
public:
	Skeleton	m_sk;
	IKSkeleton*	m_pIKSolver;

	RobotSkeleton(Robot* _pRobot);
	void UpdateStateFromRobot(Robot* _pRobot);

	int NumNonRootJoints();
	int NumDOFs();
		
	void		computeOverallMassMatrix	(RigidBody* root, MatrixNxM &massMatrix);

	///< Computes the mass matrix for rigidbody rb
	void		computeMassMatrix			(RigidBody* rb, MatrixNxM &massMatrix);
	MatrixNxM		getWorldMOI					(RigidBody* _pRB);
	V3D			computeRigidBodyDimensions	(RigidBody* _pRB);

	P3D			getWorldCoordinatesFor		(const P3D& p, RigidBody* rb);
	Quaternion	getOrientationFor			(RigidBody* rb);

	void		compute_dpdq				(const P3D& p, RigidBody* rb, MatrixNxM &dpdq);
	void		estimate_angular_jacobian	(RigidBody* rb, MatrixNxM &dRdq);
	void		compute_angular_jacobian	(RigidBody* rb, MatrixNxM &dRdq);
	


};
