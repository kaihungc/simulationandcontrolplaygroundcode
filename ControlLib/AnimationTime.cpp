
#include <ControlLib/AnimationTime.h>


IndexRest GetIndexRest(const double _t, const int _size)
{
	IndexRest ir(_t, _size);
	if (ir.m_index == _size)
	{
		ir.m_index -= 1;
		ir.m_rest = 1.0f;
	}

	return ir;
	
}
