#include "SkeletonSymmetryObjective.h"

SkeletonSymmetryObjective::SkeletonSymmetryObjective(GeneralizedCoordinatesRobotRepresentation* _gcRobot, map<int, int>* _symmMap, bool optimizeRootConfiguration, const std::string& objectiveDescription, double weight){
	this->description = objectiveDescription;
	this->weight = weight;
	this->optimizeRootConfiguration = optimizeRootConfiguration;
	symmMap = _symmMap;
	gcRobot = _gcRobot;
}

SkeletonSymmetryObjective::~SkeletonSymmetryObjective(void){
}

double SkeletonSymmetryObjective::computeValue(const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
//	IKPlan->setParametersFromList(s);

	double retVal = 0;
	
	for (auto itr = symmMap->begin(); itr != symmMap->end(); itr++)
	{
		int joint1 = itr->first;
		int joint2 = itr->second;
		if (joint1 >= joint2) continue;

		// constraint: ang(j1) = -ang(j2), j1 and j2 are symmetric.
		double diff = p[gcRobot->getQIndexForJoint(joint1)] + p[gcRobot->getQIndexForJoint(joint2)];
		retVal += diff * diff;
	}

	// constraint: x = 0
	retVal += p[0] * p[0];

	// constraints on orientation.
	retVal += p[3] * p[3];
	retVal += p[4] * p[4];

	return 0.5 * retVal * weight;
}

void SkeletonSymmetryObjective::addGradientTo(dVector& grad, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	for (auto itr = symmMap->begin(); itr != symmMap->end(); itr++){
		int joint1 = itr->first;
		int joint2 = itr->second;
		if (joint1 >= joint2) continue;
		int pIndex1 = gcRobot->getQIndexForJoint(joint1);
		int pIndex2 = gcRobot->getQIndexForJoint(joint2);

		double diff = p[pIndex1] + p[pIndex2];
		if (optimizeRootConfiguration){
			grad[pIndex1] += diff * weight;
			grad[pIndex2] += diff * weight;
		}else{
			if (pIndex1>=6) grad[pIndex1-6] += diff * weight;
			if (pIndex2>=6) grad[pIndex2-6] += diff * weight;
		}
	}
	if (optimizeRootConfiguration){
		grad[0] += p[0] * weight;
		grad[3] += p[3] * weight;
		grad[4] += p[4] * weight;
	}
}

void SkeletonSymmetryObjective::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	for (auto itr = symmMap->begin(); itr != symmMap->end(); itr++){
		int joint1 = itr->first;
		int joint2 = itr->second;
		if (joint1 >= joint2) continue;
		int pIndex1 = gcRobot->getQIndexForJoint(joint1);
		int pIndex2 = gcRobot->getQIndexForJoint(joint2);

		double diff = p[pIndex1] + p[pIndex2];
		if (optimizeRootConfiguration) {
			ADD_HES_ELEMENT(hessianEntries, pIndex1, pIndex1, 1, weight);
			ADD_HES_ELEMENT(hessianEntries, pIndex1, pIndex2, 1, weight);
			ADD_HES_ELEMENT(hessianEntries, pIndex2, pIndex2, 1, weight);
		}
		else {
			if (pIndex1 >= 6) ADD_HES_ELEMENT(hessianEntries, pIndex1 - 6, pIndex1 - 6, 1, weight);
			if (pIndex1 >= 6 && pIndex2 <= 6) ADD_HES_ELEMENT(hessianEntries, pIndex1 - 6, pIndex2 - 6, 1, weight);
			if (pIndex2 >= 6) ADD_HES_ELEMENT(hessianEntries, pIndex2 - 6, pIndex2 - 6, 1, weight);
		}
	}
	if (optimizeRootConfiguration) {
		ADD_HES_ELEMENT(hessianEntries, 0, 0, 1, weight);
		ADD_HES_ELEMENT(hessianEntries, 3, 3, 1, weight);
		ADD_HES_ELEMENT(hessianEntries, 4, 4, 1, weight);
	}
}



