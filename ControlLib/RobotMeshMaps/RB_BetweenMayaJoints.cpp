

#include <ControlLib\RobotMesh.h>
#include <ControlLib\Robot.h>
#include <FBXReaderLib\Skeleton.hpp>


void RBSMesh_RB_between_MayaJoints::initialise()
{
	ASSERT(m_pMesh != NULL && m_pRobot != NULL && m_pSkeleton != NULL, "All need to be set !");

	double inverseToBindScale = 1.0 / m_pSkeleton->getScaleDanger();
	m_scale = inverseToBindScale;
	SkinnedMesh::TransformsContainer transforms = getTransformationsFromRobotUsingSkeleton(m_pRobot, m_pSkeleton);
	m_scale = 1.0;

	m_pMesh->setCPUSkinning(true);

	for (uint i = 0; i < transforms.size(); ++i)
		m_pMesh->getBindMatrices()[i] = transforms[i].inverse();

	m_pMesh->updateSkinningMatrices(transforms);
}

///<
SkinnedMesh::TransformsContainer RBSMesh_RB_between_MayaJoints::getTransformationsFromRobotUsingSkeleton(Robot* _pRobot, Skeleton* _pSkeleton)
{
	SkinnedMesh::TransformsContainer transforms;
	transforms.resize(_pSkeleton->numJoints());

	for (int i = 0; i < (int)transforms.size(); ++i)
	{
		Skeleton::Joint* joint = _pSkeleton->FindJoint(i);
		int rbId = -1;
		if (i == 0)
		{
			rbId = 0;
		}
		else
		{
			int fbxId = joint->m_id;
			if (joint->m_childs.size() > 0)
			{
				fbxId = joint->m_childs[0].m_id;				
			}
				
			
			/// find rigidbody id from fbxBoneId
			for (int j = 0; j < (int)_pRobot->getBodies().size(); ++j)
				if (_pRobot->getBodies()[j]->fbxBoneId == fbxId) {
					rbId = j;
					break;
				}							

			ASSERT(rbId != -1, "Something wrong with robot skinning");

		}

		
		V3D rbPos = _pRobot->getBodies()[rbId]->getCMPosition();
		if (_pRobot->getBodies()[rbId]->pJoints.size() == 0)
		{
			// This is root... just take the rb position for now (see above)...
			
		}
		else
		{
			if (joint->m_childs.size() > 0)
			{
				///< this is joint
				P3D xJoint = _pRobot->getBodies()[rbId]->pJoints[0]->getWorldPosition();
				rbPos = xJoint;
			}
			else
			{
				/// This is tip:
				P3D xJoint = _pRobot->getBodies()[rbId]->pJoints[0]->getWorldPosition();
				P3D xRBMiddle = _pRobot->getBodies()[rbId]->getCMPosition();

				V3D dx = -1.0*(xJoint - xRBMiddle);
				P3D tipPos = xRBMiddle + dx;

				rbPos = tipPos;
			}
			
		}

		

		/// 
		Quaternion rbOrient = _pRobot->getBodies()[rbId]->getOrientation();
		Matrix3x3 r = rbOrient.getRotationMatrix();
		
		Matrix4x4& m = transforms[i];
		m.setIdentity();

		Matrix4x4 mS;
		mS.setIdentity();

	//	double scaleDanger = _pSkeleton->getScale();

		for (int i = 0; i < 3; ++i)
			mS(i, i) = m_scale;

		for (int k = 0; k < 3; ++k)
			for (int l = 0; l < 3; ++l)
				m(k, l) = (double)r(k, l);


		m = mS*m;

		for (int k = 0; k < 3; ++k)
			m(k, 3) = (double)rbPos[k]* m_scale;

		
	}

	return transforms;

}



