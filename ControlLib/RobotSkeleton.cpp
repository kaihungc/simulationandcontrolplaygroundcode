

#include <ControlLib\RobotSkeleton.h>

int RobotSkeleton::NumNonRootJoints()
{
	return (m_sk.Size()-1); /// num rotations = num joints -1;
}

int RobotSkeleton::NumDOFs()
{
	///< times 3.

	///< Root
	int numDOFs = 2;
	numDOFs += NumNonRootJoints(); ///Non-root joints
	return numDOFs * 3;
}

///<
void RobotSkeleton::computeOverallMassMatrix(RigidBody* root, MatrixNxM &massMatrix)
{
	int numDOFs = NumDOFs();
	if (root->pJoints.size() == 0 || root->pJoints[0]->parent == NULL)
	{
		resize(massMatrix, numDOFs, numDOFs);
	}
	MatrixNxM curMassMatrix;
	computeMassMatrix(root, curMassMatrix);
	massMatrix += curMassMatrix;

	for (uint i = 0; i < root->cJoints.size(); ++i)
		if (root->cJoints[i]->child != NULL)
			computeOverallMassMatrix(root->cJoints[i]->child, massMatrix);
}

///<
V3D RobotSkeleton::computeRigidBodyDimensions(RigidBody* _pRB)
{
	Matrix3x3 A;

	A(0, 0) = 0; A(0, 1) = 1; A(0, 2) = 1;
	A(1, 0) = 1; A(1, 1) = 0; A(1, 2) = 1;
	A(2, 0) = 1; A(2, 1) = 1; A(2, 2) = 0;

	
	MatrixNxM M(3,1);
	double invM = 1.0;
	if (_pRB->rbProperties.mass > 0)
		invM = 1.0 / _pRB->rbProperties.mass;

	for (int k = 0; k < 3; ++k)
		M(k, 0) = _pRB->rbProperties.MOI_local(k, k)*12.0 * invM;

	MatrixNxM sqrDeltas = A.inverse()*M;

	V3D result;
	for (int k = 0; k < 3; ++k)
		result[k] = sqrt(sqrDeltas(k,0));

	return result*0.5;
}




///<
void RobotSkeleton::UpdateChildren(RigidBody* _pChildRigidBody, Skeleton::Joint* _pParentJoint)
{
	if (_pChildRigidBody)
	{
		for (int i = 0; i <(int)_pParentJoint->m_childs.size(); ++i)
		{
			Skeleton::Joint* pJoint = &_pParentJoint->m_childs[i];
			Joint* pODEJoint = _pChildRigidBody->cJoints[i];
			pJoint->m_q = pODEJoint->computeRelativeOrientation();

			UpdateChildren(pODEJoint->child, pJoint);
		}
	}
}

///<
void RobotSkeleton::PopulateChildren(RigidBody* _pChildRigidBody, Skeleton::Joint* _pParentJoint, int& _id)
{
	if (_pChildRigidBody)
	{
		///< Set rigid body name, which is always the child of a joint.
		_pParentJoint->m_pChildRigidBodyName = new std::string(_pChildRigidBody->name);
		_pParentJoint->m_childs.resize(_pChildRigidBody->cJoints.size());
		for (int i = 0; i < (int)_pParentJoint->m_childs.size(); ++i)
		{
			Skeleton::Joint* pJoint = &_pParentJoint->m_childs[i];
			pJoint->m_pPrevious = _pParentJoint;

			Joint* pODEJoint = _pChildRigidBody->cJoints[i];

			pJoint->m_q = pODEJoint->computeRelativeOrientation();

			///< Hack: assume only one parent.
			if (_pChildRigidBody->pJoints.size()>0)
				pJoint->m_offset = pODEJoint->pJPos + _pChildRigidBody->pJoints[0]->cJPos*(-1.0);
			else
				pJoint->m_offset = pODEJoint->pJPos;

			_id++;
			pJoint->m_id = _id;
			pJoint->m_strName = pODEJoint->name;

			PopulateChildren(pODEJoint->child, pJoint, _id);
		}
	}
}

///<
RobotSkeleton::RobotSkeleton(Robot* _pRobot) : m_pIKSolver(NULL)
{
	m_sk.m_scaleDanger = 1.0;
	m_sk.m_rootVisDebugOffset = V3D(0, 0, 0);
	m_sk.m_rootVisDebugRot = Quaternion();

	///< Acquire same structure as robot.
	m_sk.m_pRoot = new Skeleton::Joint();
	m_sk.m_pRoot->m_strName = "Root"; 
	m_sk.m_pRoot->m_pPrevious = NULL;

	//V3D deltas = computeRigidBodyDimensions(_pRobot->getRoot());

	//m_sk.m_pRoot->m_q = _pRobot->getRoot()->state.orientation;
	m_sk.m_pRoot->m_offset = _pRobot->getRoot()->state.position;

	m_sk.m_pRoot->m_id = 0;
	int id = 0;
	PopulateChildren(_pRobot->getRoot(), m_sk.m_pRoot, id);


	///< Create an IK Solver;
	m_pIKSolver = new IKSkeleton(m_sk);
}

///<
void RobotSkeleton::UpdateStateFromRobot(Robot* _pRobot)
{
	m_sk.m_pRoot->m_q = _pRobot->getRoot()->state.orientation;
	m_sk.m_pRoot->m_offset = _pRobot->getRoot()->state.position;

	UpdateChildren(_pRobot->getRoot(), m_sk.m_pRoot);

}

///<
Skeleton::Joint* RobotSkeleton::FindRigidBodyParentJoint(RigidBody* _pRB)
{
	return m_sk.FindRigidBodyParentJoint(_pRB->name);
}

MatrixNxM RobotSkeleton::getWorldMOI(RigidBody* _pRB)
{
	MatrixNxM moi_c;
	Skeleton::Joint* pJParentJoint = FindRigidBodyParentJoint(_pRB);

	assert(pJParentJoint!=NULL);
	if (pJParentJoint)
	{
		Matrix3x3 R = m_sk.Q(pJParentJoint).getRotationMatrix();
		moi_c = R * _pRB->rbProperties.MOI_local * R.transpose();
	}

	return moi_c;
}

//computes the mass matrix for rigidbody rb
void RobotSkeleton::computeMassMatrix(RigidBody* rb, MatrixNxM &massMatrix)
{
	/*
	M = J'McJ
	Mc = [m 0 0  0
	0 m 0  0
	0 0 m  0
	0 0 0 MoI]
	*/

	int numDofs = NumDOFs();

	resize(massMatrix, numDofs, numDofs);
	MatrixNxM Mc, dRdq, dpdq, J;

	resize(Mc, 6, 6);
	Mc.setZero();
	Mc(0, 0) = Mc(1, 1) = Mc(2, 2) = rb->rbProperties.mass;

	MatrixNxM MoI = getWorldMOI(rb);

	for (int i = 3; i < 6; ++i)
		for (int j = 3; j < 6; ++j)
			Mc(i, j) = MoI(i - 3, j - 3);

	compute_dpdq(P3D(0, 0, 0), rb, dpdq);
	estimate_angular_jacobian(rb, dRdq);

	// compute_angular_jacobian(rb, dRdq);
	//print("angularJacobian", dRdq);

	resize(J, 6, numDofs);
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < numDofs; ++j)
		{
			J(i, j) = dpdq(i, j);
			J(i + 3, j) = dRdq(i, j);
		}

	/*Note: RTQL8 computes
	mM.noalias() = mMass*mJv.transpose()*mJv;
	mM.noalias() += mJw.transpose()*mIc*mJw;
	*/
	massMatrix = J.transpose() * Mc * J;

	//  print("J", J);
	//  print("Mc", MoI);
}

///<
Quaternion RobotSkeleton::getOrientationFor(RigidBody* _pRB)
{
	Skeleton::Joint* pJoint = FindRigidBodyParentJoint(_pRB);
	return m_sk.Q(pJoint);
}

P3D RobotSkeleton::getWorldCoordinatesFor(const P3D& p, RigidBody* _pRB)
{	
	Skeleton::Joint* pJoint = FindRigidBodyParentJoint(_pRB);
	int id = pJoint->m_id;
	V3D dx = m_sk.X(id + 1) - m_sk.X(id);
	P3D x = m_sk.X(id) + dx * 0.5;

	return x + m_sk.Q(id).rotate(p);
}

///< actually estimate for now...
void RobotSkeleton::compute_dpdq(const P3D& p, RigidBody* rb, MatrixNxM &dpdq)
{
	int numDofs = NumDOFs();
	resize(dpdq, 3, numDofs);
	
	///< root:
	V3D x0 = m_sk.m_pRoot->m_offset;
	P3D p_0 = getWorldCoordinatesFor(p, rb);
	double h = 0.0001;

	for (int i = 0; i < 3; i++)
	{
		m_sk.m_pRoot->m_offset[i] += h;
		P3D p_p = getWorldCoordinatesFor(p, rb);

		V3D dpdq_i = (p_p - p_0) / h;
		dpdq(0, i) = dpdq_i[0];
		dpdq(1, i) = dpdq_i[1];
		dpdq(2, i) = dpdq_i[2];

		m_sk.m_pRoot->m_offset = x0;
	}

	///< rotations.
	int numRots = NumNonRootJoints();
	for (int i = 0; i<numRots; ++i)
	{
		Skeleton::Joint* pJoint = m_sk.FindJoint(i);
		Quaternion q0 = pJoint->m_q;

	
		for (int k = 0; k < 3; ++k)
		{
			V3D r = q0.toAxisAngle();
			r[k] += h;
			pJoint->m_q = Quaternion::ExpMap(r);

			P3D p_p = getWorldCoordinatesFor(p, rb);

			V3D dpdq_i = (p_p - p_0) / h;
			dpdq(0, 3 + i * 3 + k) = dpdq_i[0];
			dpdq(1, 3 + i * 3 + k) = dpdq_i[1];
			dpdq(2, 3 + i * 3 + k) = dpdq_i[2];

			pJoint->m_q = q0;
		}
	}
}


void RobotSkeleton::compute_angular_jacobian(RigidBody* _pRB, MatrixNxM& _dRdq)
{
	estimate_angular_jacobian(_pRB, _dRdq);
}

void RobotSkeleton::estimate_angular_jacobian(RigidBody* rb, MatrixNxM &dRdq)
{
	int numDofs = NumDOFs();
	resize(dRdq, 3, numDofs);
	dRdq.fill(0);

	Skeleton::Joint* pParentJoint = FindRigidBodyParentJoint(rb);
	assert(pParentJoint != NULL);
	Quaternion Q0 = m_sk.Q(pParentJoint);

	double h = 0.0001;
	int numRots = NumNonRootJoints();
	///For each rotation joint.
	for (int i = 1; i < numRots+1; ++i)
	{
		Skeleton::Joint* pJoint = m_sk.FindJoint(i);
		Quaternion q0 = pJoint->m_q;
		
		for (int k = 0; k < 3; ++k)
		{
			V3D r(0, 0, 0);
			r[k] = h;
			Quaternion dq = Quaternion::ExpMap(r);
			pJoint->m_q = dq*q0;

			Quaternion Q_p = m_sk.Q(pParentJoint);
			Quaternion Q_dh = Q_p*Q0.getInverse();

			V3D axis;
			double angle;
			Q_dh.getAxisAngle(axis, angle);
			axis.normalize();
			axis *= angle;

			V3D dRdq_ki = axis / h;
			dRdq(0, 3 + i * 3 + k) = dRdq_ki[0];
			dRdq(1, 3 + i * 3 + k) = dRdq_ki[1];
			dRdq(2, 3 + i * 3 + k) = dRdq_ki[2];

			pJoint->m_q = q0;
		}
	}
}
