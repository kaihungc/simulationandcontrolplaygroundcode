#pragma once

#include <GUILib/GLApplication.h>

#include <MathLib/MathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/mathLib.h>

// to set the correct rotation axes
#include <RBSimLib/HingeJoint.h>
#include <RBSimLib/UniversalJoint.h>
#include <RBSimLib/BallAndSocketJoint.h>

#include <MathLib/Trajectory.h>
#include <ControlLib\AnimationTime.h>


class Robot;
class ReducedRobotState;
class LinearSubspace;

///< This class implements key framed robot reduced states.
class AnimatedRobot
{

	Robot*								m_pRobot			= NULL;
	
	///< Used for the get interpolated frame function (to return);
	ReducedRobotState*					m_pTempState		= NULL;

	/// Extract a linear subspace from the key frames.
	LinearSubspace*						m_pLinearSubspace	= NULL;

	enum RootUpdatePolicy
	{
		INTERPOLATE_ROOT=0,
		INTEGRATE_ROOT,
		N
	};

	RootUpdatePolicy m_rootUpdatePolicy = INTERPOLATE_ROOT;

	MatrixNxM		m_tempReducedM;
	MatrixNxM		m_tempFullSpace;

	dVector			vectorize					(ReducedRobotState* _pRBS);

protected:

	int									m_timer				= 0;
	int									m_maxTicks			= 200;

	int									m_framesPerSecond	= 120;

	int									m_frameIndex		= 0;
	int									m_frameIndexStep	= 1;



	std::string							m_pathName;

	void								getInterpolatedFrameImpl(const double _t, ReducedRobotState* _animatedFrame, bool _bLoopEndWithFirstFrame = true, bool _bInterpolateRoot=false);

	AnimationTimer						m_animationTimer;
//	DiscreteAnimationTimer				m_discreteTimer;

	double getCurrentTime() { return 0.0; }
public:


	std::vector<ReducedRobotState*>		m_keyFrames;


	Robot*				getRobot() { return m_pRobot; }

	AnimatedRobot(Robot* _pRobot, TwBar* _pMenu = NULL);
	~AnimatedRobot();

	/*
		Create a set of key frames.
	*/
	void				createKeyFrames				(const int _numKeyFrames);
	void				createSubsetOfKeyFrames		(int _minIndex, int _maxIndex);

	void				releaseKeyFrames			();

	void				setPathName					(const std::string& _pathName);
	void				saveKeyFrames				();
	void				loadKeyFrames				(bool _bCopyFirstAsLast = false);

	int					numKeyFrames				() { return m_keyFrames.size(); }
	
	//<
	void				updateCurrentFrameIndex		();
	int					getCurrentFrameIndex		() { return m_frameIndex; }

	//<
	void				translate					(const V3D& _x);
	void				fixDistanceToGround			();
private:
	///< Note used:
	void				alignAnimatedPoseWithGround	(ReducedRobotState* pAnimatedFrame);
public:


	/*
		* Maybe this should be a child class: a "TimedAnimatedRobot"?
	*/
	AnimationTimer&		getTimer() { return m_animationTimer; }
	int					getCurrentTimeIndex();
	ReducedRobotState*	getCurrentFrame();
	ReducedRobotState*  getCurrentInterpolatedFrame();


	/*
		Access
	*/
	ReducedRobotState*	getFrameAtIndex(int _i);
	ReducedRobotState*	getFirstFrame				();

	ReducedRobotState*  getInterpolatedFrame					(const double _t);
	void				getInterpolatedFrame					(const double _t, ReducedRobotState* _pAnimatedFrame);

	void				getInterpolatedFrameIntegrateRoot		(const double _t, int _numCycles, ReducedRobotState* _pAnimatedFrame);

	V3D					getInterpolatedVelocity					(double _t0, double _t1);
	P3D					getInterpolatedRootPosition				(double _t);


	/*
	* Closed loop: Linearly interpolate frame at time t in [0, 1], assuming the last and first frames are looping.
	*/
	void				getInterpolatedFrameLoopEndWithFirst(const double _t, ReducedRobotState* _pAnimatedFrame);


	/*
		Draw the Key Frames
	*/
	void				drawKeyFrames							(int flags, V3D* _pOffset=NULL);
	static void			drawRobotSkeleton						(Robot* m_pRobot, int flags);
	static void			drawState								(Robot* m_pRobot, ReducedRobotState* _pState, int _flags);

	/*
		* Linear subspace:
	*/
	void				createLinearSubspace				(int _dimOfSubSpace);
	dVector				reduceStateWithLinearProjection		(ReducedRobotState* _pRBS);
	dVector				reconstructSubLinearPose			(const dVector& _subSpaceX);
	int					getLinearSubSpaceDim				() const;




};


///<
class TimelineDraw
{
public:

	TimelineDraw(AnimatedRobot* _pRobotKeyFrames, Trajectory1D* _pTimeWarp, const double _t);

};

