#pragma once
#include <vector>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>

class ArticulatedCharacter;
class RBState;

/**
	This class represents the state of an articulated character.
	It consists of the state of each rigid body.
*/
class CharacterState {
private:
	/**
		Store the whole rigid body state as it is used in ODE.
		No transformations included -> getting and setting state is consistent.
	*/
	std::vector<RBState> m_rigidBodyStates;

    /**
        The underlying character (used to retrieve indices of rigid bodies and joint for computing certain values like relative orientations).
    */
    const ArticulatedCharacter* m_character = NULL;

	/**
		The precision for writing the state to file.
	*/
	int PRECISION = 25;
	
public:

	CharacterState(const ArticulatedCharacter* character);
	~CharacterState();

	void populateState(const ArticulatedCharacter* _character);

	int getJointCount();

	// ------------------------------------------------------------------------------------------------
	// get state values

	/**
		Get the position of the root in world coordinates.
	*/
	P3D getRootPosition() const;
    
	/**
		Get the orientation of the root in world coordinates.
	*/
	Quaternion getRootOrientation() const;

	/**
		Get the linear velocity of the root in world coordinates.
	*/
	V3D getRootVelocity() const;

	/**
		Get the angular velocity of the root in world coordinates.
	*/
	V3D getRootAngularVelocity() const;

	/**
		Get the position of a rigid body in world coordinates.
	*/
	P3D getRigidBodyPosition(int index) const;

	/**
		Get the orientation of a rigid body in world coordinates.
	*/
	Quaternion getRigidBodyOrientation(int index) const;

	/**
		Get the linear velocity of a rigid body in world coordinates.
	*/
	V3D getRigidBodyVelocity(int index) const;

	/**
		Get the angular velocity of a rigid body in world coordinates.
	*/
	V3D getRigidBodyAngularVelocity(int index) const;

	/**
		Get the complete rigid body state for the given index.
	*/
	RBState getRigidBodyState(int index) const;
	    
	// ------------------------------------------------------------------------------------------------
    // compute values depending on state

    /**
        Computes the joint relative orientation of the given joint.
    */
    Quaternion getJointRelativeOrientation(int index) const;

    /**
        Computes the joint relative angular velocity of the given joint (in parent coordinates).
    */
    V3D getJointRelativeAngVelocity(int index) const;
	
	// ------------------------------------------------------------------------------------------------
	// reading/writing from/to file

	/**
		Write the character state to file.
		Also writes the names of the rigid bodies if a character is provided.
	*/
	void writeToFile(const std::string& fileName, ArticulatedCharacter* character = NULL);

	/**
		Read the character state from file
	*/
	void readFromFile(const std::string fileName);
};