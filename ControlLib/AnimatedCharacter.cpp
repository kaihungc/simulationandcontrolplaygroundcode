
#include <ControlLib/AnimatedCharacter.h>
#include <ControlLib/ArticulatedCharacter.h>
#include <ControlLib/CharacterPose.h>
#include <ControlLib/CharacterState.h>
#include <MathLib/LinearSubspace.h>

///< IO
#include <sstream>
#include <fstream>


void TW_CALL SaveKeyFrames_CB_AnimatedCharacter(void* _pData) {
	((AnimatedCharacter*)_pData)->saveKeyFrames();
}

void TW_CALL LoadKeyFrames_CB_AnimatedCharacter(void* _pData)
{
	((AnimatedCharacter*)_pData)->loadKeyFrames();
}

///<
CharacterPose* AnimatedCharacter::getFirstFrame()
{
	CharacterPose* state = getFrameAtIndex(0);
	
	return state;
}

int AnimatedCharacter::getCurrentTimeIndex()
{
	double t = getTimer().getPhase();

	int index = (int)(t*(double)numKeyFrames());
	index = MIN(index, numKeyFrames() - 1);

	return index;
}

CharacterPose* AnimatedCharacter::getCurrentFrame()
{
	return getFrameAtIndex(getCurrentTimeIndex());
}

///<
CharacterPose* AnimatedCharacter::getFrameAtIndex(int _i)
{
	assert(_i >= 0 && _i < (int)m_keyFrames.size());
	return m_keyFrames[_i];
}

void AnimatedCharacter::translate(const V3D& _x)
{
	for (int i = 0; i < numKeyFrames(); ++i)
		m_keyFrames[i]->setRootPosition(m_keyFrames[i]->getRootPosition() + _x);
}

void AnimatedCharacter::updateCurrentFrameIndex()
{
	getTimer().update();
}

void AnimatedCharacter::getInterpolatedFrameLoopEndWithFirst(const double _t, CharacterPose* _pAnimatedFrame)
{
	getInterpolatedFrameImpl(_t, _pAnimatedFrame, true, false);
}

void AnimatedCharacter::getInterpolatedFrameIntegrateRoot(const double _t, int _numCycles, CharacterPose* _pAnimatedFrame)
{
	getInterpolatedFrame(_t, _pAnimatedFrame);
	P3D xRoot = _pAnimatedFrame->getRootPosition();

	///< Integrate root.		
	V3D dx = getInterpolatedRootPosition(1.0) - getInterpolatedRootPosition(0.0);
	for (int i = 0; i<_numCycles; ++i)
		xRoot += dx;

	_pAnimatedFrame->setRootPosition(xRoot);

}

P3D AnimatedCharacter::getInterpolatedRootPosition(double _t)
{
	int numKeyFrames = m_keyFrames.size();

	IndexRest ir = GetIndexRest(_t, numKeyFrames - 1);

	int lowId = ir.m_index;
	int highId = ir.m_index + 1;
	double localT = ir.m_rest;

	///< Update root position?
	P3D x0 = m_keyFrames[lowId]->getRootPosition();
	P3D x1 = m_keyFrames[highId]->getRootPosition();

	P3D x = x0*(1.0 - localT) + x1*localT;

	return x;
}

V3D AnimatedCharacter::getInterpolatedVelocity(double _t0, double _t1)
{
	return getInterpolatedRootPosition(_t1) - getInterpolatedRootPosition(_t0);
}

CharacterPose* AnimatedCharacter::getInterpolatedFrame(const double _t)
{
	if (!m_pTempState)
		m_pTempState = new CharacterPose(m_pCharacter);

	getInterpolatedFrameImpl(_t, m_pTempState, false, true);

	return m_pTempState;

}

void AnimatedCharacter::getInterpolatedFrame(const double _t, CharacterPose* _pAnimatedFrame)
{
	getInterpolatedFrameImpl(_t, _pAnimatedFrame, false, true);
}

CharacterPose* AnimatedCharacter::getCurrentInterpolatedFrame()
{
	double t = getTimer().getPhase();
	return getInterpolatedFrame(t);
}

///<
void AnimatedCharacter::getInterpolatedFrameImpl(const double _t, CharacterPose* _pAnimatedFrame, bool _bLoopEndWithFirstFrame, bool _bInterpolateRoot)
{
	// get id's of the neighbour keyframes for the given time t (wrap around from last to first for the last segment)
	int numKeyFrames = m_keyFrames.size();

	IndexRest ir = GetIndexRest(_t, numKeyFrames-1);

	int lowId = ir.m_index;
	int highId = ir.m_index + 1;
	double localT = ir.m_rest;

	if (_bLoopEndWithFirstFrame)
	{
		lowId = (int)std::floor(_t * (numKeyFrames - 1));
		highId = lowId + 1 < numKeyFrames ? lowId + 1 : 0;
		// compute the interpolation weight between these two frames
		double intervalSize = 1.0 / numKeyFrames;
		double lowerBound = intervalSize * lowId;
		localT = (_t - lowerBound) / intervalSize;
	}
	
	std::vector<Joint*>& jointList = m_pCharacter->getJoints();
	for (uint j = 0; j < jointList.size(); j++)
	{
		int jIndex = jointList[j]->jIndex;

		Quaternion lowQ = m_keyFrames[lowId]->getJointRelativeOrientation(jIndex);
		Quaternion highQ = m_keyFrames[highId]->getJointRelativeOrientation(jIndex);


		Quaternion finalQ = lowQ.sphericallyInterpolateWith(highQ, localT);
		//Quaternion finalQ = lowQ.linearlyInterpolateWith(highQ, s);

		_pAnimatedFrame->setJointRelativeOrientation(finalQ, jIndex);
		
	}

	Quaternion lowQ_root = m_keyFrames[lowId]->getRootOrientation();
	Quaternion highQ_root = m_keyFrames[highId]->getRootOrientation();
	Quaternion finalQ_root = lowQ_root.sphericallyInterpolateWith(highQ_root, localT);

	_pAnimatedFrame->setRootOrientation(finalQ_root);

	if (_bInterpolateRoot)
	{
		///< Update root position?
		P3D x0 = m_keyFrames[lowId]->getRootPosition();
		P3D x1 = m_keyFrames[highId]->getRootPosition();

		P3D x = x0*(1.0 - localT) + x1*localT;
		_pAnimatedFrame->setRootPosition(x);
	}	
}

///<
void AnimatedCharacter::drawState(ArticulatedCharacter* _pCharacter, CharacterPose* _pState, int _flags)
{
	CharacterState currentState(_pCharacter); // save state = rigid body states, no computations involved
	{
		_pCharacter->setPose(_pState);
		drawCharacterSkeleton(_pCharacter, _flags);
	}
	// reset the state back to the current state
	_pCharacter->setState(&currentState);
}

///<
void AnimatedCharacter::drawCharacterSkeleton(ArticulatedCharacter* _pCharacter, int flags)
{
	std::vector<RigidBody*>& bodies = _pCharacter->getRigidBodies();
	for (uint i = 0; i < bodies.size(); ++i)
		bodies[i]->draw(flags, V3D(1, 0.1, 1), 1.0 );
}

///<
void AnimatedCharacter::drawKeyFrames(int flags, V3D* _pOffset)
{
	// save the current state before rendering the editor stuff
	CharacterState saveCurrrentState(m_pCharacter);
	for (uint i = 0; i < m_keyFrames.size(); ++i) {
		P3D x0 = m_keyFrames[i]->getRootPosition();

		if (_pOffset)
			m_keyFrames[i]->setRootPosition(x0 + (*_pOffset)*(i+1));

		{
			m_pCharacter->setPose(m_keyFrames[i]);
			drawCharacterSkeleton(m_pCharacter, flags);
		}
		
		if (_pOffset)
			m_keyFrames[i]->setRootPosition(x0);
	}

	// reset the state back to the current state
	m_pCharacter->setState(&saveCurrrentState);
}

///<
void AnimatedCharacter::createSubsetOfKeyFrames(int _minIndex, int _maxIndex)
{
	assert(_minIndex < (int)m_keyFrames.size());  assert(_maxIndex <= (int)m_keyFrames.size());

	std::vector<CharacterPose*> newFrames;
	for (int i = _minIndex; i < _maxIndex; ++i)
	{
		newFrames.push_back(new CharacterPose(m_pCharacter));
		*newFrames[newFrames.size() - 1] = *m_keyFrames[i];
	}

	for (int i = 0; i < (int)m_keyFrames.size(); ++i)
		delete m_keyFrames[i];

	m_keyFrames.clear();
	m_keyFrames.swap(newFrames);

}

///<
AnimatedCharacter::AnimatedCharacter(ArticulatedCharacter* _pRefCharacter, TwBar* _menuBar) 
{
	m_pCharacter = _pRefCharacter;

	m_pathName = "../data/rbs/animations/tempClip/";

	///< Create IO menu:
	if (_menuBar)
	{
		TwAddButton(_menuBar, "saveCharacterKeyFrames", SaveKeyFrames_CB_AnimatedCharacter, this, " label='save character key frames' group='Viz2'");
		TwAddButton(_menuBar, "loadCharacterKeyFrames", LoadKeyFrames_CB_AnimatedCharacter, this, " label='load character key frames' group='Viz2'");
	}
}

/*
	Create key frames utility function
*/
void AnimatedCharacter::createKeyFrames(const int _numKeyFrames)
{
	releaseKeyFrames();
	m_keyFrames.resize(_numKeyFrames);
	for (int i = 0; i < (int)m_keyFrames.size(); ++i)
		m_keyFrames[i] = new CharacterPose(m_pCharacter);
}


///<
void AnimatedCharacter::setPathName(const std::string& _pathName)
{
	m_pathName = _pathName;

	Path::create(m_pathName);
}

///<
void AnimatedCharacter::saveKeyFrames()
{
	std::stringstream sizeFileName;
	sizeFileName << m_pathName << "clip_size.txt";

	std::ofstream fFile(sizeFileName.str().c_str());
		
	if (fFile)
	{
		fFile << m_keyFrames.size();
		fFile.close();
		
		for (uint i = 0; i < m_keyFrames.size(); ++i) {
			std::stringstream sname;
			sname << m_pathName << "frame" << i << ".rs";
			m_keyFrames[i]->writeToFile(sname.str().c_str(), m_pCharacter);
		}

		Logger::consolePrint("Saved \"%s\" motion style! \n", m_pathName);
	}
	else
	{
		assert(false);
		Logger::consolePrint("Failed to save key frames. \n");
	}
}

///<
void AnimatedCharacter::loadKeyFrames(bool _bCopyFirstAsLast)
{
	if (!m_pCharacter)
	{
		Logger::consolePrint("Character is not initialized in the animated character class... \n");
		return;
	}

	int numFrames = -1;

	std::stringstream sizeFileName;
	sizeFileName << m_pathName << "\\clip_size.txt";
	std::ifstream fFile(sizeFileName.str().c_str());
	if (fFile)
	{
		fFile >> numFrames;
		fFile.close();

		if (numFrames > 0)
		{
			releaseKeyFrames();
			for (int i = 0; i < numFrames; ++i)
			{
				m_keyFrames.push_back(new CharacterPose(m_pCharacter));

				std::stringstream sname;
				sname << m_pathName << "frame" << i << ".rs";
				m_keyFrames[i]->readFromFile(sname.str().c_str());
			}

			Logger::consolePrint("Loaded \"%s\" motion style! \n \n", m_pathName);
		}
		else
			Logger::consolePrint("Failed to load key frames. \n \n");

		if (_bCopyFirstAsLast)
		{
			m_keyFrames.push_back(new CharacterPose(m_pCharacter));
			*m_keyFrames[m_keyFrames.size() - 1] = *m_keyFrames[0];
			numFrames++;
		}			

		m_maxTicks = numFrames;
	}
	else
	{
		assert(false);
		Logger::consolePrint("Failed to load key frames. \n \n");
	}
}

///<
AnimatedCharacter::~AnimatedCharacter()
{
	delete m_pTempState;
	delete m_pLinearSubspace;
	
	releaseKeyFrames();
}

///<
void AnimatedCharacter::releaseKeyFrames()
{
	for (uint i = 0; i < m_keyFrames.size(); ++i)
		delete m_keyFrames[i];

	m_keyFrames.clear();
}

dVector AnimatedCharacter::vectorize(CharacterPose* _pRBS)
{
	int numJoints = m_pCharacter->getJointCount();

	// State space: angles + root position + root orientation
	int dimSpace = numJoints + 6;

	dVector state(dimSpace);
	state.fill(0);

	///< Fill the poses
	int spacialIndex = 0;
	for (int j = 0; j < numJoints; ++j)
	{
		double jAngle = getJointRelativeAngle(_pRBS, j);
		state[spacialIndex++] = jAngle;
	}

	/// Position
	{
		P3D xRoot = _pRBS->getRootPosition();
		for (int k = 0; k<3; ++k)
			state[spacialIndex++] = xRoot[k];
	}

	/// Orientation
	{
		V3D xOrient = _pRBS->getRootOrientation().toAxisAngle();
		for (int k = 0; k<3; ++k)
			state[spacialIndex++] = xOrient[k];
	}

	return state;	
}

double AnimatedCharacter::getJointRelativeAngle(CharacterPose* _pState, int _jIndex) {
	Quaternion qI = _pState->getJointRelativeOrientation(_jIndex);

	V3D qAxis;
	double angle = 0;
	qI.getAxisAngle(qAxis, angle);

	///< Could pre-compute the hinge axis...
	HingeJoint* pHinge = (HingeJoint*)m_pCharacter->getJoint(_jIndex);
	V3D hingeAxis = pHinge->rotationAxis;

	if ((qAxis - hingeAxis).length() > 0.1)
		angle = angle*-1;

	return angle;
}

///<
void AnimatedCharacter::createLinearSubspace(int _dimOfSubSpace)
{
	assert(m_pLinearSubspace == NULL);
	if (!m_pLinearSubspace)
	{
		m_pLinearSubspace = new LinearSubspace();
		m_pLinearSubspace->m_bNormalize = false;

		int numJoints = m_pCharacter->getJointCount();

		// State space: angles + root position + root orientation
		int dimSpace = numJoints + 6;

		int numKfs_time = numKeyFrames();

		Eigen::MatrixXd frames_samples;

		frames_samples.resize(dimSpace, numKfs_time);	frames_samples.fill(0);

		for (int i = 0; i < numKfs_time; ++i)
		{
			dVector stateVec = vectorize(m_keyFrames[i]);

			for (int j = 0; j < stateVec.size(); ++j)
				frames_samples(j, i) = stateVec[j];
		}

		m_pLinearSubspace->computeAndRemoveMean(frames_samples);
		m_pLinearSubspace->getMaxEigenVectors(frames_samples, _dimOfSubSpace);
	}
}

///<
int AnimatedCharacter::getLinearSubSpaceDim() const
{
	assert(m_pLinearSubspace != NULL);
	if (m_pLinearSubspace)
		return m_pLinearSubspace->numEigenVectors();

	return -1;
}

dVector AnimatedCharacter::reduceStateWithLinearProjection(CharacterPose* _pRBS)
{
	assert(m_pLinearSubspace != NULL);

	if (m_tempReducedM.rows() != m_pLinearSubspace->numEigenVectors())
	{
		m_tempReducedM.resize(m_pLinearSubspace->numEigenVectors(), 1);
		m_tempReducedM.fill(0);
	}

	dVector state = vectorize(_pRBS);
	m_pLinearSubspace->project(state, m_tempReducedM);

	dVector reduced = m_tempReducedM;
	return reduced;

}

dVector AnimatedCharacter::reconstructSubLinearPose(const dVector& _subSpaceX)
{
	assert(m_pLinearSubspace != NULL);

	int dim = getCharacter()->getJointCount() + 6; // assumes only hinge joints
	if (m_tempFullSpace.rows() != dim)
	{
		m_tempFullSpace.resize(dim, 1);
		m_tempFullSpace.fill(0);
	}

	m_pLinearSubspace->unProject(_subSpaceX, m_tempFullSpace);

	dVector rTemp = m_tempFullSpace;
	return rTemp;

}


void AnimatedCharacter::fixDistanceToGround()
{
	double minY = 1000.0;

	
	int eeCount = getCharacter()->getEndEffectorCount();

	for (int i = 0; i < eeCount; ++i)
	{
		P3D eeWorld = getCharacter()->getEndEffectorWorldPosition(i);
		eeWorld[1] -= getCharacter()->getEndEffectorRadius(i);
		if (eeWorld[1] < minY)// (Look for the minimum end effector.
			minY = eeWorld[1];
		
	}

	V3D dx(0.0, -minY, 0.0);
	translate(dx);

}


void AnimatedCharacter::alignAnimatedPoseWithGround(CharacterPose* pAnimatedFrame)
{
	V3D groundNormal = Globals::groundPlane.n;
	P3D groundPos = P3D(0.0, 0.0, 0.0);

	// save the original current character state
	CharacterState currState(m_pCharacter);

	m_pCharacter->setPose(pAnimatedFrame);
	std::vector<RigidBody*> rbs = m_pCharacter->getEndEffectorRigidBodies();

	double minOffset = 1000.0; double maxOffset = 0.0;
	for (int i = 0; i < (int)rbs.size(); i++)
		if (rbs[i]->rbProperties.getEndEffectorPointCount() > 0)
			for (int j = 0; j < rbs[i]->rbProperties.getEndEffectorPointCount(); j++)
			{
				P3D eePoint = rbs[i]->rbProperties.getEndEffectorPoint(j);
				V3D eePos = rbs[i]->getOrientation().rotate(eePoint) + rbs[i]->getCMPosition();
				double dh = eePos.dot(groundNormal) - groundPos.dot(groundNormal);

				if (dh > 0.0 && abs(dh) < minOffset)
					minOffset = abs(dh);

				if (dh < 0.0 && abs(dh) > maxOffset)
					maxOffset = abs(dh);
			}

	rbs.clear();
	V3D newRootPos = m_pCharacter->getRoot()->getCMPosition();
	if (maxOffset > 0.0)
		newRootPos += groundNormal * maxOffset;
	else
		newRootPos -= groundNormal * minOffset;

	m_pCharacter->getRoot()->setCMPosition(P3D() + newRootPos);

	pAnimatedFrame->populatePose(m_pCharacter);
	m_pCharacter->setState(currState);
}




/*
* Utility
*/
AnimatedCharacterTimelineDraw::AnimatedCharacterTimelineDraw(AnimatedCharacter* _pCharacterKeyFrames, Trajectory1D* _pTimeWarp, const double _t) {
	V3D dx(1, 0, 0);

	P3D _x0(0.0, 0, 0.5);
	V3D heightV(0, 0.1, 0);
	glColor3d(1, 0, 0);
	drawCylinder(_x0, _x0 + heightV, 0.02);
	drawCylinder(_x0 + dx, _x0 + heightV + dx, 0.02);

	///<		
	if (_pCharacterKeyFrames) {
		double tAnim = _t;
		drawCylinder(_x0 + dx*tAnim, _x0 + heightV + dx*tAnim, 0.005);

		glColor3d(0, 0, 1);
		for (int i = 0; i < _pCharacterKeyFrames->numKeyFrames(); ++i) {
			///< t is between 0 and 1.
			if (_pTimeWarp) {
				double t = _pTimeWarp->getKnotPosition(i);
				//double t = m_pSkeletonToRBSTransfer->m_pTimeWarp->getKnotValue(i);
				P3D x0 = _x0 + dx*t;

				drawCylinder(x0, x0 + heightV, 0.01);
			}

		}
	}
}
