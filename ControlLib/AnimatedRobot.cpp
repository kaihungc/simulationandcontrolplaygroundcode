
#include <ControlLib/AnimatedRobot.h>

#include <ControlLib/Robot.h>
#include <MathLib\LinearSubspace.h>

///< IO
#include <sstream>
#include <fstream>


void TW_CALL SaveKeyFramesCallback(void* _pData) {
	((AnimatedRobot*)_pData)->saveKeyFrames();
}

void TW_CALL LoadKeyFramesCallback(void* _pData)
{
	((AnimatedRobot*)_pData)->loadKeyFrames();
}

///<
ReducedRobotState* AnimatedRobot::getFirstFrame()
{
	ReducedRobotState* state = getFrameAtIndex(0);
	
	return state;
}

int AnimatedRobot::getCurrentTimeIndex()
{
	double t = getTimer().getPhase();

	int index = (int)(t*(double)numKeyFrames());
	index = MIN(index, numKeyFrames() - 1);

	return index;
}

ReducedRobotState* AnimatedRobot::getCurrentFrame()
{
	return getFrameAtIndex(getCurrentTimeIndex());
}

///<
ReducedRobotState* AnimatedRobot::getFrameAtIndex(int _i)
{
	assert(_i >= 0 && _i < (int)m_keyFrames.size());
	return m_keyFrames[_i];
}


void AnimatedRobot::translate(const V3D& _x)
{
	for (int i = 0; i < numKeyFrames(); ++i)
		m_keyFrames[i]->setPosition(m_keyFrames[i]->getPosition() + _x);
}

void AnimatedRobot::updateCurrentFrameIndex()
{
	getTimer().update();
}

void AnimatedRobot::getInterpolatedFrameLoopEndWithFirst(const double _t, ReducedRobotState* _pAnimatedFrame)
{
	getInterpolatedFrameImpl(_t, _pAnimatedFrame, true, false);
}



void AnimatedRobot::getInterpolatedFrameIntegrateRoot(const double _t, int _numCycles, ReducedRobotState* _pAnimatedFrame)
{
	getInterpolatedFrame(_t, _pAnimatedFrame);
	P3D xRoot = _pAnimatedFrame->getPosition();

	///< Integrate root.		
	V3D dx = getInterpolatedRootPosition(1.0) - getInterpolatedRootPosition(0.0);
	for (int i = 0; i<_numCycles; ++i)
		xRoot += dx;

	_pAnimatedFrame->setPosition(xRoot);

}



P3D AnimatedRobot::getInterpolatedRootPosition(double _t)
{
	int numKeyFrames = m_keyFrames.size();

	IndexRest ir = GetIndexRest(_t, numKeyFrames - 1);

	int lowId = ir.m_index;
	int highId = ir.m_index + 1;
	double localT = ir.m_rest;

	///< Update root position?
	P3D x0 = m_keyFrames[lowId]->getPosition();
	P3D x1 = m_keyFrames[highId]->getPosition();

	P3D x = x0*(1.0 - localT) + x1*localT;

	return x;
}

V3D  AnimatedRobot::getInterpolatedVelocity(double _t0, double _t1)
{
	return getInterpolatedRootPosition(_t1) - getInterpolatedRootPosition(_t0);
}

ReducedRobotState*  AnimatedRobot::getInterpolatedFrame(const double _t)
{
	if (!m_pTempState)
		m_pTempState = new ReducedRobotState(m_pRobot);

	getInterpolatedFrameImpl(_t, m_pTempState, false, true);

	return m_pTempState;

}

void AnimatedRobot::getInterpolatedFrame(const double _t, ReducedRobotState* _pAnimatedFrame)
{
	getInterpolatedFrameImpl(_t, _pAnimatedFrame, false, true);
}

ReducedRobotState*  AnimatedRobot::getCurrentInterpolatedFrame()
{
	double t = getTimer().getPhase();
	return getInterpolatedFrame(t);
}




///<
void AnimatedRobot::getInterpolatedFrameImpl(const double _t, ReducedRobotState* _pAnimatedFrame, bool _bLoopEndWithFirstFrame, bool _bInterpolateRoot)
{
	// get id's of the neighbour keyframes for the given time t (wrap around from last to first for the last segment)
	int numKeyFrames = m_keyFrames.size();

	IndexRest ir = GetIndexRest(_t, numKeyFrames-1);

	int lowId = ir.m_index;
	int highId = ir.m_index + 1;
	double localT = ir.m_rest;

	if (_bLoopEndWithFirstFrame)
	{
		lowId = (int)std::floor(_t * (numKeyFrames - 1));
		highId = lowId + 1 < numKeyFrames ? lowId + 1 : 0;
		// compute the interpolation weight between these two frames
		double intervalSize = 1.0 / numKeyFrames;
		double lowerBound = intervalSize * lowId;
		localT = (_t - lowerBound) / intervalSize;
	}
	
	for (uint j = 0; j < m_pRobot->jointList.size(); j++)
	{
		int jIndex = m_pRobot->jointList[j]->jIndex;

		Quaternion lowQ = m_keyFrames[lowId]->getJointRelativeOrientation(jIndex);
		Quaternion highQ = m_keyFrames[highId]->getJointRelativeOrientation(jIndex);


		Quaternion finalQ = lowQ.sphericallyInterpolateWith(highQ, localT);
		//Quaternion finalQ = lowQ.linearlyInterpolateWith(highQ, s);

		_pAnimatedFrame->setJointRelativeOrientation(finalQ, jIndex);
		
	}

	Quaternion lowQ_root = m_keyFrames[lowId]->getOrientation();
	Quaternion highQ_root = m_keyFrames[highId]->getOrientation();
	Quaternion finalQ_root = lowQ_root.sphericallyInterpolateWith(highQ_root, localT);

	_pAnimatedFrame->setOrientation(finalQ_root);

	if (_bInterpolateRoot)
	{
		///< Update root position?
		P3D x0 = m_keyFrames[lowId]->getPosition();
		P3D x1 = m_keyFrames[highId]->getPosition();

		P3D x = x0*(1.0 - localT) + x1*localT;
		_pAnimatedFrame->setPosition(x);
	}	
}


///<
void AnimatedRobot::drawState(Robot* _pRobot, ReducedRobotState* _pState, int _flags)
{
	ReducedRobotState currentState(_pRobot);
	{
		_pRobot->setState(_pState);
		drawRobotSkeleton(_pRobot, _flags);
	}
	// reset the state back to the current state
	_pRobot->setState(&currentState);
}

///<
void AnimatedRobot::drawRobotSkeleton(Robot* m_pRobot, int flags)
{
	std::vector<RigidBody*> bodies = m_pRobot->getBodies();
	for (uint i = 0; i < bodies.size(); ++i)
		bodies[i]->draw(flags, V3D(1, 0.1, 1), 1.0 );
}

///<
void AnimatedRobot::drawKeyFrames(int flags, V3D* _pOffset)
{
	// save the current state before rendering the editor stuff
	ReducedRobotState saveCurrrentState(m_pRobot);
	for (uint i = 0; i < m_keyFrames.size(); ++i) {
		
		P3D x0 = m_keyFrames[i]->getPosition();

		if (_pOffset)
			m_keyFrames[i]->setPosition(x0 + (*_pOffset)*(i+1));

		{
			m_pRobot->setState(m_keyFrames[i]);
			drawRobotSkeleton(m_pRobot, flags);
		}
		
		if (_pOffset)
			m_keyFrames[i]->setPosition(x0);
	}

	// reset the state back to the current state
	m_pRobot->setState(&saveCurrrentState);
}


///<
void AnimatedRobot::createSubsetOfKeyFrames(int _minIndex, int _maxIndex)
{
	assert(_minIndex < (int)m_keyFrames.size());  assert(_maxIndex <= (int)m_keyFrames.size());

	std::vector<ReducedRobotState*> newFrames;
	for (int i = _minIndex; i < _maxIndex; ++i)
	{
		newFrames.push_back(new ReducedRobotState(m_pRobot));
		*newFrames[newFrames.size() - 1] = *m_keyFrames[i];
	}

	for (int i = 0; i < (int)m_keyFrames.size(); ++i)
		delete m_keyFrames[i];

	m_keyFrames.clear();
	m_keyFrames.swap(newFrames);

}

///<
AnimatedRobot::AnimatedRobot(Robot* _pRefRobot, TwBar* _menuBar) 
{
	m_pRobot = _pRefRobot;

	m_pathName = "../data/rbs/animations/tempClip/";

	///< Create IO menu:
	if (_menuBar)
	{
		TwAddButton(_menuBar, "saveRobotKeyFrames", SaveKeyFramesCallback, this, " label='save robot key frames' group='Viz2'");
		TwAddButton(_menuBar, "loadRobotKeyFrames", LoadKeyFramesCallback, this, " label='load robot key frames' group='Viz2'");
	}
}

/*
	Create key frames utility function
*/
void AnimatedRobot::createKeyFrames(const int _numKeyFrames)
{
	releaseKeyFrames();
	m_keyFrames.resize(_numKeyFrames);
	for (int i = 0; i < (int)m_keyFrames.size(); ++i)
		m_keyFrames[i] = new ReducedRobotState(m_pRobot);
}


///<
void AnimatedRobot::setPathName(const std::string& _pathName)
{
	m_pathName = _pathName;

	Path::create(m_pathName);
}

///<
void AnimatedRobot::saveKeyFrames()
{
	std::stringstream sizeFileName;
	sizeFileName << m_pathName << "clip_size.txt";

	std::ofstream fFile(sizeFileName.str().c_str());

	
	if (fFile)
	{
		fFile << m_keyFrames.size();
		fFile.close();


		for (uint i = 0; i < m_keyFrames.size(); ++i)
		{
			std::stringstream sname;
			sname << m_pathName << "frame" << i << ".rs";
			m_keyFrames[i]->writeToFile(sname.str().c_str());
		}

		Logger::consolePrint("Saved \"%s\" motion style! \n", m_pathName);

	}
	else
	{
		assert(false);
		Logger::consolePrint("Failed to save key frames. \n");
	}
}

///<
void AnimatedRobot::loadKeyFrames(bool _bCopyFirstAsLast)
{

	if (!m_pRobot)
	{
		Logger::consolePrint("Robot is not initialized in the animated robot class... \n");
		return;
	}

	int numFrames = -1;

	std::stringstream sizeFileName;
	sizeFileName << m_pathName << "\\clip_size.txt";
	std::ifstream fFile(sizeFileName.str().c_str());
	if (fFile)
	{
		fFile >> numFrames;
		fFile.close();

		if (numFrames > 0)
		{
			releaseKeyFrames();
			for (int i = 0; i < numFrames; ++i)
			{
				m_keyFrames.push_back(new ReducedRobotState(m_pRobot));

				std::stringstream sname;
				sname << m_pathName << "frame" << i << ".rs";
				m_keyFrames[i]->readFromFile(sname.str().c_str());
			}

			Logger::consolePrint("Loaded \"%s\" motion style! \n \n", m_pathName);
		}
		else
			Logger::consolePrint("Failed to load key frames. \n \n");

		
		if (_bCopyFirstAsLast)
		{
			m_keyFrames.push_back(new ReducedRobotState(m_pRobot));
			*m_keyFrames[m_keyFrames.size() - 1] = *m_keyFrames[0];
			numFrames++;
		}			

		m_maxTicks = numFrames;
	}
	else
	{
		assert(false);
		Logger::consolePrint("Failed to load key frames. \n \n");
	}
}

///<
AnimatedRobot::~AnimatedRobot()
{
	delete m_pTempState;
	delete m_pLinearSubspace;
	
	releaseKeyFrames();
}

///<
void AnimatedRobot::releaseKeyFrames()
{
	for (uint i = 0; i < m_keyFrames.size(); ++i)
		delete m_keyFrames[i];

	m_keyFrames.clear();
}

/*
* Utility 
*/
TimelineDraw::TimelineDraw(AnimatedRobot* _pRobotKeyFrames, Trajectory1D* _pTimeWarp, const double _t)
{
	V3D dx(1, 0, 0);

	P3D _x0(0.0, 0, 0.5);
	V3D heightV(0, 0.1, 0);
	glColor3d(1, 0, 0);
	drawCylinder(_x0, _x0 + heightV, 0.02);
	drawCylinder(_x0 + dx, _x0 + heightV + dx, 0.02);

	///<		
	if (_pRobotKeyFrames)
	{
		double tAnim = _t;
		drawCylinder(_x0 + dx*tAnim, _x0 + heightV + dx*tAnim, 0.005);

		glColor3d(0, 0, 1);
		for (int i = 0; i < _pRobotKeyFrames->numKeyFrames(); ++i)
		{
			///< t is between 0 and 1.
			if (_pTimeWarp)
			{
				double t = _pTimeWarp->getKnotPosition(i);
				//double t = m_pSkeletonToRBSTransfer->m_pTimeWarp->getKnotValue(i);
				P3D x0 = _x0 + dx*t;

				drawCylinder(x0, x0 + heightV, 0.01);
			}

		}
	}
}


dVector AnimatedRobot::vectorize(ReducedRobotState* _pRBS)
{
	int numJoints = m_pRobot->getJointCount();

	// State space: angles + root position + root orientation
	int dimSpace = numJoints + 6;

	dVector state(dimSpace);
	state.fill(0);

	///< Fill the poses
	int spacialIndex = 0;
	for (int j = 0; j < numJoints; ++j)
	{
		double jAngle = m_pRobot->getJointRelativeAngle(_pRBS, j);
		state[spacialIndex++] = jAngle;
	}

	/// Position
	{
		P3D xRoot = _pRBS->getPosition();
		for (int k = 0; k<3; ++k)
			state[spacialIndex++] = xRoot[k];
	}

	/// Orientation
	{
		V3D xOrient = _pRBS->getOrientation().toAxisAngle();
		for (int k = 0; k<3; ++k)
			state[spacialIndex++] = xOrient[k];
	}

	return state;	
}

///<
void AnimatedRobot::createLinearSubspace(int _dimOfSubSpace)
{
	assert(m_pLinearSubspace == NULL);
	if (!m_pLinearSubspace)
	{
		m_pLinearSubspace = new LinearSubspace();
		m_pLinearSubspace->m_bNormalize = false;

		int numJoints = m_pRobot->getJointCount();

		// State space: angles + root position + root orientation
		int dimSpace = numJoints + 6;

		int numKfs_time = numKeyFrames();

		Eigen::MatrixXd frames_samples;

		frames_samples.resize(dimSpace, numKfs_time);	frames_samples.fill(0);

		for (int i = 0; i < numKfs_time; ++i)
		{
			dVector stateVec = vectorize(m_keyFrames[i]);

			for (int j = 0; j < stateVec.size(); ++j)
				frames_samples(j, i) = stateVec[j];
		}

		m_pLinearSubspace->computeAndRemoveMean(frames_samples);
		m_pLinearSubspace->getMaxEigenVectors(frames_samples, _dimOfSubSpace);
	}
}

///<
int AnimatedRobot::getLinearSubSpaceDim() const
{
	assert(m_pLinearSubspace != NULL);
	if (m_pLinearSubspace)
		return m_pLinearSubspace->numEigenVectors();

	return -1;
}

dVector AnimatedRobot::reduceStateWithLinearProjection(ReducedRobotState* _pRBS)
{
	assert(m_pLinearSubspace != NULL);

	if (m_tempReducedM.rows() != m_pLinearSubspace->numEigenVectors())
	{
		m_tempReducedM.resize(m_pLinearSubspace->numEigenVectors(), 1);
		m_tempReducedM.fill(0);
	}

	dVector state = vectorize(_pRBS);
	m_pLinearSubspace->project(state, m_tempReducedM);

	dVector reduced = m_tempReducedM;
	return reduced;

}

dVector AnimatedRobot::reconstructSubLinearPose(const dVector& _subSpaceX)
{
	assert(m_pLinearSubspace != NULL);

	if (m_tempFullSpace.rows() != getRobot()->getDim())
	{
		m_tempFullSpace.resize(getRobot()->getDim(), 1);
		m_tempFullSpace.fill(0);
	}

	m_pLinearSubspace->unProject(_subSpaceX, m_tempFullSpace);

	dVector rTemp = m_tempFullSpace;
	return rTemp;

}


void AnimatedRobot::fixDistanceToGround()
{
	double minY = 1000.0;

	
	int eeCount = getRobot()->getEndEffectorCount();

	for (int i = 0; i < eeCount; ++i)
	{
		P3D eeWorld = getRobot()->getEndEffectorWorldPosition(i);
		eeWorld[1] -= getRobot()->getEndEffectorRadius(i);
		if (eeWorld[1] < minY)// (Look for the minimum end effector.
			minY = eeWorld[1];
		
	}

	V3D dx(0.0, -minY, 0.0);
	translate(dx);

}


void AnimatedRobot::alignAnimatedPoseWithGround(ReducedRobotState* pAnimatedFrame)
{
	V3D groundNormal = Globals::groundPlane.n;
	P3D groundPos = P3D(0.0, 0.0, 0.0);

	// save the original current robot state
	ReducedRobotState* currState = new ReducedRobotState(m_pRobot->getReducedStateDimension());
	m_pRobot->populateState(currState);

	m_pRobot->setState(pAnimatedFrame);
	std::vector<RigidBody*> rbs = m_pRobot->getEndEffectorRBs();

	double minOffset = 1000.0; double maxOffset = 0.0;
	for (int i = 0; i < (int)rbs.size(); i++)
		if (rbs[i]->rbProperties.getEndEffectorPointCount() > 0)
			for (int j = 0; j < rbs[i]->rbProperties.getEndEffectorPointCount(); j++)
			{
				P3D eePoint = rbs[i]->rbProperties.getEndEffectorPoint(j);
				V3D eePos = rbs[i]->getOrientation().rotate(eePoint) + rbs[i]->getCMPosition();
				double dh = eePos.dot(groundNormal) - groundPos.dot(groundNormal);

				if (dh > 0.0 && abs(dh) < minOffset)
					minOffset = abs(dh);

				if (dh < 0.0 && abs(dh) > maxOffset)
					maxOffset = abs(dh);
			}

	rbs.clear();
	V3D newRootPos = m_pRobot->root->getCMPosition();
	if (maxOffset > 0.0)
		newRootPos += groundNormal * maxOffset;
	else
		newRootPos -= groundNormal * minOffset;

	m_pRobot->root->setCMPosition(P3D() + newRootPos);

	m_pRobot->populateState(pAnimatedFrame);
	m_pRobot->setState(currState);
	delete currState;
}
