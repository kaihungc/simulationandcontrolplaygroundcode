#pragma once

#include <FBXReaderLib\SkinnedMesh.h>

class Robot;
class Skeleton;


///////////////////////////////////////////////////////////////////////////
/*Only thing useful is the static function for updating transforms from the robot...*/
///////////////////////////////////////////////////////////////////////////


class RBSMesh 
{
protected:

	virtual SkinnedMesh::TransformsContainer	getTransformationsFromRobotUsingSkeleton(Robot* _pRobot, Skeleton* _pSkeleton) = 0;
	
	///< global Skeleton transforms
	double	m_scale=1.0;

	void	initialiseUsingGlobalTransforms();

public:

	Robot*		m_pRobot	= NULL;
	Skeleton*	m_pSkeleton = NULL;

	SkinnedMesh*		m_pMesh		= NULL;

	RBSMesh() {}
	~RBSMesh();

	virtual void	initialise() = 0;

	void	update();

	void	draw();

	int		searchRigidBodiesForFBXId(int _fbxId);

};


///< Initial type of mapping: adds more freedom than the Maya rig !!
class RBSMesh_RB_between_MayaJoints : public RBSMesh
{
protected:
	virtual SkinnedMesh::TransformsContainer	getTransformationsFromRobotUsingSkeleton(Robot* _pRobot, Skeleton* _pSkeleton);

public:

	virtual void	initialise();
};

///< Second type of mapping...
class RBSMesh_BranchesAndChainsMap : public RBSMesh
{

	std::vector<Quaternion>		m_initialSkeletonRelativeTransforms;
	V3D			m_initialSkeletonToRobotOffset;

	V3D		getRobotSkeletonToRobotOffset	(Robot* _pRobot, Skeleton* _pSkeleton);

	V3D		getRigidBodyPosition			(Robot* _pRobot, Skeleton* _pSkeleton, int _id);


protected:

	virtual SkinnedMesh::TransformsContainer	getTransformationsFromRobotUsingSkeleton(Robot* _pRobot, Skeleton* _pSkeleton);

public:

	virtual void	initialise();

};




