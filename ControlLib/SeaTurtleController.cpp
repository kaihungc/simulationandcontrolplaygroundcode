#include "SeaTurtleController.h"

SeaTurtleController::SeaTurtleController(AbstractRBEngine* rbEngine){
	this->rbEngine = rbEngine;
	setupSeaTurtleController();

	stridePhase = 0;
	strideDuration = 2.0;

	INITIALIZE_PARAMETER(parameterList, P_STRIDE_DURATION, 1.5, 1.0, 2.0, "stride duration");

	int trajectoryKnotCount = sizeof(TRAJ_COUNT) / sizeof(int);

	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_LEFT_SHOULDER_TWIST[i], 0, -1.0, 1.0, "left shoulder twist");
	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_RIGHT_SHOULDER_TWIST[i], 0, -1.0, 1.0, "right shoulder twist");

	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_LEFT_SHOULDER_UP_DOWN[i], 0, -1.5, 1.5, "left shoulder up/down");
	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_RIGHT_SHOULDER_UP_DOWN[i], 0, -1.5, 1.5, "right shoulder up/down");

	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_LEFT_SHOULDER_FORWARDS_BACKWARDS[i], 0, -0.2, 0.2, "left shoulder forward/back");
	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_RIGHT_SHOULDER_FORWARDS_BACKWARDS[i], 0, -0.2, 0.2, "right shoulder forward/back");

	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_LEFT_HIP_UP_DOWN[i], 0, -1.5, 1.5, "left hip up/down");
	for (int i = 0; i<trajectoryKnotCount; i++)
		INITIALIZE_PARAMETER(parameterList, P_RIGHT_HIP_UP_DOWN[i], 0, -1.5, 1.5, "right hip up/down");

	for (int i=0;i<trajectoryKnotCount;i++){
		double t = (double)i/(trajectoryKnotCount-1);
		parameterList[P_LEFT_SHOULDER_UP_DOWN[i]].value = sin(t * 2 * PI) * 0.75;
	}

	//try it out...
	for (int i=0;i<trajectoryKnotCount;i++){
		double t = (double)i/(trajectoryKnotCount-1);
		if (i >= 2 && i <= 6)
			parameterList[P_LEFT_SHOULDER_TWIST[i]].value = -0.5;
		else
			parameterList[P_LEFT_SHOULDER_TWIST[i]].value = 0.5;
	}

	setParameters();
}

SeaTurtleController::~SeaTurtleController(void){
}

void SeaTurtleController::setParameters(){
	strideDuration = parameterList[P_STRIDE_DURATION].value;

	int trajectoryKnotCount = sizeof(TRAJ_COUNT) / sizeof(int);

	setContinuousTrajectory(&leftShoulderTwist, parameterList, P_LEFT_SHOULDER_TWIST, trajectoryKnotCount);
	setContinuousTrajectory(&rightShoulderTwist, parameterList, P_RIGHT_SHOULDER_TWIST, trajectoryKnotCount);

	setContinuousTrajectory(&leftShoulderUpDown, parameterList, P_LEFT_SHOULDER_UP_DOWN, trajectoryKnotCount);
	setContinuousTrajectory(&rightShoulderUpDown, parameterList, P_RIGHT_SHOULDER_UP_DOWN, trajectoryKnotCount);

	setContinuousTrajectory(&leftShoulderForwardsBackwards, parameterList, P_LEFT_SHOULDER_FORWARDS_BACKWARDS, trajectoryKnotCount);
	setContinuousTrajectory(&rightShoulderForwardsBackwards, parameterList, P_RIGHT_SHOULDER_FORWARDS_BACKWARDS, trajectoryKnotCount);

	setContinuousTrajectory(&leftHipUpDown, parameterList, P_LEFT_HIP_UP_DOWN, trajectoryKnotCount);
	setContinuousTrajectory(&rightHipUpDown, parameterList, P_RIGHT_HIP_UP_DOWN, trajectoryKnotCount);
}

void SeaTurtleController::setupSeaTurtleController(){
	leftShoulder = rbEngine->getJointByName("leftShoulder");
	rightShoulder = rbEngine->getJointByName("rightShoulder");
	leftHip = rbEngine->getJointByName("leftHip");
	rightHip = rbEngine->getJointByName("rightHip");
	mainBody = rbEngine->getRBByName("torso");

	dragPlates.clear();
	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(0, -1, 0), P3D(-0.5, -0.1255, -0.3), P3D(-0.5, -0.1255, 0.3), P3D(0.5, -0.1255, 0.3), P3D(0.5, -0.1255, -0.3));
	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(0, 1, 0), P3D(-0.5, 0.1255, -0.3), P3D(-0.5, 0.1255, 0.3), P3D(0.5, 0.1255, 0.3), P3D(0.5, 0.1255, -0.3));

	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(-1, 0, 0), P3D(-0.5, -0.125, -0.3), P3D(-0.5, -0.125, 0.3), P3D(-0.5, 0.125, 0.3), P3D(-0.5, 0.125, -0.3), 0.5);
	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(1, 0, 0), P3D(0.5, -0.125, -0.3), P3D(0.5, -0.125, 0.3), P3D(0.5, 0.125, 0.3), P3D(0.5, 0.125, -0.3), 0.5);

	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(0, 0, -1), P3D(-0.5, -0.125, -0.3), P3D(-0.5, 0.125, -0.3), P3D(0.5, 0.125, -0.3), P3D(0.5, -0.125, -0.3));
	addDragPlates(&dragPlates, rbEngine->getRBByName("torso"), V3D(0, 0, 1), P3D(-0.5, -0.125, 0.3), P3D(-0.5, 0.125, 0.3), P3D(0.5, 0.125, 0.3), P3D(0.5, -0.125, 0.3));

	addDragPlates(&dragPlates, rbEngine->getRBByName("backFlipperLeft"), V3D(0, -1, 0), P3D(-0.15, -0.015, -0.15), P3D(-0.15, -0.015, 0.15), P3D(0.15, -0.015, 0.15), P3D(0.15, -0.015, -0.15));
	addDragPlates(&dragPlates, rbEngine->getRBByName("backFlipperLeft"), V3D(0, 1, 0), P3D(-0.15, 0.015, -0.15), P3D(-0.15, 0.015, 0.15), P3D(0.15, 0.015, 0.15), P3D(0.15, 0.015, -0.15));

	addDragPlates(&dragPlates, rbEngine->getRBByName("backFlipperRight"), V3D(0, -1, 0), P3D(-0.15, -0.015, -0.15), P3D(-0.15, -0.015, 0.15), P3D(0.15, -0.015, 0.15), P3D(0.15, -0.015, -0.15));
	addDragPlates(&dragPlates, rbEngine->getRBByName("backFlipperRight"), V3D(0, 1, 0), P3D(-0.15, 0.015, -0.15), P3D(-0.15, 0.015, 0.15), P3D(0.15, 0.015, 0.15), P3D(0.15, 0.015, -0.15));

	addFinElements(&finElems, rbEngine->getRBByName("frontFlipperLeft"), P3D(-0.06, 0, 0), P3D(0, 0, 0.25), P3D(0.06, 0, 0), P3D(0,0,-0.25), 20);
	addFinElements(&finElems, rbEngine->getRBByName("frontFlipperRight"), P3D(-0.06, 0, 0), P3D(0, 0, 0.25), P3D(0.06, 0, 0), P3D(0,0,-0.25), 20);
}

Quaternion SeaTurtleController::getDesiredBodyWorldOrientation(){
	return Quaternion();
}

Quaternion SeaTurtleController::getBodyOrientationError(){
//	return Quaternion();

	//TODO: maybe consider a heading-independent rotation here (in both places)...
	return (getDesiredBodyWorldOrientation().getInverse() * mainBody->state.orientation).getInverse();
}

Quaternion SeaTurtleController::getLeftShoulderOrientation(double t){
//	dbQc = dbQw * wQp * pQc
	return	getBodyOrientationError() *
			getRotationQuaternion(leftShoulderForwardsBackwards.evaluate_catmull_rom(t), V3D(0, 1, 0)) *
			getRotationQuaternion(leftShoulderUpDown.evaluate_catmull_rom(t), V3D(1, 0, 0)) *
			getRotationQuaternion(leftShoulderTwist.evaluate_catmull_rom(t), V3D(0, 0, 1));
}

Quaternion SeaTurtleController::getRightShoulderOrientation(double t){
	return	
			getBodyOrientationError() *
			getRotationQuaternion(rightShoulderForwardsBackwards.evaluate_catmull_rom(t), V3D(0, -1, 0)) *
			getRotationQuaternion(rightShoulderUpDown.evaluate_catmull_rom(t), V3D(-1, 0, 0)) *
			getRotationQuaternion(rightShoulderTwist.evaluate_catmull_rom(t), V3D(0, 0, 1));
}

Quaternion SeaTurtleController::getLeftHipOrientation(double t){
	return //getBodyOrientationError() * 
		getRotationQuaternion(leftHipUpDown.evaluate_catmull_rom(t), V3D(0, 0, 1));
}

Quaternion SeaTurtleController::getRightHipOrientation(double t){
	return //getBodyOrientationError() *
		getRotationQuaternion(rightHipUpDown.evaluate_catmull_rom(t), V3D(0, 0, 1));
}

void SeaTurtleController::step(double dt){
	stridePhase += dt / strideDuration;
	if (stridePhase > 1) stridePhase = 0;
}

void SeaTurtleController::computeForcesAndControlInputs(V3D fluidVel){
	//compute the desired orientations and angular velocities for the various joints in here...
	leftShoulder->desiredRelativeOrientation = getLeftShoulderOrientation(stridePhase);
	rightShoulder->desiredRelativeOrientation = getRightShoulderOrientation(stridePhase);

	leftHip->desiredRelativeOrientation = getLeftHipOrientation(stridePhase);
	rightHip->desiredRelativeOrientation = getRightHipOrientation(stridePhase);

	//TODO: might want to set desired angular velocities as well...

	//compute and apply the various hydrodynamic forces
	for (uint i=0;i<dragPlates.size();i++)
		rbEngine->applyForceTo(dragPlates[i].body, dragPlates[i].computeDragForce(fluidVel), dragPlates[i].midPoint);

	V3D lForce, dForce;
	for (uint i=0;i<finElems.size();i++){
		finElems[i].computeDragAndLiftForces(fluidVel, &dForce, &lForce);
		rbEngine->applyForceTo(finElems[i].body, dForce, finElems[i].applicationPoint);
		rbEngine->applyForceTo(finElems[i].body, lForce, finElems[i].applicationPoint);
	}
}

// FinElement code below...


void populateCoefsFromFile(FILE* fp, Trajectory1D* coefs){
	DynamicArray<double> tmp;
	readDoublesFromFile(fp, &tmp);
	for (uint i=0;i<tmp.size();i+=2)
		coefs->addKnot(tmp[i], tmp[i+1]);
}

//TODO: maybe these should belong to the fin element at some point...
Trajectory1D liftCoefficient;
Trajectory1D dragCoefficient;

FinElement::FinElement(RigidBody* p, V3D n, V3D ca, P3D ap, double a){
	body = p;
	normal = n;
	chordAxis = ca;
	applicationPoint = ap;
	area = a;

	if (liftCoefficient.getKnotCount() == 0){
		FILE* fp = fopen("../data/naca0014DragCoefs.txt", "r");
		populateCoefsFromFile(fp, &dragCoefficient);
		fclose(fp);

		fp = fopen("../data/naca0014LiftCoefs.txt", "r");
		populateCoefsFromFile(fp, &liftCoefficient);
		fclose(fp);
	}
}

void FinElement::computeDragAndLiftForces(V3D fluidVelocity, V3D* dragForce, V3D* liftForce){
	V3D relVelocity = fluidVelocity - body->getAbsoluteVelocityForLocalPoint(applicationPoint);
	V3D worldNormal = body->getWorldCoordinates(normal);

	//normally the relative velocity should be in the plane defined by the chord axis and the normal of the fin.
	//what this means is that the chord axis of the fin element changes as a function of the relative velocity
	//which means that the profile of the element (and thus the lift and drag coefficients) change also.
	//we will ignore this problem for now...


	double normalRelVComponent = relVelocity.dot(worldNormal);
	V3D tangentialRelVComponent = relVelocity - worldNormal * normalRelVComponent;

	double angleOfAttack = 0;
	if (relVelocity.length() > 0.001 && tangentialRelVComponent.length() > 0.001) angleOfAttack = relVelocity.angleWith(tangentialRelVComponent);
	if (normalRelVComponent < 0) angleOfAttack *= -1;

	//the drag force is parallel to the flow direction, while the drag force is perpendicular to it
	V3D dragDirection = relVelocity.unit();
	V3D liftDirection = (relVelocity.cross(worldNormal)).cross(relVelocity).unit();

	double fluidDensity = 1000;

	//based on the angle of attack and so on, compute the magnitude of the lift and drag forces...
	*dragForce = dragDirection * (0.5 * fluidDensity * area * relVelocity.length2() * dragCoefficient.evaluate_catmull_rom(angleOfAttack));
	*liftForce = liftDirection * (0.5 * fluidDensity * area * relVelocity.length2() * liftCoefficient.evaluate_catmull_rom(angleOfAttack));
}




