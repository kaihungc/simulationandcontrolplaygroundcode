#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "GeneralizedCoordinatesRobotRepresentation.h"
#include <map>

using namespace std;

class SkeletonSymmetryObjective : public ObjectiveFunction {

public:
	SkeletonSymmetryObjective(GeneralizedCoordinatesRobotRepresentation* _gcRobot, map<int, int>* _symmMap, bool optimizeRootConfiguration, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonSymmetryObjective(void);

	virtual double computeValue(const dVector& p);
	virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);

private:
	//the energy function operates on a motion plan...
	map<int, int>* symmMap;
	GeneralizedCoordinatesRobotRepresentation* gcRobot;
	bool optimizeRootConfiguration;
};

