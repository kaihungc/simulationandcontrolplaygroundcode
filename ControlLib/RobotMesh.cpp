

#include <ControlLib\RobotMesh.h>
#include <ControlLib\Robot.h>

#include <FBXReaderLib\Skeleton.hpp>


RBSMesh::~RBSMesh()
{

}

void RBSMesh::draw()
{
	m_pMesh->draw();
}

void RBSMesh::initialiseUsingGlobalTransforms()
{

}

///<
void RBSMesh::update()
{
	SkinnedMesh::TransformsContainer transforms = getTransformationsFromRobotUsingSkeleton(m_pRobot, m_pSkeleton);
	m_pMesh->updateSkinningMatrices(transforms);	
}

///<
int RBSMesh::searchRigidBodiesForFBXId(int _fbxId)
{
	int rbId = -1;

	for (int j = 0; j < (int)m_pRobot->getBodies().size(); ++j)
		if (m_pRobot->getBodies()[j]->fbxBoneId == _fbxId) {
			rbId = j;
			break;
		}

	return rbId;
}
