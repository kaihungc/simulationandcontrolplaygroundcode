#pragma once

#include <GUILib/GLApplication.h>
#include <string>
#include <map>

class Grid2D {
public:
	DynamicArray<DynamicArray<double>> gridVals;
	double startX = -1, startY = -1, endX = 1, endY = 1;
	int res_x = 2;
	int res_y = 2;
	double dx = (endX - startX) / (res_x - 1);
	double dy = (endY - startY) / (res_y - 1);
public:
	Grid2D() {
		resize();
	}

	Grid2D(int res_x, int res_y) {
		this->res_x = res_x;
		this->res_y = res_y;
		resize();
	}

	void setDomain(double sX, double sY, double eX, double eY) {
		startX = sX;
		startY = sY;
		endX = eX;
		endY = eY;
		dx = (endX - startX) / (res_x - 1);
		dy = (endY - startY) / (res_y - 1);
	}

	void resize() {
		gridVals.clear();
		for (int i = 0; i < res_x; i++)
			gridVals.push_back(DynamicArray<double>(res_y, 0));
		dx = (endX - startX) / (res_x - 1);
		dy = (endY - startY) / (res_y - 1);
	}

	void setTestVals() {
		for (int i = 0; i < res_x; i++)
			for (int j = 0; j < res_y; j++) {
				gridVals[i][j] = sin((i * dx) / (endX - startX) * 5.0 * M_PI) * sin((j * dy) / (endY - startY) * 5.0 * M_PI);
//				Logger::consolePrint("%lf\n", gridVals[i][j]);
			}

		gridVals[0][0] = -1;
		gridVals[1][0] = 0;
		gridVals[1][1] = 1;
		gridVals[0][1] = 0;

	}

	void setupQuadVertex(int i, int j) {
		double color = gridVals[i][j];
		if (color < 0) 
			glColor3d(1 + color, 1 + color, 1);
		else
			glColor3d(1, 1 - color, 1 - color);
		glVertex3d(startX + i * dx, startY + j * dy, 0);
	}

	void draw() {


		glDisable(GL_LIGHTING);
		glColor3d(1,1,1);
		glBegin(GL_QUADS);
			glVertex3d(startX - 0.1 * (endX - startX), startY - 0.1 * (endY - startY), 0);
			glVertex3d(startX - 0.1 * (endX - startX), endY + 0.1 * (endY - startY), 0);
			glVertex3d(endX + 0.1 * (endX - startX), endY + 0.1 * (endY - startY), 0);
			glVertex3d(endX + 0.1 * (endX - startX), startY - 0.1 * (endY - startY), 0);
		glEnd();

		glBegin(GL_QUADS);
		for (int i = 0; i < res_x - 1; i++)
			for (int j = 0; j < res_y - 1; j++) {
				setupQuadVertex(i, j);
				setupQuadVertex(i, j + 1);
				setupQuadVertex(i + 1, j + 1);
				setupQuadVertex(i + 1, j);
			}
		glEnd();

		glEnable(GL_BLEND);
		glColor4d(0.0, 0.0, 0.0, 0.1);

		glBegin(GL_LINES);
		for (int i = 0; i < res_y; i++) {
			glVertex3d(startX, startY + i * dy, 0);
			glVertex3d(endX, startY + i * dy, 0);
		}
		for (int i = 0; i < res_x; i++) {
			glVertex3d(startX + i * dx, startY, 0);
			glVertex3d(startX + i * dx, endY, 0);
		}
		glEnd();

		glDisable(GL_BLEND);
		glColor3d(0,0,0);
		//this is the code that extracts contours using a marching-squares approach
		glBegin(GL_LINES);
		for (int i = 0; i < res_x - 1; i++)
			for (int j = 0; j < res_y - 1; j++) {
				double v00 = gridVals[i + 0][j + 0];
				double v10 = gridVals[i + 1][j + 0];
				double v11 = gridVals[i + 1][j + 1];
				double v01 = gridVals[i + 0][j + 1];

				//there are 16 cases that we have to worry about, depending on which grid corner values are negative, and which ones are positive...

				//count the number of corners outside the object
				int nCount = ((v00 <= 0) ? 0 : 1) + ((v10 <= 0) ? 0 : 1) + ((v11 <= 0) ? 0 : 1) + ((v01 <= 0) ? 0 : 1);

				Logger::consolePrint("%d %d -> %d\n", i, j, nCount);

				//no corners are outside, or all corners are outside
				if (nCount == 0 || nCount == 4) {
					//the entire cell is strictly inside, or strictly outside, the implicitly-defined object, nothing to do...
					continue;
				}

				//only one corner is inside or outside the object - means we will have to 
				if (nCount == 1 || nCount == 3) {
					//the surface of the object cuts this cell exactly once... draw a line where this is happening...
					if (v00 * v10 <= 0) {
						//there must be  zero crossing along this edge... We must find t such that v1*t + v2*(1-t) = 0 => (v1-v2)*t = -v2
						double t = getZeroLineCrossing(v00, v10);
						P3D p = getPositionOfNode(i, j) * t + getPositionOfNode(i + 1, j) * (1 - t);
						glVertex3d(p.x(), p.y(), p.z());
						Logger::consolePrint("added a vertex...\n", i, j, nCount);
					}
					if (v10 * v11 <= 0) {
						//there must be  zero crossing along this edge... We must find t such that v1*t + v2*(1-t) = 0 => (v1-v2)*t = -v2
						double t = getZeroLineCrossing(v10, v11);
						P3D p = getPositionOfNode(i+1, j) * t + getPositionOfNode(i + 1, j + 1) * (1 - t);
						glVertex3d(p.x(), p.y(), p.z());
						Logger::consolePrint("added a vertex...\n", i, j, nCount);
					}
					if (v11 * v01 <= 0) {
						//there must be  zero crossing along this edge... We must find t such that v1*t + v2*(1-t) = 0 => (v1-v2)*t = -v2
						double t = getZeroLineCrossing(v11, v01);
						P3D p = getPositionOfNode(i + 1, j + 1) * t + getPositionOfNode(i, j + 1) * (1 - t);
						glVertex3d(p.x(), p.y(), p.z());
						Logger::consolePrint("added a vertex...\n", i, j, nCount);
					}
					if (v01 * v00 <= 0) {
						//there must be  zero crossing along this edge... We must find t such that v1*t + v2*(1-t) = 0 => (v1-v2)*t = -v2
						double t = getZeroLineCrossing(v01, v00);
						P3D p = getPositionOfNode(i, j + 1) * t + getPositionOfNode(i, j) * (1 - t);
						glVertex3d(p.x(), p.y(), p.z());
						Logger::consolePrint("added a vertex...\n", i, j, nCount);
					}
					continue;
				}


			}
		glEnd();

	}

	P3D getPositionOfNode(int i, int j) {
		return P3D(startX + i*dx, startY + j*dy, 0);
	}

	double getZeroLineCrossing(double v1, double v2){
		assert(v1 * v2 <= 0 && v1 != v2);
		//there must be a zero crossing along the interval [v1, v2]... We find t such that v1*t + v2*(1-t) = 0 => (v1-v2)*t = -v2
		return -v2 / (v1 - v2);
	}

};

class LevelSetFunction {
public:
	Grid2D levelSetRepresentation;

	LevelSetFunction(double domainStartX = -1, double domainStartY = -1, double domainEndX = 1, double domainEndY = 1) {
		levelSetRepresentation.startX = domainStartX;
		levelSetRepresentation.startY = domainStartY;
		levelSetRepresentation.endX = domainEndX;
		levelSetRepresentation.endY = domainEndY;

		setTestVals();
	}

	void setTestVals() {
		levelSetRepresentation.setTestVals();
	}

	void draw() {
		levelSetRepresentation.draw();
	}

};

/**
 * Test App
 */
class TestAppLevelSets2D : public GLApplication {
public:
	LevelSetFunction levelSetFunction;

	// constructor
	TestAppLevelSets2D();
	// destructor
	virtual ~TestAppLevelSets2D(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();


	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);

	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

};



