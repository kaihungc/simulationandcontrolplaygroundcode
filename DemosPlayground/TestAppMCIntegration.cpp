#include <GUILib/GLUtils.h>
#include "TestAppMCIntegration.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/Plane.h>
#include <FEMSimLib/CSTSimulationMesh2D.h>
#include <FEMSimLib/CSTSimulationMesh3D.h>
#include <FEMSimLib/MassSpringSimulationMesh3D.h>



TestAppMCIntegration::TestAppMCIntegration() {
	setWindowTitle("Test PDE Application...");

	TwAddSeparator(mainMenuBar, "sep2", "");

	showGroundPlane = false;

}

TestAppMCIntegration::~TestAppMCIntegration(void){
}

//triggered when mouse moves
bool TestAppMCIntegration::onMouseMoveEvent(double xPos, double yPos) {

	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;
	return false;
}

//triggered when mouse buttons are pressed
bool TestAppMCIntegration::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppMCIntegration::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppMCIntegration::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppMCIntegration::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppMCIntegration::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
	Logger::consolePrint("...but what to do with that?");
}

void TestAppMCIntegration::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestAppMCIntegration::process() {
	//do the work here...

	bool usedartThrowing = false;
	int upperLimit = 1000;
	if (usedartThrowing)
		upperLimit *= 100;


	for (int i = 0; i < upperLimit; i++) {
		double x = getRandomNumberInRange(-1, 1);
		double y = getRandomNumberInRange(-1, 1);
		double z = -1;
		if (usedartThrowing){
			bool dartTestPassed = true;
			for (uint k = 0; k < points.size(); k++)
				if (V3D(P3D(x, y, 0), P3D(points[k].x(), points[k].y(), 0)).length() < 0.01){
					dartTestPassed = false;
					break;
				}
			if (!dartTestPassed)
				continue;
		}

//		if (x*x + y*y < 1)
		double xp = 2.7 / 2.0 * x; double yp = 2.7 / 2.0 * y;
		if (pow(xp*xp+yp*yp-1, 3) - xp*xp*yp*yp*yp < 0)
			z = 1;

		points.push_back(P3D(x, y, z));
	}

	//the four is there because it has to do with the probability of drawing each sample. In 1D, the probability is 1/(b-a)! In 2D, it is 1/(b-a)/(c-d), where a, b, c and d are intervals along which we have to sample in x and y dimensions...
	double estimatedArea = 0;
	for (uint i = 0; i < points.size(); i++) {
		if (points[i].z() > 0)
			estimatedArea += 1.0 / points.size() * 4;
	}

	Logger::consolePrint("Total number of samples: %d. Estimated area: %lf\n", points.size(), estimatedArea);

}


// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppMCIntegration::drawScene() {
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glColor3d(1,1,1);


	glBegin(GL_QUADS);
		glVertex3d(-1, -1, 0);
		glVertex3d(1, -1, 0);
		glVertex3d(1, 1, 0);
		glVertex3d(-1, 1, 0);
	glEnd();

	glPointSize(3);
	glBegin(GL_POINTS);
	for (uint i = 0; i < points.size(); i++) {
		if (points[i].z() > 0)
			glColor3d(1, 0, 0);
		else
			glColor3d(0.7, 0.7, 0.7);
		glVertex3d(points[i].x(), points[i].y(), 0.0001);
	}
	glEnd();
	glPointSize(1);

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppMCIntegration::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppMCIntegration::restart() {

}

bool TestAppMCIntegration::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

