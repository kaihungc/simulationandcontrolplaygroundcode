#include <igl/signed_distance.h>
#include <igl/copyleft/marching_cubes.h>
#include "LevelSet.h"



LevelSet::LevelSet(AxisAlignedBoundingBox& _bbox, double _stepSize) : VertexGrid(_bbox, _stepSize, DBL_MAX)
{
	
}

LevelSet::LevelSet(LevelSet& LS, int multiplier)
{
	startPoint = LS.startPoint;
	endPoint = LS.endPoint + V3D(-1e-5, -1e-5, -1e-5);
	stepSize = LS.stepSize;
	for (int i = 0; i < 3; i++) {
		vertexNum[i] = (LS.vertexNum[i] - 1) * multiplier + 1;
		stepSize = min(stepSize, (endPoint[i] - startPoint[i]) / (vertexNum[i] - 1));
	}
		

	layerVertexNum = vertexNum[0] * vertexNum[1];
	totalVertexNum = layerVertexNum * vertexNum[2];
	
	data.resize(totalVertexNum);

	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				P3D v = getVertex(x, y, z);
				data[index++] = LS.interpolateData(v[0], v[1], v[2]);
			}
}


LevelSet::~LevelSet()
{
}

VertexGrid<V3D> LevelSet::getGradient()
{
	VertexGrid<V3D> grad;
	grad.totalVertexNum = totalVertexNum;
	grad.layerVertexNum = layerVertexNum;
	grad.startPoint = startPoint;
	grad.endPoint = endPoint;
	grad.stepSize = stepSize;
	for (int i = 0; i < 3; i++)
		grad.vertexNum[i] = vertexNum[i];
	grad.data.resize(totalVertexNum);

	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				V3D g;

				if (x == 0)
					g[0] = (getData(x + 1, y, z) - getData(x, y, z)) / stepSize;
				else if (x == vertexNum[0] - 1)
					g[0] = (getData(x, y, z) - getData(x - 1, y, z)) / stepSize;
				else
					g[0] = (getData(x + 1, y, z) - getData(x - 1, y, z)) / (2 * stepSize);

				if (y == 0)
					g[1] = (getData(x, y + 1, z) - getData(x, y, z)) / stepSize;
				else if (y == vertexNum[1] - 1)
					g[1] = (getData(x, y, z) - getData(x, y - 1, z)) / stepSize;
				else
					g[1] = (getData(x, y + 1, z) - getData(x, y - 1, z)) / (2 * stepSize);

				if (z == 0)
					g[2] = (getData(x, y, z + 1) - getData(x, y, z)) / stepSize;
				else if (z == vertexNum[2] - 1)
					g[2] = (getData(x, y, z) - getData(x, y, z - 1)) / stepSize;
				else
					g[2] = (getData(x, y, z + 1) - getData(x, y, z - 1)) / (2 * stepSize);

				grad.setData(x, y, z, g);
			}

	return grad;
}

V3D LevelSet::getGradientAt(int x, int y, int z)
{
	V3D g;

	if (x == 0)
		g[0] = (getData(x + 1, y, z) - getData(x, y, z)) / stepSize;
	else if (x == vertexNum[0] - 1)
		g[0] = (getData(x, y, z) - getData(x - 1, y, z)) / stepSize;
	else
		g[0] = (getData(x + 1, y, z) - getData(x - 1, y, z)) / (2 * stepSize);

	if (y == 0)
		g[1] = (getData(x, y + 1, z) - getData(x, y, z)) / stepSize;
	else if (y == vertexNum[1] - 1)
		g[1] = (getData(x, y, z) - getData(x, y - 1, z)) / stepSize;
	else
		g[1] = (getData(x, y + 1, z) - getData(x, y - 1, z)) / (2 * stepSize);

	if (z == 0)
		g[2] = (getData(x, y, z + 1) - getData(x, y, z)) / stepSize;
	else if (z == vertexNum[2] - 1)
		g[2] = (getData(x, y, z) - getData(x, y, z - 1)) / stepSize;
	else
		g[2] = (getData(x, y, z + 1) - getData(x, y, z - 1)) / (2 * stepSize);

	return g;
}

V3D LevelSet::interpolateGradient(double px, double py, double pz)
{
	if (IS_EQUAL(px, endPoint[0]))
		px -= 1e-6;
	if (IS_EQUAL(py, endPoint[1]))
		py -= 1e-6;
	if (IS_EQUAL(pz, endPoint[2]))
		pz -= 1e-6;
	int x = (int)((px - startPoint[0]) / stepSize);
	int y = (int)((py - startPoint[1]) / stepSize);
	int z = (int)((pz - startPoint[2]) / stepSize);

	double dx = (px - startPoint[0]) / stepSize - x;
	double dy = (py - startPoint[1]) / stepSize - y;
	double dz = (pz - startPoint[2]) / stepSize - z;

	double rx = 1 - dx;
	double ry = 1 - dy;
	double rz = 1 - dz;

	V3D V00 = rz * getGradientAt(x, y, z) + dz * getGradientAt(x, y, z + 1);
	V3D V01 = rz * getGradientAt(x, y + 1, z) + dz * getGradientAt(x, y + 1, z + 1);
	V3D V10 = rz * getGradientAt(x + 1, y, z) + dz * getGradientAt(x + 1, y, z + 1);
	V3D V11 = rz * getGradientAt(x + 1, y + 1, z) + dz * getGradientAt(x + 1, y + 1, z + 1);

	V3D V0 = ry * V00 + dy * V01;
	V3D V1 = ry * V10 + dy * V11;

	return rx * V0 + dx * V1;
}

void LevelSet::updateLevelSet(GLMesh* mesh, double(*updateFunc)(double origVal, double inputVal), bool updateAllVertices)
{
	AxisAlignedBoundingBox& bbox = mesh->getBoundingBox();
	Eigen::Vector3i minIndex, maxIndex, size;

	for (int i = 0; i < 3; i++)
	{
		if (updateAllVertices){
			minIndex[i] = 0;
			maxIndex[i] = vertexNum[i] - 1;
		}
		else {
			P3D bmin = bbox.bmin() + V3D(1, 1, 1) * -stepSize;
			P3D bmax = bbox.bmax() + V3D(1, 1, 1) * stepSize;
			minIndex[i] = max(0, (int)((bmin[i] - startPoint[i]) / stepSize));
			maxIndex[i] = min(vertexNum[i] - 1, (int)((bmax[i] - startPoint[i]) / stepSize) + 1);
		}
		
		size[i] = maxIndex[i] - minIndex[i] + 1;
	}

	Eigen::MatrixXd V, N;
	Eigen::MatrixXi F;
	mesh->getMeshMatrices(V, F, N);

	Eigen::MatrixXd P;
	P.resize(size[0] * size[1] * size[2], 3);
	int i = 0;
	for (int z = minIndex[2]; z <= maxIndex[2]; z++)
		for (int y = minIndex[1]; y <= maxIndex[1]; y++)
			for (int x = minIndex[0]; x <= maxIndex[0]; x++) {
				P.row(i++) = getVertex(x, y, z);
			}

	Eigen::VectorXd S;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;
	igl::signed_distance(P, V, F, igl::SIGNED_DISTANCE_TYPE_WINDING_NUMBER, S, I, C, N);

	i = 0;
	for (int z = minIndex[2]; z <= maxIndex[2]; z++)
		for (int y = minIndex[1]; y <= maxIndex[1]; y++)
			for (int x = minIndex[0]; x <= maxIndex[0]; x++) {
				double origVal = getData(x, y, z);
				setData(x, y, z, updateFunc ? updateFunc(origVal, S[i++]) : S[i++]);
			}
}

GLMesh* LevelSet::extractMesh(double levelSetVal)
{
	Eigen::MatrixXd P;
	Eigen::VectorXd S;
	P.resize(totalVertexNum, 3);
	S.resize(totalVertexNum);
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				P.row(index) = getVertex(x, y, z);
				S[index] = getData(index) - levelSetVal;
				index++;
			}

	Eigen::MatrixXd V;
	Eigen::MatrixXi F;
	igl::copyleft::marching_cubes(S, P, vertexNum[0], vertexNum[1], vertexNum[2], V, F);

	if (V.rows() == 0)
		return NULL;

	return new GLMesh(V, F);
}

void LevelSet::flip()
{
	for (int i = 0; i < totalVertexNum; i++)
	{
		data[i] *= -1;
	}
}

void LevelSet::addConstant(double val)
{
	for (int i = 0; i < totalVertexNum; i++)
	{
		if (data[i] < DBL_MAX)
			data[i] += val;
	}
}

void LevelSet::setConstant(double val)
{
	std::fill(data.begin(), data.end(), val);
}

void LevelSet::intersectLS(LevelSet* nLevelSet)
{
	for (int i = 0; i < totalVertexNum; i++)
	{
		data[i] = max(data[i], nLevelSet->getData(i));
	}
}

void LevelSet::unionLS(LevelSet* nLevelSet)
{
	for (int i = 0; i < totalVertexNum; i++)
	{
		data[i] = min(data[i], nLevelSet->getData(i));
	}
}

void LevelSet::minusLS(LevelSet* nLevelSet)
{
	for (int i = 0; i < totalVertexNum; i++)
	{
		data[i] = max(data[i], -nLevelSet->getData(i));
	}
}

void LevelSet::minusLSRelTrans(LevelSet* nLevelSet, Transformation& relTrans)
{
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				double val = data[index];
				if (val > 1e5) {
					index++;
					continue;
				}

				P3D v = getVertex(x, y, z);
				P3D nv = relTrans.transform(v);
				if (nLevelSet->isInside(nv[0], nv[1], nv[2]))
				{
					double signDist = nLevelSet->interpolateData(nv[0], nv[1], nv[2]);
					data[index] = max(val, -signDist);
				}			
				index++;
			}
}

void LevelSet::initCylinder(V3D axis, P3D center, double radius)
{
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				V3D v = center - getVertex(x, y, z);
				double dist = (v - axis * v.dot(axis)).norm();
				data[index] = dist - radius;
				index++;
			}
}

void LevelSet::minusCapsule(Segment& seg, double radius)
{
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				double val = data[index];
				if (val > 1e5) {
					index++;
					continue;
				}

				P3D v = getVertex(x, y, z);			
				double signDist = (v - seg.getClosestPointTo(v)).norm() - radius;
				data[index] = max(val, -signDist);
				index++;
			}
}

void LevelSet::meanCurvatureFlow(double t, double v)
{
	for (int z = 1; z < vertexNum[2] - 1; z++)
		for (int y = 1; y < vertexNum[1] - 1; y++)
			for (int x = 1; x < vertexNum[0] - 1; x++) {

				double val = getData(x, y, z);
				if (fabs(val) > stepSize) continue;

				double L = (getData(x + 1, y, z) + getData(x - 1, y, z) + getData(x, y + 1, z)
					 + getData(x, y - 1, z) + getData(x, y, z + 1) + getData(x, y, z - 1) - 6 * val) / (stepSize * stepSize);

				double n = -L * t + v;
				addData(x, y, z, -n);
			}

	fastMarchingInward(0.3 * stepSize);
	fastMarchingOutward(0.3 * stepSize, 2 * stepSize);

}


void LevelSet::saveToFile(FILE* fp)
{
	fprintf(fp, "%lf %lf %lf\n", startPoint[0], startPoint[1], startPoint[2]);
	fprintf(fp, "%lf %lf %lf\n", endPoint[0], endPoint[1], endPoint[2]);
	fprintf(fp, "%lf %d %d %d %d %d\n", stepSize, totalVertexNum, layerVertexNum
		, vertexNum[0], vertexNum[1], vertexNum[2]);

	for (int i = 0; i < totalVertexNum; i++)
	{
		if (data[i] == DBL_MAX)
		{
			fprintf(fp, "%lf ", 1e4);
		}
		else {
			fprintf(fp, "%lf ", data[i]);
		}
	}
	fprintf(fp, "\n");
}

void LevelSet::loadFromFile(FILE* fp)
{
	fscanf(fp, "%lf %lf %lf\n", &startPoint[0], &startPoint[1], &startPoint[2]);
	fscanf(fp, "%lf %lf %lf\n", &endPoint[0], &endPoint[1], &endPoint[2]);
	fscanf(fp, "%lf %d %d %d %d %d\n", &stepSize, &totalVertexNum, &layerVertexNum
		, &vertexNum[0], &vertexNum[1], &vertexNum[2]);

	data.resize(totalVertexNum);

	for (int i = 0; i < totalVertexNum; i++)
	{
		double tmp;
		fscanf(fp, "%lf ", &tmp);
		if (tmp == 1e4)
		{
			data[i] = DBL_MAX;
		}
		else {
			data[i] = tmp;
		}
	}
}

void LevelSet::saveSparseToFile(FILE* fp)
{
	fprintf(fp, "%lf %lf %lf\n", startPoint[0], startPoint[1], startPoint[2]);
	fprintf(fp, "%lf %lf %lf\n", endPoint[0], endPoint[1], endPoint[2]);
	fprintf(fp, "%lf %d %d %d %d %d\n", stepSize, totalVertexNum, layerVertexNum
		, vertexNum[0], vertexNum[1], vertexNum[2]);

	int totalNonZero = 0;
	for (int i = 0; i < totalVertexNum; i++) {
		if (data[i] < DBL_MAX)
			totalNonZero++;
	}
	fprintf(fp, "%d\n", totalNonZero);

	for (int i = 0; i < totalVertexNum; i++)
	{
		if (data[i] < DBL_MAX)
			fprintf(fp, "%d %lf\n", i, data[i]);
	}
	fprintf(fp, "\n");
}

void LevelSet::loadSparseFromFile(FILE* fp)
{
	int totalNonZero;
	fscanf(fp, "%lf %lf %lf\n", &startPoint[0], &startPoint[1], &startPoint[2]);
	fscanf(fp, "%lf %lf %lf\n", &endPoint[0], &endPoint[1], &endPoint[2]);
	fscanf(fp, "%lf %d %d %d %d %d\n", &stepSize, &totalVertexNum, &layerVertexNum
		, &vertexNum[0], &vertexNum[1], &vertexNum[2]);
	fscanf(fp, "%d\n", &totalNonZero);

	data.assign(totalVertexNum, DBL_MAX);

	for (int i = 0; i < totalNonZero; i++)
	{
		int index;
		double val;
		fscanf(fp, "%d %lf", &index, &val);
		data[index] = val;
	}
}

void LevelSet::fastMarchingInward(double boundaryWidth, double threshold)
{
	DIInQueue Q;

	// status of each vertex, -1:invalid, 0:unvisited, 1:seed, 2:fixed.
	vector<int> VStatus(totalVertexNum, -1);

	for (int i = 0; i < totalVertexNum; i++)
	{
		double val = data[i];
		if (val <= boundaryWidth && val >= -boundaryWidth)
		{
			VStatus[i] = 1;
			Q.push(DIPair(val, i));
		}
		else if (data[i] < 0) 
		{
			VStatus[i] = 0;
			data[i] = -DBL_MAX;
		}
	}

	while (!Q.empty())
	{
		DIPair DI = Q.top();
		Q.pop();
		int index = DI.index;

		if (VStatus[index] == 2) continue; // means already fixed.
		VStatus[index] = 2;

		int xy = index % layerVertexNum;
		int z = index / layerVertexNum;		
		int x = xy % vertexNum[0];
		int y = xy / vertexNum[0];

		FMConsiderVertexInward(Q, VStatus, x + 1, y, z, threshold);
		FMConsiderVertexInward(Q, VStatus, x - 1, y, z, threshold);
		FMConsiderVertexInward(Q, VStatus, x, y + 1, z, threshold);
		FMConsiderVertexInward(Q, VStatus, x, y - 1, z, threshold);
		FMConsiderVertexInward(Q, VStatus, x, y, z + 1, threshold);
		FMConsiderVertexInward(Q, VStatus, x, y, z - 1, threshold);
	}
}

void LevelSet::FMConsiderVertexInward(DIInQueue& Q, vector<int>& VStatus, int x, int y, int z, double threshold)
{
	if (x <= 0 || x >= vertexNum[0] - 1 || y <= 0 || y >= vertexNum[1] - 1 || z <= 0 || z >= vertexNum[2] - 1)
		return;

	int index = z * layerVertexNum + y * vertexNum[0] + x;
	if (VStatus[index] != 0) return;

	double T[3];
	T[0] = max(getData(x - 1, y, z), getData(x + 1, y, z));
	T[1] = max(getData(x, y - 1, z), getData(x, y + 1, z));
	T[2] = max(getData(x, y, z - 1), getData(x, y, z + 1));

	double A = 0;
	double B = 0;
	
	int n = 0;
	for (int i = 0; i < 3; i++)
	{
		if (T[i] > -1e10)
		{
			A += T[i];
			B += T[i] * T[i];
			n++;
		}		
	}
	if (n == 0)
		return;

	double dist = -DBL_MAX;

	if (n == 1)
	{
		dist = A - stepSize;
	}
	else {
		double nInv = 1.0 / n;
		double delta = A*A - n*(B - stepSize * stepSize);

		if (delta < 0)
		{
			if (n == 3)
			{
				A = T[0] + T[1]; B = T[0] * T[0] + T[1] * T[1];
				delta = A*A - 2*(B - stepSize * stepSize);
				if (delta >= 0) {
					dist = max(dist, 0.5 * (A - sqrt(delta)));
				}
				
				A = T[1] + T[2]; B = T[1] * T[1] + T[2] * T[2];
				delta = A*A - 2*(B - stepSize * stepSize);
				if (delta >= 0) {
					dist = max(dist, 0.5 * (A - sqrt(delta)));
				}

				A = T[0] + T[2]; B = T[0] * T[0] + T[2] * T[2];
				delta = A*A - 2*(B - stepSize * stepSize);
				if (delta >= 0) {
					dist = max(dist, 0.5 * (A - sqrt(delta)));
				}
			}
			if (n == 2) {
				for (int i = 0; i < 3; i++)
				{
					if (T[i] > -1e10)
					{
						dist = max(dist, T[i] - stepSize);
					}
				}
			}
		}
		else
			dist = nInv * (A - sqrt(delta));
	}
	
	if (dist > data[index] && dist > threshold)
	{
		data[index] = dist;
		Q.push(DIPair(dist, index));
	}

}

void LevelSet::fastMarchingOutward(double boundaryWidth, double threshold)
{
	DIOutQueue Q;

	// status of each vertex, -1:invalid, 0:unvisited, 1:seed, 2:fixed.
	vector<int> VStatus(totalVertexNum, -1);

	for (int i = 0; i < totalVertexNum; i++)
	{
		double val = data[i];
		if (val <= boundaryWidth && val >= -boundaryWidth)
		{
			VStatus[i] = 1;
			Q.push(DIPair(val, i));
		}
		else if (data[i] > 0)
		{
			VStatus[i] = 0;
			data[i] = DBL_MAX;
		}
	}

	while (!Q.empty())
	{
		DIPair DI = Q.top();
		Q.pop();
		int index = DI.index;

		if (VStatus[index] == 2) continue; // means already fixed.
		VStatus[index] = 2;

		int xy = index % layerVertexNum;
		int z = index / layerVertexNum;
		int x = xy % vertexNum[0];
		int y = xy / vertexNum[0];

		FMConsiderVertexOutward(Q, VStatus, x + 1, y, z, threshold);
		FMConsiderVertexOutward(Q, VStatus, x - 1, y, z, threshold);
		FMConsiderVertexOutward(Q, VStatus, x, y + 1, z, threshold);
		FMConsiderVertexOutward(Q, VStatus, x, y - 1, z, threshold);
		FMConsiderVertexOutward(Q, VStatus, x, y, z + 1, threshold);
		FMConsiderVertexOutward(Q, VStatus, x, y, z - 1, threshold);
	}
}

void LevelSet::FMConsiderVertexOutward(DIOutQueue& Q, vector<int>& VStatus, int x, int y, int z, double threshold)
{
	if (x <= 0 || x >= vertexNum[0] - 1 || y <= 0 || y >= vertexNum[1] - 1 || z <= 0 || z >= vertexNum[2] - 1)
		return;

	int index = z * layerVertexNum + y * vertexNum[0] + x;
	if (VStatus[index] != 0) return;

	double T[3];
	T[0] = min(getData(x - 1, y, z), getData(x + 1, y, z));
	T[1] = min(getData(x, y - 1, z), getData(x, y + 1, z));
	T[2] = min(getData(x, y, z - 1), getData(x, y, z + 1));

	double A = 0;
	double B = 0;

	int n = 0;
	for (int i = 0; i < 3; i++)
	{
		if (T[i] < 1e10)
		{
			A += T[i];
			B += T[i] * T[i];
			n++;
		}
	}
	if (n == 0)
		return;

	double dist = DBL_MAX;

	if (n == 1)
	{
		dist = A + stepSize;
	}
	else {
		double nInv = 1.0 / n;
		double delta = A*A - n*(B - stepSize * stepSize);

		if (delta < 0)
		{
			if (n == 3)
			{
				A = T[0] + T[1]; B = T[0] * T[0] + T[1] * T[1];
				delta = A*A - 2 * (B - stepSize * stepSize);
				if (delta >= 0) {
					dist = min(dist, 0.5 * (A + sqrt(delta)));
				}

				A = T[1] + T[2]; B = T[1] * T[1] + T[2] * T[2];
				delta = A*A - 2 * (B - stepSize * stepSize);
				if (delta >= 0) {
					dist = min(dist, 0.5 * (A + sqrt(delta)));
				}

				A = T[0] + T[2]; B = T[0] * T[0] + T[2] * T[2];
				delta = A*A - 2 * (B - stepSize * stepSize);
				if (delta >= 0) {
					dist = min(dist, 0.5 * (A + sqrt(delta)));
				}
			}
			if (n == 2) {
				for (int i = 0; i < 3; i++)
				{
					if (T[i] != DBL_MAX)
					{
						dist = min(dist, T[i] + stepSize);
					}
				}
			}
		}
		else
			dist = nInv * (A + sqrt(delta));
	}

	if (dist < data[index] && dist < threshold)
	{
		data[index] = dist;
		Q.push(DIPair(dist, index));
	}

}

void getTwoLeastNumber(vector<double>& vals, double& min1, double& min2)
{
	min1 = vals[0];
	min2 = vals[1];
	if (min1 > min2) {
		swap(min1, min2);
	}

	for (uint k = 2; k < vals.size(); k++)
	{
		double val = vals[k];
		if (val < min1)
		{
			min2 = min1;
			min1 = val;
		}
		else if (val < min2)
		{
			min2 = val;
		}
	}
}

void getTwoLeastNumberEx(vector<double>& vals, double& min1, double& min2, int& minIndex1, int& minIndex2)
{
	minIndex1 = 0;
	minIndex2 = 1;
	min1 = vals[0];
	min2 = vals[1];
	if (min1 > min2) {
		swap(min1, min2);
		swap(minIndex1, minIndex2);
	}

	for (uint k = 2; k < vals.size(); k++)
	{
		double val = vals[k];
		if (val < min1)
		{
			min2 = min1;
			min1 = val;
			minIndex2 = minIndex1;
			minIndex1 = k;
		}
		else if (val < min2)
		{
			min2 = val;
			minIndex2 = k;
		}
	}
}

bool projectMultiLevelSetsForVertex(vector<LevelSet>& levelSets, int index)
{
	int LSNum = (int)levelSets.size();
	vector<double> LSVals;
	vector<bool> flags(LSNum, false);
	for (int j = 0; j < LSNum; j++)
	{
		double val = levelSets[j].getData(index);
		if (fabs(val) > 1e10)
		{
			flags[j] = true;
		}
		else {
			LSVals.push_back(val);
		}		
	}
	if (LSVals.size() < 2)
	{
		return false;
	}

	double min1, min2;
	getTwoLeastNumber(LSVals, min1, min2);

	double avg = 0.5 * (min1 + min2);
	if (avg < 0) {
		for (int k = 0; k < LSNum; k++) {
			if (flags[k]) continue;
			levelSets[k].addData(index, -avg);
		}
		return true;
	}

	return false;
}

void projectMultiLevelSets(vector<LevelSet>& levelSets)
{
	int* vertexNum = levelSets[0].vertexNum;
	int totalVertexNum = levelSets[0].totalVertexNum;

#pragma omp parallel for
	for (int i = 0; i < totalVertexNum; i++)
	{
		int LSNum = (int)levelSets.size();
		vector<double> LSVals;
		vector<bool> flags(LSNum, false);
		for (int j = 0; j < LSNum; j++)
		{
			double val = levelSets[j].getData(i);
			if (fabs(val) > 1e10)
			{
				flags[j] = true;
			}
			else {
				LSVals.push_back(val);
			}
		}
		if (LSVals.size() < 2)
		{
			continue;
		}

		double min1, min2;
		getTwoLeastNumber(LSVals, min1, min2);

		double avg = 0.5 * (min1 + min2);
		if (avg < 0) {
			for (int k = 0; k < LSNum; k++) {
				if (flags[k]) continue;
				levelSets[k].addData(i, -avg);
			}
		}
	}
}

void projectMultiLevelSetsEx(vector<LevelSet>& levelSets, int index1, int index2)
{
	int* vertexNum = levelSets[0].vertexNum;
	int totalVertexNum = levelSets[0].totalVertexNum;
	double stepSize = levelSets[0].stepSize;

#pragma omp parallel for
	for (int i = 0; i < totalVertexNum; i++)
	{
		double LS1Val = levelSets[index1].getData(i);
		double LS2Val = levelSets[index2].getData(i);
		if ((LS1Val > stepSize && LS2Val > stepSize) || LS1Val < -stepSize || LS2Val < -stepSize)
			continue;

		int LSNum = (int)levelSets.size();
		vector<double> LSVals;
		vector<bool> flags(LSNum, false);
		int count = 0;
		for (int j = 0; j < LSNum; j++)
		{
			double val;
			if (j == index1)
				val = LS1Val;
			else if (j == index2)
				val = LS2Val;
			else
				val = levelSets[j].getData(i);

			if (fabs(val) > 1e10)
				flags[j] = true;
			else
				count++;
			LSVals.push_back(val);
		}
		if (count < 2) continue;

		double min1, min2;
		int minIndex1, minIndex2;
		getTwoLeastNumberEx(LSVals, min1, min2, minIndex1, minIndex2);

		double avg = 0.5 * (min1 + min2);
		if (avg < 0) {
			if ((minIndex1 == index1 && minIndex2 == index2) ||
				(minIndex1 == index2 && minIndex2 == index1))
			{
				for (int k = 0; k < LSNum; k++) {
					if (flags[k]) continue;
					levelSets[k].addData(i, -avg);
				}
			}
			else if (minIndex1 == index1 || minIndex2 == index1)
			{
				levelSets[index1].addData(i, -2 * avg);
			}
			else if (minIndex1 == index2 || minIndex2 == index2)
			{
				levelSets[index2].addData(i, -2 * avg);
			}
		}
	}
}

void projectMultiLevelSetsWithFixedLS(vector<LevelSet>& levelSets, vector<bool>& fixedLS)
{
	int* vertexNum = levelSets[0].vertexNum;
	int totalVertexNum = levelSets[0].totalVertexNum;

#pragma omp parallel for
	for (int i = 0; i < totalVertexNum; i++)
	{
		int LSNum = (int)levelSets.size();
		vector<double> LSVals;
		vector<bool> flags(LSNum, false);
		int count = 0;
		for (int j = 0; j < LSNum; j++)
		{
			double val = levelSets[j].getData(i);
			if (fabs(val) > 1e10)
				flags[j] = true;
			else
				count++;
			LSVals.push_back(val);
		}
		if (count < 2)
		{
			continue;
		}

		double min1, min2;
		int minIndex1, minIndex2;
		getTwoLeastNumberEx(LSVals, min1, min2, minIndex1, minIndex2);

		double avg = 0.5 * (min1 + min2);
		if (avg < 0) {
			bool findIndex1 = fixedLS[minIndex1];
			bool findIndex2 = fixedLS[minIndex2];
			if ((findIndex1 && findIndex2) || !(findIndex1 || findIndex2))
			{
				for (int k = 0; k < LSNum; k++) {
					if (flags[k]) continue;
					levelSets[k].addData(i, -avg);
				}
			}
			else if (findIndex1)
			{
				levelSets[minIndex2].addData(i, -2 * avg);
			}
			else if (findIndex2)
			{
				levelSets[minIndex1].addData(i, -2 * avg);
			}
		}
	}
}

VertexGrid<Matrix3x3> getHessian(VertexGrid<V3D>& grad)
{
	VertexGrid<Matrix3x3> H;
	H.totalVertexNum = grad.totalVertexNum;
	H.layerVertexNum = grad.layerVertexNum;
	H.startPoint = grad.startPoint;
	H.endPoint = grad.endPoint;
	H.stepSize = grad.stepSize;
	double stepSize = H.stepSize;
	int* vertexNum = grad.vertexNum;
	for (int i = 0; i < 3; i++)
		H.vertexNum[i] = vertexNum[i];
	H.data.resize(grad.totalVertexNum);

	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {
				Matrix3x3 M;

				if (x == 0)
					M.col(0) = (grad.getData(x + 1, y, z) - grad.getData(x, y, z)) / stepSize;
				else if (x == vertexNum[0] - 1)
					M.col(0) = (grad.getData(x, y, z) - grad.getData(x - 1, y, z)) / stepSize;
				else
					M.col(0) = (grad.getData(x + 1, y, z) - grad.getData(x - 1, y, z)) / (2 * stepSize);

				if (y == 0)
					M.col(1) = (grad.getData(x, y + 1, z) - grad.getData(x, y, z)) / stepSize;
				else if (y == vertexNum[1] - 1)
					M.col(1) = (grad.getData(x, y, z) - grad.getData(x, y - 1, z)) / stepSize;
				else
					M.col(1) = (grad.getData(x, y + 1, z) - grad.getData(x, y - 1, z)) / (2 * stepSize);

				if (z == 0)
					M.col(2) = (grad.getData(x, y, z + 1) - grad.getData(x, y, z)) / stepSize;
				else if (z == vertexNum[2] - 1)
					M.col(2) = (grad.getData(x, y, z) - grad.getData(x, y, z - 1)) / stepSize;
				else
					M.col(2) = (grad.getData(x, y, z + 1) - grad.getData(x, y, z - 1)) / (2 * stepSize);


				H.setData(x, y, z, 0.5 * (M + M.transpose()));
			}

	return H;
}
