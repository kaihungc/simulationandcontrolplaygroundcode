/*
  A triangle
*/

#pragma once

#include <math.h>
#include <vector>
#include <iostream>
#include "boundingBox.h"
#include "Quaternion.h"

// todo: one of these classes needs to be indexed as well... so that we can directly use this in the TriMesh...
// todo: tri mesh needs to make use of bounding box and so on. Code reuse is poor...

class TriangleBasic{
public:

  TriangleBasic(P3D first_g, P3D second_g, P3D third_g): 
	first_(first_g), second_(second_g), third_(third_g), index_(0) {}

  // accessors
  inline P3D first() {return first_ ;}
  inline P3D second() {return second_ ;}
  inline P3D third() {return third_ ;}
  inline int index() { return index_;}

  inline void setIndex(int index) {
	  index_ = index;
  }

  // squared 3d distance to a point
  double distanceToPoint2(P3D point) { std::cout << "Unimplemented..." << std::endl; return 1;} // unimplemented (it is implemented in class "TriangleWithCollisionInfo" below)
  double distanceToPoint(P3D point) { return sqrt(distanceToPoint2(point));}
  
  bool doesIntersectBox(AxisAlignedBoundingBox & bbox);

  int lineSegmentIntersection(P3D segmentStart, P3D segmentEnd, P3D * intersectionPoint);
    //    Output: intersection point (when it exists)
    //    Return: -1 = triangle is degenerate (a segment or point)
    //             0 = disjoint (no intersect)
    //             1 = intersect in unique point I1
    //             2 = are in the same plane


  void render();
  void renderEdges();

  P3D getBarycentricLocation(double alpha, double beta, double gamma) { return first_ * alpha + second_ * beta + third_ * gamma; }

protected:
  P3D first_, second_, third_;
  int index_;
};
