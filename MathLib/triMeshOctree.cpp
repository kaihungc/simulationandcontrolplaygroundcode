//  builds an octree on top of the geometry of a triangle mesh
//  Can be intersected with a sphere or a line segment. 
#include "triMeshOctree.h"

using namespace std;

template<class TriangleClass>
const double TriMeshOctree<TriangleClass>::bboxExpansionRatio = 1.05;

template<class TriangleClass>
TriMeshOctree<TriangleClass>::TriMeshOctree( TriMesh * triMeshIn, int maxNumTrianglesInLeafNode_, int maxTreeDepth_, int printInfo ) : 
  maxNumTrianglesInLeafNode(maxNumTrianglesInLeafNode_), maxTreeDepth(maxTreeDepth_) {
  // copy mesh
  TriMesh * triMesh = new TriMesh(*triMeshIn);
 
  int triangleIndex = 0;
  triangles.clear();
    for (uint j=0; j < triMesh->getNumFaces(); j++) // over all faces
    {
      P3D p0 = triMesh->getPosition(triMesh->faces[j].v1Index);
      P3D p1 = triMesh->getPosition(triMesh->faces[j].v2Index);
      P3D p2 = triMesh->getPosition(triMesh->faces[j].v3Index);
      TriangleClass triangle(p0,p1,p2);
      triangle.setIndex(triangleIndex); // 0-indexed
      triangleIndex++;
      triangles.push_back(triangle);
    }

  cout << "Total number of triangles is: " << triangles.size() << endl;

  // build the octree
  cout << "Building the octree data structure... " << endl;
  root = new Octree<TriangleClass>(maxTreeDepth); 
  root->setBuildPrintInfo(printInfo);
  root->build(triangles, maxNumTrianglesInLeafNode);

  if(printInfo == 1)
  {
    int numMaxDepthExceededCases;
    int numMaxTriInDepthExceededCases;
    root->getBuildInfo(&numMaxDepthExceededCases, &numMaxTriInDepthExceededCases);
    printf("Total number of cells with more than %d triangles: %d. Max triangles in such cells: %d.\n", maxNumTrianglesInLeafNode, numMaxDepthExceededCases, numMaxTriInDepthExceededCases);
  }

  triangles.clear(); // release memory
  delete(triMesh);
}


template TriMeshOctree<TriangleBasic>::TriMeshOctree( TriMesh * triMesh, int maxNumTrianglesInLeafNode_, int maxTreeDepth_, int printInfo );  
//template TriMeshOctree<TriangleWithCollisionInfo>::TriMeshOctree( TriMesh * triMesh, int maxNumTrianglesInLeafNode_, int maxTreeDepth_, int printInfo );  

