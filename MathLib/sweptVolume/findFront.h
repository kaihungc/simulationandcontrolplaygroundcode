#pragma once

#include <vector>
#include <deque>
#include "DistanceFields.h"

typedef vector<bool> VB;

class findFront
{
public:
	static void solve(const VD &dis, VB &outside, double step, int, int, int);
};