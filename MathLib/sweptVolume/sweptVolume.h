#pragma once
#include "DisjointSet.h"
#include "DistanceFields.h"
#include "findFront.h"
#include "genMesh.h"
#include "MarchingCubes.h"

class sweptVolume
{
	genMesh generator;
public:
	void loadTrajectory(const Quaternion &q0, const Quaternion &q1, const P3D &p0, const P3D &p1);
	void solve(GLMesh *ori, GLMesh* &des, double gridSize = 0.05);
    bool detectConnected(GLMesh* mesh);
};