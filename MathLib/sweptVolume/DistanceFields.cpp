#include "DistanceFields.h"

#define ENCODE(x,y,z,k) (((((x)*ySum+(y))*zSum)+(z))*6+(k)) 
#define ENCODETRI(x,y,z) ((((x)*(ySum+1)+(y))*(zSum+1))+(z))
#define EPS_DIS 1e-12

DistanceFields::DistanceFields(GLMesh *mesh)
{
	this->mesh = mesh->clone();
	dx = 1;
	//calculate();
}

DistanceFields::~DistanceFields()
{
	delete mesh;
}

double DistanceFields::getDistanceToPlane(const Ray& ray, const Plane& plane, P3D *closestPtOnRay) {
	//check to see if the ray is parallel to the plane...
	if (fabs(ray.direction.dot(plane.n)) < TINY) {
		if (closestPtOnRay)
			*closestPtOnRay = ray.origin;
		return 2e9;
	}

	//we know that p = origin + t * direction lies on the plane, which means that dot product between plane p and p, dotted with n is 0...
	V3D tmpV = plane.p - ray.origin;
	double t = tmpV.dot(plane.n) / ray.direction.dot(plane.n);

	//check the solution now...
	//assert(IS_ZERO(V3D(plane.p, origin + direction * t).dot(plane.n)));

	if (t < -EPS_DIS) {
		*closestPtOnRay = ray.origin;
		return 2e9;
	}

	if (closestPtOnRay)
		*closestPtOnRay = ray.origin + ray.direction * t;

	return max(0.0, t);
}
void DistanceFields::fun(double y, double ny1, double ny2, double nz1, double nz2, double &t1, double &t2)
{
	if (y < MAX(ny1, ny2) + EPS_DIS && y > MIN(ny1, ny2) - EPS_DIS) {
		double t;
		if (ny1 < ny2) {
			t = (nz2 * (y - ny1) + nz1 * (ny2 - y)) / (ny2 - ny1);
		} else {
			t = (nz1 * (y - ny2) + nz2 * (ny1 - y)) / (ny1 - ny2);
		}
		//printf("%d, %d, %d, %d, %d, %d\n", y, ny1, ny2, nz1, nz2, t);
		if (t < t1) t1 = t;
		if (t > t2) t2 = t;
	}
}
bool DistanceFields::pointIsInTri(P3D res, P3D p1, P3D p2, P3D p3)
{
	V3D v1(res, p1), v2(res, p2), v3(res, p3);
	V3D v4, v5, v6;
	v4 = v1.cross(v2);
	v5 = v2.cross(v3);
	v6 = v3.cross(v1);
	bool flag = v4.dot(v5) >= -EPS_DIS && v5.dot(v6) >= -EPS_DIS && v6.dot(v4) >= -EPS_DIS;
	return flag;
}

void DistanceFields::calculate()
{
	//FILE *fp = fopen("df.txt", "w");
	minX = minY = minZ = 2e9;
	maxX = maxY = maxZ = -2e9;
	for (int i = 0; i < mesh->getVertexCount(); i++) {
		P3D p = mesh->getVertex(i);
		if (p.x() < minX) minX = p.x();
		if (p.y() < minY) minY = p.y();
		if (p.z() < minZ) minZ = p.z();
		if (p.x() > maxX) maxX = p.x();
		if (p.y() > maxY) maxY = p.y();
		if (p.z() > maxZ) maxZ = p.z();
	}
	printf("1\n");
	minX -= 2 * dx; minY -= 2 * dx; minZ -= 2 * dx;
	maxX += 2 * dx; maxY += 2 * dx; maxZ += 2 * dx;
	xSum = (int)((maxX - minX) / dx + 1);
	ySum = (int)((maxY - minY) / dx + 1);
	zSum = (int)((maxZ - minZ) / dx + 1);
	xyzSum = MAX(MAX(xSum, ySum), zSum);
	dis.resize(xSum*ySum*zSum*6);
	for (uint i = 0; i < dis.size(); i++) {
		dis[i] = 2e9;
	}
	printf("2\n");

	for (int i = 0; i < 6; i++) {
		findTri[i].resize((xSum + 1)*(ySum+1)*(zSum+1));
		for (uint j = 0; j < findTri[i].size(); j++) {
			findTri[i][j] = 2e9;
		}
	}

	printf("3\n");
	printf("mesh->getPolyCount() = %d\n", mesh->getPolyCount());
	for (int i = 0; i < mesh->getPolyCount(); i++) {
		GLIndexedTriangle tri = mesh->getTriangle(i);
		int x1, y1, z1, x2, y2, z2;
		x1 = y1 = z1 = (int)2e9;
		x2 = y2 = z2 = (int)-2e9;
		P3D p1, p2, p3;
		p1 = mesh->getVertex(tri.indexes[0]);
		p2 = mesh->getVertex(tri.indexes[1]);
		p3 = mesh->getVertex(tri.indexes[2]);

		//printf("TRI\n");
		//for (int k = 0; k < 3; k++) {
		//	printf("%.3lf %.3lf %.3lf\n", mesh->getVertex(tri.indexes[k]).x(), mesh->getVertex(tri.indexes[k]).y(), mesh->getVertex(tri.indexes[k]).z());
		//}

		Plane plane(p1, p2, p3);
		double nx1 = p1.x(), nx2 = p2.x(), nx3 = p3.x();
		double ny1 = p1.y(), ny2 = p2.y(), ny3 = p3.y();
		double nz1 = p1.z(), nz2 = p2.z(), nz3 = p3.z();
		for (uint j = 0; j < 3; j++) {
			P3D p = mesh->getVertex(tri.indexes[j]);
			int k;
			k = (int)((p.x() - minX) / dx);
			if (k < x1) x1 = k;
			if (k > x2) x2 = k;
			k = (int)((p.y() - minY) / dx);
			if (k < y1) y1 = k;
			if (k > y2) y2 = k;
			k = (int)((p.z() - minZ) / dx);
			if (k < z1) z1 = k;
			if (k > z2) z2 = k;
		}
		// x+ x-
		for (int y = y1 - 1; y <= y2 + 1; y++) {
			//printf("y = %d\n", y);
			double t1 = 2e9, t2 = -2e9;
			if (fabs(ny1 - ny2) > EPS_DIS) fun(y*dx + minY, ny1, ny2, nz1, nz2, t1, t2);
			if (fabs(ny1 - ny3) > EPS_DIS) fun(y*dx + minY, ny1, ny3, nz1, nz3, t1, t2);
			if (fabs(ny2 - ny3) > EPS_DIS) fun(y*dx + minY, ny2, ny3, nz2, nz3, t1, t2);
			//if (y == 11) printf("z = %lf, %lf\n", t1, t2);
			for (int z = int((t1-minZ)/dx)-1; z <= (t2-minZ)/dx + 1; z++) {
				P3D ori, res; double dis;
				ori = P3D(minX, minY + y*dx, minZ + z*dx);
				dis = getDistanceToPlane(Ray(ori, V3D(1, 0, 0)), plane, &res);
				int ddx = int((dis / dx) + EPS_DIS);
				//if (z == 2 && y == 11) {
				//	printf("dis = %.3lf", dis);
				//}
				if (pointIsInTri(res, p1, p2, p3)) {
					if (dis < 1e9) {
						if (dis < findTri[0][ENCODETRI(ddx, y, z)]) {
							findTri[0][ENCODETRI(ddx, y, z)] = dis - ddx * dx;
						}
						if (dis - ddx*dx > EPS_DIS) {
							if (maxX - minX - dis < findTri[1][ENCODETRI(ddx+1, y, z)]) {
								findTri[1][ENCODETRI(ddx+1, y, z)] = (ddx+1)*dx - dis;
							}
						} else {
							findTri[1][ENCODETRI(ddx, y, z)] = 0;
						}
					}
				}
			}
		}
		// y+ y-
		for (int x = x1 - 1; x <= x2 + 1; x++) {
			double t1 = 2e9, t2 = -2e9;
			if (fabs(nx1 - nx2) > EPS_DIS) fun(x*dx + minX, nx1, nx2, nz1, nz2, t1, t2);
			if (fabs(nx1 - nx3) > EPS_DIS) fun(x*dx + minX, nx1, nx3, nz1, nz3, t1, t2);
			if (fabs(nx2 - nx3) > EPS_DIS) fun(x*dx + minX, nx2, nx3, nz2, nz3, t1, t2);
			for (int z = int((t1 - minZ) / dx)-1; z <= (t2 - minZ) / dx + 1; z++) {
				P3D ori, res; double dis;
				ori = P3D(minX + x*dx, minY, minZ + z*dx);
				dis = getDistanceToPlane(Ray(ori, V3D(0, 1, 0)), plane, &res);
				int ddx = int((dis / dx) + EPS_DIS);
				if (pointIsInTri(res, p1, p2, p3)) {
					if (dis < 1e9) {
						if (dis < findTri[2][ENCODETRI(x, ddx, z)]) {
							findTri[2][ENCODETRI(x, ddx, z)] = dis - ddx*dx;
						}
						if (dis - ddx*dx > EPS_DIS) {
							if (maxY - minY - dis < findTri[3][ENCODETRI(x, ddx+1, z)]) {
								findTri[3][ENCODETRI(x, ddx+1, z)] = (ddx+1)*dx - dis;
							}
						}
						else {
							findTri[3][ENCODETRI(x, ddx, z)] = 0;
						}
					}
				}
			}
		}
		// z+ z-
		for (int x = x1 - 1; x <= x2 + 1; x++) {
			double t1 = 2e9, t2 = -2e9;
			if (fabs(nx1 - nx2) > EPS_DIS) fun(x*dx + minX, nx1, nx2, ny1, ny2, t1, t2);
			if (fabs(nx1 - nx3) > EPS_DIS) fun(x*dx + minX, nx1, nx3, ny1, ny3, t1, t2);
			if (fabs(nx2 - nx3) > EPS_DIS) fun(x*dx + minX, nx2, nx3, ny2, ny3, t1, t2);
			for (int y = int((t1 - minY) / dx)-1; y <= (t2 - minY) / dx + 1; y++) {
				P3D ori, res; double dis;
				ori = P3D(minX + x*dx, minY + y*dx, minZ);
				dis = getDistanceToPlane(Ray(ori, V3D(0, 0, 1)), plane, &res);
				int ddx = int((dis / dx) + EPS_DIS);
				if (pointIsInTri(res, p1, p2, p3)) {
					if (dis < 1e9) {
						if (dis < findTri[4][ENCODETRI(x, y, ddx)]) {
							findTri[4][ENCODETRI(x, y, ddx)] = dis - ddx*dx;
						}
						if (dis - ddx*dx > EPS_DIS) {
							if (maxZ - minZ - dis < findTri[5][ENCODETRI(x, y, ddx+1)]) {
								findTri[5][ENCODETRI(x, y, ddx+1)] = (ddx+1)*dx - dis;
							}
						}
						else {
							findTri[5][ENCODETRI(x, y, ddx)] = 0;
						}
					}
				}
			}
		}
	}
	for (int k = 0; k < 6; k++) { // x+,x-,y+,y-,z+,z-
		int startP, dp, endP;
		VD curAns;
		V3D vp;
		switch (k) {
		case 0:
			startP = xSum - 1; dp = -1; endP = -1; curAns.resize(ySum*zSum); vp = V3D(1, 0, 0); break;
		case 1:
			startP = 0; dp = 1; endP = xSum; curAns.resize(ySum*zSum); vp = V3D(-1, 0, 0); break;
		case 2:
			startP = ySum - 1; dp = -1; endP = -1; curAns.resize(xSum*zSum); vp = V3D(0, 1, 0); break;
		case 3:
			startP = 0; dp = 1; endP = ySum; curAns.resize(xSum*zSum); vp = V3D(0, -1, 0); break;
		case 4:
			startP = zSum - 1; dp = -1; endP = -1; curAns.resize(xSum*ySum); vp = V3D(0, 0, 1); break;
		case 5:
			startP = 0; dp = 1; endP = zSum; curAns.resize(xSum*ySum); vp = V3D(0, 0, -1); break;
		default:
			break;
		}
		for (uint i = 0; i < curAns.size(); i++) {
			curAns[i] = 2e9;
		}
		for (int i = startP; i != endP; i += dp) {
			int x, y, z;
			switch (k) {
			case 0:
			case 1:
				x = i;
				for (y = 0; y < ySum; y++) {
					for (z = 0; z < zSum; z++) {
						curAns[y*zSum + z] += dx;
						if (findTri[k][ENCODETRI(x, y, z)] < 1e9) curAns[y*zSum + z] = findTri[k][ENCODETRI(x, y, z)];
						dis[ENCODE(x, y, z, k)] = curAns[y*zSum + z];
					}
				}
				break;
			case 2:
			case 3:
				y = i;
				for (x = 0; x < xSum; x++) {
					for (z = 0; z < zSum; z++) {
						curAns[x*zSum + z] += dx;
						if (findTri[k][ENCODETRI(x, y, z)] < 1e9) curAns[x*zSum + z] = findTri[k][ENCODETRI(x, y, z)];
						dis[ENCODE(x,y,z,k)] = curAns[x*zSum + z];
					}
				}
				break;
			case 4:
			case 5:
				z = i;
				for (x = 0; x < xSum; x++) {
					for (y = 0; y < ySum; y++) {
						curAns[x*ySum + y] += dx;
						if (findTri[k][ENCODETRI(x, y, z)] < 1e9) curAns[x*ySum + y] = findTri[k][ENCODETRI(x, y, z)];
						dis[ENCODE(x, y, z, k)] = curAns[x*ySum + y];
					}
				}
				break;
			}
		}
		//printf("\n");
	}
/*	for (int k = 0; k < 6; k++) {
		fprintf(fp, "\n\nk = %d\n", k);
		for (int z = 0; z < zSum; z++) {
			fprintf(fp, "\nz = %.3lf\n", minZ + z * dx);
			fprintf(fp, "%d\t", xSum);
			for (int x = 0; x < xSum; x++) {
				fprintf(fp, "%.3lf\t", minX + x * dx);
			}
			fprintf(fp, "\n");
			for (int y = 0; y < ySum; y++) {
				fprintf(fp, "%.3lf\t", minY + y * dx);
				for (int x = 0; x < xSum; x++) {
					if (dis[ENCODE(x,y,z,k)] < 1e9) fprintf(fp, "%.3lf\t", dis[ENCODE(x, y, z, k)]);
					else fprintf(fp, "INF\t");
				}
				fprintf(fp, "\n");
			}
		}
	}
	fclose(fp);*/
}
