#pragma once
#include <cstdio>
#include <algorithm>
#include <vector>
#include <MathLib\P3D.h>
#include <map>
#include <set>
#include <GUILib\GLMesh.h>
#include "DisjointSet.h"

#define PAIR(x, y, z) make_pair(x, make_pair(y, z))

using namespace std;

class MarchingCubes
{
public:
    int faceReplacement[6][8] = {
        { 0, 1, 2, 3, 4, 5, 6, 7 },
        { 1, 2, 3, 0, 5, 6, 7, 4 },
        { 2, 3, 0, 1, 6, 7, 4, 5 },
        { 3, 0, 1, 2, 7, 4, 5, 6 },
        { 4, 5, 1, 0, 7, 6, 2, 3 },
        { 3, 2, 6, 7, 0, 1, 5, 4 }
    };
    int rotateReplacement[8] = { 4, 0, 3, 7, 5, 1, 2, 6 };
    int cases[15][8] = {
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0, 0, 0, 0 },
        { 1, 1, 0, 0, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0, 1, 0, 0 },
        { 0, 1, 1, 1, 0, 0, 0, 0 },
        { 1, 1, 1, 1, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0, 0, 0 },
        { 1, 0, 1, 0, 0, 1, 0, 1 },
        { 1, 0, 1, 1, 0, 0, 0, 1 },
        { 0, 1, 1, 1, 0, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 1, 0 },
        { 1, 1, 0, 0, 0, 0, 1, 0 },
        { 0, 1, 0, 0, 1, 0, 1, 0 },
        { 1, 0, 1, 0, 1, 0, 1, 0 },
        { 1, 0, 1, 1, 0, 0, 1, 0 }
    };

    vector<double> cubes; // Nx*Ny*Nz*6
    vector<bool> outside; // Nx*Ny*Nz
    vector<int> edgeId;

    P3D startPoint;
    double width;
    int Nx, Ny, Nz;

    MarchingCubes(vector<double> &_cubes, vector<bool> &_outside, P3D _startPoint, double _width, int Nx, int Ny, int Nz);

    void Polygonise(bool Grid[], int nowGridEdgeId[], vector<pair<int, pair<int, int> > > &Triangles);
    GLMesh* reconstruct();
    P3D VertexInterp(P3D &p1, P3D &p2, double d1, double d2, bool outside1, bool outside2);
    void calculateEdgeId(GLMesh* mesh); // calculate the ids of every edge intersection and add into mesh
};

