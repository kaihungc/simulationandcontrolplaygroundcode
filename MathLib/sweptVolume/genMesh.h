#pragma once
#include <GUILib\GLMesh.h>
#include "../Quaternion.h"

class genMesh
{
	Quaternion rotation, oriq;
	V3D translation;
	P3D orip;
	V3D axis;
	double angle;

	V3D rotate(P3D a, double t);
	V3D gradient(P3D a, double t);
	int getpoint(P3D a, P3D b, double u, double v, GLMesh *ans);
	int getpoint(P3D a, P3D b, P3D c, double u, double v, GLMesh *ans);
	void subdivide(P3D a, P3D b, P3D c, GLMesh *ans, std::vector<double> u, std::vector<double> v, bool flag = false);
	void make_r(P3D a, P3D b, GLMesh *ans, P3D c);
	void make_d(P3D a, P3D b, P3D c, GLMesh *ans);


public:
	GLMesh *genRotateMesh(GLMesh *ori);
	void loadTrajectory(const Quaternion &q0, const Quaternion &q1, const P3D &p0, const P3D &p1);
};