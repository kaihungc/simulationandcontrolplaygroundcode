#include "DisjointSet.h"

DisjointSet::DisjointSet(int N)
{
    father.clear();
    for (int i = 0;i < N;++i)
        father.push_back(i);
}

void DisjointSet::connect(int x, int y)
{
    int fax = getFather(x);
    int fay = getFather(y);

    if (fax < fay) father[fay] = fax;
    else father[fax] = fay;
}

int DisjointSet::getFather(int x)
{
    if (father[x] == x) return x;

    return father[x] = getFather(father[x]);
}
