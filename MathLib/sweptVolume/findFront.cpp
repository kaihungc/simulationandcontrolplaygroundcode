#include "findFront.h"

namespace findFront_space
{
	using namespace std;
#define encode(i,j,k) (((i)*n_y+(j))*n_z+(k))
#define decode(x,i,j,k) {k=x%n_z;x/=n_z;j=x%n_y;x/=n_y;i=x%n_x;}
	const int d[6] = { 0,0,0,0,1,-1 };
	const double eps = 1e-8;
};

using namespace findFront_space;

void findFront::solve(const VD &dis, VB &outside, double step, int n_x, int n_y, int n_z)
{
	VB known;
	vector<int> pre;
	known.resize(n_x*n_y*n_z);
	outside.resize(n_x*n_y*n_z);
	pre.resize(n_x*n_y*n_z);
	deque<int> q;
	for (int i = 0; i < n_x; ++i)
		for (int j = 0; j < n_y; ++j)
		{
			int num = encode(i, j, 0);
			q.push_back(num);
			outside[num] = true;
			known[num] = true;
			if ((i==0||i==n_x-1)||(j==0||j==n_y-1))
				for (int k = 1; k < n_z-1; ++k)
				{
					num = encode(i, j, k);
					q.push_back(num);
					outside[num] = true;
					known[num] = true;
				}
			num = encode(i, j, n_z - 1);
			q.push_back(num);
			outside[num] = true;
			known[num] = true;
		}
	while (!q.empty())
	{
		int cur = q.front(), x, y, z;
		q.pop_front();
		int tmp = cur;
		decode(cur, x, y, z);
		bool flag = true;
		if (x == 2 && y == 12 && z == 13)
		{
			printf("hey!!\n");
			int x, y, z, tmp1;
			while (tmp != 0)
			{
				tmp1 = tmp;
				decode(tmp, x, y, z);
				printf("%d %d %d <- ", x, y, z);
				for (int i = 0; i < 6; ++i)
					printf("%lf ", dis[encode(x, y, z) * 6 + i]);
				printf("\n");
				tmp = pre[tmp1];
			}

		}
		for (int i = 0; i < 6; ++i)
		{
			int x1 = x + d[(i+4)%6], y1 = y + d[(i+2)%6], z1 = z + d[i];
			if (x1<0||x1>=n_x||y1<0||y1>=n_y||z1<0||z1>=n_z)
				continue;
			int num = encode(x1, y1, z1);
			if (known[num])
				continue;
			if (dis[encode(x,y,z)*6+i] > step + eps)
			{
				outside[num] = true;
				q.push_back(num);
				known[num] = true;
				pre[num] = encode(x,y,z);
			}
		}
	}
	//FILE *fp = fopen("ff.txt", "w");
	//for (int z = 0; z < n_z; z++) {
	//	for (int y = 0; y < n_y; y++) {
	//		for (int x = 0; x < n_x; x++) {
	//			fprintf(fp, "%d ", (int)outside[x][y][z]);
	//		}
	//		fprintf(fp, "\n");
	//	}
	//	fprintf(fp, "\n\n");
	//}
	//fclose(fp);
} 