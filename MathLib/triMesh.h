#pragma once

#include <math.h>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <algorithm>
#include <assert.h>
#include "P3D.h"
#include "Quaternion.h"

/*
This class stores a triangle mesh. It makes it possible to access the mesh geometric primitives and perform
various geometric calculations and operations on the mesh.

	A quick summary of the format:
	1.  vertices are specified in a global, 0-based namespace.
	2.  each face (triangle) consists of a list of 3 vertex indices
*/

class TriMesh {
public:
	// ======= member classes =======
	class Triangle {
	public:
		explicit Triangle() {}
		explicit Triangle(const int& v1, const int& v2, const int& v3) {
			v1Index = v1;
			v2Index = v2;
			v3Index = v3;
		}
		inline void reverseVertices() {
			int tmpIndex = v1Index;
			v1Index = v3Index;
			v3Index = tmpIndex;
		}
	public:
		int v1Index = -1;
		int v2Index = -1;
		int v3Index = -1;
	};

	// ======= constructors =======

	// makes an empty structure
	explicit TriMesh() {}

	// creates a triangle mesh
	explicit TriMesh(int numVertices, double * vertices, int numTriangles, int * triangles);

	// copy constructor
	explicit TriMesh(const TriMesh & TriMesh);

	// ======= basic mesh info / stats =======
	inline uint getNumVertices() const { return vertexPositions.size(); }
	unsigned int getNumFaces() const { return faces.size(); } // total number of faces

	inline P3D getPosition(int vertexIndex) const { return vertexPositions[vertexIndex]; }
	inline void setPosition(int vertexIndex, const P3D & position) { vertexPositions[vertexIndex] = position; }

	inline void addVertex(const P3D & pos) { vertexPositions.push_back(pos); }
	inline void addTriangle(const int& v1Index, const int& v2Index, const int& v3Index) { faces.push_back(Triangle(v1Index, v2Index, v3Index)); }


	// ======= geometric queries =======
	int getNumIsolatedVertices() const;
	unsigned int computeMaxFaceDegree() const;

	// the tighest fitting box is scaled by "expansionRatio"
	// expansionRatio of 1 gives a tight-fitting bounding box
	void getBoundingBox(double expansionRatio, P3D * bmin, P3D * bmax) const; // sides of the box may not be equal to each other
	void getCubicBoundingBox(double expansionRatio, P3D * bmin, P3D * bmax) const; // forces a cubic bounding box
	double getDiameter() const { return diameter; }

	void getMeshRadius(const P3D & centroid, double * radius) const;
	void getMeshGeometricParameters(P3D * centroid, double * radius) const;



	P3D computeFaceCentroid(const Triangle& tri) const;
	double computeFaceSurfaceArea(const Triangle& tri) const; 
	V3D computeFaceNormal(const TriMesh::Triangle& tri) const;

	// computes the 3D volume enclosed by the orientable surface
	double computeVolume() const;
	// computes the entire surface area
	double computeSurfaceArea() const;
	// finds the closest mesh vertex to the query position queryPos (using exhaustive search); also outputs distance to such a vertex (if distance is not NULL)
	unsigned int getClosestVertex(const P3D & queryPos, double * distance = NULL) const;

	// ======= mesh modification =======
	void scaleUniformly(const P3D & center, double factor); // scales the model uniformly, with center being the center of the scaling
	void transformRigidly(const P3D & translation, const Quaternion& rotation);
	int removeZeroAreaFaces();

	
//	void weldVertices();
//	void unweldVertices();
//	int removeIsolatedVertices(); // removes vertices that don't appear in any triangle

	// removes faces that have an edge shared by two other faces AND an edge not shared by any other face (making the mesh more manifold)
	// this function does one iteration of this process; you may need to call it again to continue removing faces, until the function returns 0
//	int removeHangingFaces();
	// collapses edges that are shared by more than two faces
	// this function does one iteration of this process; you may need to call it again to continue removing faces, until the function returns 0
//	int removeNonManifoldEdges();
//	void collapseEdge(unsigned int vertexA, unsigned int vertexB, int removeIsolatedVertices = 1); // collapses the edge between vertices vertexA and vertexB
//	void appendMesh(TriMesh * mesh); // appends "mesh" to this mesh
	// creates a set of new triangle meshes, one for each connected component
//	void splitIntoConnectedComponents(std::vector<TriMesh *> connectedComponents) const;
	// compute center of mass and moments of inertia - based on volumes, based on shells...
//	P3D computeCenterOfMass_Vertices() const; // of the vertices
//	P3D computeCenterOfMass_Triangles() const; // of the triangular surface
//	P3D computeCenterOfMass_Triangles(double massDensity) const; // second argument gives the surface mass density 
//	void computeInertiaTensor_Triangles(double IT[6]) const; // of the triangular surface, with respect to the center of mass, assumes uniform mass; density on the triangles = 1 
//	void computeInertiaTensor_Triangles(double mass, double IT[6]) const; // of the triangular surface, with respect to the center of mass, assumes uniform density on the triangles, which is set such that the total object mass equals "mass"
//	void computeInertiaTensor_Triangles(double massDensity, double IT[6]) const; // of the triangular surface, with respect to the center of mass, based on the given mass density
	// various stats on the mesh...
//	double computeMinEdgeLength() const; // computes minimum edge length in the mesh
//	double computeMedianEdgeLength() const; // computes median edge length in the mesh
//	double computeAverageEdgeLength() const; // computes average edge length in the mesh
//	double computeMaxEdgeLength() const; // computes maximum edge length in the mesh
//	double computeMinEdgeLength(int * vtxa, int * vtxb) const; // also returns the two 0-indexed vertices achieving min
//	double computeMaxEdgeLength(int * vtxa, int * vtxb) const; // also returns the two 0-indexed vertices achieving max



public:
	std::vector< Triangle > faces;
	std::vector< P3D > vertexPositions;


	// computes internal axis-aligned bounding box
	void computeBoundingBox(); // sets diameter, bmin, bmax, center, cubeHalf


protected:
	double diameter;
	P3D bmin, bmax;
	P3D center, cubeHalf;
};
