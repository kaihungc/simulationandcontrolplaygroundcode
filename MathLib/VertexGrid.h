#pragma once

#include "boundingBox.h"
#include <vector>
#include <map>
#include <Utils/Logger.h>

using namespace std;

template<class T>
class VertexGrid
{
public:
	vector<T> data;
	P3D startPoint;
	P3D endPoint;
	double stepSize;
	int totalVertexNum;
	int layerVertexNum; // the number of vertices each x-y layer
	int vertexNum[3];  // the number of vertices along x, y, z

private:
	

public:
	VertexGrid();
	VertexGrid(AxisAlignedBoundingBox& bbox, double _stepSize, const T& val = T());

	~VertexGrid();

	void setData(int x, int y, int z, const T& val);
	void setData(int index, const T& val);
	void addData(int x, int y, int z, const T& val);
	void addData(int index, const T& val);
	T& getData(int x, int y, int z);
	T& getData(int index);
	P3D getVertex(int x, int y, int z);
	P3D getVertex(int index);

	// Only vertices that have the value will be drawn.
	void glDraw(const T& val);
	// More intelligent glDraw, pass a test function and only passed vertices will be drawn
	void glDraw(bool (*test)(const T& A));
	void glDrawGridBox();

	T interpolateData(double px, double py, double pz);
	bool isInside(double px, double py, double pz);
};


template<class T>
inline VertexGrid<T>::VertexGrid()
{
}

template<class T>
inline VertexGrid<T>::VertexGrid(AxisAlignedBoundingBox& bbox, double _stepSize, const T& val)
{
	stepSize = _stepSize;
	P3D center = bbox.center();

	for (int i = 0; i < 3; i++)
	{
		int num = (int)((bbox.bmax()[i] - bbox.bmin()[i]) / stepSize) + 1;
		double ext = stepSize * num * 0.5;
		startPoint[i] = center[i] - ext;
		endPoint[i] = center[i] + ext;
		vertexNum[i] = num + 1;
	}

	layerVertexNum = vertexNum[0] * vertexNum[1];
	totalVertexNum = layerVertexNum * vertexNum[2];
	data.resize(totalVertexNum, val);
}

template<class T>
inline VertexGrid<T>::~VertexGrid()
{
}

template<class T>
inline void VertexGrid<T>::setData(int x, int y, int z, const T& val)
{
	int index = z * layerVertexNum + (y * vertexNum[0] + x);
	data[index] = val;
}

template<class T>
inline void VertexGrid<T>::setData(int index, const T& val)
{
	data[index] = val;
}

template<class T>
inline void VertexGrid<T>::addData(int x, int y, int z, const T& val)
{
	int index = z * layerVertexNum + (y * vertexNum[0] + x);
	data[index] += val;
}

template<class T>
inline void VertexGrid<T>::addData(int index, const T& val)
{
	data[index] += val;
}

template<class T>
inline T& VertexGrid<T>::getData(int x, int y, int z)
{
	int index = z * layerVertexNum + (y * vertexNum[0] + x);
	return data[index];
}

template<class T>
inline T& VertexGrid<T>::getData(int index)
{
	return data[index];
}

template<class T>
inline P3D VertexGrid<T>::getVertex(int index)
{
	int z = index / layerVertexNum;
	int t = index % layerVertexNum;
	int y = t / vertexNum[0];
	int x = t % vertexNum[0];

	P3D p;
	p[0] = startPoint[0] + x * stepSize;
	p[1] = startPoint[1] + y * stepSize;
	p[2] = startPoint[2] + z * stepSize;

	return p;
}

template<class T>
inline P3D VertexGrid<T>::getVertex(int x, int y, int z)
{

	P3D p;
	p[0] = startPoint[0] + x * stepSize;
	p[1] = startPoint[1] + y * stepSize;
	p[2] = startPoint[2] + z * stepSize;

	return p;
}

template<class T>
inline void VertexGrid<T>::glDraw(const T& val)
{
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++)
			{
				int index = z * layerVertexNum + (y * vertexNum[0] + x);
				if (data[index] == val)
					drawSphere(getVertex(x, y, z), 0.002);
			}
}

template<class T>
inline void VertexGrid<T>::glDraw(bool (*test)(const T& A))
{
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++)
			{
				int index = z * layerVertexNum + (y * vertexNum[0] + x);
				if (test(data[index]))
					drawSphere(getVertex(x, y, z), 0.005);
			}
}

template<class T>
inline void VertexGrid<T>::glDrawGridBox()
{
	P3D p0(startPoint[0], startPoint[1], startPoint[2]);
	P3D p1(endPoint[0], startPoint[1], startPoint[2]);
	P3D p2(endPoint[0], endPoint[1], startPoint[2]);
	P3D p3(startPoint[0], endPoint[1], startPoint[2]);

	P3D p4(startPoint[0], startPoint[1], endPoint[2]);
	P3D p5(endPoint[0], startPoint[1], endPoint[2]);
	P3D p6(endPoint[0], endPoint[1], endPoint[2]);
	P3D p7(startPoint[0], endPoint[1], endPoint[2]);

#define VTX(i) (i)[0],(i)[1],(i)[2]
	glBegin(GL_LINES);
	glVertex3d(VTX(p0));
	glVertex3d(VTX(p1));
	glVertex3d(VTX(p1));
	glVertex3d(VTX(p2));
	glVertex3d(VTX(p2));
	glVertex3d(VTX(p3));
	glVertex3d(VTX(p3));
	glVertex3d(VTX(p0));

	glVertex3d(VTX(p0));
	glVertex3d(VTX(p4));
	glVertex3d(VTX(p1));
	glVertex3d(VTX(p5));
	glVertex3d(VTX(p2));
	glVertex3d(VTX(p6));
	glVertex3d(VTX(p3));
	glVertex3d(VTX(p7));

	glVertex3d(VTX(p4));
	glVertex3d(VTX(p5));
	glVertex3d(VTX(p5));
	glVertex3d(VTX(p6));
	glVertex3d(VTX(p6));
	glVertex3d(VTX(p7));
	glVertex3d(VTX(p7));
	glVertex3d(VTX(p4));
	glEnd();

#undef VTX
}

template<class T>
inline T VertexGrid<T>::interpolateData(double px, double py, double pz)
{
	if (IS_EQUAL(px, endPoint[0]))
		px -= 1e-6;
	if (IS_EQUAL(py, endPoint[1]))
		py -= 1e-6;
	if (IS_EQUAL(pz, endPoint[2]))
		pz -= 1e-6;
	int x = (int)((px - startPoint[0]) / stepSize);
	int y = (int)((py - startPoint[1]) / stepSize);
	int z = (int)((pz - startPoint[2]) / stepSize);

	double dx = (px - startPoint[0]) / stepSize - x;
	double dy = (py - startPoint[1]) / stepSize - y;
	double dz = (pz - startPoint[2]) / stepSize - z;

	double rx = 1 - dx;
	double ry = 1 - dy;
	double rz = 1 - dz;

	T V00 = rz * getData(x, y, z) + dz * getData(x, y, z + 1);
	T V01 = rz * getData(x, y + 1, z) + dz * getData(x, y + 1, z + 1);
	T V10 = rz * getData(x + 1, y, z) + dz * getData(x + 1, y, z + 1);
	T V11 = rz * getData(x + 1, y + 1, z) + dz * getData(x + 1, y + 1, z + 1);

	T V0 = ry * V00 + dy * V01;
	T V1 = ry * V10 + dy * V11;

	return rx * V0 + dx * V1;
}

template<class T>
inline bool VertexGrid<T>::isInside(double px, double py, double pz)
{
	return px >= startPoint[0] && px <= endPoint[0]
		&& py >= startPoint[1] && py <= endPoint[1]
		&& pz >= startPoint[2] && pz <= endPoint[2];
}