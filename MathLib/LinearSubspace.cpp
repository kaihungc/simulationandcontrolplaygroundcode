#pragma once



#include <MathLib\LinearSubspace.h>

struct MaxRealValueSmallerThan
{
	double m_maxValue;
	MaxRealValueSmallerThan(double _max) : m_maxValue(_max) {}
	bool operator()(const std::complex<double>& _c1, const std::complex<double>& _c2)
	{
		return (abs(_c1.real()) < abs(_c2.real()) && abs(_c1.real()) < m_maxValue);
	}
};

///<
void findMaxIndex(const Eigen::EigenSolver< Eigen::MatrixXd >::EigenvalueType& _v, std::vector<int>& _indices)
{
	double max = 0;
	int maxId = 0;
	for (int i = 0; i < _v.size(); ++i)
	{
		if (abs(_v(i).real()) > max && std::find(_indices.begin(), _indices.end(), i) == _indices.end())
		{
			maxId = i;
			max = abs(_v(i).real());
		}
	}

	_indices.push_back(maxId);
}

double squared(const double _x)
{
	return _x*_x;
}

void componentWiseSqrt(Eigen::MatrixXd& _std)
{
	for (int i = 0; i < _std.rows(); ++i)
	{
		double std = IS_ZERO(_std(i)) ? 0.001 : sqrt(_std(i));
		_std(i) = std;
	}
}

///<
void LinearSubspace::computeAndRemoveMean(Eigen::MatrixXd& _A)
{
	assert(_A.cols() > 0);
	if (_A.cols() > 0 && m_bNormalize)
	{
		m_sampleMean.resize(_A.rows(), 1);	m_sampleMean.fill(0);
		m_sampleStd.resize(_A.rows(), 1); m_sampleStd.fill(0);

		for (int j = 0; j < _A.cols(); ++j)
		{
			Eigen::MatrixXd cJ = _A.col(j);
			for (int i = 0; i < _A.rows(); ++i)
				m_sampleMean(i, 0) += cJ(i);
		}

		m_sampleMean = m_sampleMean / (double)(_A.cols());

		for (int j = 0; j < _A.cols(); ++j)
		{
			Eigen::MatrixXd cJ = _A.col(j);
			for (int i = 0; i < _A.rows(); ++i)
				m_sampleStd(i,0) += squared(m_sampleMean(i, 0) - cJ(i));
		}

		m_sampleStd = m_sampleStd / (double)(_A.cols());

		componentWiseSqrt(m_sampleStd);

		///< Colum-wise substraction
		for (int j = 0; j < _A.cols(); ++j)
			for (int i = 0; i < _A.rows(); ++i)
			{
				if(m_sampleStd(i)>TINY)
					_A(i, j) = (_A(i, j) - m_sampleMean(i)) / m_sampleStd(i);
			}			
	}
}

///<
void LinearSubspace::getMaxEigenVectors(const Eigen::MatrixXd& _A, const int _numEigenModes)
{
	assert(_numEigenModes <= _A.cols());

	///< 2 Eigen Vecs (a_0, a_1): [3 + 3xM, 2]
	Eigen::MatrixXd AA = _A*_A.transpose();
	Eigen::EigenSolver< Eigen::MatrixXd  > eSolve(AA);
	Eigen::EigenSolver< Eigen::MatrixXd >::EigenvalueType eV = eSolve.eigenvalues();
	EigenVector eVec = eSolve.eigenvectors();
	
	///<
	std::vector<int> maxIndices;
	for (int i = 0; i < _numEigenModes; ++i)
		findMaxIndex(eV, maxIndices);

	m_P.resize(_A.rows(), _numEigenModes); 
	m_P.fill(0);

	for (int j = 0; j < _numEigenModes; ++j)
	{
		int colIndex = maxIndices[j];
		dVector vd(m_P.rows());
		for (int i = 0; i < m_P.rows(); ++i) {
			m_P(i, j) = eVec(i, colIndex).real();
			vd[i] = eVec(i, colIndex).real();
		}
	}
}


int LinearSubspace::numEigenVectors()const
{
	return m_P.cols();
}

Eigen::MatrixXd LinearSubspace::getEigenVector(const int _i) const
{
	return m_P.col(_i);
}

//Removes mean and projects.
void LinearSubspace::project(const Eigen::MatrixXd& _xFull, Eigen::MatrixXd& _mReduced) const
{
	if (m_bNormalize) {

		print("../out/LS/m_P", m_P);
		_mReduced = m_P.transpose()*((_xFull - m_sampleMean).cwiseQuotient(m_sampleStd));
	}
	else
		_mReduced = m_P.transpose()*_xFull;
}

///< Un projects and adds mean
void LinearSubspace::unProject(const Eigen::MatrixXd& _xSub, Eigen::MatrixXd& _mFull) const
{
	if (m_bNormalize)
		_mFull = (m_P*_xSub).cwiseProduct(m_sampleStd) + m_sampleMean;
	else
		_mFull = m_P*_xSub;
}

///< Low pass filter by:
/// Removes mean and adds it back.
void LinearSubspace::projectAndUnproject(const Eigen::MatrixXd& _mFullIn, Eigen::MatrixXd& _mFullOut) const
{
	Eigen::MatrixXd reduced;
	reduced.resize(numEigenVectors(), 1); reduced.fill(0);
	project(_mFullIn, reduced);
	unProject(reduced, _mFullOut);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Quick Principal Vector///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PrincipalVector::~PrincipalVector()
{
	
}

///< Build PCA assuming one single main vector:
PrincipalVector::PrincipalVector(const PointsContainer& _points) : m_dataPoints(_points)
{
	Eigen::MatrixXd vectorSamples;
	int numDim = m_dataPoints[0].size();
	vectorSamples.resize(numDim, m_dataPoints.size());
	vectorSamples.fill(0);

	for (uint i = 0; i < m_dataPoints.size(); ++i)
	{
		for (int j = 0; j < numDim; ++j)
			vectorSamples(j, i) = m_dataPoints[i](j);
	}

	m_pca.computeAndRemoveMean(vectorSamples);
	m_pca.getMaxEigenVectors(vectorSamples, 1);


}


///< Main direction:
V3D PrincipalVector::getPrincipalVector()
{	
	Eigen::VectorXd maxV = m_pca.getEigenVector(0);
	maxV = (maxV).cwiseProduct(m_pca.m_sampleStd);

	V3D rValue(maxV(0), maxV(1), maxV(2));
	return rValue;
}


///< Mean
V3D PrincipalVector::getMean()
{
	Eigen::VectorXd meanPt = m_pca.m_sampleMean;

	V3D rValue(meanPt(0), meanPt(1), meanPt(2));
	return rValue;
}

