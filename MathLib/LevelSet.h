#pragma once

#include <MathLib/VertexGrid.h>
#include <GUILib/GLMesh.h>
#include <MathLib/Segment.h>
#include <MathLib/Transformation.h>
#include <queue>
#include <set>

using namespace std;

struct DIPair
{
	double val;
	int index;
	DIPair(double _val, int _index) {
		val = _val;
		index = _index;
	}
};

struct CompareLSValIn {

public:
	bool operator()(const DIPair& A, const DIPair& B)
	{
		return A.val < B.val;
	}
};

struct CompareLSValOut {

public:
	bool operator()(const DIPair& A, const DIPair& B)
	{
		return A.val > B.val;
	}
};
typedef std::priority_queue<DIPair, std::vector<DIPair>, CompareLSValIn> DIInQueue;
typedef std::priority_queue<DIPair, std::vector<DIPair>, CompareLSValOut> DIOutQueue;

class LevelSet : public VertexGrid<double> {

public:
	LevelSet(AxisAlignedBoundingBox& _bbox, double _stepSize);
	// get a denser level set by interpolation
	LevelSet(LevelSet& LS, int multiplier);
	LevelSet() : VertexGrid<double>() {}
	~LevelSet();

	VertexGrid<V3D> getGradient();
	V3D getGradientAt(int x, int y, int z);
	V3D interpolateGradient(double px, double py, double pz);

	// flip the level set.
	void flip();
	// add a constant to the level set.
	void addConstant(double val);
	void setConstant(double val);

	void updateLevelSet(GLMesh* mesh, double(*updateFunc)(double origVal, double inputVal) = NULL, bool updateAllVertices = true);
	GLMesh* extractMesh(double levelSetVal);

	// CSG operations with another Level Set that lives on the same grid.
	void intersectLS (LevelSet* nLevelSet);
	void unionLS(LevelSet* nLevelSet);
	void minusLS (LevelSet* nLevelSet);
	void minusLSRelTrans(LevelSet* nLevelSet, Transformation& relTrans);

	// initialize the level set as a cylinder
	void initCylinder(V3D axis, P3D center, double radius);
	void minusCapsule(Segment& seg, double radius);

	// fast marching
	void fastMarchingInward(double boundaryWidth, double threshold = -DBL_MAX);
	void fastMarchingOutward(double boundaryWidth, double threshold = DBL_MAX);

	// mean curvature, t: step size, v: constant normal speed
	void meanCurvatureFlow(double t, double v);

	void saveToFile(FILE* fp);
	void loadFromFile(FILE* fp);
	void saveSparseToFile(FILE* fp);
	void loadSparseFromFile(FILE* fp);

private:
	void FMConsiderVertexInward(DIInQueue& Q, vector<int>& VStatus, int x, int y, int z, double threshold);
	void FMConsiderVertexOutward(DIOutQueue& Q, vector<int>& VStatus, int x, int y, int z, double threshold);
};



void getTwoLeastNumber(vector<double>& vals, double& min1, double& min2);
bool projectMultiLevelSetsForVertex(vector<LevelSet>& levelSets, int index);
void projectMultiLevelSets(vector<LevelSet>& levelSets);
void projectMultiLevelSetsEx(vector<LevelSet>& levelSets, int index1, int index2);
void projectMultiLevelSetsWithFixedLS(vector<LevelSet>& levelSets, vector<bool>& fixedLS);

VertexGrid<Matrix3x3> getHessian(VertexGrid<V3D>& grad);





