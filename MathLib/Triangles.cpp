#include <GUILib/GLUtils.h>

/*
 Triangles
*/

#if defined(_WIN32) || defined(WIN32)
  #pragma warning(disable : 4996)
  #pragma warning(disable : 4267)
  #pragma warning(disable : 4244)
#endif
#include <set>
#include "Triangles.h"
using namespace std;

// a -= b
#define VECTOR_SUBTRACTEQUAL3(a,b)\
(a)[0] -= (b)[0];\
(a)[1] -= (b)[1];\
(a)[2] -= (b)[2];

// c = a - b
#define VECTOR_SUBTRACT3(a,b,c)\
(c)[0] = (a)[0] - (b)[0];\
(c)[1] = (a)[1] - (b)[1];\
(c)[2] = (a)[2] - (b)[2];

// c = a x b
#define VECTOR_CROSS_PRODUCT(a,b,c)\
  (c)[0] = ((a)[1]) * ((b)[2]) - ((b)[1]) * ((a)[2]);\
  (c)[1] = ((b)[0]) * ((a)[2]) - ((a)[0]) * ((b)[2]);\
  (c)[2] = ((a)[0]) * ((b)[1]) - ((b)[0]) * ((a)[1])

//  a . b
#define VECTOR_DOT_PRODUCT3(a,b)\
  ( (a)[0] * (b)[0] + (a)[1] * (b)[1] + (a)[2] * (b)[2] )

/*
Tests if a 3D triangle overlaps with a 3D box.

INPUT: center of box, box half sizes (in each of the three dimensions), and the three triangle vertices v0, v1, v2.
OUTPUT: whether triangle intersects the box or not
Note: all entries in boxHalfSize must be >= 0.
Note: lower-left-front corner is boxCenter - boxHalfSize, upper-right-back corner is boxCenter + boxHalfSize.
*/
bool triBoxOverlap(double boxcenter[3], double boxhalfsize[3], double triverts[3][3]) {
	bool overlap = true;

	// translate so that box's center coincides with the origin
	VECTOR_SUBTRACTEQUAL3(triverts[0], boxcenter);
	VECTOR_SUBTRACTEQUAL3(triverts[1], boxcenter);
	VECTOR_SUBTRACTEQUAL3(triverts[2], boxcenter);

	// test AABB against minimal AABB around the triangle
	// 3 tests

	double minx = min(min(triverts[0][0], triverts[1][0]), triverts[2][0]);
	double miny = min(min(triverts[0][1], triverts[1][1]), triverts[2][1]);
	double minz = min(min(triverts[0][2], triverts[1][2]), triverts[2][2]);
	double maxx = max(max(triverts[0][0], triverts[1][0]), triverts[2][0]);
	double maxy = max(max(triverts[0][1], triverts[1][1]), triverts[2][1]);
	double maxz = max(max(triverts[0][2], triverts[1][2]), triverts[2][2]);

	if ((boxhalfsize[0] < minx) || (maxx < -boxhalfsize[0]) || (boxhalfsize[1] < miny) || (maxy < -boxhalfsize[1]) || (boxhalfsize[2] < minz) || (maxz < -boxhalfsize[2])) {
		//no overlap
		overlap = false;
		return overlap;
	}

	// normals of AABB at origin

	double e0[3] = { 1, 0, 0 };
	double e1[3] = { 0, 1, 0 };
	double e2[3] = { 0, 0, 1 };

	double f0[3];
	double f1[3];
	double f2[3];

	VECTOR_SUBTRACT3(triverts[1], triverts[0], f0);
	VECTOR_SUBTRACT3(triverts[2], triverts[1], f1);
	VECTOR_SUBTRACT3(triverts[0], triverts[2], f2);

	double * e[3] = { e0, e1, e2 };
	double * f[3] = { f0, f1, f2 };

	// aij = ej X fj
	// 9 tests
	double a[3];
	double p0, p1, p2;
	double radius;
#define LOOP1(i,j)\
    {\
      VECTOR_CROSS_PRODUCT(e[i], f[j], a);\
      p0 = VECTOR_DOT_PRODUCT3(a, triverts[0]);\
      p1 = VECTOR_DOT_PRODUCT3(a, triverts[1]);\
      p2 = VECTOR_DOT_PRODUCT3(a, triverts[2]);\
\
      radius = ( boxhalfsize[0] * fabs(a[0]) ) + ( boxhalfsize[1] * fabs(a[1]) ) + ( boxhalfsize[2] * fabs(a[2]) );\
\
      if ( (min( min(p0, p1), p2 ) > radius) || (max( max(p0, p1), p2 ) < -radius ) )\
      {\
        overlap = false;\
        return overlap;\
      }\
    }\

	LOOP1(0, 0);
	LOOP1(0, 1);
	LOOP1(0, 2);
	LOOP1(1, 0);
	LOOP1(1, 1);
	LOOP1(1, 2);
	LOOP1(2, 0);
	LOOP1(2, 1);
	LOOP1(2, 2);
#undef LOOP1

	/*
	// aij = ej X fj
	// 9 tests
	for ( int i = 0; i <= 2; i ++ )
	{
	for ( int j = 0; j <= 2; j ++ )
	{
	double a[3];
	VECTOR_CROSS_PRODUCT(e[i], f[j], a);
	double p0 = VECTOR_DOT_PRODUCT3(a, triverts[0]);
	double p1 = VECTOR_DOT_PRODUCT3(a, triverts[1]);
	double p2 = VECTOR_DOT_PRODUCT3(a, triverts[2]);

	double radius = ( boxhalfsize[0] * fabs(a[0]) ) + ( boxhalfsize[1] * fabs(a[1]) ) + ( boxhalfsize[2] * fabs(a[2]) );

	if ( (min( min(p0, p1), p2 ) > radius) || (max( max(p0, p1), p2 ) < -radius ) )
	{
	overlap = false;
	return overlap;
	}
	}
	}
	*/

	// plane and AABB overlap test
	// 1 test

	// triangle normal
	double normal[3];
	VECTOR_CROSS_PRODUCT(f0, f1, normal);
	// no need to normalize the normal as we only check the sign of expressions in the calculations that depend on normal
	// this also makes the code robust against degenerate triangles with zero-length normals
	//double len2 = normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2];
	//double invlen = 1.0 / sqrt(len2);
	//normal[0] *= invlen;
	//normal[1] *= invlen;
	//normal[2] *= invlen;

	// distance of plane from the origin
	double planeDist = -VECTOR_DOT_PRODUCT3(normal, triverts[0]);

	// get nearest and farthest corners
	double nearestPoint[3];
	double farthestPoint[3];

#define LOOP2(i)\
    if ( normal[i] < 0 )\
    {\
      nearestPoint[i] = boxhalfsize[i];\
      farthestPoint[i] = - boxhalfsize[i];\
    }\
    else\
    {\
      nearestPoint[i] = - boxhalfsize[i];\
      farthestPoint[i] = boxhalfsize[i];\
    }\

	LOOP2(0);
	LOOP2(1);
	LOOP2(2);
#undef LOOP2

	/*
	for ( int i = 0; i <= 2; i++ )
	{
	if ( n[i] < 0 )
	{
	nearestPoint[i] = boxhalfsize[i];
	farthestPoint[i] = - boxhalfsize[i];
	}
	else
	{
	nearestPoint[i] = - boxhalfsize[i];
	farthestPoint[i] = boxhalfsize[i];
	}
	}
	*/

	if (VECTOR_DOT_PRODUCT3(normal, nearestPoint) + planeDist > 0)
	{
		overlap = false;
		return overlap;
	}

	overlap = (VECTOR_DOT_PRODUCT3(normal, farthestPoint) + planeDist >= 0);

	return overlap;
}


bool TriangleBasic::doesIntersectBox(AxisAlignedBoundingBox & bbox) {
	// prepare the data and call the "triBoxOverlap" worker routine

	double boxcenter[3];
	double boxhalfsize[3];
	double triverts[3][3];

	P3D center_ = bbox.center();
	P3D halfSides_ = bbox.halfSides();
	boxcenter[0] = center_[0];
	boxcenter[1] = center_[1];
	boxcenter[2] = center_[2];
	boxhalfsize[0] = halfSides_[0];
	boxhalfsize[1] = halfSides_[1];
	boxhalfsize[2] = halfSides_[2];

	triverts[0][0] = first_[0];
	triverts[0][1] = first_[1];
	triverts[0][2] = first_[2];

	triverts[1][0] = second_[0];
	triverts[1][1] = second_[1];
	triverts[1][2] = second_[2];

	triverts[2][0] = third_[0];
	triverts[2][1] = third_[1];
	triverts[2][2] = third_[2];

	return triBoxOverlap(boxcenter, boxhalfsize, triverts);
}


int TriangleBasic::lineSegmentIntersection(P3D segmentStart, P3D segmentEnd, P3D * intersectionPoint) {
  //    Output: intersection point (when it exists)
  //    Return: -1 = triangle is degenerate (a segment or point)
  //             0 = disjoint (no intersect)
  //             1 = intersect in unique point I1
  //             2 = are in the same plane

  V3D u, v, n;             // triangle vectors
  V3D dir, w0, w;          // ray vectors
  double r, a, b;             // params to calc ray-plane intersect

  // get triangle edge vectors and plane normal
  u = second_ - first_;
  v = third_- first_;
  n = u.cross(v);             // cross product
  if ((n[0] == 0) &&         // triangle is degenerate
      (n[1] == 0) &&         
      (n[2] == 0) )          
    return -1;                 // do not deal with this case

  dir = segmentEnd - segmentStart;             // ray direction vector
  w0 = segmentStart - first_; 
  a = -n.dot(w0);
  b = n.dot(dir);
  if (b == 0)
  {     
    // ray is parallel to triangle plane
    if (a == 0)                // ray lies in triangle plane
      return 2;
    else 
      return 0;             // ray disjoint from plane
  }

  // get intersection point of ray with triangle plane
  r = a / b;
  if (r < 0.0)                   // ray goes away from triangle
    return 0;                  // => no intersect
  // for a segment, also test if (r > 1.0) => no intersect
  if (r > 1.0)                   // ray goes away from triangle
    return 0;                  // => no intersect

  *intersectionPoint = segmentStart + dir * r;           // intersect point of ray and plane

  // is intersectionPoint inside T?
  double uu, uv, vv, wu, wv, D;
  uu = u.dot(u);
  uv = u.dot(v);
  vv = v.dot(v);
  w = *intersectionPoint - first_;
  wu = w.dot(u);
  wv = w.dot(v);
  D = uv * uv - uu * vv;

  // get and test parametric coords
  double s, t;
  s = (uv * wv - vv * wu) / D;
  if (s < 0.0 || s > 1.0)        // intersectionPoint is outside T
    return 0;
  t = (uv * wu - uu * wv) / D;
  if (t < 0.0 || (s + t) > 1.0)  // intersectionPoint is outside T
    return 0;

  return 1;                      // intersectionPoint is in T
}

void TriangleBasic::render(){
   P3D a = first_;
   P3D b = second_;
   P3D c = third_;

   glBegin(GL_TRIANGLES);
     glVertex3f(a[0],a[1],a[2]);
     glVertex3f(b[0],b[1],b[2]);
     glVertex3f(c[0],c[1],c[2]);
   glEnd();
}

void TriangleBasic::renderEdges()
{
   P3D a = first_;
   P3D b = second_;
   P3D c = third_;

   glBegin(GL_LINES);
     glVertex3f(a[0],a[1],a[2]);
     glVertex3f(b[0],b[1],b[2]);
     glVertex3f(b[0],b[1],b[2]);
     glVertex3f(c[0],c[1],c[2]);
     glVertex3f(c[0],c[1],c[2]);
     glVertex3f(a[0],a[1],a[2]);
   glEnd();
}

template<class TriangleClass>
void makeUniqueList(vector<TriangleClass*> &triangleList, vector<TriangleClass*> & uniqueList)
{
  uniqueList.clear();
  set<int> indices;
  unsigned int ssize = triangleList.size();
  for(unsigned int i=0; i<ssize; i++)
  {
    int index = triangleList[i]->index();
    if (indices.find(index) == indices.end())
    {
      indices.insert(index);
      uniqueList.push_back(triangleList[i]);
    }
  }
}
