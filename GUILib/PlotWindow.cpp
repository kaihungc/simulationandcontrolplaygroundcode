#pragma once
#include "GLUtils.h"
#include <Utils\Logger.h>
#include "PlotWindow.h"
#include "FreeType.h"
#include "GLContentManager.h"

Box2d::Box2d(double posX_, double posY_, double sizeX_, double sizeY_, double originalSizeX_, double originalSizeY_, double scaleX_, double scaleY_) {
	reset(posX_, posY_, sizeX_, sizeY_, originalSizeX_, originalSizeY_, scaleX_, scaleY_);
}

Box2d::~Box2d() {}

void Box2d::reset(double posX_, double posY_, double sizeX_, double sizeY_, double originalSizeX_, double originalSizeY_, double scaleX_, double scaleY_) {
	posX = posX_;
	posY = posY_;
	sizeX = sizeX_;
	sizeY = sizeY_;
	scaleX = scaleX_;
	scaleY = scaleY_;
	originalSizeX = originalSizeX_;
	originalSizeY = originalSizeY_;

	computeFactor();
}

void Box2d::computeFactor() {
	fx = (double)(sizeX) / (double)originalSizeX * scaleX;
	fy = (double)(sizeY) / (double)originalSizeY * scaleY;
}

void Box2d::setPosition(double posX_, double posY_) {
	posX = posX_;
	posY = posY_;
}

void Box2d::move(double x, double y) {
	posX += x;
	posY += y;
}

void Box2d::resize(double sizeX_, double sizeY_) {
	sizeX = sizeX_;
	sizeY = sizeY_;
	computeFactor();
}

void Box2d::scale(double x, double y) {
	sizeX *= x;
	sizeY *= y;
	computeFactor();
}

void Box2d::reshape(double originalSizeX_, double originalSizeY_, double maxX, double maxY) {
	originalSizeX = originalSizeX_;
	originalSizeY = originalSizeY_;
	scaleX = maxX;
	scaleY = maxY;
	computeFactor();
}

// draws a vertex between 0 and 1
void Box2d::drawVertex(double x, double y, int pixelOffsetX, int pixelOffsetY) {
	glVertex2d(((double)(posX + pixelOffsetX) / (double)(sizeX)+x)*fx, ((double)(posY + pixelOffsetY) / (double)(sizeY)+y)*fy);
}

double Box2d::getBoxPositionX(double x, int pixelOffsetX) {
	return ((double)(posX + pixelOffsetX) / (double)(sizeX)+x)*fx;
}
double Box2d::getBoxPositionY(double y, int pixelOffsetY) {
	return ((double)(posY + pixelOffsetY) / (double)(sizeY)+y)*fy;
}

void Box2d::outline() {
	glBegin(GL_LINES);
	drawVertex(0, 0, 0, -1);
	drawVertex(0, 1);
	drawVertex(0, 1);
	drawVertex(1, 1);
	drawVertex(1, 1);
	drawVertex(1, 0);
	drawVertex(1, 0);
	drawVertex(0, 0);
	glEnd();
}

void Box2d::fill() {
	glBegin(GL_QUADS);
	drawVertex(0, 0);
	drawVertex(0, 1);
	drawVertex(1, 1);
	drawVertex(1, 0);
	glEnd();
}


PlotData::PlotData(const char *name){
	strcpy(this->name, name);
	color.resize(3, 0.);

	bDrawLines = true;
	bDrawPoints = true;

	lineWidth = 1.0;
}

PlotData::~PlotData(){
	clearData();
}

void PlotData::add(double x, double y){
	data.addKnot(x, y);
}


void PlotData::clearData(){
	data.clear();
}

char* PlotData::getName(){
	return name;
}

void PlotData::setColor(double r, double g, double b){
	color[0] = r;
	color[1] = g;
	color[2] = b;
}

void PlotData::getColor(double &r, double &g, double &b){
	r = color[0];
	g = color[1];
	b = color[2];
}

void PlotData::setDrawLines(bool bDrawLines){
	this->bDrawLines = bDrawLines;
}

void PlotData::setDrawPoints(bool bDrawPoints){
	this->bDrawPoints = bDrawPoints;
}

bool PlotData::getDrawLines(){
	return bDrawLines;
}

bool PlotData::getDrawPoints(){
	return bDrawPoints;
}


double PlotData::getLineWidth(){
	return lineWidth;
}

void PlotData::setLineWidth(double lineWidth){
	this->lineWidth = lineWidth;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PLOT WIDGET START // // PLOT WIDGET START // // PLOT WIDGET START // // PLOT WIDGET START // // PLOT WIDGET START // // PLOT WIDGET START // // PLOT WIDGET START // 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PlotWindow::PlotWindow(double posX, double posY, double sizeX, double sizeY) : GLWindow2D((int)posX, (int)posY, (int)sizeX, (int)sizeY) {
	xAxTickHeight = 30;
	yAxTickWidth = 60;
	titleHeight = 30;
	legendWidth = 80;
}

int PlotWindow::findDataByName(const char* name){
	for(uint i=0; i<plotData.size(); i++)
		if(strcmp(name, plotData[i]->getName())==0)
			return (int)i;
	return -1;
}

void PlotWindow::setTitle(char* title){
	strcpy(this->title, title);
}

char* PlotWindow::getTitle(){
	return title;
}

void PlotWindow::setLineLook(const char* name, bool bDrawLines, bool bDrawPoints, double lineWidth){
	int index = findDataByName(name);
	if(index>=0){
		setLineLook(index, bDrawLines, bDrawPoints, lineWidth);
	}
	else
		Logger::consolePrint("PlotWindow: '%s' not found", name);
}

void PlotWindow::setLineLook(int index, bool bDrawLines, bool bDrawPoints, double lineWidth){
	plotData[index]->setDrawLines(bDrawLines);
	plotData[index]->setDrawPoints(bDrawPoints);
	plotData[index]->setLineWidth(lineWidth);
}

void PlotWindow::createData(const char* name, double r, double g, double b){
	PlotData* pData = new PlotData(name);
	pData->setColor(r, g, b);
	plotData.push_back(pData);
}

void PlotWindow::addDataPoint(const char* name, double x, double y){
	int index = findDataByName(name);
	if(index>=0){
		addDataPoint(index, x, y);
	}
	else
		Logger::consolePrint("PlotWindow: '%s' not found", name);
}

void PlotWindow::addDataPoint(int index, double x, double y){
	plotData[index]->add(x, y);
}

void PlotWindow::addDataPoint(double x, double y) {
	plotData[plotData.size()-1]->add(x, y);
}

void PlotWindow::addDataPoint(int index, double y) {
	double x = 0;
	if (plotData[index]->data.getKnotCount() > 0)
		x = plotData[index]->data.getMaxPosition() + 1;
	plotData[index]->add(x, y);
}


void PlotWindow::clearData(bool deleteContainer){
	for(uint i=0; i<plotData.size(); i++){
		plotData[i]->clearData();
		if(deleteContainer)
			delete plotData[i];
	}
	if(deleteContainer)
		plotData.clear();
}

void PlotWindow::getExpForm(const double a, double &x, int &n){
	double d = a;
	if(!IS_ZERO(d) && abs(d) < HUGE_VAL){
		int steps = 0;
		while(abs(steps)<1/0.0000000001){
			if(d>=10){
				steps++;
				d/=10;
			}
			else if(d<1){
				steps--;
				d*=10;
			}
			else
				break;
		}
		x = d;
		n = steps;
	}
	else{
		x = 0; n = 1;
	}
}

void PlotWindow::glV2d(double x, double y){
	glVertex2d(maxX*x/viewportWidth, maxY*y/viewportHeight);
}


void PlotWindow::getYDataRange(double minX, double maxX, double &minY, double &maxY) {
	if (!autoRangeY) {
		minY = minYVal;
		maxY = maxYVal;
		return;
	}

	minY = HUGE_VAL;
	maxY = -HUGE_VAL;
	for (uint i = 0; i < plotData.size(); i++) {
		if (plotData[i]->data.getKnotCount() > 0){
			double yMin = plotData[i]->data.evaluate_linear(minX);
			double yMax = plotData[i]->data.evaluate_linear(maxX);
			if (minY > yMin) minY = yMin;
			if (maxY < yMin) maxY = yMin;
			if (minY > yMax) minY = yMax;
			if (maxY < yMax) maxY = yMax;
		}

		for (int j = 0; j < plotData[i]->data.getKnotCount(); j++) {
			double x = plotData[i]->data.getKnotPosition(j);
			if (x >= minX && x <= maxX) {
				double y = plotData[i]->data.getKnotValue(j);
				if (minY > y) minY = y;
				if (maxY < y) maxY = y;
			}
		}
	}
}

void PlotWindow::getXDataRange(double &minX, double &maxX) {
	if (!autoRangeX && !fixedXRange) {
		minX = minXVal;
		maxX = maxXVal;
		return;
	}

	minX = HUGE_VAL;
	maxX = -HUGE_VAL;

	for (uint i = 0; i < plotData.size(); i++) {
		for (int j = 0; j < plotData[i]->data.getKnotCount(); j++) {
			double x = plotData[i]->data.getKnotPosition(j);
			if (minX > x) minX = x;
			if (maxX < x) maxX = x;
		}
	}


	if (!autoRangeX && fixedXRange) {
		minX = maxX - rangeX;
	}
}

void PlotWindow::draw(){
	preDraw();
	FreeTypeFont* font = GLContentManager::getFont("../data/fonts/arial.ttf 14");

	/* --> At some point, it will look like this...
		|---------------------------------------|
		|	         title              |	l	| 
		|-------------------------------|	e	|
		| y	|							|   g	|
		| a |							|   e	|
		| i |		plot				|   n	|
		| s |							|   d	|
		| - |---------------------------|		|
		|   |   x axis 					|		|
		|-------------------------------|-------|
	*/

	// main box
	Box2d mainBox(1, 1, viewportWidth-1, viewportHeight-1, viewportWidth, viewportHeight, maxX, maxY);
	glColor4d(1, 1, 1, 1);
	mainBox.fill();
	glColor4d(0, 0, 0, 1);
	mainBox.outline();

	// plotBox
	Box2d plotBox(yAxTickWidth, xAxTickHeight, viewportWidth-yAxTickWidth-legendWidth, viewportHeight-xAxTickHeight-titleHeight, viewportWidth, viewportHeight, maxX, maxY);
	glColor4d(1, 1, 1, .9);
	plotBox.fill();
	glColor4d(0, 0, 0, .5);
	plotBox.outline();

	//title box
	Box2d titleBox(plotBox.posX, plotBox.posY + plotBox.sizeY, plotBox.sizeX, titleHeight, viewportWidth, viewportHeight, maxX, maxY);
	//glColor4d(1, 0, 1, 1);
	//titleBox.fill();
	//glColor4d(0, 0, 0, 1);
	//titleBox.outline();
	//glColor4d(0, 0, 0, 1);
//	titleBox->printCharAt(title, .5, .5, 0, 0, ANCHOR_CENTER, ANCHOR_MIDDLE, GLUT_BITMAP_HELVETICA_12);

	// legend
	Box2d legendBox(plotBox.posX+plotBox.sizeX, plotBox.posY, legendWidth, plotBox.sizeY, viewportWidth, viewportHeight, maxX, maxY);
	//glColor4d(0, 1, 1, 1);
	//legendBox.fill();
	//legendBox.printCharAt("legend:", .5, 1, 0, -10, ANCHOR_CENTER, ANCHOR_TOP);
	
	// x Axis labels
	Box2d xAxisLabelBox(plotBox.posX, plotBox.posY-xAxTickHeight, plotBox.sizeX, xAxTickHeight, viewportWidth, viewportHeight, maxX, maxY);
	//glColor4d(1, 0.3, 0, 1);
	//xAxisLabelBox.fill();

	// y Axis labels
	Box2d yAxisLabelBox(plotBox.posX-yAxTickWidth, plotBox.posY, yAxTickWidth, plotBox.sizeY, viewportWidth, viewportHeight, maxX, maxY);
	//glColor4d(1, 0.3, 0, 1);
	//yAxisLabelBox.fill();

	// compute min/max ranges. These numbers can either be preset or found automatically from the data
	double xMaxVal, xMinVal, yMinVal, yMaxVal;
	getXDataRange(xMinVal, xMaxVal);
	getYDataRange(xMinVal, xMaxVal, yMinVal, yMaxVal);

	double xMinMaxDist = xMaxVal - xMinVal;
	double yMinMaxDist = yMaxVal - yMinVal;

	if(xMinMaxDist < 1.0e-7){
		xMinMaxDist += 1.0e-7;
		xMinVal -= .5e-7;
		xMaxVal += .5e-7;
	}

	if(yMinMaxDist < 1.0e-7){
		yMinMaxDist += 1.0e-7;
		yMinVal -= .5e-7;
		yMaxVal += .5e-7;
	}

	// plot data!
	for(int ipD=plotData.size()-1; ipD >= 0; ipD--){
		if (plotData[ipD]->data.getKnotCount() <= 0) continue;
		glLineWidth((float)plotData[ipD]->getLineWidth());

		DynamicArray<double> xData, yData;

		for (int i = 0; i < plotData[ipD]->data.getKnotCount()-1; i++) {
			double t1 = plotData[ipD]->data.getKnotPosition(i);
			double t2 = plotData[ipD]->data.getKnotPosition(i+1);
			if (t1 < xMinVal) {
				if (t2 < xMinVal)
					continue;
				else {
					xData.push_back(xMinVal);
					yData.push_back(plotData[ipD]->data.evaluate_linear(xMinVal));
				}
			} else if (t2 > xMaxVal){
				if (t1 > xMaxVal)
					continue;
				else {
					xData.push_back(t1);
					yData.push_back(plotData[ipD]->data.getKnotValue(i));
					xData.push_back(xMaxVal);
					yData.push_back(plotData[ipD]->data.evaluate_linear(xMaxVal));
				}
			} else {
				xData.push_back(t1);
				yData.push_back(plotData[ipD]->data.getKnotValue(i));
			}
		}

		//now, add the last point... worst case scenario, we add it twice..
		if (xMaxVal <= plotData[ipD]->data.getKnotPosition(plotData[ipD]->data.getKnotCount() - 1)){
			xData.push_back(xMaxVal);
			yData.push_back(plotData[ipD]->data.evaluate_linear(xMaxVal));
		}

		for (uint i = 0; i<xData.size(); i++) {
			xData[i] = (xData[i] - xMinVal) / xMinMaxDist;
			yData[i] = (yData[i] - yMinVal) / yMinMaxDist;
		}

		// color
		double r, g, b;
		plotData[ipD]->getColor(r, g, b);
		glColor3d(r, g, b);
		
		if(plotData[ipD]->getDrawLines()){
			if (xData.size() == 1) {
				glPointSize(15.0);
				glBegin(GL_POINTS);
					plotBox.drawVertex(xData[0], yData[0]);
				glEnd();
			}

			// draw lines
			glLineWidth(5.0);
			glBegin(GL_LINE_STRIP);
			for(uint i=0; i<xData.size(); i++){
				plotBox.drawVertex(xData[i], yData[i]);
			}
			glEnd();
			glLineWidth(1.0);

			// draw shadow
			if (plotData[ipD]->bDrawShadow){
				glColor4d(r, g, b, 0.1);
				glBegin(GL_QUAD_STRIP);
				for(uint i=0; i<xData.size(); i++){
					double y0 = (0 - yMinVal) / yMinMaxDist;
					boundToRange(&y0, 0, 1);
					glColor4d(r, g, b, 0.4*fabs(yData[i]-y0) + .1);
					plotBox.drawVertex(xData[i], yData[i]);
					glColor4d(r, g, b, .2*y0 + .1);
					plotBox.drawVertex(xData[i], y0);
				}
				glEnd();
			}
		}
	}

	// x axis labels
	if(xMinMaxDist > 0.){
		int nAxis = 4;
		for (int i = 0; i < nAxis; i++) {
			double xPlot = (double)i / nAxis;

			glLineWidth(1.0);
			glColor4d(0, 0, 0, 0.1);
			glBegin(GL_LINE_STRIP);
			glColor4d(0,0,0, 0.1);
			plotBox.drawVertex(xPlot, 0);
			plotBox.drawVertex(xPlot, 1);
			glEnd();

//			glColor4d(0, 0, 0, 1);
//			char format[50]; sprintf(format, "%%.%de", (int)(1/hLabel));
//			char label[100]; sprintf(label, "%G", (abs(x) > 1e-10)?x:0.);
//			xAxisLabelBox.printCharAt(label, xPlot, .5, 0, 0, ANCHOR_CENTER, ANCHOR_MIDDLE);
		}
	}

	// y axis labels
	if(yMinMaxDist > 0.){
		int nAxis = 4;
		for (int i = 0; i < nAxis; i++) {
			double yPlot = (double)i / nAxis;

			glLineWidth(1.0);
			glColor4d(0, 0, 0, 0.1);
			glBegin(GL_LINE_STRIP);
			glColor4d(0,0,0, 0.1);
			plotBox.drawVertex(0, yPlot);
			plotBox.drawVertex(1, yPlot);
			glEnd();

//			glColor4d(0, 0, 0, .5);
//			char label[100]; sprintf(label, "%g", y);
//			yAxisLabelBox.printCharAt(label, 1, yPlot, -5, 0, ANCHOR_RIGHT, ANCHOR_MIDDLE);
		}
	}
/*
	// legend
	for(uint ipD=0; ipD<plotData.size(); ipD++){
		
		GLdouble r, g, b;
		plotData[ipD]->getColor(r, g, b);
		glLineWidth((float)plotData[ipD]->getLineWidth());
		
		double y = (double)(plotData.size()-ipD)/(double)(plotData.size());

		legendBoxes[ipD]->setPosition(legendBox.posX, y*legendBox.sizeY / * + legendBox.posY* / - titleHeight);
		legendBoxes[ipD]->reshape(viewportWidth, viewportHeight, maxX, maxY);

		glColor4d(r, g, b, 1);
//		legendBoxes[ipD]->printCharAt(plotData[ipD]->getName(), 0.5, 0, 0, 0, ANCHOR_CENTER, ANCHOR_BOTTOM);

		int pxlOff = 10;
		const int N = 4;
		int pts[N];
		pts[0] = 10;
		pts[1] = 20;
		pts[2] = 12;
		pts[3] = 28;

		if(plotData[ipD]->getDrawLines()){
			// line
			glColor4d(r, g, b, 1);
			glBegin(GL_LINE_STRIP);
			for(int i=0; i<N; i++)
				legendBoxes[ipD]->drawVertex( (double)i/(double)(N), 0, pxlOff, pts[i]+15);
			glEnd();

			// shadow
			glColor4d(r, g, b, .1);
			glBegin(GL_QUAD_STRIP);
			for(int i=0; i<N; i++){
				legendBoxes[ipD]->drawVertex( (double)i/(double)(N), 0, pxlOff, pts[i]+15);
				legendBoxes[ipD]->drawVertex( (double)i/(double)(N), 0, pxlOff, 20);
			}
			glEnd();
		}

		if(plotData[ipD]->getDrawPoints()){
			glColor4d(r, g, b, 1);
			for(int i=0; i<N; i++){
				drawCircle(legendBoxes[ipD]->getBoxPositionX((double)i/(double)(N), pxlOff), legendBoxes[ipD]->getBoxPositionY(0, pts[i]+15), 0.01, 20);
			}
		}

	}
	*/
	postDraw();

}

