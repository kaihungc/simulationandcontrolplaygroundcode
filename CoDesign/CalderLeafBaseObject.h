#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include <OptimizationLib\ObjectiveFunction.h>

const double GRAVITY = 9.81;

class CalderLeafBaseObject {
public:
	//The world position of each leaf
	P3D position;																
	P3D leafOrigin = P3D(0, 0, 0); // origin of the branch for branching leaf or that of parent branch for terminal leaf
	double radius = 0.02; // leaf size -- each leaf is a sphere
	double mass = 1; // leaf mass

	//this is the index at which parameters for this object appear in a global list of parameters
	int pIndexStart = -1;

public:
	// constructor
	CalderLeafBaseObject() {};
	// destructor
	virtual ~CalderLeafBaseObject();

	/********GUI**********/
	//flags that affect visualization of this object
	bool picked = false;
	//returns scaling coefficients to make sihouettes rougly equaly along all dimensions
	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) = 0;
	//methods for rendering the object
	virtual void drawSilhouette(double scale, double r, double g, double b);
	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) = 0;
	virtual void draw();
	void setupGLTransformationMatrices();

	virtual P3D getPosition() {
		return position;
	}

	virtual void setPosition(const P3D& p) {
		position = p;
	}

	P3D getWorldCoordinates(const P3D& p);

	P3D getLocalCoordinates(const P3D& p);

	virtual bool pickWith(Ray mouseRay, TranslateWidget* tWidget);

	void setWidgetsFromObjectState(TranslateWidget* tWidget);
	virtual void setObjectStateFromWidgets(TranslateWidget* tWidget) {};

	//for the GUI...
	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() { return DynamicArray<std::pair<char*, double*>>(); }

	//pushes the object's state parameters to list, starting at the given index, given the set of flags that are passed in as parameters
	virtual void pushDesignParametersToList(DynamicArray<double>&paramList, bool addMass, bool addLength) = 0;

	virtual void readDesignParametersFromList(DynamicArray<double>&paramList, int& pIndex, bool addMass, bool addLength) = 0;

	/**
	writes to txt file
	*/
	virtual void writeToFile(FILE* fp) {};

	/**
	reads from txt file
	*/
	virtual void loadFromFile(FILE* fp) {};

	void setParameterStartIndex(int pStartIndex) {
		this->pIndexStart = pStartIndex;
	}

	int getParameterStartIndex() {
		return pIndexStart;
	}
};


class BranchingLeaf: public CalderLeafBaseObject {

public:

	BranchingLeaf* parentBranch = NULL;
	CalderLeafBaseObject* child = NULL;

	// design params
	double xLen1 = 0.1;
	double yLen1 = 0.05;
	double xLen2 = 0.1;
	double yLen2 = 0.05;

	// fixed params
	int branchId = 1; // leftbranch is 0, rightbranch is 1 // this also helps in deciding torque sign (right branch +ve torque)

	BranchingLeaf() {};
	BranchingLeaf(int branchSide) {
		branchId = branchSide;
	}
	BranchingLeaf(double lx1, double ly1, double lx2, double ly2, int branchSide, BranchingLeaf* p) {
		xLen1 = lx1;
		yLen1 = ly1;
		xLen2 = lx2;
		yLen2 = ly2;
		branchId = branchSide;
		parentBranch = p;
	}
	~BranchingLeaf() {};

	virtual P3D setLeafPosition() {
		P3D terminalPos = leafOrigin;
		if (branchId == 1) {
			position = leafOrigin + P3D(xLen1, -yLen1, 0);
			terminalPos = leafOrigin + P3D(-xLen2, -yLen2, 0);
		}
		else {
			position = leafOrigin + P3D(-xLen2, -yLen2, 0);
			terminalPos = leafOrigin + P3D(xLen1, -yLen1, 0);
		}
		//Logger::consolePrint("pos:%lf %lf %lf\n", position[0], position[1], position[2]);
		//Logger::consolePrint("terminal pos:%lf %lf %lf\n", terminalPos[0], terminalPos[1], terminalPos[2]);
		return terminalPos;
	}

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	void drawBranches();

	double getTorque();

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimX = dimY = dimZ = radius;
	}

	virtual void setObjectStateFromWidgets(TranslateWidget* tWidget);

	void getChildTerminalLeafParams(double& lx, double &ly, P3D &origin, int &branchId, P3D &pos);

	void setFromChildTerminalLeafParams(double lx, double ly);

	P3D getChildLeafPosition();

	virtual void pushDesignParametersToList(DynamicArray<double>&paramList, bool addMass, bool addLength) {
		if (addMass) paramList.push_back(mass);
		if (addLength) {
			paramList.push_back(xLen1);
			paramList.push_back(yLen1);
			paramList.push_back(xLen2);
			paramList.push_back(yLen2);
		}
	}

	virtual void readDesignParametersFromList(DynamicArray<double>&paramList, int& pIndex, bool addMass, bool addLength) {
		if (addMass) mass = paramList[pIndex++];
		if (addLength) {
			xLen1 = paramList[pIndex++];
			yLen1 = paramList[pIndex++];
			xLen2 = paramList[pIndex++];
			yLen2 = paramList[pIndex++];
		}
	}

	// get net length of branch's main leaf from the hook
	virtual double getNetLengthFromHook();
	dVector getTorqueAnalyticalDerivative(bool addMass, bool addLength);
};


class TerminalLeaf : public CalderLeafBaseObject {

public:
	BranchingLeaf* parentBranch = NULL;

	// design params
	double xLen = 0.1;
	double yLen = 0.05;

	// fixed params
	int branchId = 1; // leftbranch is 0, rightbranch is 1 // this also helps in deciding torque sign (right branch +ve torque)

	TerminalLeaf(BranchingLeaf* l) {
		parentBranch = l;
		if(parentBranch)
			parentBranch->getChildTerminalLeafParams(xLen, yLen, leafOrigin, branchId, position);
	};
	~TerminalLeaf() {};

	// get net length of branch's main leaf from the hook
	virtual double getNetLengthFromHook();
	dVector getTorqueAnalyticalDerivative(bool addMass, bool addLength);

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimX = dimY = dimZ = radius;
	}

	virtual void setObjectStateFromWidgets(TranslateWidget* tWidget);

	virtual void pushDesignParametersToList(DynamicArray<double>&paramList, bool addMass, bool addLength) {
		if (addMass) paramList.push_back(mass);
	}

	virtual void readDesignParametersFromList(DynamicArray<double>&paramList, int& pIndex, bool addMass, bool addLength) {
		if (addMass) mass = paramList[pIndex++];
	}
};