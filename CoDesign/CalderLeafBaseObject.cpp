#include <include/glew.h>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include "CalderLeafBaseObject.h"
#include <GUILib/GLContentManager.h>
#include <GUILib/GLUtils.h>
#include <MathLib/ConvexHull3D.h>

CalderLeafBaseObject::~CalderLeafBaseObject() {
}

P3D CalderLeafBaseObject::getWorldCoordinates(const P3D& p) {
	return getPosition();
}


P3D CalderLeafBaseObject::getLocalCoordinates(const P3D& p) {
	return P3D();
}

void CalderLeafBaseObject::setWidgetsFromObjectState(TranslateWidget* tWidget) {
	tWidget->pos = getPosition();
}

bool CalderLeafBaseObject::pickWith(Ray mouseRay, TranslateWidget* tWidget) {
	P3D p;

	double dist = mouseRay.getDistanceToPoint(this->position, &p);
	if (dist <= this->radius) {
		picked = true;
		setWidgetsFromObjectState(tWidget);
	}
	else
		picked = false;

	return picked;
}

void CalderLeafBaseObject::drawSilhouette(double scale, double r, double g, double b) {
	//could first draw the original object in stencil buffer, and then draw silhouete where the stencil wasn't... to get rid of internal lines...

	// render the silhouette of the object...
	//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_CULL_FACE); // enable culling
	glCullFace(GL_FRONT); // enable culling of front faces
	GLShaderMaterial tmpMat;
	tmpMat.setShaderProgram(GLContentManager::getShaderProgram("silhouette"));
	tmpMat.setFloatParam("u_offset1", 0.0f);
	tmpMat.setFloatParam("u_color1", (float)r, (float)g, (float)b);
	double bbX, bbY, bbZ;
	getSilhouetteScalingDimensions(bbX, bbY, bbZ);
	double maxDim = MAX(bbX, bbY); maxDim = MAX(maxDim, bbZ);
	glPushMatrix();
	//	V3D geomOffset = getGeometryCenterLocalOffset();
	V3D geomOffset;
	glTranslated(geomOffset[0], geomOffset[1], geomOffset[2]);
	glScaled((bbX / maxDim + scale / maxDim) / (bbX / maxDim), (bbY / maxDim + scale / maxDim) / (bbY / maxDim), (bbZ / maxDim + scale / maxDim) / (bbZ / maxDim));
	glTranslated(-geomOffset[0], -geomOffset[1], -geomOffset[2]);
	drawObjectGeometry(&tmpMat);
	glPopMatrix();
	glDisable(GL_CULL_FACE);
}

void CalderLeafBaseObject::setupGLTransformationMatrices() {
	P3D p = getPosition();
	glTranslated(p[0], p[1], p[2]);
}

void CalderLeafBaseObject::draw() {
	checkOGLErrors();
	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);

	glStencilFunc(GL_ALWAYS, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	setupGLTransformationMatrices();

	// render the model to indicate places where the silhouette should not appear
	drawObjectGeometry();

	glStencilFunc(GL_NOTEQUAL, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	if (picked)
		drawSilhouette(0.001, 1, 0.5, 0);

	glDisable(GL_STENCIL_TEST);

	// render the normal model
	drawObjectGeometry();
	glPopMatrix();
}

void BranchingLeaf::drawObjectGeometry(GLShaderMaterial* material) {
	GLShaderMaterial tmpMat;
	GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

	if (material == NULL) {
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.7;
	}

	matToUse->apply();
	drawSphere(P3D(), radius, 12);
	matToUse->end();
}

void BranchingLeaf::drawBranches() {
	drawCapsule(leafOrigin, leafOrigin + P3D(xLen1, 0, 0), 0.005);
	drawCapsule(leafOrigin, leafOrigin + P3D(-xLen2, 0, 0), 0.005);
	drawCapsule(leafOrigin + P3D(xLen1, 0, 0), leafOrigin + P3D(xLen1, 0, 0) + P3D(0, -yLen1, 0), 0.005);
	drawCapsule(leafOrigin + P3D(-xLen2, 0, 0), leafOrigin + P3D(-xLen2, 0, 0) + P3D(0, -yLen2, 0), 0.005);
}

void TerminalLeaf::drawObjectGeometry(GLShaderMaterial* material) {
	GLShaderMaterial tmpMat;
	GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

	if (material == NULL) {
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.7;
	}

	matToUse->apply();
	drawSphere(P3D(), radius, 12);
	matToUse->end();
}

void BranchingLeaf::setObjectStateFromWidgets(TranslateWidget* tWidget) {
	V3D newL = leafOrigin - tWidget->pos;
	if (branchId == 1) {
		xLen1 = abs(newL[0]);
		yLen1 = abs(newL[1]);
	}
	else {
		xLen2 = abs(newL[0]);
		yLen2 = abs(newL[1]);
	}
}

void BranchingLeaf::getChildTerminalLeafParams(double& lx, double &ly, P3D &origin, int &branchid, P3D &pos) {
	if (branchId == 1) {
		lx = xLen2;
		ly = yLen2;
		branchid = 0;
	}
	else {
		lx = xLen1;
		ly = yLen1;
		branchid = 1;
	}
	origin = leafOrigin;
	pos = getChildLeafPosition();
}

void BranchingLeaf::setFromChildTerminalLeafParams(double lx, double ly) {
	if (branchId == 1) {
		xLen2 = lx;
		yLen2 = ly;
	}
	else {
		xLen1 = lx;
		yLen1 = ly;
	}
}

P3D BranchingLeaf::getChildLeafPosition() {
	P3D childPos = leafOrigin;
	if (branchId == 1) {
		childPos = leafOrigin + P3D(-xLen2, -yLen2, 0);
	}
	else {
		childPos = leafOrigin + P3D(xLen1, -yLen1, 0);
	}

	return childPos;
}

void TerminalLeaf::setObjectStateFromWidgets(TranslateWidget* tWidget) {
	setPosition(tWidget->pos);

	V3D newL = leafOrigin - tWidget->pos;
	xLen = abs(newL[0]);
	yLen = abs(newL[1]);

	if(parentBranch)
		parentBranch->setFromChildTerminalLeafParams(xLen, yLen);
}

double BranchingLeaf::getTorque() {
	double t = 0;
	double childLen = 0;

	// torque from branch's main leaf
	if (branchId == 1) {
		t = GRAVITY* mass*xLen1;
		childLen = -xLen2;
	}
	else {
		t = -GRAVITY * mass * xLen2;
		childLen = xLen1;
	}

	// torque contribution from all children leaves
	double netChildMass = 0;
	CalderLeafBaseObject* currentChild = this->child;

	while (!dynamic_cast<TerminalLeaf*>(currentChild)) {
		netChildMass += currentChild->mass;
		currentChild = dynamic_cast<BranchingLeaf*>(currentChild)->child;
	}

	if (dynamic_cast<TerminalLeaf*>(currentChild))
		netChildMass += currentChild->mass;

	//Logger::consolePrint("netChildmass:%lf, childLen:%lf\n", netChildMass, childLen);
	t += netChildMass*childLen*GRAVITY;
	return t;
}

dVector TerminalLeaf::getTorqueAnalyticalDerivative(bool addMass, bool addLength) {
	// get all design parameters of leaf
	DynamicArray<double> params;
	this->pushDesignParametersToList(params, addMass, addLength);

	// create grad vector of the size of design parameters of the leaf
	dVector grad(params.size());
	if(addMass)
		grad[0] = GRAVITY*getNetLengthFromHook();
	return grad;
}

double BranchingLeaf::getNetLengthFromHook() {

	double netL = 0;

	if (branchId == 1)
		netL += xLen1;
	else
		netL += -xLen2;

	BranchingLeaf* currentParent = this->parentBranch;

	while (currentParent) {
		if (currentParent->branchId == 1)
			netL = netL - currentParent->xLen2;
		else
			netL = netL + currentParent->xLen1;

		currentParent = currentParent->parentBranch;
	}

	return netL;
}

double TerminalLeaf::getNetLengthFromHook() {

	double netL = 0;
	BranchingLeaf* currentParent = this->parentBranch;

	while (currentParent) {
		if (currentParent->branchId == 1)
			netL = netL - currentParent->xLen2;
		else
			netL = netL + currentParent->xLen1;

		currentParent = currentParent->parentBranch;
	}

	return netL;
}

dVector BranchingLeaf::getTorqueAnalyticalDerivative(bool addMass, bool addLength) {

	// get all design parameters of leaf
	DynamicArray<double> params;
	this->pushDesignParametersToList(params, true, true);

	// create grad vector of the size of design parameters of the leaf
	dVector grad(params.size());
	// grad is zero wrt y-length params
	grad[2] = 0;
	grad[4] = 0;

	// get net mass of all children leaves
	double netChildMass = 0;
	CalderLeafBaseObject* currentChild = this->child;

	while (!dynamic_cast<TerminalLeaf*>(currentChild)) {
		netChildMass += currentChild->mass;
		currentChild = dynamic_cast<BranchingLeaf*>(currentChild)->child;
	}

	if (dynamic_cast<TerminalLeaf*>(currentChild))
		netChildMass += currentChild->mass;

	if (branchId == 1) {
		grad[0] = GRAVITY*getNetLengthFromHook();
		grad[1] = GRAVITY*mass;
		grad[3] = -GRAVITY*netChildMass;

	}
	else {
		grad[0] = GRAVITY * getNetLengthFromHook();
		grad[3] = -GRAVITY*mass;
		grad[1] = GRAVITY*netChildMass;
	}


	// get all design parameters of leaf
	DynamicArray<double> finalparams;
	this->pushDesignParametersToList(finalparams, addMass, addLength);
	dVector finalGrad(finalparams.size());
	if (addMass) {
		finalGrad[0] = grad[0];
	}
	if (addLength && addMass) {
		finalGrad[1] = grad[1];
		finalGrad[2] = grad[2];
		finalGrad[3] = grad[3];
		finalGrad[4] = grad[4];
	}
	if (addLength && !addMass) {
		finalGrad[0] = grad[1];
		finalGrad[1] = grad[2];
		finalGrad[2] = grad[3];
		finalGrad[3] = grad[4];
	}

	return finalGrad;
}