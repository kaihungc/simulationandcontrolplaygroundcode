#pragma once

#include "SkeletonFoldingEnergyFunction.h"
#include "SkeletonFoldingPlan.h"
#include "OptimizationLib/BFGSFunctionMinimizer.h"


/**
	This is a locomotion engine that works for arbitrary robot types
*/
class SkeletonFoldingOptimizer{
public:
	SkeletonFoldingOptimizer(SkeletonFoldingPlan* plan, SkeletonFoldingEnergyFunction* energyFunction);
	virtual ~SkeletonFoldingOptimizer(void);

	double optimizePlan(int maxIterNum);

public:
	BFGSFunctionMinimizer* BFGS_minimizer = NULL;
	SkeletonFoldingEnergyFunction* energyFunction;
	SkeletonFoldingPlan* plan;
};



