#include "SkeletonFoldingEnergyFunction.h"
#include "SkeletonFoldingInsideObjective.h"
#include <ControlLib/SkeletonSymmetryObjective.h>
#include "SkeletonFoldingRepulsionObjective.h"
#include "SkeletonFoldingRobotStateRegularizer.h"

SkeletonFoldingEnergyFunction::SkeletonFoldingEnergyFunction(SkeletonFoldingPlan* plan){
	this->plan = plan;
	regularizer = 1;

	printDebugInfo = false;

	setupSubObjectives();
}

SkeletonFoldingEnergyFunction::~SkeletonFoldingEnergyFunction(void){
	for (uint i=0; i < objectives.size(); i++)
		delete objectives[i];
}

void SkeletonFoldingEnergyFunction::setupSubObjectives(){
	for (uint i=0; i < objectives.size(); i++)
		delete objectives[i];

	objectives.clear();

	objectives.push_back(new SkeletonFoldingInsideObjective(plan, "fold skeleton inside objective", 10));
	objectives.push_back(new SkeletonFoldingRepulsionObjective(plan, "repulsion objective", 1e-4));
	objectives.push_back(new SkeletonSymmetryObjective(plan->gcRobotRepresentation, plan->symmMap, true, "symmetry objective", 1e6));
	//objectives.push_back(new SkeletonFoldingRobotStateRegularizer(plan, 6, plan->gcRobotRepresentation->getDimensionCount(), "robot state regularizer", 1e-2));
}

double SkeletonFoldingEnergyFunction::computeValueWithoutRegularizer(const dVector& p, bool printInfo)
{
	plan->setParametersFromList(p);

	if (printInfo)
	{
		Logger::consolePrint("-------------------------------\n");
		Logger::logPrint("-------------------------------\n");
	}

	double totalVal = 0;
	for (uint i = 0; i < objectives.size(); i++) {
		double w = objectives[i]->weight;
		double v = objectives[i]->computeValue(p);
		totalVal += v;
		if (printInfo)
		{
			Logger::consolePrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
			Logger::logPrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
		}
	}

	if (printInfo)
	{
		Logger::consolePrint("=====> total cost: %lf\n", totalVal);
		Logger::logPrint("=====> total cost: %lf\n", totalVal);
	}

	return totalVal;
}

double SkeletonFoldingEnergyFunction::computeValue(const dVector& p){
	//assert(s.size() == theMotionPlan->paramCount);

	plan->setParametersFromList(p);

	double totalEnergy = 0;

	for (uint i=0; i<objectives.size(); i++)
		totalEnergy += objectives[i]->computeValue(p);

	//add the regularizer contribution
	if (regularizer > 0){
		tmpVec.resize(p.size());
		if (m_p0.size() != p.size()) m_p0 = p;
		tmpVec = p - m_p0;
		totalEnergy += 0.5*regularizer*tmpVec.dot(tmpVec);
	}

	return totalEnergy;
}

//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
void SkeletonFoldingEnergyFunction::updateRegularizingSolutionTo(const dVector &currentP){
	m_p0 = currentP;
}

void SkeletonFoldingEnergyFunction::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
	plan->setParametersFromList(p);

	//add the contribution from the regularizer
	if (regularizer > 0){
		for (int i=0;i<p.size();i++)
			ADD_HES_ELEMENT(hessianEntries, i, i, regularizer, 1);
	}

	//and now the contributions of the individual objectives
	for (uint i = 0; i < objectives.size(); i++)
		objectives[i]->addHessianEntriesTo(hessianEntries, p);
}

void SkeletonFoldingEnergyFunction::addGradientTo(dVector& grad, const dVector& p) {
	plan->setParametersFromList(p);

	resize(grad, p.size());

	//add the contribution from the regularizer
	if (regularizer > 0) {
		if (m_p0.size() != p.size()) m_p0 = p;
		grad = (p - m_p0) * regularizer;
	}

	//and now the contributions of the individual objectives
	for (uint i = 0; i<objectives.size(); i++)
		objectives[i]->addGradientTo(grad, p);
}

//this method gets called whenever a new best solution to the objective function is found
void SkeletonFoldingEnergyFunction::setCurrentBestSolution(const dVector& p){
	updateRegularizingSolutionTo(p);
	plan->setParametersFromList(p);

	if (printDebugInfo){
		Logger::consolePrint("-------------------------------\n");
		Logger::logPrint("-------------------------------\n");

		double totalVal = 0;

		for (uint i=0; i<objectives.size(); i++){
			double w = objectives[i]->weight;
			double v = objectives[i]->computeValue(p);
			totalVal += v;
			Logger::consolePrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
			Logger::logPrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
		}

		Logger::consolePrint("=====> total cost: %lf\n", totalVal);
		Logger::logPrint("=====> total cost: %lf\n", totalVal);
	}
}


