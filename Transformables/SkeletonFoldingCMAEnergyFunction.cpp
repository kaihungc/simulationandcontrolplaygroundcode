#include "SkeletonFoldingCMAEnergyFunction.h"
#include "SkeletonDecompObjective.h"
#include "SkeletonEmbedInsideObjective.h"
#include "SkeletonCollisionObjective.h"
#include "SkeletonJointCarvingObjective.h"
#include "SkeletonMorphRegularizer.h"
#include "TransformEngine.h"
#include <ControlLib/SkeletonSymmetryObjective.h>


SkeletonFoldingCMAEnergyFunction::SkeletonFoldingCMAEnergyFunction(SkeletonFoldingPlan* plan){
	this->plan = plan;

	setupSubObjectives();
}

SkeletonFoldingCMAEnergyFunction::~SkeletonFoldingCMAEnergyFunction(void){
	for (uint i=0; i < objectives.size(); i++)
		delete objectives[i];
}

void SkeletonFoldingCMAEnergyFunction::setupSubObjectives(){
	for (uint i=0; i < objectives.size(); i++)
		delete objectives[i];

	objectives.clear();

	TransformEngine* engine = (TransformEngine*)plan->transformEngine;

	objectives.push_back(new SkeletonDecompObjective(plan, "Decomposition Objective", 1));
	objectives.push_back(new SkeletonEmbedInsideObjective(plan, "Inside Objective", engine->transParams->useLivingBracket ? 1e2 : 1e3));
	objectives.push_back(new SkeletonCollisionObjective(plan, "Collision Objective", 1e3));
	objectives.push_back(new SkeletonJointCarvingObjective(plan, "Joint Carving Objective", 1));
	objectives.push_back(new SkeletonMorphRegularizer(plan, "Morphology Regularizer", 1e1));
}


double SkeletonFoldingCMAEnergyFunction::computeValue(const dVector& p){

	// plan->setParametersFromList(p);
	// plan->setSymParamsFromList(p);
	plan->setSymMorphParamsFromList(p);

	double totalVal = 0;

	for (uint i=0; i<objectives.size(); i++)
		totalVal += objectives[i]->computeValue(p);

	return totalVal;
}

double SkeletonFoldingCMAEnergyFunction::printValue(const dVector& p)
{
	plan->setSymMorphParamsFromList(p);

	Logger::consolePrint("-------------------------------\n");
	Logger::logPrint("-------------------------------\n");

	double totalVal = 0;
	for (uint i = 0; i < objectives.size(); i++) {
		double w = objectives[i]->weight;
		double v = objectives[i]->computeValue(p);
		totalVal += v;
		Logger::consolePrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
		Logger::logPrint("%s: %lf (weight: %lf)\n", objectives[i]->description.c_str(), v, w);
	}

	Logger::consolePrint("=====> total cost: %lf\n", totalVal);
	Logger::logPrint("=====> total cost: %lf\n", totalVal);

	return totalVal;
}
