#include "FabricationEngine.h"
#include "TransformUtils.h"
#include <MathLib/MeshBoolean.h>
#include <MathLib/ConvexHull3D.h>


FabricationEngine::FabricationEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine)
{
	generateHardcodedFeaturePoints();
}

FabricationEngine::~FabricationEngine()
{
	TransformUtils::clearMeshMap(jointSlitMeshes);
	TransformUtils::clearMeshMap(skeletonLSMeshes);
}

void FabricationEngine::getTransferDirection()
{
	transDir.clear();

	Robot* robot = transformEngine->robot;
	auto& RBIndexMap = transformEngine->RBIndexMap;
	auto& MlevelSets = transformEngine->MlevelSets;

	robot->setState(transformEngine->foldedState);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		P3D jPos = joint->getWorldPosition();
		int parentID = RBIndexMap[joint->parent];
		int childID = RBIndexMap[joint->child];

		double parentVal = MlevelSets[parentID].interpolateData(jPos[0], jPos[1], jPos[2]);
		double childVal = MlevelSets[childID].interpolateData(jPos[0], jPos[1], jPos[2]);

		if (parentVal < childVal)
			transDir.push_back(0);
		else
			transDir.push_back(1);

		// Logger::print("joint %d, dir %d\n", i, transDir.back());
	}
}

void FabricationEngine::attachSkeletonMeshes() {

	double radius = SKELETON_MESH_RADIUS;
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;

	for (uint i = 1; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		GLMesh* resMesh = NULL;
		vector<P3D> endPoints;
		endPoints.push_back(rb->pJoints[0]->cJPos);
		for (auto cJoint : rb->cJoints)
			endPoints.push_back(cJoint->pJPos);
		if (!rb->rbProperties.endEffectorPoints.empty())
			endPoints.push_back(rb->rbProperties.endEffectorPoints[0].coords);

		if (endPoints.size() > 2)
		{
			P3D start;
			for (P3D end : endPoints)
			{
				V3D delta = SKELETON_MESH_CLEARANCE * (end - start).normalized();
				end -= delta;
				GLMesh* cylinderMesh = TransformUtils::getCylinderMesh(start, end, radius);
				if (resMesh)
				{
					MESHBOOLEAN_INTRUSIVE(resMesh, cylinderMesh, "Union");
				}
				else {
					resMesh = cylinderMesh;
				}
			}
		}
		else {
			P3D start = endPoints[0];
			P3D end = endPoints[1];
			V3D delta = SKELETON_MESH_CLEARANCE * (end - start).normalized();
			end -= delta;
			start += delta;
			resMesh = TransformUtils::getCylinderMesh(start, end, radius);
		}

		if (rb->meshes.empty()) {
			robotMeshes[rb] = resMesh;
			rb->meshes.push_back(resMesh);
		}
		else {
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], resMesh, "Union");
			rb->meshes[0] = robotMeshes[rb];
		}
	}
}

void FabricationEngine::generateJointSlitMeshes()
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	jointScale = transformEngine->transParams->jointScale;

	TransformUtils::clearMeshMap(jointSlitMeshes);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		RigidBody* rbA;
		RigidBody* rbB;
		P3D orig;
		P3D end;
		if (transDir[i] == 0) {
			rbA = joint->parent;
			rbB = joint->child;
			orig = joint->pJPos;
		}
		else {
			rbA = joint->child;
			rbB = joint->parent;
			orig = joint->cJPos;
		}

		vector<P3D> endPoints;
		vector<ReducedRobotState> animationStates;
		double sampleStep = 0.2;
		TransformUtils::getAnimationStatesForJoint(animationStates, i, foldedState, unfoldedState, sampleStep);
		for (uint j = 0; j < animationStates.size(); j++)
		{
			robot->setState(&animationStates[j]);
			P3D localEnd = rbA->getLocalCoordinates(rbB->getWorldCoordinates(end));
			localEnd += (localEnd - orig).normalized();
			endPoints.push_back(localEnd);
		}

		GLMesh* slitMesh = TransformUtils::computeJointSlitMesh(orig, endPoints, SLIT_WIDTH, SLIT_RADIUS);

		if (jointSlitMeshes.count(rbA)) {
			MESHBOOLEAN_INTRUSIVE(jointSlitMeshes[rbA], slitMesh, "Union");
			delete slitMesh;
		}
		else {
			jointSlitMeshes[rbA] = slitMesh;
		}
	}

}

void FabricationEngine::handleMainBody()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& parentMap = transformEngine->parentMap;
	Robot* robot = transformEngine->robot;

	RigidBody* root = robot->getRoot();
	GLMesh* resMesh = NULL;

	// carve slit and sockets
	if (!root->meshes.empty())
	{
		if (jointSlitMeshes.count(root)) {
			resMesh = jointSlitMeshes[root]->clone();
		}

		for (uint i = 0; i < root->cJoints.size(); i++)
		{
			Joint* joint = root->cJoints[i];
			GLMesh* jointCarveMesh = transDir[joint->jIndex] == 0 ? jointSocketCarveMesh : jointPlugCarveMesh;
			GLMesh* cCarveMesh = TransformUtils::getTransformedJointMesh(jointCarveMesh, joint, jointScale, true);

			if (resMesh)
			{
				MESHBOOLEAN_INTRUSIVE(resMesh, cCarveMesh, "Union");
				delete cCarveMesh;
			}
			else {
				resMesh = cCarveMesh;
			}
		}

		MESHBOOLEAN_INTRUSIVE(robotMeshes[root], resMesh, "Minus");
		delete resMesh;
	}

	// carve for skeleton meshes of other parts
	/*for (uint i = 1; i < RBs.size(); i++)
	{

		RigidBody* rb = RBs[i];
		if (parentMap[rb] == root)
			continue;

		P3D start = rb->pJoints[0]->cJPos;
		P3D end;
		if (!rb->cJoints.empty())
			end = rb->cJoints[0]->pJPos;
		else if (!rb->rbProperties.endEffectorPoints.empty()) {
			for (uint j = 0; j < rb->rbProperties.endEffectorPoints.size(); j++)
				end += rb->rbProperties.endEffectorPoints[j].coords;

			end /= rb->rbProperties.endEffectorPoints.size();
		}
		V3D delta = BODY_SLIT_CLEARANCE * (end - start).normalized();
		start -= delta;
		end += delta;

		start = root->getLocalCoordinates(rb->getWorldCoordinates(start));
		end = root->getLocalCoordinates(rb->getWorldCoordinates(end));

		GLMesh* cylinderMesh = TransformUtils::getCylinderMesh(start, end, BODY_SLIT_RADIUS);

		if (resMesh)
		{
			MESHBOOLEAN_INTRUSIVE(resMesh, cylinderMesh, "Union");
			delete cylinderMesh;
		}
		else {
			resMesh = cylinderMesh;
		}
	}*/

	resMesh = NULL;

	// union skeleton mesh (can be unnecessary)
	for (uint i = 0; i < root->cJoints.size(); i++)
	{
		P3D start;
		Joint* joint = root->cJoints[i];
		if (transDir[joint->jIndex] == 0) continue;
		
		P3D end = joint->pJPos;
		V3D delta = SKELETON_MESH_CLEARANCE * (end - start).normalized();
		end -= delta;
		start -= delta;

		GLMesh* cylinderMesh = TransformUtils::getCylinderMesh(start, end, SKELETON_MESH_RADIUS);
		if (resMesh)
		{
			meshBooleanIntrusive(resMesh, cylinderMesh, "Union");
			delete cylinderMesh;
		}
		else {
			resMesh = cylinderMesh;
		}
	}

	// attach joint meshes
	for (uint i = 0; i < root->cJoints.size(); i++)
	{
		Joint* joint = root->cJoints[i];
		GLMesh* jointMesh = transDir[joint->jIndex] == 0 ? jointSocketMesh : jointPlugMesh;
		GLMesh* cMesh = TransformUtils::getTransformedJointMesh(jointMesh, joint, jointScale, true);

		if (resMesh)
		{
			MESHBOOLEAN_INTRUSIVE(resMesh, cMesh, "Union");
			delete cMesh;
		}
		else {
			resMesh = cMesh;
		}
	}

	if (robotMeshes.count(root))
	{
		MESHBOOLEAN_INTRUSIVE(robotMeshes[root], resMesh, "Union");
		delete resMesh;
	}
	else {
		robotMeshes[root] = resMesh;
	}

	root->meshes[0] = robotMeshes[root];
}

void FabricationEngine::attachJointMeshes()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;

	RigidBody* root = RBs[0];

	for (uint i = 1; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty()) {
			continue;
		}

		// do the slit carving before attaching socket
		if (jointSlitMeshes.count(rb)) {
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], jointSlitMeshes[rb], "Minus");
		}

		// attach plugs
		for (auto joint : rb->pJoints)
		{
			GLMesh* jointCarveMesh = transDir[joint->jIndex] == 0 ? jointPlugCarveMesh : jointSocketCarveMesh;
			GLMesh* pCarveMesh = TransformUtils::getTransformedJointMesh(jointCarveMesh, joint, jointScale, false);
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], pCarveMesh, "Minus");
			delete pCarveMesh;

			GLMesh* jointMesh = transDir[joint->jIndex] == 0 ? jointPlugMesh : jointSocketMesh;
			GLMesh* pMesh = TransformUtils::getTransformedJointMesh(jointMesh, joint, jointScale, false);
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], pMesh, "Union");
			delete pMesh;
		}

		// attach sockets
		for (auto joint : rb->cJoints)
		{
			GLMesh* jointCarveMesh = transDir[joint->jIndex] == 0 ? jointSocketCarveMesh : jointPlugCarveMesh;
			GLMesh* cCarveMesh = TransformUtils::getTransformedJointMesh(jointCarveMesh, joint, jointScale, true);
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], cCarveMesh, "Minus");
			delete cCarveMesh;

			GLMesh* jointMesh = transDir[joint->jIndex] == 0 ? jointSocketMesh : jointPlugMesh;
			GLMesh* cMesh = TransformUtils::getTransformedJointMesh(jointMesh, joint, jointScale, true);
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], cMesh, "Union");
			delete cMesh;
		}

		rb->meshes[0] = robotMeshes[rb];
	}

}

void FabricationEngine::prepareForLSCarving()
{
	auto& MlevelSets = transformEngine->MlevelSets;
	auto& largeShellLevelSet = transformEngine->largeShellLevelSet;

	if (transformEngine->transParams->shellOnly)
	{
		for (uint i = 0; i < MlevelSets.size(); i++)
		{
			MlevelSets[i].intersectLS(&largeShellLevelSet);
		}
	}

	// generateSkeletonLevelSets();
}

void FabricationEngine::generateSkeletonLevelSets()
{
	auto& RBs = transformEngine->RBs;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	double LSStepSize = transformEngine->transParams->LSStepSize;
	bool shellOnly = transformEngine->transParams->shellOnly;
	bool useLivingBracket = transformEngine->transParams->useLivingBracket;
	double clearance = shellOnly? 0.004 : 0.002;
	if (useLivingBracket)
		clearance = 0.006;

	TransformUtils::clearMeshMap(skeletonLSMeshes);
	skeletonLSMeshes.clear();
	skeletonLevelSets.clear();

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		auto& origMeshes = RBOrigMeshes[rb];
		auto& carveMeshes = rb->carveMeshes;
		auto& meshTrans = rb->meshTransformations;
		auto& meshDescriptions = rb->meshDescriptions;
		vector<GLMesh*> transCarveMeshes;

		if (useLivingBracket)
		{
			if (origMeshes.empty())
				continue;

			set<string> meshFilter = {
				"skeleton", "motor"
			};

			map<string, string> motorFileMap;
			motorFileMap["../data/robotDesigner/motorMeshes/XM-430.obj"]
				= "../data/robotDesigner/motorMeshes/XM-430_simp.obj";

			for (uint j = 0; j < origMeshes.size(); j++)
			{
				GLMesh* tMesh = origMeshes[j];
				Transformation& trans = meshTrans[j];
				if (meshFilter.count(meshDescriptions[j]) == 0) continue;

				if (motorFileMap.count(tMesh->path))
					tMesh = GLContentManager::getGLMesh(motorFileMap[tMesh->path].c_str());

				tMesh = tMesh->clone();
				tMesh->transform(trans);
				transCarveMeshes.push_back(tMesh);
			}
		}
		else if (shellOnly)
		{
			if (carveMeshes.empty())
				continue;

			transCarveMeshes = carveMeshes;
			for (uint j = 0; j < carveMeshes.size(); j++)
			{
				if (!carveMeshes[j]) continue;
				transCarveMeshes[j] = carveMeshes[j]->clone();
				transCarveMeshes[j]->transform(meshTrans[j]);
			}
		}
		else {
			double radius = 0.015;
			P3D start = rb->pJoints.empty() ? P3D() : rb->pJoints[0]->cJPos;
			for (auto& joints : rb->cJoints)
			{
				P3D end = joints->pJPos;
				V3D delta = BODY_SLIT_CLEARANCE * (end - start).normalized();
				P3D nStart = start + delta * -1;
				P3D nEnd = end + delta;
				GLMesh* cylinderMesh = TransformUtils::getCylinderMesh(nStart, nEnd, radius);
				transCarveMeshes.push_back(cylinderMesh);
			}
			auto& EEs = rb->rbProperties.endEffectorPoints;
			if (EEs.size() > 0)
			{
				P3D end = EEs[0].coords;
				V3D delta = BODY_SLIT_CLEARANCE * (end - start).normalized();
				P3D nStart = start + delta * -1;
				P3D nEnd = end;
				GLMesh* cylinderMesh = TransformUtils::getCylinderMesh(nStart, nEnd, radius);
				transCarveMeshes.push_back(cylinderMesh);
			}
		}

		AxisAlignedBoundingBox bbox;
		bbox.empty();
		for (uint j = 0; j < transCarveMeshes.size(); j++)
		{
			GLMesh* cMesh = transCarveMeshes[j];
			if (!cMesh) continue;

			for (int k = 0; k < cMesh->getVertexCount(); k++)
				bbox.addPoint(cMesh->getVertex(k));
		}
		bbox.setbmax(bbox.bmax() + V3D(1, 1, 1) * 2 * LSStepSize);
		bbox.setbmin(bbox.bmin() + V3D(1, 1, 1) * -2 * LSStepSize);

		skeletonLevelSets[rb] = LevelSet(bbox, 0.5 * LSStepSize);

		bool flag = false;
		for (uint j = 0; j < transCarveMeshes.size(); j++)
		{
			if (transCarveMeshes[j]) {
				flag = true;
				skeletonLevelSets[rb].updateLevelSet(transCarveMeshes[j], [](double origVal, double inputVal) {return min(origVal, inputVal); });
			}	
		}
		if (skeletonMeshes.count(rb)) {
			flag = true;
			skeletonLevelSets[rb].updateLevelSet(skeletonMeshes[rb], [](double origVal, double inputVal) {return min(origVal, inputVal); });
		}
			
		if (flag) {
			skeletonLevelSets[rb].addConstant(-clearance);
			GLMesh* sMesh = skeletonLevelSets[rb].extractMesh(0);
			if (sMesh)
			{
				skeletonLSMeshes[rb] = sMesh;
				skeletonLSMeshes[rb]->computeNormals();
			}
		}

		for (uint j = 0; j < transCarveMeshes.size(); j++)
			delete transCarveMeshes[j];
	}
}

void FabricationEngine::generateHardcodedFeaturePoints()
{
	double clearance = 0.0003;

	vector<P3D> hornFP, bottomFP, footFP, plugFP, socketFP;

	// horn
	hornFP.push_back(P3D(0, 0.015 - clearance, 0));
	// hornFP.push_back(P3D(0, -0.005, 0));
	hornFP.push_back(P3D(-0.015, 0.015 - clearance, -0.018));
	hornFP.push_back(P3D(0.015, 0.015 - clearance, -0.018));
	hornFP.push_back(P3D(0.015, 0.015 - clearance, 0.018));
	hornFP.push_back(P3D(-0.015, 0.015 - clearance, 0.018));

	// bottom
	bottomFP.push_back(P3D(0, -0.01 + clearance, 0));
	// bottomFP.push_back(P3D(0, 0.005, 0));
	bottomFP.push_back(P3D(-0.012, -0.01 + clearance, -0.015));
	bottomFP.push_back(P3D(0.012, -0.01 + clearance, -0.015));
	bottomFP.push_back(P3D(0.012, -0.01 + clearance, 0.015));
	bottomFP.push_back(P3D(-0.012, -0.01 + clearance, 0.015));

	// foot
	footFP.push_back(P3D(0, 0.015 - clearance, 0));
	// footFP.push_back(P3D(0, -0.005, 0));
	footFP.push_back(P3D(-0.015, 0.015 - clearance, -0.018));
	footFP.push_back(P3D(0.015, 0.015 - clearance, -0.018));
	footFP.push_back(P3D(0.015, 0.015 - clearance, 0.018));
	footFP.push_back(P3D(-0.015, 0.015 - clearance, 0.018));

	// socket
	socketFP.push_back(P3D(0, 0, -0.010 + clearance));
	// plug
	plugFP.push_back(P3D(0, 0, -0.010 + clearance));

	moduleFeaturePoint["../data/robotDesigner/motorMeshes/XL-320_HornBracket.obj"] = hornFP;
	moduleFeaturePoint["../data/robotDesigner/motorMeshes/XL-320_BottomBracket.obj"] = bottomFP;
	moduleFeaturePoint["../data/robotDesigner/motorMeshes/XL-320_PointFoot.obj"] = footFP;
	moduleFeaturePoint["../data/transformables/jointMeshes/HingeJointSocket.obj"] = socketFP;
	moduleFeaturePoint["../data/transformables/jointMeshes/HingeJointPlug.obj"] = plugFP;
}

void FabricationEngine::generateCylinderSkeletonMeshes(RigidBody* selectedRB)
{
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	auto& RBs = transformEngine->RBs;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	bool useVolume = !(transformEngine->transParams->shellOnly);
	double radius = useVolume ? 0.006 : 0.012;

	if (!selectedRB)
		TransformUtils::clearMeshMap(skeletonMeshes);
	else if (skeletonMeshes.count(selectedRB)) {
		delete skeletonMeshes[selectedRB];
		skeletonMeshes.erase(selectedRB);
	}

	for (uint i = 1; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (selectedRB && selectedRB != rb) continue;

		auto& origMeshes = RBOrigMeshes[rb];
		auto& meshTrans = rb->meshTransformations;
		vector<P3D> FPs;

		for (uint j = 0; j < origMeshes.size(); j++)
		{
			auto itr = moduleFeaturePoint.find(origMeshes[j]->path);
			if (itr != moduleFeaturePoint.end())
			{
				P3D p = meshTrans[j].transform((itr->second)[0]);
				FPs.push_back(p);
			}
		}

		if (FPs.size() == 1 && rb->rbProperties.endEffectorPoints.size() > 0) {
			FPs.push_back(P3D(rb->rbProperties.endEffectorPoints[0].coords));
		}
		if (FPs.size() < 2 || (FPs[0] - FPs[1]).norm() < 0.01) continue;

		GLMesh* tMesh = TransformUtils::getCylinderMesh(FPs[0], FPs[1], radius);
		skeletonMeshes[rb] = tMesh->getSplitVerticeMesh();
		delete tMesh;
	}

	// generateBodyMesh();
}

void FabricationEngine::generateBodyMesh()
{
	GLMesh*& bodyMesh = transformEngine->bodyMesh;
	delete bodyMesh;
	bodyMesh = new GLMesh();

	double halfHeight = 0.01;
	vector<P3D> points;
	
	RigidBody* root = transformEngine->robot->root;
	auto& rootMeshes = transformEngine->RBOrigMeshes[root];
	bool shellOnly = transformEngine->transParams->shellOnly;
	double radius = shellOnly ? 0.012 : 0.006;

	if (shellOnly)
	{
		for (uint i = 0; i < rootMeshes.size(); i++)
		{
			GLMesh* rMesh = rootMeshes[i];
			Transformation& trans = root->meshTransformations[i];
			AxisAlignedBoundingBox bbox = rMesh->calBoundingBox();
			bbox.setbmin(bbox.bmin() + V3D(1, 1, 1) * 1e-5);
			bbox.setbmax(bbox.bmax() + V3D(1, 1, 1) * -1e-5);

			for (int j = 0; j < 8; j++)
			{
				points.push_back(trans.transform(bbox.getVertex(j)));
			}
		}

		/*points.push_back(P3D(0, halfHeight, 0));
		points.push_back(P3D(0, -halfHeight, 0));

		for (auto& joint : root->cJoints)
		{
			P3D jPos = joint->pJPos;
			points.push_back(jPos + V3D(0, halfHeight, 0));
			points.push_back(jPos + V3D(0, -halfHeight, 0));
		}*/

		if (points.size() < 4) return;

		ConvexHull3D::computeConvexHullFromSetOfPoints(points, bodyMesh, true);
	}
	else {
		for (auto& joint : root->cJoints)
		{
			GLMesh* tMesh = TransformUtils::getCylinderMesh(P3D(), joint->pJPos, radius);
			tMesh->splitVertices();
			bodyMesh->append(tMesh);
			delete tMesh;
		}
	}
	
}

void FabricationEngine::carveLS(int mode)
{
	prepareForLSCarving();

	Robot* robot = transformEngine->robot;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;

	double sampleStep = 0.0125;
	vector<ReducedRobotState> stateSequence;
	TransformUtils::getAnimationStates(stateSequence, transformEngine->foldingOrder, unfoldedState, foldedState, sampleStep);
	auto& motionPlanStates = transformEngine->motionPlanStates;
	stateSequence.insert(stateSequence.begin(), motionPlanStates.begin(), motionPlanStates.end());
	Logger::print("Begin LevelSet Carving, state size: %d\n", stateSequence.size());
	transformEngine->timer.restart();
	if (mode & 1) {
		Logger::print("Carve For Skeleton...\n");
		carveSkeletonLSForStates(stateSequence);
	}

	if (mode & 2) {
		Logger::print("Carve For Meshes...\n");
		carveRBMeshForStates(stateSequence);
	}

	Logger::print("Finished LevelSet Carving, time: %lf\n", transformEngine->timer.timeEllapsed());
}

void FabricationEngine::carveSkeletonLSForStates(vector<ReducedRobotState>& stateSequence) {

	//if (attachmentSegs.empty())
	//	generateAttachmentSegs();

	Robot* robot = transformEngine->robot;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	auto& MlevelSets = transformEngine->MlevelSets;
	auto& RBs = transformEngine->RBs;
	auto& parentMap = transformEngine->parentMap;
	bool shellOnly = transformEngine->transParams->shellOnly;
	map<RigidBody*, Transformation> foldedStateL2W;
	map<RigidBody*, Transformation> foldedStateW2L;

	robot->setState(foldedState);
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		foldedStateL2W[rb] = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
		foldedStateW2L[rb] = foldedStateL2W[rb].inverse();
	}

//	if (shellOnly)
//	{
//#pragma omp parallel for
//		for (int j = 0; j < (int)RBs.size(); j++)
//		{
//			RigidBody* rb = RBs[j];
//			if (attachmentSegs.count(rb) == 0) continue;
//			Segment RBSeg = attachmentSegs[rb];
//			V3D dir = RBSeg.b - RBSeg.a;
//			int n = 10;
//			double spacing = 0.012 / n;
//
//			for (int i = 0; i <= n; i++)
//			{
//				Transformation trans;
//				trans.T = dir * spacing * i;
//				MlevelSets[j].minusLSRelTrans(&skeletonLevelSets[rb], trans * foldedStateW2L[rb]);
//			}
//		}
//	}

	for (uint k = 0; k < stateSequence.size(); k++)
	{
		robot->setState(&stateSequence[k]);
		for (auto& itr : skeletonLevelSets)
		{
			RigidBody* o_rb = itr.first;
			LevelSet& skeletonLS = itr.second;

#pragma omp parallel for
			for (int j = 0; j < (int)RBs.size(); j++)
			{
				RigidBody* n_rb = RBs[j];
				 if ((parentMap[n_rb] == o_rb || parentMap[o_rb] == n_rb))
					 continue;

				if (n_rb != o_rb)
				{
					Transformation n_W2L = foldedStateW2L[n_rb];
					Transformation n_L2W(n_rb->state.orientation.getRotationMatrix(), n_rb->state.position);
					Transformation o_W2L = Transformation(o_rb->state.orientation.getRotationMatrix(), o_rb->state.position).inverse();
					Transformation relTrans = o_W2L * n_L2W * n_W2L;
					MlevelSets[j].minusLSRelTrans(&skeletonLS, relTrans);
				}
			}
		}
	}

}

void FabricationEngine::carveRBMeshForStates(vector<ReducedRobotState>& stateSequence)
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	auto& MlevelSets = transformEngine->MlevelSets;
	auto& RBs = transformEngine->RBs;
	auto& RBIndexMap = transformEngine->RBIndexMap;
	auto& parentMap = transformEngine->parentMap;
	map<RigidBody*, Transformation> foldedStateL2W;
	map<RigidBody*, Transformation> foldedStateW2L;

	auto shellLevelSets = MlevelSets;
	if (transformEngine->transParams->shellOnly)
	{
		for (uint i = 0; i < MlevelSets.size(); i++)
		{
			shellLevelSets[i].intersectLS(&transformEngine->shellLevelSet);
		}
	}

	robot->setState(foldedState);
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		foldedStateL2W[rb] = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
		foldedStateW2L[rb] = foldedStateL2W[rb].inverse();
	}

	for (uint k = 0; k < stateSequence.size(); k++)
	{
		robot->setState(&stateSequence[k]);
		for (int i = 0; i < robot->getJointCount(); i++)
		{
			RigidBody* parentRB = robot->getJoint(i)->parent;
			RigidBody* childRB = robot->getJoint(i)->child;
			int parentIndex = RBIndexMap[parentRB];
			int childIndex = RBIndexMap[childRB];
			Transformation parentW2L = foldedStateW2L[parentRB];
			Transformation parentL2W(parentRB->state.orientation.getRotationMatrix(), parentRB->state.position);
			Transformation childW2L = Transformation(childRB->state.orientation.getRotationMatrix(), childRB->state.position).inverse();
			Transformation childL2W = foldedStateL2W[childRB];
			Transformation relTrans = childL2W * childW2L * parentL2W * parentW2L;

			MlevelSets[parentIndex].minusLSRelTrans(&shellLevelSets[childIndex], relTrans);
		}

#pragma omp parallel for
		for (int i = 0; i < (int)RBs.size() - 1; i++)
		{
			RigidBody* rbI = RBs[i];
			Transformation rbIW2L = foldedStateW2L[rbI];
			Transformation rbIL2W(rbI->state.orientation.getRotationMatrix(), rbI->state.position);

			for (int j = i + 1; j < (int)RBs.size(); j++)
			{
				RigidBody* rbJ = RBs[j];
				if (parentMap[rbJ] == rbI || parentMap[rbI] == rbJ) continue;

				Transformation rbJW2L = Transformation(rbJ->state.orientation.getRotationMatrix(), rbJ->state.position).inverse();
				Transformation rbJL2W = foldedStateL2W[rbJ];
				Transformation relTrans = rbJL2W * rbJW2L * rbIL2W * rbIW2L;

				MlevelSets[i].minusLSRelTrans(&shellLevelSets[j], relTrans);
			}
		}
	}

}

void FabricationEngine::carveSkeletonPreciselyForRB()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty()) continue;
		auto& carveMeshes = rb->carveMeshes;
		auto& meshTrans = rb->meshTransformations;

		if (skeletonMeshes.count(rb))
			meshBooleanIntrusive(robotMeshes[rb], skeletonMeshes[rb], "Minus");

		for (uint j = 0; j < carveMeshes.size(); j++)
		{
			if (!carveMeshes[j]) continue;

			GLMesh* cMesh = carveMeshes[j]->clone();
			cMesh->transform(meshTrans[j]);
			bool res = meshBooleanIntrusive(robotMeshes[rb], cMesh, "Minus");
			delete cMesh;
			if (!res)
			{
				Logger::print("%s vertex num: %d\n", carveMeshes[j]->path.c_str(), robotMeshes[rb]->getVertexCount());
				break;
			}
		}

		rb->meshes[0] = robotMeshes[rb];
	}
}

void FabricationEngine::generateAttachmentSegs()
{
	auto& robot = transformEngine->robot;
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	auto& meshLSGrad = transformEngine->meshLSGrad;
	auto& meshLevelSet = transformEngine->meshLevelSet;

	robot->setState(transformEngine->foldedState);
	attachmentSegs.clear();

	for (uint i = 1; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty()) continue;
		auto& meshTrans = rb->meshTransformations;
		auto& origMeshes = RBOrigMeshes[rb];
		Transformation L2W = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
		Transformation W2L = L2W.inverse();

		vector<P3D> FPs;
		for (uint j = 0; j < origMeshes.size(); j++)
		{
			auto itr = moduleFeaturePoint.find(origMeshes[j]->path);
			if (itr != moduleFeaturePoint.end())
			{
				P3D p = meshTrans[j].transform((itr->second)[0]);
				FPs.push_back(p);
			}
		}
		if (FPs.size() < 2 || (FPs[0] - FPs[1]).norm() < 0.01) continue;

		P3D midP = (FPs[0] + FPs[1]) * 0.5;
		P3D wMidP = L2W.transform(midP);
		V3D normal = (FPs[1] - FPs[0]).normalized();
		V3D wNormal = L2W.transform(normal);
		GLMesh* rbMesh = robotMeshes[rb];
		rbMesh->calBoundingBox();

		V3D dir;
		if (meshLevelSet.interpolateData(wMidP[0], wMidP[1], wMidP[2]) > -0.01)
		{
			V3D grad = meshLSGrad.interpolateData(wMidP[0], wMidP[1], wMidP[2]).normalized();
			dir = grad - wNormal * grad.dot(wNormal);
			dir.normalized();
		}
		else {
			V3D x = normal.cross(V3D(1, 0, 0)).normalized();
			V3D y = normal.cross(x).normalized();
			bool isIsect[36];
			for (int i = 0; i < 36; i++)
			{
				double theta = i * PI / 18;
				V3D d = cos(theta) * x + sin(theta) * y;
				Ray ray(midP, d);
				isIsect[i] = rbMesh->getDistanceToRayOriginIfHit(ray);
				// if (isIsect[i]) Logger::print("%d ", i);
			}

			int midIndex;
			if (isIsect[0])
			{
				int startIndex = 35;
				int endIndex = 0;
				while (isIsect[startIndex] && startIndex > 0)
					startIndex--;
				while (isIsect[endIndex] && endIndex < 36)
					endIndex++;
				if (startIndex == 0)
					midIndex = 0;
				else
					midIndex = (startIndex - 36 + endIndex) / 2;
			}
			else {
				int startIndex = 0;
				int endIndex = 35;
				while (!isIsect[startIndex] && startIndex < 36)
					startIndex++;
				while (!isIsect[endIndex] && endIndex > 0)
					endIndex--;
				if (endIndex == 0)
					midIndex = 0;
				else
					midIndex = (startIndex + endIndex) / 2;
			}
			// Logger::print(", midIndex: %d\n", midIndex);

			double theta = midIndex * PI / 18;
			dir = x * cos(theta) + y * sin(theta);

			// dir = L2W.transform(dir.normalized());
		}

		attachmentSegs[rb] = Segment(midP, midP + dir);
	}
}

void FabricationEngine::generateSocketStructureForRB()
{
	Logger::print("generate attachment structure...");
	if (attachmentSegs.empty())
		generateAttachmentSegs();

	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	GLMesh* socketPlugMesh = GLContentManager::getGLMesh("../data/transformables/openScad/plug.obj");
	GLMesh* socketCarveMesh = GLContentManager::getGLMesh("../data/transformables/openScad/socket.obj");
	GLMesh* modelMesh = transformEngine->mesh;
	Robot* robot = transformEngine->robot;
	robot->setState(transformEngine->foldedState);

	for (uint i = 1; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty() || attachmentSegs.count(rb) == 0) continue;
		Transformation L2W = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
		Transformation W2L = L2W.inverse();

		Segment RBSeg = attachmentSegs[rb];
		V3D dir = RBSeg.b - RBSeg.a;
		P3D p = RBSeg.a;

		Eigen::Quaterniond q;
		q.setFromTwoVectors(V3D(0, 0, 1), dir);
		Transformation sTrans(q.toRotationMatrix(), p);

		// attach plug on shell mesh
		{
			GLMesh* cMesh = socketPlugMesh->clone();
			GLMesh* tMesh = modelMesh->clone();
			tMesh->transform(W2L);
			cMesh->transform(sTrans);
			meshBooleanIntrusive(cMesh, tMesh, "Intersect");
			for (uint j = 0; j < rb->carveMeshes.size(); j++)
			{
				GLMesh* carveMesh = rb->carveMeshes[j]->clone();
				if (!carveMesh) continue;
				carveMesh->transform(rb->meshTransformations[j]);
				meshBooleanIntrusive(cMesh, carveMesh, "Minus");
				delete carveMesh;
			}

			meshBooleanIntrusive(robotMeshes[rb], cMesh, "Union");
			delete cMesh;
			delete tMesh;
		}

		// carve skeleton
		{
			GLMesh* cMesh = socketCarveMesh->clone();
			cMesh->transform(sTrans);
			meshBooleanIntrusive(skeletonMeshes[rb], cMesh, "Minus");
			delete cMesh;
		}

		rb->meshes[0] = robotMeshes[rb];
	}
}

void FabricationEngine::mergeAndSaveSkeletonMeshForRB(const char* path)
{
	map<string, string> meshMap;
	meshMap["../data/robotDesigner/motorMeshes/XL-320_HornBracket.obj"]
		= "../data/robotDesigner/motorMeshes/XL-320_HornBracket_clean.obj";
	meshMap["../data/robotDesigner/motorMeshes/XL-320_BottomBracket.obj"]
		= "../data/robotDesigner/motorMeshes/XL-320_BottomBracket_clean.obj";
	meshMap["../data/robotDesigner/motorMeshes/XL-320_PointFoot.obj"]
		= "../data/robotDesigner/motorMeshes/XL-320_PointFoot_clean.obj";

	auto& RBs = transformEngine->RBs;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	Robot* robot = transformEngine->robot;
	robot->setState(transformEngine->foldedState);

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		auto& meshTrans = rb->meshTransformations;
		auto& origMeshes = RBOrigMeshes[rb];
		GLMesh* resMesh = NULL;

		if (skeletonMeshes.count(rb))
		{
			resMesh = skeletonMeshes[rb]->clone();
		}

		for (uint j = 0; j < origMeshes.size(); j++)
		{
			if (meshMap.count(origMeshes[j]->path) == 0) continue;
			GLMesh* cMesh = GLContentManager::getGLMesh(meshMap[origMeshes[j]->path].c_str());

			if (resMesh)
			{
				GLMesh* tMesh = cMesh->clone();
				tMesh->transform(meshTrans[j]);
				meshBooleanIntrusive(resMesh, tMesh, "Union");
				delete tMesh;
			}
			else {
				resMesh = cMesh->clone();
				resMesh->transform(meshTrans[j]);
			}
		}

		if (resMesh)
		{
			string sPath = path + rb->name + "_skeleton.obj";
			FILE* fp = fopen(sPath.c_str(), "w+");
			resMesh->renderToObjFile(fp, 0, Quaternion(), P3D());
			fclose(fp);
			delete resMesh;
		}
	}
}

void FabricationEngine::createAndSaveStructuresForBody(const char* path)
{
	Robot* robot = transformEngine->robot;
	robot->setState(transformEngine->foldedState);
	RigidBody* rb = robot->root;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;

	map<string, string> meshMap;
	meshMap["../data/robotDesigner/motorMeshes/XL-320.obj"]
		= "../data/robotDesigner/motorMeshes/XL-320-carveMesh.obj";

	auto& meshTrans = rb->meshTransformations;
	auto& origMeshes = RBOrigMeshes[rb];
	GLMesh* resMesh = NULL;

	// ******************* body motors ****************************
	for (uint j = 0; j < origMeshes.size(); j++)
	{
		if (meshMap.count(origMeshes[j]->path) == 0) continue;
		GLMesh* cMesh = GLContentManager::getGLMesh(meshMap[origMeshes[j]->path].c_str());

		if (resMesh)
		{
			GLMesh* tMesh = cMesh->clone();
			tMesh->transform(meshTrans[j]);
			resMesh->append(tMesh);
			delete tMesh;
		}
		else {
			resMesh = cMesh->clone();
			resMesh->transform(meshTrans[j]);
		}
	}

	if (resMesh)
	{
		string sPath = path + string("body_motors.obj");
		FILE* fp = fopen(sPath.c_str(), "w+");
		resMesh->renderToObjFile(fp, 0, Quaternion(), P3D());
		fclose(fp);
		delete resMesh;
	}

	// ******************* union and carving ****************************
	GLMesh* socketPlugMesh = GLContentManager::getGLMesh("../data/transformables/openScad/plug.obj");
	GLMesh* socketCarveMesh = GLContentManager::getGLMesh("../data/transformables/openScad/socket.obj");
	GLMesh* modelMesh = transformEngine->mesh;

	Transformation L2W = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
	Transformation W2L = L2W.inverse();

	V3D dir(0, 1, 0);
	P3D p;

	Eigen::Quaterniond q;
	q.setFromTwoVectors(V3D(0, 0, 1), dir);
	Transformation sTrans(q.toRotationMatrix(), p);

	// attach plug on shell mesh
	{
		GLMesh* cMesh = socketPlugMesh->clone();
		GLMesh* tMesh = modelMesh->clone();
		tMesh->transform(W2L);
		cMesh->transform(sTrans);
		meshBooleanIntrusive(cMesh, tMesh, "Intersect");

		string sPath = path + string("body_union.obj");
		FILE* fp = fopen(sPath.c_str(), "w+");
		cMesh->renderToObjFile(fp, 0, Quaternion(), P3D());
		fclose(fp);
		delete cMesh;
		delete tMesh;
	}

	// carve skeleton
	{
		GLMesh* cMesh = socketCarveMesh->clone();
		cMesh->transform(sTrans);
		string sPath = path + string("body_carve.obj");
		FILE* fp = fopen(sPath.c_str(), "w+");
		cMesh->renderToObjFile(fp, 0, Quaternion(), P3D());
		fclose(fp);
		delete cMesh;
	}
}

void FabricationEngine::attachSkeletonToVolume()
{
	auto& RBs = transformEngine->RBs;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	auto& skeletonMeshes = transformEngine->skeletonMeshes;
	auto& robotMeshes = transformEngine->robotMeshes;
	Robot* robot = transformEngine->robot;
	robot->setState(transformEngine->foldedState);

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		auto& meshTrans = rb->meshTransformations;
		auto& origMeshes = RBOrigMeshes[rb];
		GLMesh* resMesh = NULL;
		if (robotMeshes.count(rb) == 0) continue;

		if (skeletonMeshes.count(rb))
		{
			resMesh = skeletonMeshes[rb]->clone();
		}

		for (uint j = 0; j < origMeshes.size(); j++)
		{
			GLMesh* cMesh = origMeshes[j];

			if (resMesh)
			{
				GLMesh* tMesh = cMesh->clone();
				tMesh->transform(meshTrans[j]);
				meshBooleanIntrusive(resMesh, tMesh, "Union");
				delete tMesh;
			}
			else {
				resMesh = cMesh->clone();
				resMesh->transform(meshTrans[j]);
			}
		}

		if (resMesh)
		{
			meshBooleanIntrusive(robotMeshes[rb], resMesh, "Union");
			rb->meshes[0] = robotMeshes[rb];
			delete resMesh;
		}
	}
}

void FabricationEngine::prepareForFabrication()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (robotMeshes.count(rb) == 0) {
			continue;
		}

		robotMeshes[rb]->scale(0.99, P3D());
	}

	// transformEngine->carveMeshForCurrentOrder();
}

void FabricationEngine::carveLSForMeshesAndSkeletonsForLivingBracket()
{
	generateSkeletonLevelSets();
	carveLS(3);
	transformEngine->extractMeshesFromLS();
}

void FabricationEngine::generateJointSlitMeshesForLivingBracket()
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;

	set<string> meshFilter = {
		"skeleton", "motor"
	};

	map<string, string> motorFileMap;
	motorFileMap["../data/robotDesigner/motorMeshes/XM-430.obj"]
		= "../data/robotDesigner/motorMeshes/XM-430_simp.obj";

	TransformUtils::clearMeshMap(jointSlitMeshes);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		for (int t = 0; t < 2; t++)
		{
			RigidBody* rbA;
			RigidBody* rbB;
			if (t == 0)
			{
				rbA = joint->parent;
				rbB = joint->child;
			}
			else {
				rbA = joint->child;
				rbB = joint->parent;
			}

			auto& origMeshesA = RBOrigMeshes[rbA];
			auto& origMeshesB = RBOrigMeshes[rbB];
			vector<P3D> convP;
			vector<ReducedRobotState> animationStates;
			double sampleStep = 0.2;
			TransformUtils::getAnimationStatesForJoint(animationStates, i, foldedState, unfoldedState, sampleStep);
			// Logger::print("MSize: %d\n", transformEngine->motionPlanStates.size());
			for (uint j = 0; j < transformEngine->motionPlanStates.size(); j++)
			{
				animationStates.push_back(transformEngine->motionPlanStates[j]);
			}

			for (uint j = 0; j < animationStates.size(); j++)
			{
				robot->setState(&animationStates[j]);
				for (uint k = 0; k < origMeshesB.size(); k++)
				{
					GLMesh* tMesh = origMeshesB[k];
					Transformation& trans = rbB->meshTransformations[k];
					if (meshFilter.count(rbB->meshDescriptions[k]) == 0) continue;
					
					if (motorFileMap.count(tMesh->path))
						tMesh = GLContentManager::getGLMesh(motorFileMap[tMesh->path].c_str());

					for (int c = 0; c < tMesh->getVertexCount(); c++)
					{
						P3D v = tMesh->getVertex(c);
						P3D localV = rbA->getLocalCoordinates(rbB->getWorldCoordinates(trans.transform(v)));
						convP.push_back(localV);
					}
				}
			}

			for (uint k = 0; k < origMeshesA.size(); k++)
			{
				GLMesh* tMesh = origMeshesA[k];
				Transformation& trans = rbA->meshTransformations[k];
				if (rbA->meshDescriptions[k] == joint->name)
				{
					if (motorFileMap.count(tMesh->path))
						tMesh = GLContentManager::getGLMesh(motorFileMap[tMesh->path].c_str());

					for (int c = 0; c < tMesh->getVertexCount(); c++)
					{
						P3D v = tMesh->getVertex(c);
						P3D localV = trans.transform(v);
						convP.push_back(localV);
					}
				}
			}

			// Logger::print("convPSize: %d\n", convP.size());

			GLMesh* slitMesh = new GLMesh();
			ConvexHull3D::computeConvexHullFromSetOfPoints(convP, slitMesh);
			slitMesh->scale(1.02, slitMesh->getCenterOfMass());
			slitMesh->computeNormals();

			// Logger::print("meshVNum: %d\n", slitMesh->getVertexCount());

			if (jointSlitMeshes.count(rbA)) {
				MESHBOOLEAN_INTRUSIVE(jointSlitMeshes[rbA], slitMesh, "Union");
				delete slitMesh;
			}
			else {
				jointSlitMeshes[rbA] = slitMesh;
			}
		}
	}
}

void FabricationEngine::carveForJointSlits()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (robotMeshes.count(rb) == 0) {
			continue;
		}

		if (jointSlitMeshes.count(rb)) {
			MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], jointSlitMeshes[rb], "Minus");
		}
		rb->meshes[0] = robotMeshes[rb];
	}
}

void FabricationEngine::mergeSkeletonAndSkinMeshes()
{
	auto& RBs = transformEngine->RBs;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (robotMeshes.count(rb) == 0) {
			continue;
		}

		auto& origMeshes = RBOrigMeshes[rb];
		for (uint j = 0; j < origMeshes.size(); j++)
		{
			GLMesh* oMesh = origMeshes[j];
			if (rb->meshDescriptions[j] == "skeleton") {
				MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], oMesh, "Union");
			}
		}

		for (uint j = 0; j < origMeshes.size(); j++)
		{
			if (rb->meshDescriptions[j] == "carving") {
				GLMesh* tMesh = origMeshes[j]->clone();
				tMesh->transform(rb->meshTransformations[j]);
				MESHBOOLEAN_INTRUSIVE(robotMeshes[rb], tMesh, "Minus");
				delete tMesh;
			}
		}

		rb->meshes[0] = robotMeshes[rb];
	}
}
