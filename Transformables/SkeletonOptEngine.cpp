#include "SkeletonOptEngine.h"
#include <RBSimLib/HingeJoint.h>
#include "SkeletonDecompObjective.h"


SkeletonOptEngine::SkeletonOptEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine) {
	skeletonFoldingPlan = new SkeletonFoldingPlan(transformEngine);
	skeletonFoldingFunc = new SkeletonFoldingEnergyFunction(skeletonFoldingPlan);
	skeletonFoldingOptimizer = new SkeletonFoldingOptimizer(skeletonFoldingPlan, skeletonFoldingFunc);
	
	skeletonFoldingCMAFunc = new SkeletonFoldingCMAEnergyFunction(skeletonFoldingPlan);
	skeletonFoldingCMAOptimizer = new SkeletonFoldingCMAOptimizer(skeletonFoldingPlan, skeletonFoldingCMAFunc);
}

SkeletonOptEngine::~SkeletonOptEngine()
{
	delete skeletonFoldingCMAOptimizer;
	delete skeletonFoldingCMAFunc;

	delete skeletonFoldingOptimizer;
	delete skeletonFoldingFunc;
	delete skeletonFoldingPlan;

	delete approxMesh;
}

void SkeletonOptEngine::reinitialize()
{
	skeletonFoldingPlan->reinitalize();
}

void SkeletonOptEngine::getRobotStateSamples(ReducedRobotState* robotState, vector<ReducedRobotState>& samples, double d_pos, double d_angle, map<int, int>* symmetryMap)
{
	// each joint take 2 angle samples (-d_angle, +d_angle), root position takes 4 angle samples (-p_z, +p_z, -p_y, +p_y)

	Robot* robot = transformEngine->robot;

	if (!symmetryMap || symmetryMap->empty())
	{
		// *************** without joint symmetry map *************

		samples.resize(2 * robotState->getJointCount() + 6, ReducedRobotState(*robotState));

		P3D rootPos = robotState->getPosition();
		d_pos *= getRandomNumberInRange(0.0, 1.0);

		samples[0].setPosition(rootPos + V3D(0, 0, d_pos));
		samples[1].setPosition(rootPos + V3D(0, 0, -d_pos));
		samples[2].setPosition(rootPos + V3D(0, d_pos, 0));
		samples[3].setPosition(rootPos + V3D(0, -d_pos, 0));

		// tweak root orientation
		Quaternion q = robotState->getOrientation();
		double rand_angle = getRandomNumberInRange(0.0, 1.0) * d_angle;
		Quaternion d_q = getRotationQuaternion(rand_angle, V3D(1, 0, 0));
		samples[4].setOrientation(d_q * q);
		samples[5].setOrientation(d_q.getInverse() * q);

		for (int i = 0; i < robotState->getJointCount(); i++)
		{
			V3D rotationAxis = static_cast<HingeJoint*>(robot->getJoint(i))->rotationAxis;
			Quaternion q = robotState->getJointRelativeOrientation(i);
			Quaternion d_q = getRotationQuaternion(getRandomNumberInRange(0.0, 1.0) * d_angle, rotationAxis);
			samples[6 + 2 * i].setJointRelativeOrientation(d_q * q, i);
			samples[6 + 2 * i + 1].setJointRelativeOrientation(d_q.getInverse() * q, i);
		}

		return;
	}

	// *************** with joint symmetry map *************
	samples.clear();

	P3D rootPos = robotState->getPosition();

	samples.push_back(*robotState);
	samples.back().setPosition(rootPos + V3D(0, 0, d_pos * getRandomNumberInRange(0.0, 1.0)));
	samples.push_back(*robotState);
	samples.back().setPosition(rootPos + V3D(0, 0, -d_pos * getRandomNumberInRange(0.0, 1.0)));
	samples.push_back(*robotState);
	samples.back().setPosition(rootPos + V3D(0, d_pos * getRandomNumberInRange(0.0, 1.0), 0));
	samples.push_back(*robotState);
	samples.back().setPosition(rootPos + V3D(0, -d_pos * getRandomNumberInRange(0.0, 1.0), 0));

	// tweak root orientation
	Quaternion q = robotState->getOrientation();
	double rand_angle = getRandomNumberInRange(0.0, 1.0) * d_angle;
	Quaternion d_q = getRotationQuaternion(rand_angle, V3D(1, 0, 0));
	samples.push_back(*robotState);
	samples.back().setOrientation(d_q * q);
	samples.push_back(*robotState);
	samples.back().setOrientation(d_q.getInverse() * q);

	for (int i = 0; i < robotState->getJointCount(); i++)
	{
		int j = (*symmetryMap)[i];
		if (j <= i) continue;

		V3D rotationAxis1 = static_cast<HingeJoint*>(robot->getJoint(i))->rotationAxis.normalized();
		V3D rotationAxis2 = static_cast<HingeJoint*>(robot->getJoint(j))->rotationAxis.normalized();
		Quaternion q1 = robotState->getJointRelativeOrientation(i);
		Quaternion q2 = robotState->getJointRelativeOrientation(j);
		double rand_angle = getRandomNumberInRange(0.0, 1.0) * d_angle;
		Quaternion d_q1 = getRotationQuaternion(rand_angle, rotationAxis1);
		Quaternion d_q2 = getRotationQuaternion(rand_angle, rotationAxis2);
		samples.push_back(*robotState);
		samples.back().setJointRelativeOrientation(d_q1 * q1, i);
		samples.back().setJointRelativeOrientation(d_q2.getInverse() * q2, j);


		samples.push_back(*robotState);
		samples.back().setJointRelativeOrientation(d_q1.getInverse() * q1, i);
		samples.back().setJointRelativeOrientation(d_q2 * q2, j);
	}
}

double SkeletonOptEngine::computeObjectiveEuclidean(ReducedRobotState* robotState)
{
	double objVal = 0;

	transformEngine->robot->setState(robotState);
	TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);
	VoxelGrid<int>* voxelGrid = transformEngine->voxelGrid;

	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				if (voxelGrid->getData(x, y, z) == -2) continue;

				double dist;
				P3D center = voxelGrid->getVoxelCenter(x, y, z);
				RigidBody* rb = TransformUtils::getRobotClosestRigidBody(center, transformEngine->capsulesMap, &dist);
				double err = dist - rb->rbProperties.thickness;
				if (err > 0)
				{
					objVal += 0.5 * err * err;
				}
			}

	return objVal;
}

double SkeletonOptEngine::computeObjectiveGeodesic(ReducedRobotState* robotState)
{
	double objVal = 0;

	transformEngine->robot->setState(robotState);
	TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);
	VoxelGrid<int>* voxelGrid = transformEngine->voxelGrid;

	transformEngine->geodesicVoxelAssignment();

	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				int rbId = voxelGrid->getData(x, y, z);
				if (rbId == -1) return 1e4;
				if (rbId == -2) continue;

				P3D center = voxelGrid->getVoxelCenter(x, y, z);
				RigidBody* rb = transformEngine->RBs[rbId];
				vector<Capsule>& capsules = transformEngine->capsulesMap[rb];
				double dist = TransformUtils::getClosestDistToCapsules(center, capsules);

				double err = dist - rb->rbProperties.thickness;
				if (err > 0)
				{
					objVal += 0.5 * err * err;
				}
			}

	return objVal;
}

double SkeletonOptEngine::computeObjectiveBulkiness(ReducedRobotState* robotState)
{
	double objVal = 0;
	
	auto& RBs = transformEngine->RBs;
	auto& capsulesMap = transformEngine->capsulesMap;
	VoxelGrid<int>* voxelGrid = transformEngine->voxelGrid;
	double voxelSize = transformEngine->transParams->voxelSize;
	double sktInsideThreshold = transformEngine->transParams->sktInsideThreshold;
	P3D sp = voxelGrid->startPoint;
	GLMesh* MCMesh = transformEngine->MCMesh;

	transformEngine->robot->setState(robotState);
	TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);
	transformEngine->geodesicVoxelAssignment();

	for (int i = 0; i < MCMesh->getVertexCount(); i++)
	{
		P3D v = MCMesh->getVertex(i);
		Eigen::Vector3i vIndex;
		for (int k = 0; k < 3; k++)
			vIndex[k] = (int)((v[k] - sp[k]) / voxelSize);

		int rbId = voxelGrid->getData(vIndex[0], vIndex[1], vIndex[2]);
		if (rbId < 0) {
			// Logger::print("invalid rbId: %d\n", rbId);
			objVal = 1e4;
			break;
		}
		RigidBody* rb = RBs[rbId];

		vector<Capsule>& capsules = transformEngine->capsulesMap[rb];
		double dist = TransformUtils::getClosestDistToCapsules(v, capsules);
		double diff = dist - sktInsideThreshold;
		double weight = diff < sktInsideThreshold ? 1e1 : 1;

		objVal += weight * diff * diff;
	}

	return objVal;
}

double SkeletonOptEngine::computeCollisionObjective(ReducedRobotState* robotState, bool printInfo)
{
	double loss = 0;
	Robot* robot = transformEngine->robot;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	VertexGrid<V3D>& LSGrad = transformEngine->meshLSGrad;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		RigidBody* parent = joint->parent;
		RigidBody* child = joint->child;

		// all in local coordinate
		P3D parentPointB = parent == robot->root ? P3D() : parent->pJoints[0]->cJPos;
		P3D parentPointA = joint->pJPos;
		V3D parentDir = (parentPointB - parentPointA).normalized();
		P3D childPointA = joint->cJPos;
		P3D childPointB = child->cJoints.empty() ? child->rbProperties.endEffectorPoints[0].coords : child->cJoints[0]->pJPos;
		V3D childDir = (childPointB - childPointA).normalized();

		// all in parent's coordinate
		robot->setState(robotState);
		P3D wJPos = joint->getWorldPosition();
		V3D childDir1 = parent->getLocalCoordinates(child->getWorldCoordinates(childDir));
		if (!LSGrad.isInside(wJPos[0], wJPos[1], wJPos[2]))
			return 1e10;
		
		V3D normal = LSGrad.interpolateData(wJPos[0], wJPos[1], wJPos[2]).normalized();
		normal = parent->getLocalCoordinates(normal);
		V3D rot = normal.cross(childDir1).normalized();
		V3D signAxis = ((HingeJoint*)joint)->rotationAxis;
		if (signAxis.dot(rot) < 0)
			signAxis = -signAxis;

		robot->setState(unfoldedState);
		V3D childDir2 = parent->getLocalCoordinates(child->getWorldCoordinates(childDir));
		
		loss += childDir2.cross(childDir1).dot(signAxis);
	}

	loss *= 0.1;
	if (printInfo)
		Logger::consolePrint("collision objective: %lf\n", loss);

	return loss;
}

double SkeletonOptEngine::computeLSDiffObjective(ReducedRobotState* robotState, bool printInfo /*= false*/)
{
	double loss = 0;
	Robot* robot = transformEngine->robot;
	auto& RBs = transformEngine->RBs;
	auto& capsulesMap = transformEngine->capsulesMap;
	LevelSet& meshLevelSet = transformEngine->meshLevelSet;
	LevelSet approxLS = meshLevelSet;

	robot->setState(robotState);
	TransformUtils::updateCapsuleMap(capsulesMap);

	int* vertexNum = meshLevelSet.vertexNum;
	int totalVertexNum = meshLevelSet.totalVertexNum;

#pragma omp parallel for
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {

				P3D center = meshLevelSet.getVertex(x, y, z);
				double LSVal = DBL_MAX;
				
				for (uint i = 0; i < RBs.size(); i++)
				{
					double dist = TransformUtils::getClosestDistToCapsules(center, capsulesMap[RBs[i]]);
					double radius = 0.03;
					double val = dist - radius;
					LSVal = min(val, LSVal);
				}

				approxLS.setData(x, y, z, LSVal);
			}

	double avgDiff = 0;

	for (int i = 0; i < totalVertexNum; i++)
	{
		avgDiff += meshLevelSet.getData(i) - approxLS.getData(i);
	}
	avgDiff /= totalVertexNum;

	approxLS.addConstant(avgDiff);

	for (int i = 0; i < totalVertexNum; i++)
	{
		double diff = meshLevelSet.getData(i) - approxLS.getData(i);
		loss += diff * diff;
	}

	loss *= 0.2;

	if (printInfo) {
		approxMesh = approxLS.extractMesh(0);
		Logger::consolePrint("LSDiff objective: %lf\n", loss);
	}	

	return loss;

}

double SkeletonOptEngine::computeObjectiveContinous(ReducedRobotState* robotState, bool printInfo)
{
	skeletonFoldingPlan->setRobotState(*robotState);
	dVector params;
	skeletonFoldingPlan->writeParametersToList(params);
	double objVal = skeletonFoldingFunc->computeValueWithoutRegularizer(params, printInfo);

	return objVal;
}

double SkeletonOptEngine::optimizeRobotState(ReducedRobotState* robotState, double d_pos, double d_angle, map<int, int>* symmetryMap)
{
	vector<ReducedRobotState> samples;
	getRobotStateSamples(robotState, samples, d_pos, d_angle, symmetryMap);

	double minVal = computeObjectiveBulkiness(robotState);
	ReducedRobotState* bestState = robotState;

	for (uint i = 0; i < samples.size(); i++)
	{
		double val = computeObjectiveBulkiness(&samples[i]);
		if (val < minVal)
		{
			bestState = &samples[i];
			minVal = val;
		}
	}

	if (robotState != bestState)
		*robotState = *bestState;

	Logger::consolePrint("RandomSample ObjVal: %lf\n", minVal);
		
	transformEngine->foldingOrder.clear();

	return minVal;
}

double SkeletonOptEngine::optimizeRobotStateContinous(ReducedRobotState* robotState, map<int, int>* symmetryMap)
{
	Robot* robot = transformEngine->robot;

	robot->setState(robotState);
	skeletonFoldingPlan->setRobotState(*robotState);
	skeletonFoldingPlan->setTargetRobotState(*transformEngine->unfoldedState);

	double val = skeletonFoldingOptimizer->optimizePlan(1);
	skeletonFoldingPlan->getRobotState(*robotState);

	return val;
}

double SkeletonOptEngine::optimizeRobotStateCMA(ReducedRobotState* robotState)
{
	if (reintializeCMA) {
		skeletonFoldingCMAOptimizer->reinitialize();
		reintializeCMA = false;
	}

	double val = skeletonFoldingCMAOptimizer->optimizePlan(1);
	skeletonFoldingPlan->getRobotState(*robotState);
	// Logger::consolePrint("ObjVal: %lf\n", val);

	return val;
}
