#pragma once
#include <RobotDesignerLib/MorphologicalRobotDesign.h>
#include "TransformEngine.h"

class TransformEngine;

class RobotEditor
{
public:
	TransformEngine* transformEngine = NULL;
	MorphologicalRobotDesign* robotDesign = NULL;
	Robot* robot = NULL;
	map<RigidBody*, vector<Transformation>> origMeshTrans;
	map<RigidBody*, vector<int>> nearestJoints;
	string pointEEPath;

public:
	RobotEditor(TransformEngine* _transformEngine, Robot* _robot);
	~RobotEditor();

	void getNearestJoints();
	void scalePickedRobot(RigidBody* rb, double scale, bool changeBodyLength = false);
	void scalePickedRobotSym(RigidBody* rb, double scale, bool changeBodyLength = false);

	void adjustMeshTransForRB(RigidBody* rb);
	
	void saveParamsToFile(const char* fName);
	void loadParamsFromFile(const char* fName);

	void adjustMeshTrans();

};

