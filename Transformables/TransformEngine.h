#pragma once

#include <Utils/Timer.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/ODERBEngine.h>
#include <GUILib/GLContentManager.h>
#include <RobotDesignerLib/ModularDesignWindow.h>
#include "TransformUtils.h"
#include "SkeletonOptEngine.h"
#include "FoldingOptEngine.h"
#include "SmoothingEngine.h"
#include "CarvingEngine.h"
#include "CollisionEngine.h"
#include "FabricationEngine.h"
#include "RobotEditor.h"

class SkeletonOptEngine;
class FoldingOptEngine;
class SmoothingEngine;
class CarvingEngine;
class LevelSet;
class CollisionEngine;
class FabricationEngine;
class RobotEditor;

class TransformEngine
{
public:
	Timer timer;

	SkeletonOptEngine* skeletonOptEngine = NULL;
	FoldingOptEngine* foldingOptEngine = NULL;
	SmoothingEngine* smoothingEngine = NULL;
	CarvingEngine* carvingEngine = NULL;
	CollisionEngine* collisionEngine = NULL;
	FabricationEngine* fabricationEngine = NULL;
	RobotEditor* robotEditor = NULL;

	// ***** initialized when construction ****** //
	Robot* robot = NULL;
	map<RigidBody*, vector<Capsule>> capsulesMap;
	map<RigidBody*, int> RBIndexMap;
	vector<RigidBody*> RBs;
	map<RigidBody*, RigidBody*> parentMap; // second is first's parent
	ReducedRobotState* unfoldedState = NULL;  // the start state of the entire transformation
	ReducedRobotState* foldedState = NULL;   // the end state of the entire transformation
	ReducedRobotState* startState = NULL;
	TransformParams* transParams = NULL;
	map<int, int> jointSymmetryMap;

	// reintialization flag
	bool robotObsolete = false;
	// initialization  flag
	bool initialized = false;

	map<RigidBody*, GLMesh*> robotMeshes;
	map<RigidBody*, vector<GLMesh*>> RBOrigMeshes;
	map<RigidBody*, GLMesh*> skeletonMeshes;
	map<RigidBody*, vector<P3D>> rbVertices;

	// body mesh
	GLMesh* bodyMesh = NULL;

	// level sets
	vector<LevelSet> MlevelSets;
	vector<VertexGrid<V3D>> MLevelSetsGrad;
	LevelSet meshLevelSet;
	VertexGrid<V3D> meshLSGrad;
	VertexGrid<Matrix3x3> meshLSHessian;
	LevelSet largeMeshLevelSet;
	LevelSet shellLevelSet;
	LevelSet largeShellLevelSet;
	AxisAlignedBoundingBox LSBBox;

	// original mesh
	GLMesh* mesh = NULL;
	// shell mesh of the original mesh
	GLMesh* shellMesh = NULL;
	// the marching cubes version of the original mesh
	GLMesh* MCMesh = NULL;

	// joint meshes
	GLMesh* jointPlugMesh = NULL;
	GLMesh* jointPlugCarveMesh = NULL;
	GLMesh* jointSocketMesh = NULL;
	GLMesh* jointSocketCarveMesh = NULL;

	// voxelGrid 
	VoxelGrid<int>* voxelGrid = NULL;
	VoxelGrid<int>* origVoxelGrid = NULL;
	VoxelGrid<int>* expandedVoxelGrid = NULL;
	VoxelGrid<int>* boundaryVoxelGrid = NULL;  // 1 means on the boundary, 0 otherwise.

	// folding order variables
	vector<int> foldingOrder;
	double minCost = DBL_MAX;

	// map containing signed distance field of the mesh assigned to the rigidbody.
	map<RigidBody*, LevelSet> SDFMap;

	// contour of each rigid body mesh
	vector<MeshInterface> meshInterfaces;
	vector<MeshInterfaceKnot> meshIFKnots;

	// 2D interfaces between level sets
	vector<LSInterface> LSIFs;

	vector<ReducedRobotState> motionPlanStates;
	dVector maxMPJointAngles;
	dVector minMPJointAngles;

	vector<bool> fixedLS;

	// Modular design window
	ModularDesignWindow* modularDesign = NULL;

public:
	TransformEngine(Robot* _robot, ReducedRobotState* _foldedState, ReducedRobotState* _unfoldedState, TransformParams* _transParams, map<int, int>& _jointSymmetryMap, vector<ReducedRobotState>& _motionPlanStates);
	~TransformEngine();

	// get a map that maps each RigidBody of the robot to its Capsules.
	void buildRobotRigidBodyMaps();
	void loadRBOrigMeshesFromDesign(bool mergeMeshes = false, bool initialLoad = false);
	void loadRBOrigMeshes();

	// get mesh vertices from design
	void getMeshVerticesForRBs();

	// get range of motion
	void getMPRangeOfMotion();

	// reinitialization
	void reinitializeRobot();

	// assign mesh to robot RigidBody base on nearest distance.
	void assignVoxelMeshToRobot();
	void euclideanVoxelAssignment();
	void geodesicVoxelAssignment();

	// Levelset based method intialization
	void initialize();
	void saveInitialization();
	void loadInitialization(const char* fName);

	void generateTransMeshes();

	// doing mesh transfer on level sets to avoid collision between parent and child.
	void transferMeshLS(int maxIterNum = 100, bool parentToChild = false);
	void transferMeshLSForJoint(int jIndex, int maxIterNum = 100, bool parentToChild = false);
	double getTransferMaxRForJoint(int jIndex, vector<RBState>& parentStates, vector<RBState>& childStates, int maxIterNum = 100, bool parentToChild = false);
	void execTransferMeshForJoint(int jIndex, double maxR, bool parentToChild);

	void clearRBLevelSet(RigidBody* rb);

	// assign meshes using distance flooding.
	void prepareForFlooding();
	void levelSetFlooding();
	void extractMeshesFromLS(double levelSetVal = 0);
	void extractMeshesFromLSForRB(int index, double levelSetVal = 0);
	void assignLSMeshToRobot();

	// level set editing
	// void levelSetEditing();
	void levelSetEditingNew(int interfaceIndex);
	P3D projectPointToSurf(P3D p);
	void buildMeshInterfaces();
	void calculateInterfaceBetween(MeshInterface& meshIF, P3D p);
	void recalculateInterface(int interfaceIndex);
	void checkAdditionalInterface(MeshInterface& meshIF);
	void generateIFKnots();

	// collision aware interface optimization
	void buildLSInterfaces();
	void evolveLSIFs();
	void evolveLSInterface(int interfaceIndex);
	void calEvolveVelocityField();
	void evolveSamplePoints();
	void projectPointsBackToIF();

	// save level sets
	void saveLSToFile(const char* fName, bool sparse = true);
	void loadLSFromFile(const char* fName, bool sparse = true);

	void voxelize();
	void clearVoxelGrid();

	// random sampling optimization
	double optimizeRobotState();
	double optimizeRobotStateCMA();
	
	double optimizeRobotStateContinuous();

	// folding order optimization
	void optimizeFoldingOrder();

	// carve mesh using current folding order
	void carveMeshForCurrentOrder();

	// attach joint structures
	void attachJointStructures();

	// living design
	void attachJointStructuresForLivingBracket();

};

