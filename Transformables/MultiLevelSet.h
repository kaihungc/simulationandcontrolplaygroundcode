#pragma once

#include <MathLib/VertexGrid.h>
#include <GUILib/GLMesh.h>

using namespace std;

// use index in grid to define a box region
struct IndexBox
{
	Eigen::Vector3i minIndex;
	Eigen::Vector3i maxIndex;
	Eigen::Vector3i size;
	int N;
};

class MultiLevelSet : VertexGrid<dVector> {
public:
	int N;
	vector<IndexBox> activeBoxes;

public:
	MultiLevelSet(int N, AxisAlignedBoundingBox& _bbox, double _stepSize);
	~MultiLevelSet();

	void setVal(int bIndex, int x, int y, int z, double val) {
		int index = z * layerVertexNum + (y * vertexNum[0] + x);
		data[index][bIndex] = val;
	}

	void setVal(int bIndex, int index, double val) {
		data[index][bIndex] = val;
	}

	double getVal(int bIndex, int x, int y, int z) {
		int index = z * layerVertexNum + (y * vertexNum[0] + x);
		return data[index][bIndex];
	}

	double getVal(int bIndex, int index) {
		return data[index][bIndex];
	}

	void updateActiveBox(int bIndex, const P3D& bmin, P3D& bmax);
	void updateLevelSet(int bIndex, GLMesh* mesh);
	GLMesh* extractMesh(int bIndex);
};


