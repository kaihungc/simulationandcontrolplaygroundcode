#include "SkeletonMorphRegularizer.h"
#include "TransformEngine.h"

SkeletonMorphRegularizer::SkeletonMorphRegularizer(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonMorphRegularizer::~SkeletonMorphRegularizer(void){
}

double SkeletonMorphRegularizer::computeValue(const dVector& p){

	double retVal = 0;
	
	int morphParamsDim = plan->morphParamsDim;
	dVector morphP = p.tail(morphParamsDim);

	double overallScale = morphP.sum() / morphParamsDim;
	// Logger::print("overall scale: %lf\n", overallScale);
	morphP /= overallScale;
	retVal = (morphP - dVector::Ones(morphParamsDim)).squaredNorm();

	return retVal * weight;
}