#include "SkeletonFoldingCMAOptimizer.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include "TransformEngine.h"

SkeletonFoldingCMAOptimizer::SkeletonFoldingCMAOptimizer(SkeletonFoldingPlan* plan, SkeletonFoldingCMAEnergyFunction* energyFunction){
	this->plan = plan;
	this->energyFunction = energyFunction;
}

SkeletonFoldingCMAOptimizer::~SkeletonFoldingCMAOptimizer(void){
	delete CMA_minimizer;
}

double SkeletonFoldingCMAOptimizer::optimizePlan(int maxIterNum){

	bool res = CMA_minimizer->step(maxIterNum);
	dVector params;
	double objVal = CMA_minimizer->getCurrentBestSolution(params);
	// plan->setSymParamsFromList(params);
	plan->setSymMorphParamsFromList(params);
	energyFunction->printValue(params);

	return objVal;
}

void SkeletonFoldingCMAOptimizer::reinitialize()
{
	delete CMA_minimizer;

	TransformEngine* transformEngine = (TransformEngine*)plan->transformEngine;
	int symParamsDim = plan->symParamsDim;
	int morphParamsDim = plan->morphParamsDim;

	plan->setRobotState(*transformEngine->foldedState);
	dVector params;
	//plan->writeSymParamsToList(params);
	plan->writeSymMorphParamsToList(params);

	CMA_minimizer = new CMAIterativeFunctionMinimizer();

	dVector pMin(params.size());
	dVector pMax(params.size());
	dVector SD(params.size());
	
	// body positions
	pMin.head(2).setConstant(-0.5);
	pMax.head(2).setConstant(0.5);
	SD.head(2).setConstant(0.1);
	// joint angles
	pMin.segment(2, symParamsDim - 2).setConstant(-PI);
	pMax.segment(2, symParamsDim - 2).setConstant(PI);
	SD.segment(2, symParamsDim - 2).setConstant(0.5);
	// body width and length
	pMin.segment(symParamsDim, 2).setConstant(0.2);
	pMax.segment(symParamsDim, 2).setConstant(3.0);
	SD.segment(symParamsDim, 2).setConstant(0.5);
	// bone lengths
	pMin.tail(morphParamsDim - 2).setConstant(0.2);
	pMax.tail(morphParamsDim - 2).setConstant(3.0);
	SD.tail(morphParamsDim - 2).setConstant(0.5);


	CMA_minimizer->setInitialParameterSet(params, pMin, pMax);
	CMA_minimizer->setInitialStandardDeviation(SD, 0.1);
	CMA_minimizer->setObjective(energyFunction);
}
