#pragma once
#include "TransformEngine.h"

class TransformEngine;

class CollisionEngine {
public:
	TransformEngine* transformEngine;

	// voxelsMap for rigidly transformed voxels
	map<RigidBody*, RBVoxelInfo> voxelInfoMap;

public:
	CollisionEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine) {

	}
	~CollisionEngine();

	// voxels carving
	void buildVoxelInfoMap();
	void carveVoxelsForCurrentOrder();
	void carveVoxelsForCurrentState();
	bool checkCollisionVoxelsBetweenRB(RigidBody* RB_i, RigidBody* RB_j);

	void getJointCollisionMap(map<Joint*, double>& jointCollisionMap);
	double getCollisionForJoint(Joint* joint);

	double getCollisionBetweenLSImplicit(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates, double cylinderR, V3D axis, P3D center);
	double getCollisionBetweenLS(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates);
	double getCollisionBetweenLS(LevelSet& parentLS, LevelSet& childLS, vector<RBState>& parentStates, vector<RBState>& childStates);
	double getCollisionBetweenMeshLS(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates);
	double getMaxCollisionRadius(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates, V3D axis, P3D center);
};

