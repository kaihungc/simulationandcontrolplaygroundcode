#pragma once

#include <MathLib/MathLib.h>
#include <ControlLib/Robot.h>
#include <ControlLib/GeneralizedCoordinatesRobotRepresentation.h>
#include <MathLib/LevelSet.h>
#include <MathLib/Capsule.h>
#include <RobotDesignerLib/MorphologicalRobotDesign.h>

using namespace std;

struct RBBone {
	vector<P3D> localPoints;
	RigidBody* rb;

	RBBone() {}
	RBBone(RigidBody* _rb, vector<P3D>& points) : rb(_rb), localPoints(points) {}
};

struct JointBoundary
{
	// all in parent's local coordinate
	vector<P3D> points;
	vector<V3D> normals;
};


class SkeletonFoldingPlan
{
public:
	SkeletonFoldingPlan(void* _transformEngine);

	void generateRBBones();

	~SkeletonFoldingPlan();

	void reinitalize();
	void generateJointToParamsMap();
	void generateRBToParamsMap();

	void setRobotState(const ReducedRobotState& rs);
	void setTargetRobotState(const ReducedRobotState& rs);
	void getRobotState(ReducedRobotState& rs);

	void getSamplePointsForSegment(P3D a, P3D b, int n, vector<P3D>& samplePoints);
	RBBone getRBBoneForRB(RigidBody* rb);
	void generateJointBoundaries();
	void getRangeOfMotion();

	void draw(RigidBody* rb = NULL);

	virtual void writeParametersToList(dVector& p) {
		p = currentRobotState;
	}

	virtual void setParametersFromList(const dVector& p) {
		currentRobotState = p;
		gcRobotRepresentation->setQ(currentRobotState);
		extraRobotSetup();
	}

	// optimize only robot state
	void writeSymParamsToList(dVector& p);
	void setSymParamsFromList(const dVector& p);

	// optimize robot state and morphology
	void writeSymMorphParamsToList(dVector& p);
	void setSymMorphParamsFromList(const dVector& p);

	void extraRobotSetup();

public:
	dVector currentRobotState;
	dVector targetRobotState;
	Robot* robot;
	ReducedRobotState* unfoldedState;
	ReducedRobotState* foldedState;
	GeneralizedCoordinatesRobotRepresentation* gcRobotRepresentation;

	void* transformEngine;
	MorphologicalRobotDesign* robotDesign;
	map<int, int> parentRBIndexMap;

	LevelSet* levelSet;
	VertexGrid<V3D>* LSGrad;
	VertexGrid<Matrix3x3>* LSHessian;
	map<int, int>* symmMap;
	bool bodySymm;
	vector<RBBone> bones;

	map<int, int> jointToSymParamsMap;
	dVector mappingScale;
	int symParamsDim;
	int jStartIndex;

	map<int, int> RBToSymParamsMap;
	int morphParamsDim;
	int totalDim;

	vector<JointBoundary> jointBoundaries;
	// joint angles, zero is the joint angle in folded state.
	dVector minJointAngles;
	dVector maxJointAngles;
};

