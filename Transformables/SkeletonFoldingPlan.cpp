#include "SkeletonFoldingPlan.h"
#include <GUILib/GLApplication.h>
#include <Transformables/TransformUtils.h>
#include "TransformEngine.h"


SkeletonFoldingPlan::SkeletonFoldingPlan(void* _transformEngine) {

	transformEngine = _transformEngine;
	TransformEngine* engine = (TransformEngine*)transformEngine;
	robotDesign = engine->robotEditor->robotDesign;

	robot = engine->robot;
	unfoldedState = engine->unfoldedState;
	foldedState = engine->foldedState;
	levelSet = &(engine->meshLevelSet);
	LSGrad = &(engine->meshLSGrad);
	LSHessian = &(engine->meshLSHessian);
	symmMap = &(engine->jointSymmetryMap);
	bodySymm = symmMap->size() > 0;
	gcRobotRepresentation = new GeneralizedCoordinatesRobotRepresentation(robot);
	setRobotState(ReducedRobotState(robot));
	//create the end effector trajectories here based on the robot configuration...
	
	generateJointToParamsMap();
	generateRBToParamsMap();

	totalDim = symParamsDim + morphParamsDim;
	
	auto& RBIndexMap = engine->RBIndexMap;
	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		parentRBIndexMap[RBIndexMap[joint->child]] = RBIndexMap[joint->parent];
	}

	getRangeOfMotion();
}

SkeletonFoldingPlan::~SkeletonFoldingPlan(void) {
	delete gcRobotRepresentation;
}

void SkeletonFoldingPlan::reinitalize()
{
	*gcRobotRepresentation = GeneralizedCoordinatesRobotRepresentation(robot);
	generateRBBones();
}

void SkeletonFoldingPlan::generateJointToParamsMap()
{
	jStartIndex = bodySymm ? 3 : 6;
	jointToSymParamsMap.clear();
	mappingScale.resize(gcRobotRepresentation->getDimensionCount());
	int pIndex = 0;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		int oIndex1 = gcRobotRepresentation->getQIndexForJoint(i);
		auto itr = symmMap->find(i);
		if (itr != symmMap->end())
		{
			int symJoint = itr->second;
			int oIndex2 = gcRobotRepresentation->getQIndexForJoint(symJoint);
			if (symJoint > i)
			{
				jointToSymParamsMap[oIndex1] = pIndex + jStartIndex;
				jointToSymParamsMap[oIndex2] = pIndex + jStartIndex;
				mappingScale[oIndex1] = 0.5;
				mappingScale[oIndex2] = -0.5;
				pIndex++;
			}
		}
		else {
			jointToSymParamsMap[oIndex1] = pIndex + jStartIndex;
			mappingScale[oIndex1] = 1.0;
			pIndex++;
		}
	}

	symParamsDim = pIndex + jStartIndex;

	Logger::print("SymDim: %d %d\n", symParamsDim, mappingScale.size());
}

void SkeletonFoldingPlan::generateRBToParamsMap()
{
	auto& RBIndexMap = ((TransformEngine*)transformEngine)->RBIndexMap;
	RBToSymParamsMap.clear();
	int pIndex = 2;

	for (int i = 1; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		int oIndex1 = i + 1;
		auto itr = symmMap->find(rb->pJoints[0]->jIndex);
		if (itr != symmMap->end())
		{
			int symRBIndex = RBIndexMap[robot->getJoint(itr->second)->child];
			int oIndex2 = symRBIndex + 1;
			if (symRBIndex > i)
			{
				RBToSymParamsMap[oIndex1] = pIndex;
				RBToSymParamsMap[oIndex2] = pIndex;
				pIndex++;
			}
		}
		else {
			RBToSymParamsMap[oIndex1] = pIndex;
			pIndex++;
		}
	}

	morphParamsDim = pIndex;

	Logger::print("MorphDim: %d %d\n", morphParamsDim, robot->getRigidBodyCount() + 1);
}

void SkeletonFoldingPlan::generateRBBones()
{
	TransformEngine* engine = (TransformEngine*)transformEngine;

	if (engine->transParams->useLivingBracket)
	{
		bones.clear();
		engine->getMeshVerticesForRBs();
		for (int i = 0; i < robot->getRigidBodyCount(); i++) {
			RigidBody* rb = robot->getRigidBody(i);
			bones.push_back(RBBone(rb, engine->rbVertices[rb]));
		}
	}
	else {
		bones.clear();
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
			bones.push_back(getRBBoneForRB(robot->getRigidBody(i)));
	}
	
}

void SkeletonFoldingPlan::getRobotState(ReducedRobotState& rs) {
	gcRobotRepresentation->setQ(currentRobotState);
	gcRobotRepresentation->getReducedRobotState(rs);
}

void SkeletonFoldingPlan::setTargetRobotState(const ReducedRobotState& rs) {
	gcRobotRepresentation->getQFromReducedState(rs, targetRobotState);
}

void SkeletonFoldingPlan::setRobotState(const ReducedRobotState& rs) {
	gcRobotRepresentation->getQFromReducedState(rs, currentRobotState);
}

RBBone SkeletonFoldingPlan::getRBBoneForRB(RigidBody* rb){

	int n = 4;
	RBBone bone;
	bone.rb = rb;

	if (robot->getRoot() == rb)
	{
		for (uint i = 0; i < rb->cJoints.size(); i++)
		{
			getSamplePointsForSegment(P3D(), rb->cJoints[i]->pJPos, n, bone.localPoints);
		}
	}
	else {
		P3D a = rb->pJoints[0]->cJPos;
		P3D b;
		if (rb->cJoints.size() > 0) {
			b = rb->cJoints[0]->pJPos;
		}
		else {
			auto& endEffectors = rb->rbProperties.endEffectorPoints;
			for (uint i = 0; i < endEffectors.size(); i++)
			{
				b += endEffectors[i].coords;
			}
			b /= endEffectors.size();
		}
		getSamplePointsForSegment(a, b, n, bone.localPoints);
	}

	
	return bone;
}

void SkeletonFoldingPlan::generateJointBoundaries()
{
	TransformEngine* engine = (TransformEngine*)transformEngine;
	VoxelGrid<int>* voxelGrid = engine->voxelGrid;
	VoxelGrid<int>* boundaryGrid = engine->boundaryVoxelGrid;
	auto& RBIndexMap = engine->RBIndexMap;
	auto& RBs = engine->RBs;
	jointBoundaries.clear();
	jointBoundaries.resize(robot->getJointCount());
	int* voxelNum = voxelGrid->voxelNum;

	for (int z = 0; z < voxelNum[2]; z++)
		for (int y = 0; y < voxelNum[1]; y++)
			for (int x = 0; x < voxelNum[0]; x++) {
				if (boundaryGrid->getData(x, y, z) == 0) continue;

				int childID = voxelGrid->getData(x, y, z);
				if (childID <= 0) continue;

				int jIndex = RBs[childID]->pJoints[0]->jIndex;
				int parentID = parentRBIndexMap[childID];
				RigidBody* parentRB = RBs[parentID];
				P3D childP = voxelGrid->getVertex(x, y, z);
				P3D bPoint;
				V3D bNormal;
				int count = 0;
				
				for (int dz = -1; dz < 2; dz++)
					for (int dy = -1; dy < 2; dy++)
						for (int dx = -1; dx < 2; dx++) {
							int nx = x + dx;
							int ny = y + dy;
							int nz = z + dz;
							if (voxelGrid->isInside(nx, ny, nz))
							{
								if (boundaryGrid->getData(nx, ny, nz) == 1
									&& voxelGrid->getData(nx, ny, nz) == parentID)
								{
									P3D parentP = voxelGrid->getVertex(nx, ny, nz);
									bPoint += (parentP + childP) * 0.5;
									bNormal += (parentP - childP).normalized();
									count++;
								}
							}
						}

				if (count > 0)
				{
					bPoint = engine->projectPointToSurf(bPoint / count);
					bPoint = parentRB->getLocalCoordinates(bPoint);
					bNormal = parentRB->getLocalCoordinates(bNormal.normalized());
					jointBoundaries[jIndex].points.push_back(bPoint);
					jointBoundaries[jIndex].normals.push_back(bNormal);
				}
			}

	/*for (int i = 0; i < robot->getJointCount(); i++)
	{
		JointBoundary jointBoundary;
		Joint* joint = robot->getJoint(i);
		RigidBody* parentRB = joint->parent;
		RigidBody* childRB = joint->child;
		int parentID = RBIndexMap[parentRB];
		int childID = RBIndexMap[childRB];

		for (int z = 0; z < voxelNum[2]; z++)
			for (int y = 0; y < voxelNum[1]; y++)
				for (int x = 0; x < voxelNum[0]; x++) {
					if (boundaryGrid->getData(x, y, z) == 0
						|| voxelGrid->getData(x, y, z) != parentID) continue;

					P3D parentP = voxelGrid->getVertex(x, y, z);
					P3D bPoint;
					V3D bNormal;
					int count = 0;
					for (int dz = -1; dz < 2; dz++)
						for (int dy = -1; dy < 2; dy++)
							for (int dx = -1; dx < 2; dx++) {
								int nx = x + dx;
								int ny = y + dy;
								int nz = z + dz;
								if (voxelGrid->isInside(nx, ny, nz))
								{
									if (boundaryGrid->getData(nx, ny, nz) == 1
										&& voxelGrid->getData(nx, ny, nz) == childID)
									{
										P3D childP = voxelGrid->getVertex(nx, ny, nz);
										bPoint += (parentP + childP) * 0.5;
										bNormal += (parentP - childP).normalized();
										count++;
									}
								}
							}

					if (count > 0)
					{
						bPoint = engine->projectPointToSurf(bPoint / count);
						bPoint = parentRB->getLocalCoordinates(bPoint);
						bNormal = parentRB->getLocalCoordinates(bNormal.normalized());
						jointBoundary.points.push_back(bPoint);
						jointBoundary.normals.push_back(bNormal);
					}
				}

		jointBoundaries.push_back(jointBoundary);
	}*/
}

void SkeletonFoldingPlan::getRangeOfMotion()
{
	TransformEngine* engine = (TransformEngine*)transformEngine;
	int jointNum = robot->getJointCount();
	dVector zeroAngles = dVector::Zero(jointNum);
	dVector foldedAngles = TransformUtils::getJointAnglesForCurrentState(robot, foldedState);
	dVector unfoldedAngles = TransformUtils::getJointAnglesForCurrentState(robot, unfoldedState);
	dVector MPMinAngles = engine->minMPJointAngles;
	dVector MPMaxAngles = engine->maxMPJointAngles;

	unfoldedAngles -= foldedAngles;
	MPMinAngles -= foldedAngles;
	MPMaxAngles -= foldedAngles;

	for (int i = 0; i < jointNum; i++)
	{
		if (unfoldedAngles[i] > PI) {
			unfoldedAngles[i] -= 2 * PI;
			MPMaxAngles[i] -= 2 * PI;
			MPMinAngles[i] -= 2 * PI;
		}
			
		if (unfoldedAngles[i] < -PI) {
			unfoldedAngles[i] += 2 * PI;
			MPMaxAngles[i] += 2 * PI;
			MPMinAngles[i] += 2 * PI;
		}
	}

	minJointAngles = MPMinAngles.array().min(unfoldedAngles.array().min(zeroAngles.array()));
	maxJointAngles = MPMaxAngles.array().max(unfoldedAngles.array().max(zeroAngles.array()));

	/*for (int i = 0; i < jointNum; i++)
	{
		Logger::print("max: %lf, min: %lf\n", maxJointAngles[i], minJointAngles[i]);
	}*/
}

void SkeletonFoldingPlan::getSamplePointsForSegment(P3D a, P3D b, int n, vector<P3D>& samplePoints)
{
	double minSpacing = 0.0;
	V3D v = (b - a).normalized();
	double len = (b - a).norm();
	double spacing = len / (n+1);

	while (n > 1 && spacing < minSpacing)
	{
		n = max(n - 1, 1);
		spacing = len / (n + 1);
	}
	
	for (double d = spacing - 1e-4; d <= len; d += spacing)
	{
		P3D p = a + v * d;
		samplePoints.push_back(p);
	}
}

void SkeletonFoldingPlan::draw(RigidBody* rb)
{
	double sphereSize = ((TransformEngine*)transformEngine)->transParams->useLivingBracket ? 0.002 : 0.01;

	for (uint i = 0; i < bones.size(); i++)
	{
		RBBone& bone = bones[i];
		if (rb && bone.rb != rb)
			continue;

		glPushMatrix();
		TransformUtils::applyGLTransformForRB(bone.rb);
		
		for (uint j = 0; j < bone.localPoints.size(); j++)
		{
			glColor3d(0.0, 1.0, 1.0);
			drawSphere(bone.localPoints[j], sphereSize, 12);
		}

		glPopMatrix();
	}

}

void SkeletonFoldingPlan::writeSymParamsToList(dVector& p)
{
	p.resize(symParamsDim);
	p.setZero();
	if (bodySymm) {
		p[0] = currentRobotState[1];
		p[1] = currentRobotState[2];
		p[2] = currentRobotState[5];
	}
	else {
		p.head(6) = currentRobotState.head(6);
	}
	
	
	for (int i = 6; i < currentRobotState.size(); i++)
	{
		p[jointToSymParamsMap[i]] += currentRobotState[i] * mappingScale[i];
	}
}

void SkeletonFoldingPlan::setSymParamsFromList(const dVector& p)
{
	currentRobotState.setZero();
	if (bodySymm) {
		currentRobotState[1] = p[0];
		currentRobotState[2] = p[1];
		currentRobotState[5] = p[2];
	}
	else {
		currentRobotState.head(6) = p.head(6);
	}
	

	for (int i = 6; i < (int)currentRobotState.size(); i++)
	{
		currentRobotState[i] = p[jointToSymParamsMap[i]] * (mappingScale[i] > 0 ? 1 : -1);
	}

	gcRobotRepresentation->setQ(currentRobotState);

	extraRobotSetup();
}

void SkeletonFoldingPlan::writeSymMorphParamsToList(dVector& p)
{
	p.resize(totalDim);
	p.setZero();

	// ************************* Robot State ******************************
	if (bodySymm) {
		p[0] = currentRobotState[1];
		p[1] = currentRobotState[2];
		p[2] = currentRobotState[5];
	}
	else {
		p.head(6) = currentRobotState.head(6);
	}

	for (int i = 6; i < currentRobotState.size(); i++)
	{
		p[jointToSymParamsMap[i]] += currentRobotState[i] * mappingScale[i];
	}

	// ************************* Morphology ******************************
	vector<double> morphParams;
	robotDesign->getCurrentSetOfParameters(morphParams);
	p[symParamsDim] = morphParams[0];
	p[symParamsDim + 1] = morphParams[1];
	for (int i = 2; i < (int)morphParams.size(); i++)
	{
		p[symParamsDim + RBToSymParamsMap[i]] = morphParams[i];
	}

}

void SkeletonFoldingPlan::setSymMorphParamsFromList(const dVector& p)
{
	currentRobotState.setZero();

	// ************************* Robot State ******************************
	if (bodySymm) {
		currentRobotState[1] = p[0];
		currentRobotState[2] = p[1];
		currentRobotState[5] = p[2];
	}
	else {
		currentRobotState.head(6) = p.head(6);
	}

	for (int i = 6; i < currentRobotState.size(); i++)
	{
		currentRobotState[i] = p[jointToSymParamsMap[i]] * (mappingScale[i] > 0 ? 1 : -1);
	}

	// ************************* Morphology ******************************
	vector<double> morphParams(robot->getRigidBodyCount() + 1);
	morphParams[0] = p[symParamsDim];
	morphParams[1] = p[symParamsDim + 1];
	for (int i = 2; i < (int)morphParams.size(); i++)
	{
		morphParams[i] = p[symParamsDim + RBToSymParamsMap[i]];
	}
	robotDesign->setParameters(morphParams);

	// robot setup
	reinitalize();
	gcRobotRepresentation->setQ(currentRobotState);
	extraRobotSetup();
}

void SkeletonFoldingPlan::extraRobotSetup()
{
	TransformEngine* engine = (TransformEngine*)transformEngine;
	TransformUtils::updateCapsuleMapGC(engine->capsulesMap, gcRobotRepresentation, engine->transParams->skeletonRadius);
	engine->geodesicVoxelAssignment();

	gcRobotRepresentation->getReducedRobotState(*foldedState);
	robot->setState(foldedState);

	generateJointBoundaries();
	getRangeOfMotion();
}
