#pragma once

#include <ControlLib/Robot.h>
#include <MathLib/VoxelGrid.h>
#include <MathLib/Ray.h>
#include <MathLib/Capsule.h>
#include <MathLib/boundingBox.h>
#include <MathLib/LevelSet.h>
#include <ControlLib/GeneralizedCoordinatesRobotRepresentation.h>
#include <set>

using namespace std;

struct TransformParams {
	double voxelSize;
	double LSStepSize;
	double d_pos;
	double d_angle;
	double sampleStep;   // sampling step size when doing collision detection.
	double animationStep;
	double jointScale;
	bool smoothVoxels;
	int maxFloodIter;
	bool shellOnly;
	bool sharpFeet;
	double LSCylinderR;
	double IFPSpace;
	double skeletonRadius;
	double LSFlood_L;
	double LSFlood_alpha;
	double LSFlood_v;
	double shellThickness;
	double sktInsideThreshold;
	double minTransferR;
	bool useLivingBracket;
};

struct LSInterface {
	int index1;
	int index2;
	RigidBody* rb1;
	RigidBody* rb2;
	vector<P3D> points;
	vector<V3D> normals1;
	vector<V3D> normals2;
	dVector v1;
	dVector v2;

	LSInterface() {}
	LSInterface(int _index1, int _index2, RigidBody* _rb1, RigidBody* _rb2)
		: index1(_index1), index2(_index2), rb1(_rb1), rb2(_rb2) {}
};

struct MeshInterface {
	int index1;
	int index2;
	RigidBody* rb1;
	RigidBody* rb2;
	vector<P3D> points;
	int startKnotIndex = -1;
	int endKnotIndex = -1;
	bool loop = false;

	MeshInterface(int _index1, int _index2, RigidBody* _rb1, RigidBody* _rb2)
		: index1(_index1), index2(_index2), rb1(_rb1), rb2(_rb2) {}
};

struct MeshInterfaceKnot {
	vector<int> interfaceIndices;
	vector<int> interfaceFlags; // head or tail
	P3D position;
};

struct RBVoxelInfo
{
	// number of voxels
	int N = 0;
	int curIndex = 0;

	// Nx3 matrix containing voxels' center positions in the RB's local coordinate.
	// It should only be intialized once when assigning voxels to the RB.
	MatrixNxM localVoxels;

	// Nx3 matrix containing voxels' center positions in world coordinate.
	MatrixNxM worldVoxels;

	// N-d Vector containing voxels' indices in the voxelGrid.
	Eigen::VectorXi voxelIndices;

	// 1-valid   0-invalid    used for collision based carving
	Eigen::VectorXi voxelFlags;
};

// the infomation for folding one joint
struct FoldingInfo {
	// index into the animation states
	Joint* joint;
	int startIndex;
	int endIndex;
	set<RigidBody*> movingRBs;
};


class TransformUtils
{
public:
	TransformUtils();
	~TransformUtils();

	static void getCapsulesForRigidBody(RigidBody* rb, vector<Capsule>& capsules);
	static double getClosestDistToCapsules(const P3D& point, vector<Capsule>& capsules, int* capsuleIndex = NULL);
	static double getClosestDistToCapsules(const Ray& ray, vector<Capsule>& capsules);
	// get the closet RigidBody to the given point.
	static RigidBody* getRobotClosestRigidBody(const P3D& point, map<RigidBody*, vector<Capsule>>& capsulesMap, double* dist = NULL, int* capsuleIndex = NULL);

	// update capsule map based on current rigid body transformation.
	static void updateCapsuleMap(map<RigidBody*, vector<Capsule>>& capsulesMap, bool clearRadius = false);
	static void updateCapsuleMapGC(map<RigidBody*, vector<Capsule>>& capsulesMap, GeneralizedCoordinatesRobotRepresentation* gcRobot, double radius);
	static void updateVoxelInfoMap(map<RigidBody*, RBVoxelInfo>& voxelInfoMap);

	static void clearMeshMap(map<RigidBody*, GLMesh*>& meshMap);

	static void transformMeshFromWorldToLocalCoords(GLMesh* inputMesh, RigidBody* parent);
	static void transformMeshFromLocalToWorldCoords(GLMesh* inputMesh, RigidBody* parent);
	static void transformMeshBetweenStates(GLMesh* inputMesh, RBState& startState, RBState& endState);

	static void getAnimationStates(vector<ReducedRobotState>& animationStates, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep);
	static void getAnimationStatesForMotor(vector<ReducedRobotState>& animationStates, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep);
	static vector<FoldingInfo> getAnimationStatesAndFoldingSequence(vector<ReducedRobotState>& animationStates, Robot* robot, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep);
	static void getMovingRB(set<RigidBody*>& movingRBs, Robot* robot, ReducedRobotState* curState, ReducedRobotState* targetState);

	static void getAnimationStatesForJoint(vector<ReducedRobotState>& animationStates, int JointIndex, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep);
	static void getParentAndChildStatesForJoint(vector<RBState>& parentStates, vector<RBState>& childStates, Robot* robot, int JointIndex, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep);

	static void saveVoxelGridToFile(const char* fName, VoxelGrid<int>* voxelGrid);
	static void saveVoxelGridToFile(FILE* fp, VoxelGrid<int>* voxelGrid);
	static VoxelGrid<int>* loadVoxelGridFromFile(const char* fName);
	static VoxelGrid<int>* loadVoxelGridFromFile(FILE* fp);

	static VoxelGrid<int>* voxelize(GLMesh* mesh, AxisAlignedBoundingBox bbox, double voxelSize, double levelSetVal, bool shellOnly = false);
	static VoxelGrid<int>* getBoundaryVoxelGrid(VoxelGrid<int>* origGrid);

	static bool testMeshIntersectSDF(GLMesh* mesh, RigidBody* rb, LevelSet* SDF);

	static void clearVoxelAssignment(VoxelGrid<int>* voxelGrid);

	static void updateCapsulesMapRadius(vector<RigidBody*>& RBs, map<RigidBody*, vector<Capsule>>& capsulesMap);

	static GLMesh* getCylinderMesh(const P3D& start, const P3D& end, double radius);

	static Quaternion getPlugJointQuaternion(Joint* joint);
	static Quaternion getSocketJointQuaternion(Joint* joint);
	static Transformation getPlugJointTransformation(Joint* joint);
	static Transformation getSocketJointTransformation(Joint* joint);
	static GLMesh* getTransformedJointMesh(GLMesh* jointMesh, Joint* joint, double scale, bool childJoint = true);

	static double getPointDistToLine(const P3D& point, const P3D& pLine, const V3D& normal);

	static GLMesh* computeJointSlitMesh(P3D orig, P3D endA, P3D endB, double width, double radius);

	static GLMesh* computeJointSlitMesh(P3D orig, vector<P3D>& endPoints, double width, double radius);

	static void applyGLTransformForRB(RigidBody* rb);

	static double GaussianKernel(double d, double sigma2, double a);

	static dVector getJointAnglesForCurrentState(Robot* robot, ReducedRobotState* robotState);
};

