#include "SkeletonFoldingRepulsionObjective.h"

SkeletonFoldingRepulsionObjective::SkeletonFoldingRepulsionObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonFoldingRepulsionObjective::~SkeletonFoldingRepulsionObjective(void){
}

double SkeletonFoldingRepulsionObjective::computeValue(const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
//	IKPlan->setParametersFromList(s);

	double retVal = 0;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	
	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size() - 1; i++)
	{
		for (uint j = i + 1; j < bones.size(); j++)
		{
			RigidBody* rbI = bones[i].rb;
			RigidBody* rbJ = bones[j].rb;
			vector<P3D> localPointsI = bones[i].localPoints;
			vector<P3D> localPointsJ = bones[j].localPoints;
			
			for (uint p = 0; p < localPointsI.size(); p++)
				for (uint q = 0; q < localPointsJ.size(); q++)
				{
					P3D w_p = gcRobot->getWorldCoordinatesFor(localPointsI[p], rbI);
					P3D w_q = gcRobot->getWorldCoordinatesFor(localPointsJ[q], rbJ);
					retVal += 1 / (w_p - w_q).norm();
				}
		}
	}

	return retVal * weight;
}

void SkeletonFoldingRepulsionObjective::addGradientTo(dVector& grad, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	MatrixNxM dpdqI, dpdqJ;

	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;

	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size() - 1; i++)
	{
		for (uint j = i + 1; j < bones.size(); j++)
		{
			RigidBody* rbI = bones[i].rb;
			RigidBody* rbJ = bones[j].rb;
			vector<P3D> localPointsI = bones[i].localPoints;
			vector<P3D> localPointsJ = bones[j].localPoints;

			for (uint p = 0; p < localPointsI.size(); p++)
				for (uint q = 0; q < localPointsJ.size(); q++)
				{
					P3D w_p = gcRobot->getWorldCoordinatesFor(localPointsI[p], rbI);
					P3D w_q = gcRobot->getWorldCoordinatesFor(localPointsJ[q], rbJ);
					gcRobot->compute_dpdq(localPointsI[p], rbI, dpdqI);
					gcRobot->compute_dpdq(localPointsJ[q], rbJ, dpdqJ);
					V3D dF = (w_p - w_q) / pow((w_p - w_q).norm(), 3);
					grad += dpdqI.transpose() * -dF * weight;
					grad += dpdqJ.transpose() * dF * weight;
				}
		}
	}
}


void SkeletonFoldingRepulsionObjective::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	MatrixNxM dpdc, dqdc, dpdc_di, dqdc_di;

	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;

	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size() - 1; i++)
	{
		for (uint j = i + 1; j < bones.size(); j++)
		{
			RigidBody* rbI = bones[i].rb;
			RigidBody* rbJ = bones[j].rb;
			vector<P3D> localPointsI = bones[i].localPoints;
			vector<P3D> localPointsJ = bones[j].localPoints;

			for (uint p = 0; p < localPointsI.size(); p++)
				for (uint q = 0; q < localPointsJ.size(); q++)
				{
					P3D w_p = gcRobot->getWorldCoordinatesFor(localPointsI[p], rbI);
					P3D w_q = gcRobot->getWorldCoordinatesFor(localPointsJ[q], rbJ);
					gcRobot->compute_dpdq(localPointsI[p], rbI, dpdc);
					gcRobot->compute_dpdq(localPointsJ[q], rbJ, dqdc);
					V3D v_pq = w_p - w_q;
					V3D n_pq = v_pq.normalized();
					MatrixNxM T = (Eigen::Matrix3d::Identity() - 3 * n_pq * n_pq.transpose()) / pow(v_pq.norm(), 3);
					MatrixNxM H1 = -dpdc.transpose() * T * dpdc - dqdc.transpose() * T * dqdc
						+ dqdc.transpose() * T * dpdc + dpdc.transpose() * T * dqdc;

					for (int i = 0; i < H1.rows(); i++)
						for (int j = i; j < H1.cols(); j++) {
							ADD_HES_ELEMENT(hessianEntries, i, j, H1(i, j), weight);
						}						

					V3D dF = v_pq / pow(v_pq.norm(), 3);
					for (int k = 0; k < plan->gcRobotRepresentation->getDimensionCount(); k++) {
						plan->gcRobotRepresentation->compute_ddpdq_dqi(localPointsI[p], rbI, dpdc_di, k);
						plan->gcRobotRepresentation->compute_ddpdq_dqi(localPointsJ[q], rbJ, dqdc_di, k);
						for (int l = k; l < plan->gcRobotRepresentation->getDimensionCount(); l++) {
							double val = dpdc_di.col(l).transpose().dot(-dF)
								+ dqdc_di.col(l).transpose().dot(dF);
							ADD_HES_ELEMENT(hessianEntries, k, l, val, weight);
						}
					}
				}
		}
	}
}



