#pragma once
#include "TransformEngine.h"

class TransformEngine;

class FoldingOptEngine
{
public:
	TransformEngine* transformEngine;

public:
	FoldingOptEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine) {

	}
	~FoldingOptEngine();

	void prepareForFoldingOrderOpt();
	void optimizeFoldingOrder();
	void createInitialFoldingOrder();

	// greedy search
	void optimizeFoldingOrderGreedy();

	// cost functions
	double computeOrderCost(vector<int>& order);  // compute the whole cost for the folding order.
	double computeStateCost();  // compute the cost for a certain time step in the folding process.
	double computeStateCostCapsules();
	double computeCapsuleCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j);

	// signed distance field based collision detection
	void buildSDFMap();
	double computeSDFCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j);
	void debugSDFCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j);
};

