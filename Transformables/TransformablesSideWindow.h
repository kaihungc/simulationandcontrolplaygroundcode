#pragma once

#include <string>
#include <map>
#include <algorithm>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <GUILib/GLApplication.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/Robot.h>
#include <GUILib/GLWindow3D.h>
#include "TransformEngine.h"

#define NOJOINT_ID -2
#define ROOT_ID -1


 // Some control parameters shared by main window and other windows
struct TransformablesControlParams
{
	bool showRobotMeshes = true;
	bool showRobotAbstractView = true;
	bool showAllThickness = false;
	bool showCapsuleRadius = false;
	bool showSkeletonCapsules = false;
	bool showCarveMeshes = false;
	bool showSkeletonLSMeshes = false;
	bool showSkeletonOrigMeshes = true;
	bool showSkeletonSamples = false;
	bool showApproxMesh = true;
	bool useMaterial = false;
	bool useHingeJoint = true;
	map<RigidBody*, V3D>* colorMap = NULL;
	map<int, int>* jointSymmetryMap = NULL;
	TransformEngine* transformEngine = NULL;
	GLShaderMaterial shellMaterial;
	GLShaderMaterial skeletonMaterial;
};

class TransformablesSideWindow : public GLWindow3D {
public:
	GLApplication* glApp;
	RotateWidget* rWidget = NULL;
	TranslateWidget* tWidget = NULL;

	Robot* robot = NULL;
	ReducedRobotState* foldedState = NULL;
	ReducedRobotState* unfoldedState = NULL;

	TransformablesControlParams* controlParams;
	TransformParams* transParams;

	int pickedJoint = -2;
	RigidBody* pickedRB = NULL;
	int pickedInterface = -1;
	int pickedInterfaceP = -1;

	// Animation Variables
	ReducedRobotState* startState = NULL;  // the start state of the entire transformation
	ReducedRobotState* endState = NULL;   // the end state of the entire transformation
	double animationStep = 0.05;
	bool playAnimation = false;
	vector<int> foldOrder;
	vector<ReducedRobotState> animationStates;
	double sIndex = 0;
	ReducedRobotState* curState = NULL;

	

public:

	// constructor
	TransformablesSideWindow(int x, int y, int w, int h, GLApplication* glApp);
	// destructor
	virtual ~TransformablesSideWindow(void);

	// Draw the AppRobotDesigner scene - camera transformations, lighting, shadows, reflections, etc AppRobotDesignerly to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();

	//mouse & keyboard event callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);

	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	void setupLights();

	bool pickJoint(double xPos, double yPos);
	bool pickRB(double xPos, double yPos);


	void prepareForAnimation(vector<int>& order, ReducedRobotState* _startState, ReducedRobotState* _endState, double _animationStep);
	
	void prepareForMotionPlanAnimation(vector<ReducedRobotState>& motionPlanStates);
	
	void postAnimation();
	void resetAnimation();

	void drawInterface();
	void drawRBOrigMeshes();
	void drawSkeletonLSMeshes();
	void drawAttachementSegs();
};



