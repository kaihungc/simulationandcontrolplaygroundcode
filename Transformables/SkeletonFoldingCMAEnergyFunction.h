#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonFoldingCMAEnergyFunction : public ObjectiveFunction {
public:
	DynamicArray<ObjectiveFunction*> objectives;

	SkeletonFoldingCMAEnergyFunction(SkeletonFoldingPlan* plan);
	virtual ~SkeletonFoldingCMAEnergyFunction(void);

	virtual double computeValue(const dVector& p);
	double printValue(const dVector& p);

	void setupSubObjectives();


private:

	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
};

