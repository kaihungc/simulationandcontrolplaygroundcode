#include "SkeletonEmbedInsideObjective.h"
#include "TransformEngine.h"

SkeletonEmbedInsideObjective::SkeletonEmbedInsideObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	levelSet = plan->levelSet;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonEmbedInsideObjective::~SkeletonEmbedInsideObjective(void){
}

double SkeletonEmbedInsideObjective::computeValue(const dVector& p){

	double retVal = 0;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	TransformEngine* transformEngine = (TransformEngine*)plan->transformEngine;
	double sktInsideThreshold = transformEngine->transParams->sktInsideThreshold;
	
	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size(); i++)
	{
		vector<P3D>& localPoints = bones[i].localPoints;
		RigidBody* rb = bones[i].rb;
		for (uint j = 0; j < localPoints.size(); j++)
		{
			P3D point = gcRobot->getWorldCoordinatesFor(localPoints[j], rb);
			double LSVal = 1e2;
			if (levelSet->isInside(point[0], point[1], point[2]))
			{			
				LSVal = levelSet->interpolateData(point[0], point[1], point[2]);
			}
			double diff = LSVal + sktInsideThreshold;
			if (diff > 0)
				retVal += 0.5 * diff * diff;
		}
	}

	return retVal * weight;
}
