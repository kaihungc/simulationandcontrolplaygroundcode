#include "SkeletonFoldingOptimizer.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>

SkeletonFoldingOptimizer::SkeletonFoldingOptimizer(SkeletonFoldingPlan* plan, SkeletonFoldingEnergyFunction* energyFunction){
	this->plan = plan;
	this->energyFunction = energyFunction;
	BFGS_minimizer = new BFGSFunctionMinimizer(1);
}

SkeletonFoldingOptimizer::~SkeletonFoldingOptimizer(void){
	delete BFGS_minimizer;
}

double SkeletonFoldingOptimizer::optimizePlan(int maxIterNum){
	NewtonFunctionMinimizer minimizer(maxIterNum);

	energyFunction->printDebugInfo = true;
	dVector params;
	this->plan->writeParametersToList(params);

	//energyFunction->testGradientWithFD(params);
	//energyFunction->testHessianWithFD(params);

	double val = 0;
	//BFGS_minimizer->minimize(energyFunction, params, val);
	minimizer.minimize(energyFunction, params, val);
//	Logger::consolePrint("IK energy val: %lf\n", val);

	return val;
}
