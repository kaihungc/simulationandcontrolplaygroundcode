#include "SkeletonFoldingRobotStateRegularizer.h"

SkeletonFoldingRobotStateRegularizer::SkeletonFoldingRobotStateRegularizer(SkeletonFoldingPlan* mp, int startQIndex, int endQIndex, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->startQIndex = startQIndex;
	this->endQIndex = endQIndex;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonFoldingRobotStateRegularizer::~SkeletonFoldingRobotStateRegularizer(void){
}

double SkeletonFoldingRobotStateRegularizer::computeValue(const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	double retVal = 0;
	
	for (int i=startQIndex;i<=endQIndex;i++){	
		double tmpV = (plan->targetRobotState[i] - plan->currentRobotState[i]);
		retVal += 0.5 * tmpV*tmpV * weight;
	}
	
	return retVal;
}

void SkeletonFoldingRobotStateRegularizer::addGradientTo(dVector& grad, const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	//and now compute the gradient with respect to the robot q's
	for (int i=startQIndex; i<=endQIndex; i++)
		grad[i] += -(plan->targetRobotState[i] - plan->currentRobotState[i]) * weight;
}

void SkeletonFoldingRobotStateRegularizer::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	for (int i=startQIndex;i<=endQIndex;i++)
		ADD_HES_ELEMENT(hessianEntries, i, i, 1, weight);
}
