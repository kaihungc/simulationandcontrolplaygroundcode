#include "SkeletonJointCarvingObjective.h"
#include "TransformEngine.h"

SkeletonJointCarvingObjective::SkeletonJointCarvingObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonJointCarvingObjective::~SkeletonJointCarvingObjective(void){
}

double SkeletonJointCarvingObjective::computeValue(const dVector& p){

	double retVal = 0;
	Robot* robot = plan->robot;
	auto& jointBoundaries = plan->jointBoundaries;
	dVector& minJointAngles = plan->minJointAngles;
	dVector& maxJointAngles = plan->maxJointAngles;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		HingeJoint* joint = (HingeJoint*)robot->getJoint(i);
		P3D jointPos = joint->pJPos;
		V3D jointAxis = joint->rotationAxis.normalized();
		auto& points = jointBoundaries[i].points;
		auto& normals = jointBoundaries[i].normals;

		for (int j = 0; j < (int)points.size(); j++)
		{
			P3D& p = points[j];
			V3D& n = normals[j];

			double tmp = jointAxis.cross(p - jointPos).dot(n);
			if (tmp > 0)
				retVal += maxJointAngles[i] * tmp;
			else
				retVal += minJointAngles[i] * tmp;
		}
	}

	return retVal * weight;
}