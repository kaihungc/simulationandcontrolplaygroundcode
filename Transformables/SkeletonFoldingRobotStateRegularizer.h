#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonFoldingRobotStateRegularizer : public ObjectiveFunction {
public:
	SkeletonFoldingRobotStateRegularizer(SkeletonFoldingPlan* mp, int startQIndex, int endQIndex, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonFoldingRobotStateRegularizer(void);

	virtual double computeValue(const dVector& p);
	virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);


private:

	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;

	//these are the start and end indices for the parts of the state space that we penalize
	int startQIndex, endQIndex;
};

