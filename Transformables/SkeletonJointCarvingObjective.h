#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonJointCarvingObjective : public ObjectiveFunction {

public:
	SkeletonJointCarvingObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonJointCarvingObjective(void);

	virtual double computeValue(const dVector& p);

private:
	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
};

