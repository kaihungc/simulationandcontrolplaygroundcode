#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonFoldingEnergyFunction : public ObjectiveFunction {
public:
	DynamicArray<ObjectiveFunction*> objectives;

	SkeletonFoldingEnergyFunction(SkeletonFoldingPlan* plan);
	virtual ~SkeletonFoldingEnergyFunction(void);

	//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
	void updateRegularizingSolutionTo(const dVector &currentP);
	virtual double computeValue(const dVector& p);

	virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);

	//this method gets called whenever a new best solution to the objective function is found
	virtual void setCurrentBestSolution(const dVector& p);

	void setupSubObjectives();

	double computeValueWithoutRegularizer(const dVector& p, bool printInfo = false);

	bool printDebugInfo;

private:
	//this is the solution used as a regularizer...
	dVector m_p0;
	dVector tmpVec;
	double regularizer;
	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
};

