#include "SkeletonCollisionObjective.h"
#include "TransformEngine.h"

SkeletonCollisionObjective::SkeletonCollisionObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonCollisionObjective::~SkeletonCollisionObjective(void){
}

double SkeletonCollisionObjective::computeValue(const dVector& p){

	double retVal = 0;
	TransformEngine* transformEngine = (TransformEngine*)plan->transformEngine;

	retVal = transformEngine->foldingOptEngine->computeStateCostCapsules();

	return retVal * weight;
}