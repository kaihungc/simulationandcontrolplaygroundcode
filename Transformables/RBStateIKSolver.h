#pragma once

#include <RobotDesignerLib/RDController.h>
#include <RobotDesignerLib/RDControllerIK.h>

using namespace std;

class RBStateIKSolver {
public:
	RBStateIKSolver(Robot* robot);
	~RBStateIKSolver(void);

	ReducedRobotState computeNewState(V3D offset, RigidBody* targetlimb, RigidBody* targetSymLimb = NULL);

protected:
	Robot* robot;
	RDControllerIK_Plan *ikPlan;
	IK_EnergyFunction *ikEnergyFunction;
	IK_Optimizer *ikOptimizer;
};

