#pragma once
#include "TransformEngine.h"
#include <MathLib/VertexGrid.h>
#include <MathLib/LevelSet.h>

using namespace std;

class TransformEngine;
struct TransformParams;

class CarvingEngine
{
public:
	TransformEngine* transformEngine;

	double sampleStep = 0.05;
	vector<RigidBody*> RBs;
	map<RigidBody*, RBState> preRBStates;
	map<RigidBody*, GLMesh*> carvedMeshes;

	// state in folding process
	vector<ReducedRobotState> animationStates;
	vector<FoldingInfo> foldingSequence;
	int stateIndex = 0;
	int sequenceIndex = 0;

	AxisAlignedBoundingBox gridBBox;
	GLMesh* sweptVolumeMesh = NULL;

public:
	CarvingEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine) {

	}
	~CarvingEngine();

	void carveMesh();

	void initialSetup();

	bool carveForCurrentSequence();
	bool carveJointParentForCurrentSequence();

	void transformCarvedMesh();
};

