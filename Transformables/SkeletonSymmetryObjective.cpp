#include "SkeletonSymmetryObjective.h"

SkeletonSymmetryObjective::SkeletonSymmetryObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
	symmMap = plan->symmMap;
}

SkeletonSymmetryObjective::~SkeletonSymmetryObjective(void){
}

double SkeletonSymmetryObjective::computeValue(const dVector& p){
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
//	IKPlan->setParametersFromList(s);

	double retVal = 0;
	GeneralizedCoordinatesRobotRepresentation2* gcRobot = plan->gcRobotRepresentation;
	
	for (auto itr = symmMap->begin(); itr != symmMap->end(); itr++)
	{
		int joint1 = itr->first;
		int joint2 = itr->second;
		if (joint1 >= joint2) continue;

		// constraint: ang(j1) = -ang(j2), j1 and j2 are symmetric.
		double diff = p[gcRobot->getQIndexForJoint(joint1)] + p[gcRobot->getQIndexForJoint(joint2)];
		retVal += diff * diff;
	}

	// constraint: x = 0
	retVal += p[0] * p[0];

	// constraints on orientation.
	retVal += p[3] * p[3];
	retVal += p[4] * p[4];

	return 0.5 * retVal * weight;
}

void SkeletonSymmetryObjective::addGradientTo(dVector& grad, const dVector& p) {
	//assume the parameters of the motion plan have been set already by the collection of objective functions class
	//IKPlan->setParametersFromList(p);

	GeneralizedCoordinatesRobotRepresentation2* gcRobot = plan->gcRobotRepresentation;

	for (auto itr = symmMap->begin(); itr != symmMap->end(); itr++)
	{
		int joint1 = itr->first;
		int joint2 = itr->second;
		if (joint1 >= joint2) continue;
		int pIndex1 = gcRobot->getQIndexForJoint(joint1);
		int pIndex2 = gcRobot->getQIndexForJoint(joint2);

		double diff = p[pIndex1] + p[pIndex2];
		grad[pIndex1] += diff * weight;
		grad[pIndex2] += diff * weight;
	}

	grad[0] += p[0] * weight;
	grad[3] += p[3] * weight;
	grad[4] += p[4] * weight;
}
//
//void SkeletonFoldingInsideObjective::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
//	//assume the parameters of the motion plan have been set already by the collection of objective functions class
//	//IKPlan->setParametersFromList(p);
//
//	MatrixNxM dEndEffectordq, ddEndEffectordq_dqi;
//	int nEE = plan->endEffectors.size();
//
//	for (int i=0;i<nEE;i++){
//		V3D err(plan->endEffectors[i].targetEEPos, plan->gcRobotRepresentation->getWorldCoordinatesFor(plan->endEffectors[i].endEffectorLocalCoords, plan->endEffectors[i].endEffectorRB));
//
//			//and now compute the gradient with respect to the robot q's
//		plan->gcRobotRepresentation->compute_dpdq(plan->endEffectors[i].endEffectorLocalCoords, plan->endEffectors[i].endEffectorRB, dEndEffectordq);
//
//		for (int k=0;k<plan->gcRobotRepresentation->getDimensionCount();k++){
//			bool hasNonZeros = plan->gcRobotRepresentation->compute_ddpdq_dqi(plan->endEffectors[i].endEffectorLocalCoords, plan->endEffectors[i].endEffectorRB, ddEndEffectordq_dqi, k);
//			if (hasNonZeros == false) continue;
//			for (int l=k;l<plan->gcRobotRepresentation->getDimensionCount();l++)
//				for (int m=0;m<3;m++){
//					double val = ddEndEffectordq_dqi(m, l) * err[m];
//					ADD_HES_ELEMENT(hessianEntries, k, l, val, weight);
//				}
//		}
//
//		//now add the outer product of the jacobians...
//		for (int k=0;k<plan->gcRobotRepresentation->getDimensionCount();k++){
//			for (int l=k;l<plan->gcRobotRepresentation->getDimensionCount();l++){
//				double val = 0;
//				for (int m=0;m<3;m++)
//					val += dEndEffectordq(m, k) * dEndEffectordq(m, l);
//				ADD_HES_ELEMENT(hessianEntries, k, l, val, weight);
//			}
//		}
//	}
//}
//


